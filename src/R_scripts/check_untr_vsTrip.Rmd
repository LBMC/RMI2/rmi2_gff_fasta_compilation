---
title: "Check diff untreated Triptolide"
author: "`r Sys.info()['user']`"
date: "`r Sys.time()`"
output: 
  pdf_document:
    toc: true
    toc_depth: 2
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Lymphocytes

## Parameters
```{r, include=FALSE}
setwd("../")
pks <- list("DESeq2","tidyverse", "RColorBrewer", "pheatmap", "ggvenn", "gridExtra", "reshape2")
lapply(pks , require , character.only = T)

filename <- "data/HTSeq_count_stats_all_libraries.csv"
data <- read.csv(filename, sep = ",")
rownames(data) <- data$Gene_id
data <- cbind(data[grep("^ENSMUSG", data$Gene_id),
                   grep("^(R|A).*untreated.*exon", colnames(data))
              ],
              data[grep("^ENSMUSG", data$Gene_id),
                   grep("^(R|A).*0h_Triptolide_exon", colnames(data))
              ]
        )

samples <- colnames(data)
words_df <- as.data.frame(matrix(ncol = length(samples), nrow = 6))
colnames(words_df) <- samples

for (i in samples){
    words_df[,i] <- strsplit(x = i, split = "_")
}

repliate <- substr(words_df[1,], start = 2, stop = 3)
times <- unlist(words_df[4,])
conditions <- unlist(words_df[3,])
drug <- unlist(words_df[5,])

coldata <- data.frame(samples)
coldata$condition <- conditions
coldata$time <- times
coldata$replicate <- repliate
coldata$drug <- drug
```

## PCA of all samples

```{r}
dds <- DESeqDataSetFromMatrix(countData = data,
                              colData = coldata,
                              design = ~ condition + time + drug + replicate)
dds <- DESeq(dds)

plotDispEsts(dds)
vsd <- varianceStabilizingTransformation(dds, blind=TRUE)
plotPCA(vsd, intgroup = c("time", "condition", "drug"))

rld <- rlogTransformation(dds, blind=TRUE)
plotPCA(rld, intgroup = c("time", "condition", "drug"))
```

## DE with untreated 

```{r}
coldata3h <- filter(coldata, (time == "0h" & condition == "Resting" & drug == "untreated") |
                             (time == "0h" & condition == "Activated" & drug == "untreated"))

data3h <- data[,colnames(data) %in% coldata3h$samples]

dds3h <- DESeqDataSetFromMatrix(countData = data3h,
                              colData = coldata3h,
                              design = ~ condition + replicate)
dds3h <- DESeq(dds3h)

NormCount_unt <- DESeq2::counts(dds3h, normalized = TRUE)

resUnt <- results(dds3h,
                 contrast = c("condition", "Activated", "Resting"))
resUnt
```

## DE with Triptolide 

```{r}
coldata3h <- filter(coldata, (time == "0h" & condition == "Resting" & drug == "untreated") |
                             (time == "0h" & condition == "Activated" & drug == "Triptolide"))

data3h <- data[,colnames(data) %in% coldata3h$samples]

dds3h <- DESeqDataSetFromMatrix(countData = data3h,
                              colData = coldata3h,
                              design = ~ condition + replicate)
dds3h <- DESeq(dds3h)

NormCount_trip <- DESeq2::counts(dds3h, normalized = TRUE)

resTrip <- results(dds3h,
                 contrast = c("condition", "Activated", "Resting"))
resTrip
```

## Venn Diagramm
```{r}
genes <- list("untreated" = rownames(resUnt[resUnt$padj < 0.05 & !is.na(resUnt$padj),]), 
              "triptolide" = rownames(resTrip[resTrip$padj < 0.05 & !is.na(resTrip$padj),])
         )

ggvenn(data = genes, columns = c("untreated", "triptolide"))

```

## DE with untreated and Trip

```{r}
coldata3h <- filter(coldata, time == "0h")

data3h <- data[,colnames(data) %in% coldata3h$samples]

dds3h <- DESeqDataSetFromMatrix(countData = data3h,
                              colData = coldata3h,
                              design = ~ condition + replicate)
dds3h <- DESeq(dds3h)
plotDispEsts(dds3h)
rld <- rlogTransformation(dds3h, blind=TRUE)
plotPCA(rld, intgroup = c("condition","drug"))

NormCount <- DESeq2::counts(dds3h, normalized = TRUE)
```

## Merge data and scatter plots

```{r}

# NormCount <- as.data.frame(NormCount[(rownames(NormCount) %in% rownames(resUnt[resUnt$padj < 0.05 & !is.na(resUnt$padj),]) )& 
#                                       (rownames(NormCount) %in% rownames(resTrip[resTrip$padj < 0.05 & !is.na(resTrip$padj),])),
#                                     ]
#                            )

NormCount <- as.data.frame(NormCount)

NormCount$ensemblID <- rownames(NormCount)
NormCount$ensemblID <- str_extract(NormCount$ensemblID, "ENSMUSG...........")

db <- read.csv("../data/Global_Database.csv")
db <- dplyr::as_tibble(db)

TDDindexes <- dplyr::select(db, 
                            "gene_id", 
                            "asIndexTDD_t3TripTripCHX.0hTrip_LR",
                            "asIndexTDD_t3TripTripCHX.0hTrip_LA", 
                            "RiboDens_LymphoR",
                            "RiboDens_LymphoA", 
                            "Length.cds",
                            "Deg3hTrip_LR",
                            "Deg3hTrip_LA"
)
TDDindexes$RiboDens_LymphoR <- as.numeric(as.character(TDDindexes$RiboDens_LymphoR))
TDDindexes$RiboDens_LymphoA <- as.numeric(as.character(TDDindexes$RiboDens_LymphoA))
colnames(TDDindexes) <- c("ensemblID", "asTDDindex_LymphoR", "asTDDindex_LymphoA", "RiboDens_LymphoR", "RiboDens_LymphoA", "Length.cds", "Deg3h_LR", "Deg3h_LA")
TDDindexes$ensemblID <- str_extract(TDDindexes$ensemblID, "ENSMUSG...........")

data2 <- merge(NormCount, TDDindexes, by = "ensemblID") # loose 333 genes
data2$meanCountsRestingUntreated <- rowMeans(data2[,grep("Resting_0h_untreated",colnames(data2))])
data2$meanCountsRestingTriptolide <- rowMeans(data2[,grep("Resting_0h_Triptolide",colnames(data2))])
data2$meanCountsActivatedUntreated <- rowMeans(data2[,grep("Activated_0h_untreated",colnames(data2))])
data2$meanCountsActivatedTriptolide <- rowMeans(data2[,grep("Activated_0h_Triptolide",colnames(data2))])

data2[data2$Deg3h_LR < 0 , "Deg3h_LR"] <- 0
data2[data2$Deg3h_LA < 0 & !is.na(data2$Deg3h_LA) , "Deg3h_LA"] <- 0
ggplot(data = data2, aes(x = log10(meanCountsActivatedTriptolide), y = log10(meanCountsActivatedUntreated), color = (Deg3h_LA))) + 
  geom_point(alpha = 0.5) + scale_colour_gradient2(midpoint = 0.5) + geom_abline(slope = 1, intercept = 0)

ggplot(data = data2, aes(x = log10(meanCountsRestingTriptolide), y = log10(meanCountsRestingUntreated), color = (Deg3h_LR))) + 
  geom_point(alpha = 0.5) + scale_colour_gradient2(midpoint = 0.5) + geom_abline(slope = 1, intercept = 0)
```

# Macrophages

## Parameters
```{r, include=FALSE}
setwd("../")

filename <- "data/HTSeq_count_stats_all_libraries.csv"
data <- read.csv(filename, sep = ",")
rownames(data) <- data$Gene_id
data <- cbind(data[grep("^ENSMUSG", data$Gene_id),
                   grep("^LPS.*untreated.*exon", colnames(data))
              ],
              data[grep("^ENSMUSG", data$Gene_id),
                   grep("^LPS.*0h_Triptolide_(m|i).*exon", colnames(data))
              ]
        )

samples <- colnames(data)
words_df <- as.data.frame(matrix(ncol = length(samples), nrow = 6))
colnames(words_df) <- samples

for (i in samples){
    words_df[,i] <- strsplit(x = i, split = "_")
}

repliate <- substr(words_df[5,], start = 2, stop = 3)
times <- unlist(words_df[3,])
conditions <- unlist(words_df[1,])
drug <- unlist(words_df[4,])

coldata <- data.frame(samples)
coldata$condition <- conditions
coldata$time <- times
coldata$replicate <- repliate
coldata$drug <- drug
```

## PCA of all samples

```{r}
dds <- DESeqDataSetFromMatrix(countData = data,
                              colData = coldata,
                              design = ~ condition + time + drug + replicate)
dds <- DESeq(dds)

plotDispEsts(dds)
vsd <- varianceStabilizingTransformation(dds, blind=TRUE)
plotPCA(vsd, intgroup = c("time", "condition", "drug"))

rld <- rlogTransformation(dds, blind=TRUE)
plotPCA(rld, intgroup = c("time", "condition", "drug"))
```

# Render 
```{r}
# rmarkdown::render("check_untr_vsTrip.Rmd", output_file = paste0('check_untr_vsTrip_',Sys.time(),'.pdf'))
```

