---
title: "Retrieve GO terms"
author: "`r Sys.info()[['user']]`"
date: "`r format(Sys.time(), '%Y/%m/%d %H:%M:%S')`"
output:
  pdf_document:
    toc: true
    toc_depth: 4
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### Install and Load libraries
```{r eval=False}
install.packages("BiocManager")
BiocManager::install("biomaRt")
```


```{r}
library(biomaRt)
```


### Load databases
```{r}
load("/home.users/dcluet/Programmes/RMI2_GFF_FASTA_compilation/src/data/database.RData")
mart <- useDataset("mmusculus_gene_ensembl", useMart("ensembl"))
```


### Get the GO for all gene

## Function list.biomart
```{r}
genes = unique(c(LR, LA, MR, MA))
genes = str_extract(genes, "ENSMUSG...........")

# 2. Recover goTerm for this genes and keep go term with at least --------

mart <- useDataset("mmusculus_gene_ensembl", useMart("ensembl"))
result <- getBM(attributes=c("ensembl_gene_id","go_id","name_1006"), filters="ensembl_gene_id", values=genes, mart = mart)

sum_result = as.data.frame(table(result$name_1006))
goTermUsed <- as.character(sum_result[sum_result$Freq>20,"Var1"])
goTermUsed = goTermUsed[-1]
db_gene_GO = result[result$name_1006%in%goTermUsed,]
colnames(db_gene_GO)[1] = "ensemblID" 
```

## Get a dataframe from the database and ensembl GO



