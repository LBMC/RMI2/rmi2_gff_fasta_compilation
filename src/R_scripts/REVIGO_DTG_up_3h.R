

# A plotting R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800


# --------------------------------------------------------------------------
# If you don't have the ggplot2 package installed, uncomment the following line:
# install.packages( "ggplot2" );
library( ggplot2 );
# --------------------------------------------------------------------------
# If you don't have the scales package installed, uncomment the following line:
# install.packages( "scales" );
library( scales );


# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","frequency_%","plot_X","plot_Y","plot_size","log10_p_value","uniqueness","dispensability");
revigo.data <- rbind(c("GO:0002181","cytoplasmic translation", 0.064,-5.931, 3.039, 3.915,-18.9656,0.716,0.000),
                     c("GO:0002479","antigen processing and presentation of exogenous peptide antigen via MHC class I, TAP-dependent", 0.001,-5.345,-3.782, 1.892,-3.1457,0.953,0.000),
                     c("GO:0042254","ribosome biogenesis", 1.422, 2.527,-3.387, 5.261,-11.9363,0.611,0.000),
                     c("GO:0050821","protein stabilization", 0.045,-0.716, 8.866, 3.763,-2.0720,0.855,0.000),
                     c("GO:0098609","cell-cell adhesion", 0.251,-7.065,-2.432, 4.507,-3.5378,0.953,0.000),
                     c("GO:1901998","toxin transport", 0.008, 6.148, 5.518, 3.026,-3.9661,0.928,0.000),
                     c("GO:0071353","cellular response to interleukin-4", 0.005, 6.005, 0.259, 2.841,-7.2295,0.875,0.019),
                     c("GO:0051085","chaperone mediated protein folding requiring cofactor", 0.010,-1.761,-5.337, 3.117,-4.6496,0.902,0.020),
                     c("GO:0001649","osteoblast differentiation", 0.040, 2.772,-0.857, 3.710,-2.3305,0.919,0.021),
                     c("GO:0006457","protein folding", 0.903,-2.715,-2.770, 5.064,-8.6009,0.917,0.027),
                     c("GO:1904871","positive regulation of protein localization to Cajal body", 0.002, 2.128, 6.758, 2.364,-3.8676,0.748,0.135),
                     c("GO:0008380","RNA splicing", 0.413,-5.779, 5.748, 4.725,-5.9000,0.698,0.203),
                     c("GO:0009615","response to virus", 0.117, 6.199, 1.683, 4.175,-5.6509,0.910,0.223),
                     c("GO:0051603","proteolysis involved in cellular protein catabolic process", 0.759,-6.866, 2.090, 4.988,-2.1121,0.768,0.273),
                     c("GO:0032212","positive regulation of telomere maintenance via telomerase", 0.010,-0.329, 4.440, 3.114,-3.0370,0.530,0.395),
                     c("GO:0006413","translational initiation", 0.518,-5.494, 3.444, 4.823,-7.3359,0.678,0.429),
                     c("GO:1900087","positive regulation of G1/S transition of mitotic cell cycle", 0.009, 1.064, 6.831, 3.061,-2.3799,0.744,0.441),
                     c("GO:1901224","positive regulation of NIK/NF-kappaB signaling", 0.012, 2.530, 4.948, 3.189,-2.6779,0.711,0.448),
                     c("GO:0051131","chaperone-mediated protein complex assembly", 0.016, 2.402,-3.823, 3.324,-2.1986,0.707,0.462),
                     c("GO:1904874","positive regulation of telomerase RNA localization to Cajal body", 0.004, 1.724, 6.944, 2.674,-2.9886,0.725,0.463),
                     c("GO:0006986","response to unfolded protein", 0.037, 6.092, 0.667, 3.677,-2.2832,0.891,0.527),
                     c("GO:0006417","regulation of translation", 0.692,-3.500, 4.947, 4.948,-5.1833,0.575,0.528),
                     c("GO:0006414","translational elongation", 0.777,-5.847, 3.800, 4.999,-4.3766,0.669,0.550),
                     c("GO:0042026","protein refolding", 0.069,-2.149,-5.256, 3.949,-2.1986,0.898,0.567),
                     c("GO:0006397","mRNA processing", 0.561,-5.717, 5.467, 4.857,-4.3899,0.692,0.624),
                     c("GO:0000028","ribosomal small subunit assembly", 0.020, 2.040,-3.415, 3.409,-13.2170,0.647,0.636),
                     c("GO:0010628","positive regulation of gene expression", 0.653,-1.481, 5.863, 4.923,-2.0326,0.590,0.643),
                     c("GO:1904851","positive regulation of establishment of protein localization to telomere", 0.002, 2.016, 6.419, 2.410,-3.6959,0.747,0.653),
                     c("GO:0006412","translation", 5.686,-5.433, 3.931, 5.863,-89.9615,0.622,0.696));

one.data <- data.frame(revigo.data);
names(one.data) <- revigo.names;
one.data <- one.data [(one.data$plot_X != "null" & one.data$plot_Y != "null"), ];
one.data$plot_X <- as.numeric( as.character(one.data$plot_X) );
one.data$plot_Y <- as.numeric( as.character(one.data$plot_Y) );
one.data$plot_size <- as.numeric( as.character(one.data$plot_size) );
one.data$log10_p_value <- as.numeric( as.character(one.data$log10_p_value) );
one.data$frequency <- as.numeric( as.character(one.data$frequency) );
one.data$uniqueness <- as.numeric( as.character(one.data$uniqueness) );
one.data$dispensability <- as.numeric( as.character(one.data$dispensability) );
#head(one.data);


# --------------------------------------------------------------------------
# Names of the axes, sizes of the numbers and letters, names of the columns,
# etc. can be changed below
library(tidyverse)
one.x_range = max(one.data$plot_X) - min(one.data$plot_X);
one.y_range = max(one.data$plot_Y) - min(one.data$plot_Y);
ex <- one.data %>% filter (term_ID %in% c("GO:0006412",
                                          "GO:0006397",
                                          "GO:0008380",
                                          "GO:0042254",
                                          "GO:0071353",
                                          "GO:0006457",
                                          "GO:0050821"
                                          )) 
require("ggrepel")
set.seed(42)
ggplot( data = one.data ) + 
  geom_point( aes( plot_X, plot_Y, colour = log10_p_value, size = plot_size), alpha = I(0.7) ) +
  scale_colour_gradientn( colours = c("red", "blue", "#69b3a2"), limits = c( min(one.data$log10_p_value), 0) ) + 
  # geom_point( aes(plot_X, plot_Y, size = plot_size), shape = 21, fill = "transparent", colour = I (alpha ("black", 0.6) )) + 
  scale_size( range=c(1, 20)) + 
  theme_bw() +
  labs (x = "semantic space x", y = "semantic space y") +
  # xlim(min(one.data$plot_X)-one.x_range/10,max(one.data$plot_X)+one.x_range/10) + 
  # ylim(min(one.data$plot_Y)-one.y_range/10,max(one.data$plot_Y)+one.y_range/10) +
  geom_text_repel( data = ex, aes(plot_X, plot_Y, label = description), colour = "black", size = 5,
                   box.padding = unit(0.5, "lines"),
                   point.padding = unit(0.5, "lines"), 
                   segment.size = 1) + 
  ggtitle("Differential ribosome density - upregulated with the activation ")



# --------------------------------------------------------------------------
# Output the plot to screen

p1;

# Uncomment the line below to also save the plot to a file.
# The file type depends on the extension (default=pdf).

# ggsave("C:/Users/path_to_your_file/revigo-plot.pdf");




# A treemap R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800

# author: Anton Kratz <anton.kratz@gmail.com>, RIKEN Omics Science Center, Functional Genomics Technology Team, Japan
# created: Fri, Nov 02, 2012  7:25:52 PM
# last change: Fri, Nov 09, 2012  3:20:01 PM

# -----------------------------------------------------------------------------
# If you don't have the treemap package installed, uncomment the following line:
# install.packages( "treemap" );
library(treemap) 								# treemap package by Martijn Tennekes

# Set the working directory if necessary
# setwd("C:/Users/username/workingdir");

# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","freqInDbPercent","abslog10pvalue","uniqueness","dispensability","representative");
revigo.data <- rbind(c("GO:0002181","cytoplasmic translation",0.064,16.1763,0.673,0.000,"cytoplasmic translation"),
                     c("GO:0006412","translation",5.686,86.8712,0.575,0.696,"cytoplasmic translation"),
                     c("GO:0006417","regulation of translation",0.692,3.2973,0.557,0.528,"cytoplasmic translation"),
                     c("GO:0006364","rRNA processing",0.952,12.6424,0.387,0.218,"cytoplasmic translation"),
                     c("GO:0006413","translational initiation",0.518,5.3249,0.632,0.429,"cytoplasmic translation"),
                     c("GO:0006414","translational elongation",0.777,2.5657,0.623,0.550,"cytoplasmic translation"),
                     c("GO:0008380","RNA splicing",0.413,3.9559,0.639,0.656,"cytoplasmic translation"),
                     c("GO:0006397","mRNA processing",0.561,2.5556,0.633,0.677,"cytoplasmic translation"),
                     c("GO:0000028","ribosomal small subunit assembly",0.020,10.7292,0.596,0.637,"cytoplasmic translation"),
                     c("GO:0002479","antigen processing and presentation of exogenous peptide antigen via MHC class I, TAP-dependent",0.001,1.4776,0.934,0.000,"antigen processing and presentation of exogenous peptide antigen via MHC class I, TAP-dependent"),
                     c("GO:0098609","cell-cell adhesion",0.251,1.8485,0.935,0.000,"cell-cell adhesion"),
                     c("GO:1901998","toxin transport",0.008,2.1994,0.900,0.000,"toxin transport"),
                     c("GO:1904871","positive regulation of protein localization to Cajal body",0.002,2.1406,0.768,0.135,"toxin transport"),
                     c("GO:0032212","positive regulation of telomere maintenance via telomerase",0.010,1.3870,0.557,0.395,"toxin transport"),
                     c("GO:0090666","scaRNA localization to Cajal body",0.001,1.3674,0.830,0.301,"toxin transport"),
                     c("GO:1904851","positive regulation of establishment of protein localization to telomere",0.002,1.9880,0.767,0.653,"toxin transport"),
                     c("GO:0071353","cellular response to interleukin-4",0.005,5.2532,0.896,0.019,"cellular response to interleukin-4"),
                     c("GO:0009615","response to virus",0.117,3.7368,0.920,0.223,"cellular response to interleukin-4"),
                     c("GO:0051085","chaperone mediated protein folding requiring cofactor",0.010,2.7902,0.909,0.020,"chaperone mediated protein folding requiring cofactor"),
                     c("GO:0006457","protein folding",0.903,6.3557,0.900,0.027,"protein folding"));

stuff <- data.frame(revigo.data);
names(stuff) <- revigo.names;

stuff$abslog10pvalue <- as.numeric( as.character(stuff$abslog10pvalue) );
stuff$freqInDbPercent <- as.numeric( as.character(stuff$freqInDbPercent) );
stuff$uniqueness <- as.numeric( as.character(stuff$uniqueness) );
stuff$dispensability <- as.numeric( as.character(stuff$dispensability) );

# by default, outputs to a PDF file
pdf( file="revigo_treemap.pdf", width=16, height=9 ) # width and height are in inches

# check the tmPlot command documentation for all possible parameters - there are a lot more
tmPlot(
  stuff,
  index = c("representative","description"),
  vSize = "abslog10pvalue",
  type = "categorical",
  vColor = "representative",
  title = "REVIGO Gene Ontology treemap",
  inflate.labels = FALSE,      # set this to TRUE for space-filling group labels - good for posters
  lowerbound.cex.labels = 0,   # try to draw as many labels as possible (still, some small squares may not get a label)
  bg.labels = "#CCCCCCAA",     # define background color of group labels
  # "#CCCCCC00" is fully transparent, "#CCCCCCAA" is semi-transparent grey, NA is opaque
  position.legend = "none"
)

dev.off()
