

# A plotting R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800


# --------------------------------------------------------------------------
# If you don't have the ggplot2 package installed, uncomment the following line:
# install.packages( "ggplot2" );
library( ggplot2 );
# --------------------------------------------------------------------------
# If you don't have the scales package installed, uncomment the following line:
# install.packages( "scales" );
library( scales );


# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","frequency_%","plot_X","plot_Y","plot_size","log10_p_value","uniqueness","dispensability");
revigo.data <- rbind(c("GO:0002376","immune system process", 0.600,-1.710,-2.534, 4.886,-3.4131,0.985,0.000),
                     c("GO:0034097","response to cytokine", 0.136,-6.637, 2.900, 4.242,-5.2348,0.787,0.000),
                     c("GO:0042254","ribosome biogenesis", 1.422, 4.214,-2.140, 5.261,-27.4263,0.861,0.000),
                     c("GO:0044406","adhesion of symbiont to host", 0.008,-3.772,-3.533, 3.012,-2.7034,0.958,0.000),
                     c("GO:0045944","positive regulation of transcription from RNA polymerase II promoter", 0.369, 1.677, 5.620, 4.675,-10.8760,0.611,0.000),
                     c("GO:0048511","rhythmic process", 0.077,-5.010,-3.128, 3.994,-3.4407,0.985,0.000),
                     c("GO:0032259","methylation", 3.103,-5.623,-3.751, 5.600,-5.0561,0.975,0.015),
                     c("GO:0006915","apoptotic process", 0.406, 3.839,-3.481, 4.717,-6.0995,0.750,0.029),
                     c("GO:0000060","protein import into nucleus, translocation", 0.012,-2.598,-4.999, 3.175,-2.9866,0.920,0.056),
                     c("GO:0008283","cell proliferation", 0.394,-4.707,-2.395, 4.704,-5.3056,0.936,0.074),
                     c("GO:0000082","G1/S transition of mitotic cell cycle", 0.062, 4.048,-4.314, 3.903,-3.1042,0.896,0.145),
                     c("GO:0016049","cell growth", 0.153, 4.769,-4.508, 4.294,-2.8584,0.883,0.156),
                     c("GO:0006730","one-carbon metabolic process", 0.328, 5.219,-3.383, 4.625,-3.0853,0.858,0.167),
                     c("GO:0043388","positive regulation of DNA binding", 0.013,-0.636, 3.495, 3.217,-2.3228,0.817,0.173),
                     c("GO:0050821","protein stabilization", 0.045,-0.174, 3.787, 3.763,-2.2505,0.844,0.189),
                     c("GO:0042832","defense response to protozoan", 0.004,-6.409, 3.789, 2.682,-3.1888,0.825,0.220),
                     c("GO:0008285","negative regulation of cell proliferation", 0.128,-0.576, 4.762, 4.214,-5.2237,0.695,0.221),
                     c("GO:0010501","RNA secondary structure unwinding", 0.025, 2.126, 8.464, 3.507,-5.2657,0.861,0.222),
                     c("GO:0038061","NIK/NF-kappaB signaling", 0.018,-3.977, 4.566, 3.360,-4.0092,0.727,0.242),
                     c("GO:0006413","translational initiation", 0.518, 5.383, 4.334, 4.823,-8.1503,0.792,0.244),
                     c("GO:0006564","L-serine biosynthetic process", 0.075, 6.588,-0.821, 3.981,-2.9330,0.817,0.271),
                     c("GO:0032922","circadian regulation of gene expression", 0.012, 2.719, 1.094, 3.190,-3.1705,0.734,0.271),
                     c("GO:0001522","pseudouridine synthesis", 0.350, 4.295, 6.623, 4.652,-4.3781,0.798,0.274),
                     c("GO:0009303","rRNA transcription", 0.022, 3.028, 7.565, 3.457,-2.9456,0.808,0.286),
                     c("GO:0006955","immune response", 0.337,-5.682, 3.618, 4.635,-2.8073,0.757,0.300),
                     c("GO:0046777","protein autophosphorylation", 0.077, 6.226, 3.150, 3.993,-3.5987,0.840,0.327),
                     c("GO:1903146","regulation of mitophagy", 0.009, 3.596,-0.083, 3.081,-2.1748,0.754,0.332),
                     c("GO:0006396","RNA processing", 3.210, 4.273, 6.303, 5.615,-2.9593,0.787,0.341),
                     c("GO:0034976","response to endoplasmic reticulum stress", 0.100,-6.034, 4.752, 4.106,-2.3244,0.796,0.344),
                     c("GO:0002931","response to ischemia", 0.004,-5.816, 2.750, 2.747,-2.0304,0.857,0.347),
                     c("GO:0006397","mRNA processing", 0.561, 3.765, 7.044, 4.857,-2.9652,0.791,0.358),
                     c("GO:0006479","protein methylation", 0.343, 6.371, 3.820, 4.643,-4.2181,0.832,0.370),
                     c("GO:0007249","I-kappaB kinase/NF-kappaB signaling", 0.057,-3.843, 4.725, 3.861,-2.3228,0.709,0.384),
                     c("GO:0051881","regulation of mitochondrial membrane potential", 0.014,-0.084, 2.487, 3.264,-2.0110,0.853,0.395),
                     c("GO:0006366","transcription from RNA polymerase II promoter", 1.430, 4.047, 6.453, 5.264,-2.2679,0.784,0.406),
                     c("GO:0006865","amino acid transport", 0.813,-2.333,-4.842, 5.018,-2.1969,0.913,0.411),
                     c("GO:0035914","skeletal muscle cell differentiation", 0.015, 1.613,-5.288, 3.286,-3.0140,0.840,0.441),
                     c("GO:0006446","regulation of translational initiation", 0.091, 2.809, 4.186, 4.067,-5.2097,0.691,0.441),
                     c("GO:0042149","cellular response to glucose starvation", 0.016,-5.900, 4.595, 3.301,-2.4899,0.793,0.446),
                     c("GO:0071456","cellular response to hypoxia", 0.022,-6.474, 3.577, 3.457,-2.4211,0.768,0.450),
                     c("GO:0045822","negative regulation of heart contraction", 0.004, 0.661,-2.823, 2.682,-2.3327,0.746,0.456),
                     c("GO:0016569","covalent chromatin modification", 0.424, 6.470, 1.826, 4.736,-3.9353,0.787,0.462),
                     c("GO:0007569","cell aging", 0.049, 1.870,-5.648, 3.794,-2.4899,0.852,0.474),
                     c("GO:1904874","positive regulation of telomerase RNA localization to Cajal body", 0.004,-1.163, 1.402, 2.674,-3.5595,0.776,0.503),
                     c("GO:0001701","in utero embryonic development", 0.056, 0.855,-5.881, 3.860,-2.4298,0.883,0.503),
                     c("GO:1904628","cellular response to phorbol 13-acetate 12-myristate", 0.001,-6.869, 2.023, 2.146,-2.9348,0.796,0.518),
                     c("GO:0006468","protein phosphorylation", 4.137, 5.688, 4.426, 5.725,-3.9937,0.789,0.533),
                     c("GO:0035458","cellular response to interferon-beta", 0.003,-6.888, 2.535, 2.575,-3.5097,0.780,0.548),
                     c("GO:0006418","tRNA aminoacylation for protein translation", 1.099, 4.806, 3.463, 5.149,-7.4517,0.661,0.553),
                     c("GO:0001889","liver development", 0.023, 0.536,-5.811, 3.471,-2.7707,0.883,0.561),
                     c("GO:0051092","positive regulation of NF-kappaB transcription factor activity", 0.025, 1.202, 6.107, 3.502,-3.4527,0.657,0.572),
                     c("GO:0061158","3'-UTR-mediated mRNA destabilization", 0.004, 2.387, 3.575, 2.700,-2.9456,0.683,0.576),
                     c("GO:0031397","negative regulation of protein ubiquitination", 0.017, 2.168, 3.854, 3.342,-2.8582,0.674,0.578),
                     c("GO:0009451","RNA modification", 1.778, 4.541, 6.078, 5.358,-2.6270,0.775,0.581),
                     c("GO:0009791","post-embryonic development", 0.163, 0.683,-5.827, 4.320,-2.3836,0.876,0.589),
                     c("GO:0051591","response to cAMP", 0.010,-6.646, 1.947, 3.111,-3.3064,0.806,0.591),
                     c("GO:0043123","positive regulation of I-kappaB kinase/NF-kappaB signaling", 0.037,-3.373, 4.861, 3.676,-5.6520,0.630,0.604),
                     c("GO:0006954","inflammatory response", 0.110,-6.082, 4.132, 4.151,-2.9823,0.814,0.605),
                     c("GO:0008053","mitochondrial fusion", 0.017, 5.515,-2.158, 3.338,-2.1372,0.826,0.608),
                     c("GO:0006351","transcription, DNA-templated",10.659, 4.164, 5.770, 6.136,-12.1815,0.739,0.615),
                     c("GO:0016310","phosphorylation", 7.764, 2.162,-0.482, 5.998,-2.4421,0.888,0.615),
                     c("GO:0002829","negative regulation of type 2 immune response", 0.002,-3.422, 3.571, 2.425,-2.2789,0.664,0.617),
                     c("GO:0051726","regulation of cell cycle", 0.547, 3.151,-0.384, 4.846,-2.4608,0.762,0.619),
                     c("GO:0034599","cellular response to oxidative stress", 0.224,-6.183, 3.744, 4.458,-2.0377,0.737,0.627),
                     c("GO:1990440","positive regulation of transcription from RNA polymerase II promoter in response to endoplasmic reticulum stress", 0.002,-1.939, 5.683, 2.410,-3.3410,0.624,0.637),
                     c("GO:0007507","heart development", 0.127, 0.744,-5.854, 4.211,-2.2441,0.872,0.640),
                     c("GO:0032496","response to lipopolysaccharide", 0.043,-6.478, 2.685, 3.745,-4.6310,0.780,0.651),
                     c("GO:0060546","negative regulation of necroptotic process", 0.002, 0.638, 0.475, 2.362,-2.1802,0.697,0.660),
                     c("GO:1902895","positive regulation of pri-miRNA transcription from RNA polymerase II promoter", 0.004, 0.967, 6.746, 2.728,-3.3606,0.699,0.667),
                     c("GO:0000463","maturation of LSU-rRNA from tricistronic rRNA transcript (SSU-rRNA, 5.8S rRNA, LSU-rRNA)", 0.037, 4.577, 5.209, 3.676,-3.1317,0.751,0.668),
                     c("GO:0072655","establishment of protein localization to mitochondrion", 0.116,-3.021,-5.259, 4.172,-2.0581,0.950,0.668),
                     c("GO:0002437","inflammatory response to antigenic stimulus", 0.007,-6.214, 4.513, 2.978,-2.3621,0.774,0.669),
                     c("GO:0098609","cell-cell adhesion", 0.251,-5.504,-4.560, 4.507,-2.3566,0.954,0.671),
                     c("GO:0008652","cellular amino acid biosynthetic process", 2.932, 5.943, 0.052, 5.575,-4.6670,0.769,0.677),
                     c("GO:1904385","cellular response to angiotensin", 0.005,-6.499, 2.140, 2.785,-2.0581,0.782,0.685),
                     c("GO:0006611","protein export from nucleus", 0.020,-2.720,-5.001, 3.399,-2.5797,0.944,0.685),
                     c("GO:0033209","tumor necrosis factor-mediated signaling pathway", 0.016,-4.468, 3.700, 3.305,-2.9881,0.654,0.698));

one.data <- data.frame(revigo.data);
names(one.data) <- revigo.names;
one.data <- one.data [(one.data$plot_X != "null" & one.data$plot_Y != "null"), ];
one.data$plot_X <- as.numeric( as.character(one.data$plot_X) );
one.data$plot_Y <- as.numeric( as.character(one.data$plot_Y) );
one.data$plot_size <- as.numeric( as.character(one.data$plot_size) );
one.data$log10_p_value <- as.numeric( as.character(one.data$log10_p_value) );
one.data$frequency <- as.numeric( as.character(one.data$frequency) );
one.data$uniqueness <- as.numeric( as.character(one.data$uniqueness) );
one.data$dispensability <- as.numeric( as.character(one.data$dispensability) );
#head(one.data);


# --------------------------------------------------------------------------
# Names of the axes, sizes of the numbers and letters, names of the columns,
# etc. can be changed below
library(tidyverse)
one.x_range = max(one.data$plot_X) - min(one.data$plot_X);
one.y_range = max(one.data$plot_Y) - min(one.data$plot_Y);
ex <- one.data %>% filter (term_ID %in% c("GO:0034097", 
                                          "GO:0038061", 
                                          "GO:0006915", 
                                          "GO:0042254", 
                                          "GO:0045944", 
                                          "GO:0042254", 
                                          "GO:0006413",
                                          "GO:0006397",
                                          "GO:0002376",
                                          "GO:0006418")) 
require("ggrepel")
set.seed(42)
ggplot( data = one.data ) + 
  geom_point( aes( plot_X, plot_Y, colour = log10_p_value, size = plot_size), alpha = I(0.7) ) +
  scale_colour_gradientn( colours = c("red", "blue", "#69b3a2"), limits = c( min(one.data$log10_p_value), 0) ) + 
  # geom_point( aes(plot_X, plot_Y, size = plot_size), shape = 21, fill = "transparent", colour = I (alpha ("black", 0.6) )) + 
  scale_size( range=c(1, 20)) + 
  theme_bw() +
  labs (x = "semantic space x", y = "semantic space y") +
  # xlim(min(one.data$plot_X)-one.x_range/10,max(one.data$plot_X)+one.x_range/10) + 
  # ylim(min(one.data$plot_Y)-one.y_range/10,max(one.data$plot_Y)+one.y_range/10) +
  geom_text_repel( data = ex, aes(plot_X, plot_Y, label = description), colour = "black", size = 5,
                   box.padding = unit(0.5, "lines"),
                   point.padding = unit(0.5, "lines"), 
                   segment.size = 1) + 
  ggtitle("Differential gene expression - upregulated with the activation ")


write.csv(one.data, file ="results/GO_analysis_up_DEG.csv")

# --------------------------------------------------------------------------
# Output the plot to screen

p1;

# Uncomment the line below to also save the plot to a file.
# The file type depends on the extension (default=pdf).

# ggsave("C:/Users/path_to_your_file/revigo-plot.pdf");




# A treemap R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800

# author: Anton Kratz <anton.kratz@gmail.com>, RIKEN Omics Science Center, Functional Genomics Technology Team, Japan
# created: Fri, Nov 02, 2012  7:25:52 PM
# last change: Fri, Nov 09, 2012  3:20:01 PM

# -----------------------------------------------------------------------------
# If you don't have the treemap package installed, uncomment the following line:
# install.packages( "treemap" );
library(treemap) 								# treemap package by Martijn Tennekes

# Set the working directory if necessary
# setwd("C:/Users/username/workingdir");

# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","freqInDbPercent","abslog10pvalue","uniqueness","dispensability","representative");
revigo.data <- rbind(c("GO:0000122","negative regulation of transcription from RNA polymerase II promoter",0.199,7.9134,0.549,0.000,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0043123","positive regulation of I-kappaB kinase/NF-kappaB signaling",0.037,3.1748,0.630,0.192,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0010501","RNA secondary structure unwinding",0.025,2.8730,0.764,0.213,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0006413","translational initiation",0.518,5.3050,0.655,0.231,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0034097","response to cytokine",0.136,2.8669,0.847,0.255,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0001522","pseudouridine synthesis",0.350,2.1710,0.682,0.260,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0046777","protein autophosphorylation",0.077,1.5378,0.757,0.327,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0006479","protein methylation",0.343,2.0438,0.733,0.370,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0038061","NIK/NF-kappaB signaling",0.018,1.8800,0.740,0.374,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:1904874","positive regulation of telomerase RNA localization to Cajal body",0.004,1.5110,0.729,0.439,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0006446","regulation of translational initiation",0.091,2.8876,0.590,0.441,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0006351","transcription, DNA-templated",10.659,9.0352,0.571,0.471,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0051092","positive regulation of NF-kappaB transcription factor activity",0.025,1.4392,0.580,0.491,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:1902895","positive regulation of pri-miRNA transcription from RNA polymerase II promoter",0.004,1.3797,0.578,0.528,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0006468","protein phosphorylation",4.137,1.8783,0.679,0.533,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:1990440","positive regulation of transcription from RNA polymerase II promoter in response to endoplasmic reticulum stress",0.002,1.3703,0.562,0.534,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0035458","cellular response to interferon-beta",0.003,1.4845,0.834,0.548,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0006418","tRNA aminoacylation for protein translation",1.099,4.6734,0.523,0.553,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0051591","response to cAMP",0.010,1.3461,0.856,0.591,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0008285","negative regulation of cell proliferation",0.128,2.8793,0.689,0.610,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0032496","response to lipopolysaccharide",0.043,2.3886,0.850,0.651,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0008652","cellular amino acid biosynthetic process",2.932,2.4061,0.689,0.677,"negative regulation of transcription from RNA polymerase II promoter"),
                     c("GO:0002376","immune system process",0.600,1.4212,0.955,0.000,"immune system process"),
                     c("GO:0042254","ribosome biogenesis",1.422,23.8028,0.834,0.000,"ribosome biogenesis"),
                     c("GO:0016569","covalent chromatin modification",0.424,1.8335,0.734,0.462,"ribosome biogenesis"),
                     c("GO:0048511","rhythmic process",0.077,1.4380,0.955,0.000,"rhythmic process"),
                     c("GO:0032259","methylation",3.103,2.7553,0.939,0.014,"methylation"),
                     c("GO:0006915","apoptotic process",0.406,3.5176,0.743,0.028,"apoptotic process"),
                     c("GO:0008283","cell proliferation",0.394,2.8865,0.921,0.074,"cell proliferation"));

stuff <- data.frame(revigo.data);
names(stuff) <- revigo.names;

stuff$abslog10pvalue <- as.numeric( as.character(stuff$abslog10pvalue) );
stuff$freqInDbPercent <- as.numeric( as.character(stuff$freqInDbPercent) );
stuff$uniqueness <- as.numeric( as.character(stuff$uniqueness) );
stuff$dispensability <- as.numeric( as.character(stuff$dispensability) );

# by default, outputs to a PDF file
pdf( file="revigo_treemap.pdf", width=16, height=9 ) # width and height are in inches

# check the tmPlot command documentation for all possible parameters - there are a lot more
tmPlot(
  stuff,
  index = c("representative","description"),
  vSize = "abslog10pvalue",
  type = "categorical",
  vColor = "representative",
  title = "REVIGO Gene Ontology treemap",
  inflate.labels = FALSE,      # set this to TRUE for space-filling group labels - good for posters
  lowerbound.cex.labels = 0,   # try to draw as many labels as possible (still, some small squares may not get a label)
  bg.labels = "#CCCCCCAA",     # define background color of group labels
  # "#CCCCCC00" is fully transparent, "#CCCCCCAA" is semi-transparent grey, NA is opaque
  position.legend = "none"
)

dev.off()
