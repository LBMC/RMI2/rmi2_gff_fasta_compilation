####################################
## Random Forest for Lymphocytes ##
###################################

# Configuration -------------------
pks <- list('ggplot2' , 'tidyverse' , 'randomForest', 'caret', 'doParallel', 'gridExtra', 'grid')
lapply(pks , library , character.only = T, quietly = FALSE)
theme_set(theme_bw())

#color point
color_resting = "#69b3a2"
color_activated = "#404080"

# expressed Genes 
expressedGenes <- list(unlist(read.csv(file = "~/RMI2/gitlab/tdd/results/filtred_genes_Lympho_Resting.csv")),
                       unlist(read.csv(file = "~/RMI2/gitlab/tdd/results/filtred_genes_Lympho_Activated.csv")),
                       unlist(read.csv(file = "~/RMI2/gitlab/tdd/results/filtred_genes_Macro_Resting.csv")),
                       unlist(read.csv(file = "~/RMI2/gitlab/tdd/results/filtred_genes_Macro_Activated.csv")))
names(expressedGenes) <- c("Lympho_Resting", "Lympho_Activated", "Macro_Resting", "Macro_Activated")

# funtions ----------------------------------------------------------------
plotImp <- function(model, model_name) {
  imp <- varImp(model)
  imp <- imp$importance
  colnames(imp)[1] <- "importance"
  imp$variable <- rownames(imp)
  order_var <- imp[order(imp[,1], decreasing = FALSE),"variable"]
  imp$variable <- factor(imp$variable, levels = order_var)
  
  # Determine in the state type
  if (length(grep("Activated|delta", model_name, value = TRUE)) == 1 ) {
    color <- "#404080"
  }  else if (length(grep("Resting", model_name, value = TRUE)) == 1 ) {
    color <- "#69b3a2"
  }
  
  p1 <- ggplot(data = imp, aes_string(x = "variable", y = colnames(imp)[1])) +
    geom_point(stat = "identity", color = color) + 
    coord_flip() + ggtitle(model_name) +
    theme(legend.position = "none",
          axis.text.y = element_text(size = 10)) +
  ggtitle(model_name)
  return(p1)
}


makeRandomForest <- function(x, filtering = TRUE, method = "regression") {
  
  message(x)
  
  # Determine the cell type
  if (length(grep("Lympho",x, value = TRUE)) == 1 ) {
    cell <- "Lympho"
    if (length(grep("delta", x, value = TRUE)) == 1 ) {
      state <- "delta"
      color <- color_activated
      spe <- index_delta_Lympho
    } else {
      spe <- index_lympho
    }
  }  else if (length(grep("Macro",x, value = TRUE)) == 1 ) {
    cell <- "Macro"
    if (length(grep("delta", x, value = TRUE)) == 1 ) {
      state <- "delta"
      color <- color_activated
      spe <- index_delta_Macro
    } else {
    spe <- index_macro
    }
  }
  # Determine in the state type
  if (length(grep("Activated", x, value = TRUE)) == 1 ) {
    state <- "Activated"
    color <- color_activated
    spe <- gsub(pattern = "Resting", replacement = "Activated", x = spe)
  }  else if (length(grep("Resting", x, value = TRUE)) == 1 ) {
    state <- "Resting"
    color <- color_resting
  } 
  
  # create sub_data for the analysis i.e. expressed genes with only column of interest
  
  if (filtering) {
    if (state == "delta") {
      sub_data <- table %>% filter(gene_id %in% expressedGenes[[paste0(cell, "_Resting")]] & 
                                   gene_id %in% expressedGenes[[paste0(cell, "_Activated")]]) %>%
        select(x, index, spe)
    } else {
      sub_data <- table %>% filter(gene_id %in% expressedGenes[[paste0(cell, "_", state)]]) %>%
        select(x, index, spe)
    }
  } else {
    sub_data <- table %>% select(x, index, spe)
  }
  
  sub_data[is.na(sub_data$sumKozakuOFR), "sumKozakuOFR"] <- 0
  
  # clean the table if necessary
  sub_data <- na.exclude(sub_data) 
  sub_data <- sub_data[is.finite(rowSums(sub_data)),]
  
  if (method == "classification") {
    stats <- boxplot(sub_data[,1], plot = FALSE)$stats
    sub_data$predict <- "HighTDD"
    sub_data[sub_data[,1] < stats[4,1], "predict"] <- "mediumTDD"
    sub_data[sub_data[,1] < stats[2,1], "predict"] <- "lowTDD"
    sub_data <- sub_data[,-1]
    sub_data$predict <- as.factor(sub_data$predict)
  } else if (method == "regression") {
    colnames(sub_data)[1] <- "predict"
  } else {
    stop(paste("method must be \"regression\" or \"classification\""))
  }

  # Divde the data in train and validation table
  set.seed(1043)
  train <- sample(nrow(sub_data), round(nrow(sub_data)*0.7))
  train_set <-  sub_data[train,]
  valid_set <- sub_data[-train,]
  
  message(paste("number of genes analysed : ", nrow(sub_data)))
  message(paste("number of genes in trainning dataset :", nrow(train_set)))
  message(paste("number of genes in valdation dataset :", nrow(valid_set)))
  
  # perform random Forest
  cl <- makePSOCKcluster(5)
  registerDoParallel(cl)
  mod  <- train(predict ~ ., data = train_set, method = "ranger", importance = 'impurity')
  rf_model <- mod$finalModel
  stopCluster(cl)
  
  if (method == "regression") {
    pred_train <- predict(rf_model, train_set %>% select(-predict))
    data <- as.data.frame(cbind(pred_train$predictions, train_set$predict))
    colnames(data) <- c("predicted_index", "real_index")
    lm_model <- lm(predicted_index ~ real_index, data)
    r_cor <- cor(data$predicted_index, data$real_index, method = "spearman") 
    sentence <- paste0("linear regression : ", 
                       round(lm_model[["coefficients"]]["real_index"], digits = 3),
                       "x + ", 
                       round(lm_model[["coefficients"]]["(Intercept)"], digits = 3),
                       ". Spearman correlation coefficient : ",
                       round(r_cor, digits = 3))
    
    if (!state == "delta") {
      p1 <- ggplot(data = data, aes(x = real_index, y = predicted_index)) + 
        geom_point(alpha = 0.5, color = color) + 
        xlim(-0.5,2) + ylim(-0.5,2)  + geom_abline(slope = 1, alpha = 0.6, linetype = "dashed") +
        ggtitle(paste("Train set : ", x)) +
        geom_smooth(method = "lm") +
        annotate(geom = "text", x = 0, y = 1.2, label = sentence, color = "black") +
        theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
              panel.background = element_blank(), axis.line = element_line(colour = "black")) 
    } else {
      p1 <- ggplot(data = data, aes(x = real_index, y = predicted_index)) + 
        geom_point(alpha = 0.5, color = color_activated) + 
        xlim(-1,1) + ylim(-1,1) + geom_abline(slope = 1, alpha = 0.6, linetype = "dashed") +
        ggtitle(paste("Train set : ", x)) +
        geom_smooth(method = "lm") +
        annotate(geom = "text", x = -0.3, y = 0.8, label = sentence, color = "black") +
        theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
              panel.background = element_blank(), axis.line = element_line(colour = "black")) 
    }
    
    pred_valid <- predict(rf_model, valid_set %>% select(-predict))
    data <- as.data.frame(cbind(pred_valid$predictions, valid_set$predict))
    colnames(data) <- c("predicted_index", "real_index")
    lm_model <- lm(predicted_index ~ real_index, data)
    r_cor <- cor(data$predicted_index, data$real_index, method = "spearman") 
    sentence <- paste0("linear regression : ", 
                       round(lm_model[["coefficients"]]["real_index"], digits = 3),
                       "x + ", 
                       round(lm_model[["coefficients"]]["(Intercept)"], digits = 3),
                       ". Spearman correlation coefficient : ",
                       round(r_cor, digits = 3))
    
    if (!state == "delta") {
      p2 <- ggplot(data = data, aes(x = real_index, y = predicted_index)) + 
        geom_point(alpha = 0.5, color = color) + 
        ylim(-0.5,2) + xlim(-0.5,2) +
        geom_smooth(method = "lm") +
        annotate(geom = "text", x = 0, y = 1.2, label = sentence, color = "black") +
        ggtitle(paste("Validation set : ", x)) + geom_abline(slope = 1, alpha = 0.6, linetype = "dashed") +
        theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
              panel.background = element_blank(), axis.line = element_line(colour = "black"))
    } else {
     p2 <- ggplot(data = data, aes(x = real_index, y = predicted_index)) + 
        geom_point(alpha = 0.5, color = color_activated) + 
        xlim(-1,1) + ylim(-1,1) + geom_abline(slope = 1, alpha = 0.6, linetype = "dashed") +
        ggtitle(paste("Valid set : ", x)) +
        geom_smooth(method = "lm") +
        annotate(geom = "text", x = -0.3, y = 0.8, label = sentence, color = "black") +
        theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
              panel.background = element_blank(), axis.line = element_line(colour = "black")) 
    }
    
    p3 <- plotImp(mod, x)
    plots <- list(mod, p1,p2,p3)
    names(plots) <- c("rf_model", "training_set", "valid_set", "ImpVar")
    
  } else if (method == "classification") {
    pred_valid <- predict(rf_model, valid_set %>% select (- predict))
    table <- table(pred_valid$predictions, valid_set$predict)
    class.err <- c(sum(table[1,c(2,3)])/sum(table[1,]),
             sum(table[2,c(1,3)])/sum(table[2,]),
             sum(table[3,c(1,2)])/sum(table[3,]))
    table <- cbind(table, class.err)
    p3 <- plotImp(mod, x)
    plots <- list(mod, table, p3)
    names(plots) <- c("rf_model", "valid_set", "ImpVar")
  }
  
  return(plots)
}
# loading data ----------------------------------------------

db <- "~/RMI2/gitlab/tdd/data/databases/2020-03-26_07-39-50_Subset_Data_processed.csv"


table <- as_tibble(read.csv(db))
table$deltaTDD_Lympho <- table$Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h - table$Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h
table$deltaNonTDD_Lympho <- table$Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h - table$Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h
table$deltaRiboDens_Lympho <- table$RiboDens.Lympho_Activated - table$RiboDens.Lympho_Resting
table$deltaRiboDens_Macro <- table$RiboDens.Macro_Activated - table$RiboDens.Macro_Resting
table$deltaUTRlength_Lympho <- table$Exp_UTR3_Lympho_Activated_Mean - table$Exp_UTR3_Lympho_Resting_Mean
table$deltaUTRlength_Macro <- table$Exp_UTR3_Macro_Activated_Mean - table$Exp_UTR3_Macro_Resting_Mean

res <- read.csv("~/RMI2/gitlab/tdd/results/DESeqresults_lympho_activation_untreated.csv")
res2 <- as.data.frame(res)
res2 <- res2[!is.na(res2$log2FoldChange),]
colnames(res2)[1] <- "gene_id"
res2$gene_id <- str_extract(res2$gene_id, "ENSMUSG...........")
table <- merge(table, select(res2, gene_id, log2FoldChange, padj), by = "gene_id", all.x = TRUE)

# Set indexes -------------------------------------------------------------

index <- c("Length.utr5", 
           # "UTR5_length", 
           "UTR5_GC", 
           "Length.cds", 
           # "CDS_length",
           "CDS_GC",
           # "Length.utr3",
           # "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h",
           # "UTR3_length", 
           "UTR3_GC", 
           "trans_length",
           # "n_m6ASeq_Peaks", 
           "total_m6ASeq_score",
           "CDS_DG",
           # "UTR5_DG", 
           # "UTR3_DG", 
           "X47UTR5.30CDS_DG",
           # "X31CDS.End_DG",
           # "Kozack_score_frequency", 
           # "Kozack_score_efficiency", 
           # "Kozack_score_dinucleotide", 
           # "n_introns_in_UTR5", 
           # "n_CDS_fragments", 
           # "n_introns_in_UTR3", 
           "n_exon",
           # "n_intron",
           # "Total_AUUUA", 
           "N_AUUUA_UTR5",
           "N_AUUUA_CDS",
           "N_AUUUA_UTR3",
           # "Total_UUAUUUAUU", 
           # "N_UUAUUUAUU_UTR5", 
           # "N_UUAUUUAUU_CDS", 
           # "N_UUAUUUAUU_UTR3",
           "n4G_UTR5",
           "n4G_CDS",
           "n4G_UTR3",
           # "Stretches_AAA_AAG_score",
           # "Stretches_AAA_AAG_pondarated_score",
           # "Stretches_GAC_GAT_score",
           # "Stretches_GAC_GAT_pondarated_score",
           # "Stretches_GAA_GAG_score",
           # "Stretches_GAA_GAG_pondarated_score",
           # "Stretches_AAA_AAG_GAC_GAT_GAA_GAG_score",
           # "Stretches_AAA_AAG_GAC_GAT_GAA_GAG_pondarated_score",
           # "percent_ALA_A",
           # "percent_ARG_R",
           # "percent_ASN_N",
           # "percent_ASP_D",
           # "percent_CYS_C",
           # "percent_GLU_E",
           # "percent_GLN_Q",
           # "percent_GLY_G",
           # "percent_HIS_H",
           # "percent_ILE_I",
           # "percent_LEU_L",
           # "percent_LYS_K",
           # "percent_MET_M",
           # "percent_PHE_F",
           # "percent_PRO_P",
           # "percent_SER_S",
           # "percent_THR_T",
           # "percent_TRP_W",
           # "percent_TYR_Y",
           # "percent_VAL_V",
           "sumKozakuOFR",
           # "N_.AU..AU..AU.UAUUUAU.AU..AU..AU._UTR5",
           # "N_.AU..AU..AU.UAUUUAU.AU..AU..AU._CDS",
           # "N_.AU..AU..AU.UAUUUAU.AU..AU..AU._UTR3"
           # "Hwang.transcript_half.life"
           "GC3_percent"
)

  index_lympho <- c("RiboDens.Lympho_Resting" ,
                  # "DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h",
                  # "Hwang.Lympho_Resting.polyA_length"
                  "Exp_UTR3_Lympho_Resting_Mean",
                  "basal_expr_Lympho_Resting_RPKM",
                  "basal_expr_Lympho_Activated_RPKM"
                  )

index_macro <- c("RiboDens.Macro_Resting", 
                 # "DegFold.Macro_Resting.Trip.Ref_Trip_0h.3h",
                 "Exp_UTR3_Macro_Resting_Mean",
                 "basal_expr_Macro_Resting_RPKM",
                 "basal_expr_Macro_Activated_RPKM")

tddindex_resting <- c("Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h", 
                      # "Abs.TDD..Lympho_Resting.Trip_CHX.Ref_untreated_0h.3h", 
                      "Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.1h", 
                      # "Abs.TDD..Lympho_Resting.Trip_CHX.Ref_untreated_0h.1h", 
                      "Abs.TDD..Lympho_Resting.Trip_Harr.Ref_Trip_0h.3h", 
                      # "Abs.TDD..Lympho_Resting.Trip_Harr.Ref_untreated_0h.3h", 
                      "Abs.TDD..Lympho_Resting.Trip_Harr.Ref_Trip_0h.1h", 
                      # "Abs.TDD..Lympho_Resting.Trip_Harr.Ref_untreated_0h.1h",
                      "Abs.TDD..Macro_Resting.Trip_CHX.Ref_Trip_0h.3h", 
                      "Abs.TDD..Macro_Resting.Trip_Harr.Ref_Trip_0h.3h" ,
                      # "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h", 
                      # "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_untreated_0h.3h", 
                      # "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.1h", 
                      # "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_untreated_0h.1h", 
                      # "Abs.NonTDD..Lympho_Resting.Trip_Harr.Ref_Trip_0h.3h", 
                      # "Abs.NonTDD..Lympho_Resting.Trip_Harr.Ref_untreated_0h.3h", 
                      # "Abs.NonTDD..Lympho_Resting.Trip_Harr.Ref_Trip_0h.1h", 
                      # "Abs.NonTDD..Lympho_Resting.Trip_Harr.Ref_untreated_0h.1h", 
                      # "Abs.NonTDD..Macro_Resting.Trip_CHX.Ref_Trip_0h.3h", 
                      # "Abs.NonTDD..Macro_Resting.Trip_Harr.Ref_Trip_0h.3h", 
                      "Rel.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h",
                      # "Rel.TDD..Lympho_Resting.Trip_CHX.Ref_untreated_0h.3h", 
                      "Rel.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.1h",
                      # "Rel.TDD..Lympho_Resting.Trip_CHX.Ref_untreated_0h.1h", 
                      "Rel.TDD..Lympho_Resting.Trip_Harr.Ref_Trip_0h.3h",
                      # "Rel.TDD..Lympho_Resting.Trip_Harr.Ref_untreated_0h.3h", 
                      "Rel.TDD..Lympho_Resting.Trip_Harr.Ref_Trip_0h.1h",
                      # "Rel.TDD..Lympho_Resting.Trip_Harr.Ref_untreated_0h.1h", 
                      "Rel.TDD..Macro_Resting.Trip_CHX.Ref_Trip_0h.3h",
                      "Rel.TDD..Macro_Resting.Trip_Harr.Ref_Trip_0h.3h"
                      # "Rel.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h",
                      # "Rel.NonTDD..Lympho_Resting.Trip_CHX.Ref_untreated_0h.3h", 
                      # "Rel.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.1h", 
                      # "Rel.NonTDD..Lympho_Resting.Trip_CHX.Ref_untreated_0h.1h", 
                      # "Rel.NonTDD..Lympho_Resting.Trip_Harr.Ref_Trip_0h.3h", 
                      # "Rel.NonTDD..Lympho_Resting.Trip_Harr.Ref_untreated_0h.3h", 
                      # "Rel.NonTDD..Lympho_Resting.Trip_Harr.Ref_Trip_0h.1h", 
                      # "Rel.NonTDD..Lympho_Resting.Trip_Harr.Ref_untreated_0h.1h", 
                      # "Rel.NonTDD..Macro_Resting.Trip_CHX.Ref_Trip_0h.3h", 
                      # "Rel.NonTDD..Macro_Resting.Trip_Harr.Ref_Trip_0h.3h",
                      # "DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h", 
                      # "DegFold.Lympho_Resting.Trip.Ref_untreated_0h.3h", 
                      # "DegFold.Lympho_Resting.Trip.Ref_Trip_0h.1h", 
                      # "DegFold.Lympho_Resting.Trip.Ref_untreated_0h.1h", 
                      # "DegFold.Macro_Resting.Trip.Ref_Trip_0h.3h",
                      # "Abs.NonDEG..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h", 
                      # "Abs.NonDEG..Lympho_Resting.Trip_CHX.Ref_untreated_0h.3h", 
                      # "Abs.NonDEG..Lympho_Resting.Trip_CHX.Ref_Trip_0h.1h", 
                      # "Abs.NonDEG..Lympho_Resting.Trip_CHX.Ref_untreated_0h.1h", 
                      # "Abs.NonDEG..Lympho_Resting.Trip_Harr.Ref_Trip_0h.3h", 
                      # "Abs.NonDEG..Lympho_Resting.Trip_Harr.Ref_untreated_0h.3h", 
                      # "Abs.NonDEG..Lympho_Resting.Trip_Harr.Ref_Trip_0h.1h", 
                      # "Abs.NonDEG..Lympho_Resting.Trip_Harr.Ref_untreated_0h.1h", 
                      # "Abs.NonDEG..Macro_Resting.Trip_CHX.Ref_Trip_0h.3h", 
                      # "Abs.NonDEG..Macro_Resting.Trip_Harr.Ref_Trip_0h.3h"
)

tddindex_activated <- c("Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h", 
                        # "Abs.TDD..Lympho_Activated.Trip_CHX.Ref_untreated_0h.3h", 
                        "Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.1h", 
                        # "Abs.TDD..Lympho_Activated.Trip_CHX.Ref_untreated_0h.1h", 
                        "Abs.TDD..Lympho_Activated.Trip_Harr.Ref_Trip_0h.3h", 
                        # "Abs.TDD..Lympho_Activated.Trip_Harr.Ref_untreated_0h.3h", 
                        "Abs.TDD..Lympho_Activated.Trip_Harr.Ref_Trip_0h.1h", 
                        # "Abs.TDD..Lympho_Activated.Trip_Harr.Ref_untreated_0h.1h", 
                        "Abs.TDD..Macro_Activated.Trip_CHX.Ref_Trip_0h.3h", 
                        "Abs.TDD..Macro_Activated.Trip_Harr.Ref_Trip_0h.3h",
                        # "Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h", 
                        # "Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_untreated_0h.3h", 
                        # "Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.1h", 
                        # "Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_untreated_0h.1h", 
                        # "Abs.NonTDD..Lympho_Activated.Trip_Harr.Ref_Trip_0h.3h", 
                        # "Abs.NonTDD..Lympho_Activated.Trip_Harr.Ref_untreated_0h.3h", 
                        # "Abs.NonTDD..Lympho_Activated.Trip_Harr.Ref_Trip_0h.1h", 
                        # "Abs.NonTDD..Lympho_Activated.Trip_Harr.Ref_untreated_0h.1h", 
                        # "Abs.NonTDD..Macro_Activated.Trip_CHX.Ref_Trip_0h.3h", 
                        # "Abs.NonTDD..Macro_Activated.Trip_Harr.Ref_Trip_0h.3h", 
                        "Rel.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h",
                        # "Rel.TDD..Lympho_Activated.Trip_CHX.Ref_untreated_0h.3h", 
                        "Rel.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.1h",
                        # "Rel.TDD..Lympho_Activated.Trip_CHX.Ref_untreated_0h.1h", 
                        "Rel.TDD..Lympho_Activated.Trip_Harr.Ref_Trip_0h.3h",
                        # "Rel.TDD..Lympho_Activated.Trip_Harr.Ref_untreated_0h.3h", 
                        "Rel.TDD..Lympho_Activated.Trip_Harr.Ref_Trip_0h.1h",
                        # "Rel.TDD..Lympho_Activated.Trip_Harr.Ref_untreated_0h.1h", 
                        "Rel.TDD..Macro_Activated.Trip_CHX.Ref_Trip_0h.3h",
                        "Rel.TDD..Macro_Activated.Trip_Harr.Ref_Trip_0h.3h",
                        # "Rel.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h", 
                        # "Rel.NonTDD..Lympho_Activated.Trip_CHX.Ref_untreated_0h.3h", 
                        # "Rel.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.1h", 
                        # "Rel.NonTDD..Lympho_Activated.Trip_CHX.Ref_untreated_0h.1h", 
                        # "Rel.NonTDD..Lympho_Activated.Trip_Harr.Ref_Trip_0h.3h", 
                        # "Rel.NonTDD..Lympho_Activated.Trip_Harr.Ref_untreated_0h.3h", 
                        # "Rel.NonTDD..Lympho_Activated.Trip_Harr.Ref_Trip_0h.1h", 
                        # "Rel.NonTDD..Lympho_Activated.Trip_Harr.Ref_untreated_0h.1h", 
                        # "Rel.NonTDD..Macro_Activated.Trip_CHX.Ref_Trip_0h.3h", 
                        # "Rel.NonTDD..Macro_Activated.Trip_Harr.Ref_Trip_0h.3h",
                        # "DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h", 
                        # "DegFold.Lympho_Activated.Trip.Ref_untreated_0h.3h", 
                        # "DegFold.Lympho_Activated.Trip.Ref_Trip_0h.1h", 
                        # "DegFold.Lympho_Activated.Trip.Ref_untreated_0h.1h",
                        # "DegFold.Macro_Activated.Trip.Ref_Trip_0h.3h",
                        # "Abs.NonDEG..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h", 
                        # "Abs.NonDEG..Lympho_Activated.Trip_CHX.Ref_untreated_0h.3h", 
                        # "Abs.NonDEG..Lympho_Activated.Trip_CHX.Ref_Trip_0h.1h", 
                        # "Abs.NonDEG..Lympho_Activated.Trip_CHX.Ref_untreated_0h.1h", 
                        # "Abs.NonDEG..Lympho_Activated.Trip_Harr.Ref_Trip_0h.3h", 
                        # "Abs.NonDEG..Lympho_Activated.Trip_Harr.Ref_untreated_0h.3h", 
                        # "Abs.NonDEG..Lympho_Activated.Trip_Harr.Ref_Trip_0h.1h", 
                        # "Abs.NonDEG..Lympho_Activated.Trip_Harr.Ref_untreated_0h.1h", 
                        # "Abs.NonDEG..Macro_Activated.Trip_CHX.Ref_Trip_0h.3h", 
                        # "Abs.NonDEG..Macro_Activated.Trip_Harr.Ref_Trip_0h.3h",
                        "Abs.TDD..Lympho_Activated.DRB_CHX.Ref_DRB_0h.3h", 
                        "Abs.TDD..Lympho_Activated.DRB_CHX.Ref_DRB_0h.1h", 
                        "Abs.TDD..Lympho_Activated.DRB_Harr.Ref_DRB_0h.3h", 
                        "Abs.TDD..Lympho_Activated.DRB_Harr.Ref_DRB_0h.1h"
                        # "Abs.NonTDD..Lympho_Activated.DRB_CHX.Ref_DRB_0h.3h", 
                        # "Abs.NonTDD..Lympho_Activated.DRB_CHX.Ref_DRB_0h.1h", 
                        # "Abs.NonTDD..Lympho_Activated.DRB_Harr.Ref_DRB_0h.3h", 
                        # "Abs.NonTDD..Lympho_Activated.DRB_Harr.Ref_DRB_0h.1h", 
                        # "Abs.NonDEG..Lympho_Activated.DRB_CHX.Ref_DRB_0h.3h", 
                        # "Abs.NonDEG..Lympho_Activated.DRB_CHX.Ref_DRB_0h.1h", 
                        # "Abs.NonDEG..Lympho_Activated.DRB_Harr.Ref_DRB_0h.3h", 
                        # "Abs.NonDEG..Lympho_Activated.DRB_Harr.Ref_DRB_0h.1h", 
                        # "Rel.TDD..Lympho_Activated.DRB_CHX.Ref_DRB_0h.3h", 
                        # "Rel.TDD..Lympho_Activated.DRB_CHX.Ref_DRB_0h.1h", 
                        # "Rel.TDD..Lympho_Activated.DRB_Harr.Ref_DRB_0h.3h", 
                        # "Rel.TDD..Lympho_Activated.DRB_Harr.Ref_DRB_0h.1h", 
                        # "Rel.NonTDD..Lympho_Activated.DRB_CHX.Ref_DRB_0h.3h", 
                        # "Rel.NonTDD..Lympho_Activated.DRB_CHX.Ref_DRB_0h.1h", 
                        # "Rel.NonTDD..Lympho_Activated.DRB_Harr.Ref_DRB_0h.3h", 
                        # "Rel.NonTDD..Lympho_Activated.DRB_Harr.Ref_DRB_0h.1h"
)

index_delta_Lympho <- c("log2FoldChange",
                        "deltaUTRlength_Lympho",
                        "Exp_UTR3_Lympho_Resting_Mean")

index_delta_Macro <- c("deltaRiboDens_Macro",
                       "deltaUTRlength_Macro",
                       "Exp_UTR3_Macro_Resting_Mean")
