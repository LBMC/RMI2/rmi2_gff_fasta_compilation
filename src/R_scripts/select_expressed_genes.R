# 
library(tidyverse)
cutoff = 15


norm_counts <- read.csv(file = "results/dbNormCountsAll_exon.csv")
norm_counts_small <- norm_counts[,2:4]
norm_counts_wide <- pivot_wider(data = norm_counts_small, 
                                id_cols = "ensemblID",
                                names_from = "librairies",
                                values_from = "normReadsCounts")
norm_counts_wide$ensemblID <- gsub("(^[^.]*)(.*$)", "\\1",  norm_counts_wide$ensemblID)


# expressed genes = cutoff of 15 reads + 10 % of degradation --------------

# lymphoResting
sub_norm <- norm_counts_wide %>% select(ensemblID,
                                        R4_6_Resting_3h_Triptolide_exon,
                                        R3_6_Resting_3h_Triptolide_exon,
                                        R2_6_Resting_3h_Triptolide_exon,
                                        R4_4_Resting_0h_Triptolide_exon,
                                        R3_4_Resting_0h_Triptolide_exon,
                                        R2_4_Resting_0h_Triptolide_exon)
sub_norm <- filter(sub_norm, (R4_6_Resting_3h_Triptolide_exon + R3_6_Resting_3h_Triptolide_exon +  R2_6_Resting_3h_Triptolide_exon)/3 > cutoff & 
                             (R4_4_Resting_0h_Triptolide_exon + R3_4_Resting_0h_Triptolide_exon +  R2_4_Resting_0h_Triptolide_exon)/3 > cutoff &
                             (((R4_4_Resting_0h_Triptolide_exon - R4_6_Resting_3h_Triptolide_exon) / R4_4_Resting_0h_Triptolide_exon) + 
                              ((R3_4_Resting_0h_Triptolide_exon - R3_6_Resting_3h_Triptolide_exon) / R3_4_Resting_0h_Triptolide_exon) + 
                              ((R2_4_Resting_0h_Triptolide_exon - R2_6_Resting_3h_Triptolide_exon) / R2_4_Resting_0h_Triptolide_exon) / 3 > 0.10))
keep_LR <- as.character(sub_norm$ensemblID)

# lymphoActivated
sub_norm <- norm_counts_wide %>% select(ensemblID,
                                        A2_9_Activated_3h_Triptolide_exon,
                                        A3_9_Activated_3h_Triptolide_exon,
                                        A4_9_Activated_3h_Triptolide_exon,
                                        A2_7_Activated_0h_Triptolide_exon,
                                        A3_7_Activated_0h_Triptolide_exon,
                                        A4_7_Activated_0h_Triptolide_exon)
sub_norm <- filter(sub_norm, (A2_9_Activated_3h_Triptolide_exon + A3_9_Activated_3h_Triptolide_exon +  A4_9_Activated_3h_Triptolide_exon)/3 > cutoff & 
                             (A2_7_Activated_0h_Triptolide_exon + A3_7_Activated_0h_Triptolide_exon +  A4_7_Activated_0h_Triptolide_exon)/3 > cutoff &
                             (((A2_7_Activated_0h_Triptolide_exon - A2_9_Activated_3h_Triptolide_exon) / A2_7_Activated_0h_Triptolide_exon) + 
                                ((A3_7_Activated_0h_Triptolide_exon - A3_9_Activated_3h_Triptolide_exon) / A3_7_Activated_0h_Triptolide_exon) + 
                                ((A4_7_Activated_0h_Triptolide_exon - A4_9_Activated_3h_Triptolide_exon) / A4_7_Activated_0h_Triptolide_exon) / 3 > 0.10))
keep_LA <- as.character(sub_norm$ensemblID)

# macroResting
sub_norm <- norm_counts_wide %>% select(ensemblID,
                                        LPSno_macro_3h_Triptolide_i3_6_exon,
                                        LPSno_macro_3h_Triptolide_i4_6_exon,
                                        LPSno_macro_3h_Triptolide_i5_6_exon,
                                        LPSno_macro_0h_Triptolide_i3_4_exon,
                                        LPSno_macro_0h_Triptolide_i4_4_exon,
                                        LPSno_macro_0h_Triptolide_i5_4_exon)
sub_norm <- filter(sub_norm, (LPSno_macro_3h_Triptolide_i3_6_exon + LPSno_macro_3h_Triptolide_i4_6_exon +  LPSno_macro_3h_Triptolide_i5_6_exon)/3 > cutoff &
                             (LPSno_macro_0h_Triptolide_i3_4_exon + LPSno_macro_0h_Triptolide_i4_4_exon +  LPSno_macro_0h_Triptolide_i5_4_exon)/3 > cutoff &
                               (((LPSno_macro_0h_Triptolide_i3_4_exon - LPSno_macro_3h_Triptolide_i3_6_exon) / LPSno_macro_0h_Triptolide_i3_4_exon) + 
                                  ((LPSno_macro_0h_Triptolide_i4_4_exon - LPSno_macro_3h_Triptolide_i4_6_exon) / LPSno_macro_0h_Triptolide_i4_4_exon) + 
                                  ((LPSno_macro_0h_Triptolide_i5_4_exon - LPSno_macro_3h_Triptolide_i5_6_exon) / LPSno_macro_0h_Triptolide_i5_4_exon) / 3 > 0.10))
keep_MR <- as.character(sub_norm$ensemblID)

# macroActivated
sub_norm <- norm_counts_wide %>% select(ensemblID,
                                        LPS_macro_3h_Triptolide_m3_6_exon,
                                        LPS_macro_3h_Triptolide_m4_6_exon,
                                        LPS_macro_3h_Triptolide_m5_6_exon,
                                        LPS_macro_0h_Triptolide_m3_4_exon,
                                        LPS_macro_0h_Triptolide_m4_4_exon,
                                        LPS_macro_0h_Triptolide_m5_4_exon)
sub_norm <- filter(sub_norm, (LPS_macro_3h_Triptolide_m3_6_exon + LPS_macro_3h_Triptolide_m4_6_exon +  LPS_macro_3h_Triptolide_m5_6_exon)/3 > cutoff & 
                             (LPS_macro_0h_Triptolide_m3_4_exon + LPS_macro_0h_Triptolide_m4_4_exon +  LPS_macro_0h_Triptolide_m5_4_exon)/3 > cutoff &
                             (((LPS_macro_0h_Triptolide_m3_4_exon - LPS_macro_3h_Triptolide_m3_6_exon) / LPS_macro_0h_Triptolide_m3_4_exon) + 
                                ((LPS_macro_0h_Triptolide_m4_4_exon - LPS_macro_3h_Triptolide_m4_6_exon) / LPS_macro_0h_Triptolide_m4_4_exon) + 
                                ((LPS_macro_0h_Triptolide_m5_4_exon - LPS_macro_3h_Triptolide_m5_6_exon) / LPS_macro_0h_Triptolide_m5_4_exon) / 3 > 0.10))
keep_MA <- as.character(sub_norm$ensemblID)

write.csv(x = keep_LR, file = "results/filtred_genes_Lympho_Resting.csv", row.names = FALSE)
write.csv(x = keep_LA, file = "results/filtred_genes_Lympho_Activated.csv", row.names = FALSE)
write.csv(x = keep_MR, file = "results/filtred_genes_Macro_Resting.csv", row.names = FALSE)
write.csv(x = keep_MA, file = "results/filtred_genes_Macro_Activated.csv", row.names = FALSE)


# pseudo expressed genes  = cutoff of reads only ---------------------------------

# lymphoResting
sub_norm <- norm_counts_wide %>% select(ensemblID,
                                        R4_6_Resting_3h_Triptolide_exon,
                                        R3_6_Resting_3h_Triptolide_exon,
                                        R2_6_Resting_3h_Triptolide_exon,
                                        R4_4_Resting_0h_Triptolide_exon,
                                        R3_4_Resting_0h_Triptolide_exon,
                                        R2_4_Resting_0h_Triptolide_exon)
sub_norm <- filter(sub_norm, (R4_6_Resting_3h_Triptolide_exon + R3_6_Resting_3h_Triptolide_exon +  R2_6_Resting_3h_Triptolide_exon)/3 > cutoff & 
                     (R4_4_Resting_0h_Triptolide_exon + R3_4_Resting_0h_Triptolide_exon +  R2_4_Resting_0h_Triptolide_exon)/3 > cutoff )
keep_LR <- as.character(sub_norm$ensemblID)

# lymphoActivated
sub_norm <- norm_counts_wide %>% select(ensemblID,
                                        A2_9_Activated_3h_Triptolide_exon,
                                        A3_9_Activated_3h_Triptolide_exon,
                                        A4_9_Activated_3h_Triptolide_exon,
                                        A2_7_Activated_0h_Triptolide_exon,
                                        A3_7_Activated_0h_Triptolide_exon,
                                        A4_7_Activated_0h_Triptolide_exon)
sub_norm <- filter(sub_norm, (A2_9_Activated_3h_Triptolide_exon + A3_9_Activated_3h_Triptolide_exon +  A4_9_Activated_3h_Triptolide_exon)/3 > cutoff & 
                     (A2_7_Activated_0h_Triptolide_exon + A3_7_Activated_0h_Triptolide_exon +  A4_7_Activated_0h_Triptolide_exon)/3 > cutoff )
keep_LA <- as.character(sub_norm$ensemblID)

# macroResting
sub_norm <- norm_counts_wide %>% select(ensemblID,
                                        LPSno_macro_3h_Triptolide_i3_6_exon,
                                        LPSno_macro_3h_Triptolide_i4_6_exon,
                                        LPSno_macro_3h_Triptolide_i5_6_exon,
                                        LPSno_macro_0h_Triptolide_i3_4_exon,
                                        LPSno_macro_0h_Triptolide_i4_4_exon,
                                        LPSno_macro_0h_Triptolide_i5_4_exon)
sub_norm <- filter(sub_norm, (LPSno_macro_3h_Triptolide_i3_6_exon + LPSno_macro_3h_Triptolide_i4_6_exon +  LPSno_macro_3h_Triptolide_i5_6_exon)/3 > cutoff &
                     (LPSno_macro_0h_Triptolide_i3_4_exon + LPSno_macro_0h_Triptolide_i4_4_exon +  LPSno_macro_0h_Triptolide_i5_4_exon)/3 > cutoff )
keep_MR <- as.character(sub_norm$ensemblID)

# macroActivated
sub_norm <- norm_counts_wide %>% select(ensemblID,
                                        LPS_macro_3h_Triptolide_m3_6_exon,
                                        LPS_macro_3h_Triptolide_m4_6_exon,
                                        LPS_macro_3h_Triptolide_m5_6_exon,
                                        LPS_macro_0h_Triptolide_m3_4_exon,
                                        LPS_macro_0h_Triptolide_m4_4_exon,
                                        LPS_macro_0h_Triptolide_m5_4_exon)
sub_norm <- filter(sub_norm, (LPS_macro_3h_Triptolide_m3_6_exon + LPS_macro_3h_Triptolide_m4_6_exon +  LPS_macro_3h_Triptolide_m5_6_exon)/3 > cutoff & 
                     (LPS_macro_0h_Triptolide_m3_4_exon + LPS_macro_0h_Triptolide_m4_4_exon +  LPS_macro_0h_Triptolide_m5_4_exon)/3 > cutoff )
keep_MA <- as.character(sub_norm$ensemblID)

write.csv(x = keep_LR, file = "results/pseudo_filtred_genes_Lympho_Resting.csv", row.names = FALSE)
write.csv(x = keep_LA, file = "results/pseudo_filtred_genes_Lympho_Activated.csv", row.names = FALSE)
write.csv(x = keep_MR, file = "results/pseudo_filtred_genes_Macro_Resting.csv", row.names = FALSE)
write.csv(x = keep_MA, file = "results/pseudo_filtred_genes_Macro_Activated.csv", row.names = FALSE)
