## select genes for FISH
library(tidyverse)

## load data
table <- read.csv("data/databases/2020-03-26_07-39-50_Subset_Data_processed.csv")

## 1. more abundant than Dync1h1
min_expr <- min(table[table$gene_name == "Dync1h1", c("basal_expr_Lympho_Resting_RPKM")])

subset <- table[table$basal_expr_Lympho_Resting_RPKM > (min_expr -0.5),]

## 2. TDD

# 1.Densité de ribosomes en début du plateau (entre 1,5 et 2 dans l'unité que tu as utilisé pour faire la figure de binning).
# 2. Taille 3'UTR < 500nt
# 3. Taille 5'UTR < 100nt
# 4. %GC3 région codante > entre 60 et 79%

# TDD <- subset %>% #filter(RiboDens.Lympho_Resting > 1.5 & RiboDens.Lympho_Resting < 2  ) %>% # RiboDens.Lympho_Activated > 1.5 & RiboDens.Lympho_Activated < 2
#                   filter(Length.utr3 < 500) %>%
#                   filter(Length.utr5 < 100) %>%
#                   filter(GC3_percent > 60 & GC3_percent < 75)

TDD <- subset %>% filter(Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h > 0.5)

column_to_show <- c("gene_name",
                    "gene_id",
                    "basal_expr_Lympho_Resting_RPKM",
                    "basal_expr_Lympho_Activated_RPKM",
                    "RiboDens.Lympho_Resting",
                    "Length.utr3",
                    "Length.utr5",
                    "Length.cds",
                    "GC3_percent",
                    "Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h",
                    "Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h",
                    "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h",
                    "Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h")

# TDD <- TDD[TDD$Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h > 0.5,]
a <- TDD[,colnames(TDD) %in% column_to_show]


## 3. TID 
# 1. Nombre d'exons < 5
# 2. 3'UTR > 1500nt
# 3. 5'UTR > 200nt
# 4. %GC3 région codante < 39%
# 5. Plutôt faible ribo-densité < 0,5

TID <- subset %>% filter(Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h > 0.5) %>%
  filter(Length.utr3 > 1500) %>%
  filter(Length.utr5 > 200) %>%
  filter(GC3_percent < 39) %>%
  filter(RiboDens.Lympho_Resting < 0.5)

b <- TID[,colnames(TID) %in% column_to_show]


## switch de TDD vers non TDD
switch <- subset %>% filter(Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h > Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h & 
                              Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h < Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h)

c <- switch[, colnames(switch) %in% column_to_show]
c$diff_TDD <- c$Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h - c$Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h
c$diff_TID <- c$Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h - c$Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h

d <- subset %>% filter(Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h > 0.8) %>%
  select(all_of(column_to_show))

## choose stable gene
e <- subset %>% filter(Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h + Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h < 0.1) %>%
  select(all_of(column_to_show))

load(file = "results/stableGenesInEachCondition.RData")
lymphoStableCounts <- stablesGenes[[1]]
lymphoStableCounts <- gsub("\\..*", "", lymphoStableCounts)
e <- e[e$gene_id %in% lymphoStableCounts,]

## switch de non TID vers TID
switch_TID <- subset %>% filter(Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h > Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h)

f <- switch_TID[, colnames(switch_TID) %in% column_to_show]
f$diff_TDD <- f$Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h - f$Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h
f$diff_TID <- f$Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h - f$Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h




selected <- c("Psmb3", "H2afj", "Bin2", "Nosip")
subset[subset$gene_name %in% selected, colnames(subset) %in% column_to_show]


# Checking robustness across replicates -----------------------------------

cantidates <- c("ENSMUSG00000060032",
                "ENSMUSG00000069744",
                "ENSMUSG00000098112",
                "ENSMUSG00000022280",
                "ENSMUSG00000020362",
                "ENSMUSG00000051495",
                "ENSMUSG00000000740",
                "ENSMUSG00000028991")

load(file = "results/dbNormCountsAll_exon.RData")

LR <- dball %>% filter(cell == "Lympho" & 
                         `Activated/Resting` == "Resting" & 
                         treatment %in% c("Trip", "TripCHX") & 
                         time %in% c("0h", "3h"))
LR$ensemblID <- as.character(LR$ensemblID)
LR$ensemblID <- gsub("\\..*", "", LR$ensemblID)

LR <- pivot_wider(LR, id_cols = c(ensemblID, replicate), names_from = c(treatment, time), values_from = normReadsCounts )
LR <- LR %>% filter(ensemblID %in% cantidates)
LR$TDDi <- (LR$TripCHX_3h - LR$Trip_3h ) / LR$Trip_0h


candidates2 <- c("ENSMUSG00000022336",
                 "ENSMUSG00000019432",
                 "ENSMUSG00000005656",
                 "ENSMUSG00000015143",
                 "ENSMUSG00000040940",
                 "ENSMUSG00000012114",
                 "ENSMUSG00000022443",
                 "ENSMUSG00000039221",
                 "ENSMUSG00000028936",
                 "ENSMUSG00000021113")

candidates2 <- c("ENSMUSG00000073409")

LR <- dball %>% filter(cell == "Lympho" & 
                         `Activated/Resting` == "Resting" & 
                         treatment %in% c("untreated", "Trip", "TripCHX") & 
                         time %in% c("0h", "3h"))
LR$ensemblID <- as.character(LR$ensemblID)
LR$ensemblID <- gsub("\\..*", "", LR$ensemblID)

LR <- pivot_wider(LR, id_cols = c(ensemblID, replicate), names_from = c(treatment, time), values_from = normReadsCounts )
LR <- LR %>% filter(ensemblID %in% candidates2)
LR$TDDi <- (LR$TripCHX_3h - LR$Trip_3h ) / LR$Trip_0h
LR$TIDi <- (LR$Trip_0h - LR$TripCHX_3h ) / LR$Trip_0h

LA <- dball %>% filter(cell == "Lympho" & 
                         `Activated/Resting` == "Activated" & 
                         treatment %in% c("untreated","Trip", "TripCHX") & 
                         time %in% c("0h", "3h"))
LA$ensemblID <- as.character(LA$ensemblID)
LA$ensemblID <- gsub("\\..*", "", LA$ensemblID)

LA <- pivot_wider(LA, id_cols = c(ensemblID, replicate), names_from = c(treatment, time), values_from = normReadsCounts )
LA <- LA %>% filter(ensemblID %in% candidates2)
LA$TDDi <- (LA$TripCHX_3h - LA$Trip_3h ) / LA$Trip_0h
LA$TIDi <- (LA$Trip_0h - LA$TripCHX_3h ) / LA$Trip_0h

