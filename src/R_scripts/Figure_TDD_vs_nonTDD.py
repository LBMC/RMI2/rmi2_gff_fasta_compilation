import os
from config import path_Values
from config import path_Output
from config import path_gff3, path_fa, path_tdd
from utils.Object_shredder_simple import Shredder_simple
from utils.time_stamp import string_time
from config import Dimension, H, W, Left, Bottom
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np


def Figure():
    """Determine hits and distribution for GO and compare to global pop."""
    print('   #   ' + string_time() + '   #   Analysing TDD vs GO')

    # 1. Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # If the specific input folder doesn't exist
    if os.path.exists(input_folder) is False:
        print('   #   ' + 'Expected output folder doesn t exist.')
        # Quit
        exit()
    # Open the database values as a shredder dictionnary
    db = Shredder_simple(path_tdd, ',')

    dict_couples = {}
    dict_couples['Lympho_Resting'] = ['asIndexTDD_t3TripTripCHX.0hTrip_LR',
                                      'asNonTDDindex_LR']
    dict_couples['Lympho_Activated'] = ['asIndexTDD_t3TripTripCHX.0hTrip_LA',
                                        'asNonTDDindex_LA']

    couples = ['Lympho_Resting', 'Lympho_Activated']

    # Analyse all TDD values for each Condition
    # Create the PdfPages
    pdf = PdfPages(path_Output + '_TDD_vs_nonTDD.pdf')
    for c in couples:
        Explore_TDD(c,
                    dict_couples[c],
                    db,
                    pdf)
    pdf.close()


def Explore_TDD(c,
                couple,
                database,
                pdf):
    """Analyse the TDD as a fonction of the condition."""
    print('   #   ' + string_time() + '   #   Treating ' + c)
    indexTDD = couple[0]
    indexnonTDD = couple[1]

    # Initialize all required variables
    raw_TDD = []
    raw_nonTDD = []

    # Loop in all genes
    for gene in database._First_Col:
        tdd = database._dict_values[gene, indexTDD]
        nontdd = database._dict_values[gene, indexnonTDD]
        if tdd != 'NA' and nontdd != 'NA':
            # Update the TDD index values list
            raw_TDD = np.append(raw_TDD,
                                float(tdd))
            raw_nonTDD = np.append(raw_nonTDD,
                                   float(nontdd))
    print('   #   ' + string_time() + '   #   Generating distributions')
    # Now that we have all TDD values we generate the probability density
    (density_TDD,
     bin_edges) = np.histogram(raw_TDD,
                               bins=25,
                               range=(-1, 1),
                               density=True)
    bins_meanTDD = format_bin_edges(bin_edges)
    medTDD = np.median(raw_TDD)

    (density_nonTDD,
     bin_edges) = np.histogram(raw_nonTDD,
                               bins=25,
                               range=(-1, 1),
                               density=True)
    # Convert bin_edges in mean values to have the same number of values in
    # density_TDD and bins_mean
    bins_meannonTDD = format_bin_edges(bin_edges)
    mednonTDD = np.median(raw_nonTDD)

    drawDistribution(bins_meanTDD,
                     density_TDD,
                     c + ' ' + str(medTDD) + ' ' + str(mednonTDD),
                     pdf,
                     X2=bins_meannonTDD,
                     Y2=density_nonTDD)
    # Close the pdf file


def drawDistribution(X1,
                     Y1,
                     title,
                     pdf,
                     X2=[],
                     Y2=[]):
    """Draw the distribution of TDD and GO."""
    # create plot
    fig = plt.figure(figsize=Dimension)
    fig.add_axes([Left,
                  Bottom,
                  W,
                  H
                  ])

    plt.plot(X1,
             Y1,
             color=(0, 0, 0),
             linewidth=2)
    if len(X2) != 0 and len(Y2) != 0:
        plt.plot(X2,
                 Y2,
                 color=(1, 0.5, 0),
                 linewidth=2)
    plt.suptitle(title)
    plt.savefig(pdf,
                format='pdf',
                dpi=300)

    # Close the figure to spare memory
    plt.close()


def find_best_GO(list_GO,
                 hit_GO):
    """Find the GO with the most entries."""
    # Find the 20 best
    ranked_hits = []
    ranked_names = []

    for iteration in range(0, len(hit_GO), 1):
        top = 0
        candidat = ''
        for go in list_GO:
            if hit_GO[go] > top:
                top = hit_GO[go]
                candidat = go
        ranked_hits.append(top)
        ranked_names.append(candidat)

        # remove name already in the list
        list_GO.remove(candidat)

    return ranked_names, ranked_hits


def format_bin_edges(bin_edges):
    """Calculate the mean value for the bin edges."""
    values = []
    for i in range(0, len(bin_edges)-1, 1):
        m = round((bin_edges[i] + bin_edges[i+1])/2,
                  2)
        values = np.append(values, m)
    return values


if __name__ == "__main__":
    # execute only if run as a script
    Figure()
