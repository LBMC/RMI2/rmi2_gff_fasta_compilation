

# A plotting R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800


# --------------------------------------------------------------------------
# If you don't have the ggplot2 package installed, uncomment the following line:
# install.packages( "ggplot2" );
library( ggplot2 );
# --------------------------------------------------------------------------
# If you don't have the scales package installed, uncomment the following line:
# install.packages( "scales" );
library( scales );


# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","frequency_%","plot_X","plot_Y","plot_size","log10_p_value","uniqueness","dispensability");
revigo.data <- rbind(c("GO:0038083","peptidyl-tyrosine autophosphorylation", 0.011,-4.086, 5.277, 3.157,-4.0037,0.449,0.000),
                     c("GO:0098609","cell-cell adhesion", 0.251, 1.785,-5.854, 4.507,-2.2735,0.791,0.000),
                     c("GO:0035556","intracellular signal transduction", 4.000, 4.735, 3.216, 5.710,-3.2104,0.567,0.026),
                     c("GO:0045944","positive regulation of transcription from RNA polymerase II promoter", 0.369, 2.182, 0.919, 4.675,-2.6613,0.445,0.307),
                     c("GO:0071333","cellular response to glucose stimulus", 0.019, 4.875, 4.961, 3.394,-2.0653,0.624,0.361),
                     c("GO:0006468","protein phosphorylation", 4.137,-4.206, 2.098, 5.725,-3.2159,0.407,0.385),
                     c("GO:0046777","protein autophosphorylation", 0.077,-4.596, 3.782, 3.993,-3.1145,0.467,0.456),
                     c("GO:0006351","transcription, DNA-templated",10.659,-2.801,-2.141, 6.136,-2.1606,0.561,0.509),
                     c("GO:0016310","phosphorylation", 7.764,-5.776, 3.144, 5.998,-4.1807,0.481,0.615),
                     c("GO:0042523","positive regulation of tyrosine phosphorylation of Stat5 protein", 0.003,-0.521, 4.024, 2.544,-2.5209,0.325,0.639));

one.data <- data.frame(revigo.data);
names(one.data) <- revigo.names;
one.data <- one.data [(one.data$plot_X != "null" & one.data$plot_Y != "null"), ];
one.data$plot_X <- as.numeric( as.character(one.data$plot_X) );
one.data$plot_Y <- as.numeric( as.character(one.data$plot_Y) );
one.data$plot_size <- as.numeric( as.character(one.data$plot_size) );
one.data$log10_p_value <- as.numeric( as.character(one.data$log10_p_value) );
one.data$frequency <- as.numeric( as.character(one.data$frequency) );
one.data$uniqueness <- as.numeric( as.character(one.data$uniqueness) );
one.data$dispensability <- as.numeric( as.character(one.data$dispensability) );
#head(one.data);


# --------------------------------------------------------------------------
# Names of the axes, sizes of the numbers and letters, names of the columns,
# etc. can be changed below
library(tidyverse)
one.x_range = max(one.data$plot_X) - min(one.data$plot_X);
one.y_range = max(one.data$plot_Y) - min(one.data$plot_Y);
ex <- one.data %>% filter (term_ID %in% c("GO:0038083",
                                          "GO:0098609",
                                          "GO:0035556",
                                          "GO:0045944",
                                          "GO:0071333",
                                          "GO:0006468",
                                          "GO:0046777",
                                          "GO:0006351",
                                          "GO:0016310",
                                          "GO:0042523"
                                          )) 
require("ggrepel")
set.seed(42)
ggplot( data = one.data ) + 
  geom_point( aes( plot_X, plot_Y, colour = log10_p_value, size = plot_size), alpha = I(0.7) ) +
  scale_colour_gradientn( colours = c("red", "blue", "#69b3a2"), limits = c( min(one.data$log10_p_value), 0) ) + 
  # geom_point( aes(plot_X, plot_Y, size = plot_size), shape = 21, fill = "transparent", colour = I (alpha ("black", 0.6) )) + 
  scale_size( range=c(1, 20)) + 
  theme_bw() +
  labs (x = "semantic space x", y = "semantic space y") +
  # xlim(min(one.data$plot_X)-one.x_range/10,max(one.data$plot_X)+one.x_range/10) + 
  # ylim(min(one.data$plot_Y)-one.y_range/10,max(one.data$plot_Y)+one.y_range/10) +
  geom_text_repel( data = ex, aes(plot_X, plot_Y, label = description), colour = "black", size = 5,
                   box.padding = unit(0.5, "lines"),
                   point.padding = unit(0.5, "lines"), 
                   segment.size = 1) + 
  ggtitle("Differential ribosome density - downregulated with the activation ")



# --------------------------------------------------------------------------
# Output the plot to screen

p1;

# Uncomment the line below to also save the plot to a file.
# The file type depends on the extension (default=pdf).

# ggsave("C:/Users/path_to_your_file/revigo-plot.pdf");




# A treemap R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800

# author: Anton Kratz <anton.kratz@gmail.com>, RIKEN Omics Science Center, Functional Genomics Technology Team, Japan
# created: Fri, Nov 02, 2012  7:25:52 PM
# last change: Fri, Nov 09, 2012  3:20:01 PM

# -----------------------------------------------------------------------------
# If you don't have the treemap package installed, uncomment the following line:
# install.packages( "treemap" );
library(treemap) 								# treemap package by Martijn Tennekes

# Set the working directory if necessary
# setwd("C:/Users/username/workingdir");

# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","freqInDbPercent","abslog10pvalue","uniqueness","dispensability","representative");
revigo.data <- rbind(c("GO:0002181","cytoplasmic translation",0.064,16.1763,0.673,0.000,"cytoplasmic translation"),
                     c("GO:0006412","translation",5.686,86.8712,0.575,0.696,"cytoplasmic translation"),
                     c("GO:0006417","regulation of translation",0.692,3.2973,0.557,0.528,"cytoplasmic translation"),
                     c("GO:0006364","rRNA processing",0.952,12.6424,0.387,0.218,"cytoplasmic translation"),
                     c("GO:0006413","translational initiation",0.518,5.3249,0.632,0.429,"cytoplasmic translation"),
                     c("GO:0006414","translational elongation",0.777,2.5657,0.623,0.550,"cytoplasmic translation"),
                     c("GO:0008380","RNA splicing",0.413,3.9559,0.639,0.656,"cytoplasmic translation"),
                     c("GO:0006397","mRNA processing",0.561,2.5556,0.633,0.677,"cytoplasmic translation"),
                     c("GO:0000028","ribosomal small subunit assembly",0.020,10.7292,0.596,0.637,"cytoplasmic translation"),
                     c("GO:0002479","antigen processing and presentation of exogenous peptide antigen via MHC class I, TAP-dependent",0.001,1.4776,0.934,0.000,"antigen processing and presentation of exogenous peptide antigen via MHC class I, TAP-dependent"),
                     c("GO:0098609","cell-cell adhesion",0.251,1.8485,0.935,0.000,"cell-cell adhesion"),
                     c("GO:1901998","toxin transport",0.008,2.1994,0.900,0.000,"toxin transport"),
                     c("GO:1904871","positive regulation of protein localization to Cajal body",0.002,2.1406,0.768,0.135,"toxin transport"),
                     c("GO:0032212","positive regulation of telomere maintenance via telomerase",0.010,1.3870,0.557,0.395,"toxin transport"),
                     c("GO:0090666","scaRNA localization to Cajal body",0.001,1.3674,0.830,0.301,"toxin transport"),
                     c("GO:1904851","positive regulation of establishment of protein localization to telomere",0.002,1.9880,0.767,0.653,"toxin transport"),
                     c("GO:0071353","cellular response to interleukin-4",0.005,5.2532,0.896,0.019,"cellular response to interleukin-4"),
                     c("GO:0009615","response to virus",0.117,3.7368,0.920,0.223,"cellular response to interleukin-4"),
                     c("GO:0051085","chaperone mediated protein folding requiring cofactor",0.010,2.7902,0.909,0.020,"chaperone mediated protein folding requiring cofactor"),
                     c("GO:0006457","protein folding",0.903,6.3557,0.900,0.027,"protein folding"));

stuff <- data.frame(revigo.data);
names(stuff) <- revigo.names;

stuff$abslog10pvalue <- as.numeric( as.character(stuff$abslog10pvalue) );
stuff$freqInDbPercent <- as.numeric( as.character(stuff$freqInDbPercent) );
stuff$uniqueness <- as.numeric( as.character(stuff$uniqueness) );
stuff$dispensability <- as.numeric( as.character(stuff$dispensability) );

# by default, outputs to a PDF file
pdf( file="revigo_treemap.pdf", width=16, height=9 ) # width and height are in inches

# check the tmPlot command documentation for all possible parameters - there are a lot more
tmPlot(
  stuff,
  index = c("representative","description"),
  vSize = "abslog10pvalue",
  type = "categorical",
  vColor = "representative",
  title = "REVIGO Gene Ontology treemap",
  inflate.labels = FALSE,      # set this to TRUE for space-filling group labels - good for posters
  lowerbound.cex.labels = 0,   # try to draw as many labels as possible (still, some small squares may not get a label)
  bg.labels = "#CCCCCCAA",     # define background color of group labels
  # "#CCCCCC00" is fully transparent, "#CCCCCCAA" is semi-transparent grey, NA is opaque
  position.legend = "none"
)

dev.off()
