---
title: "Looking codon association with TDD index"
author: "`r Sys.info()['user']`"
date: "`r Sys.time()`"
output: 
  pdf_document:
    toc: true
    toc_depth: 2
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(fig.width = 12, fig.height = 12)
```

# Load functions, variables, and calculate generic tables
```{r, include=FALSE}
library(gridExtra)
source(file = "~/RMI2/gitlab/tdd/src/CSC_functions.R")

color_resting = "#69b3a2"
color_activated = "#404080"

names_codon_aa <- c("AAA (Lys)", "AAC (Asn)", "AAG (Lys)", "AAT (Asn)", "ACA (Thr)", "ACC (Thr)", "ACG (Thr)", "ACT (Thr)", "AGA (Arg)", "AGC (Ser)", "AGG (Arg)", "AGT (Ser)", "ATA (Ile)", "ATC (Ile)", "ATG (Met)", "ATT (Ile)", "CAA (Gln)", "CAC (His)", "CAG (Gln)", "CAT (His)", "CCA (Pro)", "CCC (Pro)", "CCG (Pro)", "CCT (Pro)", "CGA (Arg)", "CGC (Arg)", "CGG (Arg)", "CGT (Arg)", "CTA (Leu)", "CTC (Leu)", "CTG (Leu)", "CTT (Leu)", "GAA (Glu)", "GAC (Glu)", "GAG (Glu)", "GAT (Glu)", "GCA (Ala)", "GCC (Ala)", "GCG (Ala)", "GCT (Ala)", "GGA (Gly)", "GGC (Gly)", "GGG (Gly)", "GGT (Gly)", "GTA (Val)", "GTC (Val)", "GTG (Val)", "GTT (Val)", "TAA (Stop)", "TAC (Tyr)", "TAG (Stop)", "TAT (Tyr)", "TCA (Ser)", "TCC (Ser)", "TCG (Ser)", "TCT (Ser)", "TGA (Stop)", "TGC (Cys)", "TGG (Trp)", "TGT (Cys)", "TTA (Leu)", "TTC (Phe)", "TTG (Leu)", "TTT (Phe)")

fastaFile <- readDNAStringSet("~/RMI2/gitlab/tdd/data/cds_seq_mm10.txt")
codon_counts <- count_codon(fastaFile)
codon_freq <- freq_codon(codon_counts)
df_codon_freq <- list_to_df(codon_freq)
df_codon_count <- list_to_df(codon_counts)
```

# What are we testing

We're looking here if there is any codon that can be correlated or associated with TDD transcripts

# Looking with rCSC manner

## With Absolute TDD index TripCHX 3h in Resting and Activated lymphocytes

I first calculate CSC scores in resting and activated lymphocytes

```{r}
index_resting <- "Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h"
index_activated <- "Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h"

load(file = "~/RMI2/gitlab/tdd/results/pvalCSC_Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h.RData")
load(file = "~/RMI2/gitlab/tdd/results/pvalCSC_Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h.RData")

suppressWarnings(rCSC_resting <- get_rCSC(df_codon_freq = df_codon_freq, index = index_resting))
suppressWarnings(rCSC_activated <- get_rCSC(df_codon_freq = df_codon_freq, index = index_activated))

names(rCSC_resting) <- names_codon_aa
barplot_resting <- barplot_rCSC(rCSC_resting)
barplot_resting <- barplot_resting + ggtitle(index_resting)

names(rCSC_activated) <- names_codon_aa
barplot_activated <- barplot_rCSC(rCSC_activated)
barplot_activated <- barplot_activated + ggtitle(index_activated)

grid.arrange(barplot_resting, barplot_activated, ncol = 1)

```

Comparaison between CSC scores in resting and activated lymhpcytes

```{r}
data <- cbind(as.data.frame(rCSC_resting),as.data.frame(rCSC_activated))
data$Name <- rownames(data)

ggplot(data = data, aes(x = rCSC_resting, y = rCSC_activated)) + 
  geom_point() + 
  geom_abline(slope = 1, intercept = 0) + 
  geom_text(aes(label=Name),hjust=0, vjust=0) 

```


For control, I randomly permute the sequence of each CDS and I calculate again the CSC with it. Here is an exemple of CSC obtained from permutated ORFome

```{r}
rCSC_perm_resting <- get_rCSC(df_codon_freq = list_to_df(freq_codon(count_permutated_seq(fastaFile))),
                              index_name = index_resting,
                              filtred = TRUE)

rCSC_perm_activated <- get_rCSC(df_codon_freq = list_to_df(freq_codon(count_permutated_seq(fastaFile))),
                                index_name = index_activated,
                                filtred = TRUE)

names(rCSC_perm_resting) <- names_codon_aa
barplot_perm_resting <- barplot_rCSC(rCSC_perm_resting)
barplot_perm_resting <- barplot_perm_resting + ggtitle(index_resting)

names(rCSC_perm_activated) <- names_codon_aa
barplot_perm_activated <- barplot_rCSC(rCSC_perm_activated)
barplot_perm_activated <- barplot_perm_activated + ggtitle(index_activated)

grid.arrange(barplot_perm_resting, barplot_perm_activated, ncol = 1)


```

Here is a plot of CSC obtained with Abs TDD index in resing lymphocytes with the real CDS vs permutated CDS. We can see a kind of correlation due to the same GC content in each transcript. 

```{r}
data <- cbind(as.data.frame(rCSC_resting),as.data.frame(rCSC_perm_resting))
data$Name <- rownames(data)
ggplot(data = data, aes(x = rCSC_resting, rCSC_perm_resting)) + 
  geom_point(alpha = 0.5) + 
  geom_abline(slope = 1, intercept = 0) + 
  geom_text(aes(label=Name),hjust=0, vjust=0)

data <- cbind(as.data.frame(rCSC_activated),as.data.frame(rCSC_perm_activated))
data$Name <- rownames(data)
ggplot(data = data, aes(x = rCSC_activated, rCSC_perm_activated)) + 
  geom_point(alpha = 0.5) + 
  geom_abline(slope = 1, intercept = 0) + 
  geom_text(aes(label=Name),hjust=0, vjust=0, check_overlap = TRUE)
```


To have a p-value, I permutated the sequence of all the CDS 10.000 times and I count how many times the absolute value of the permutated CSC is higher than the CSC obtained with the real CDS (calcul duration : ~ 5h30 by index). As I permutated 10.000 times, pval cannot be lesser than 1e-04. 
Here I show again the barplot colored according their p-value  : 

```{r}
barplot_resting <- barplot_rCSC(rCSC_resting, pval_resting$FDR)
barplot_resting <- barplot_resting + ggtitle(index_resting)

barplot_activated <- barplot_rCSC(rCSC_activated, pval_activated$FDR)
barplot_activated <- barplot_activated + ggtitle(index_activated)

grid.arrange(barplot_resting, barplot_activated, ncol = 1)
```

To compare between resting and activated, I plot the CSC resting vs activated and I colored codon with a pval < 0.01 in resting, activated or both lymphocytes : 

```{r}
data <- cbind(as.data.frame(rCSC_resting),as.data.frame(rCSC_activated))
data$Name <- rownames(data)
data <- cbind(data, pval_resting$FDR, pval_activated$FDR)
data$sign <- "n.s"
data <- na.exclude(data)
data[data$`pval_resting$FDR` < 0.05 & data$`pval_activated$FDR` > 0.05, "sign"] <- "resting"
data[data$`pval_resting$FDR` > 0.05 & data$`pval_activated$FDR` < 0.05, "sign"] <- "activated"
data[data$`pval_resting$FDR` < 0.05 & data$`pval_activated$FDR` < 0.05, "sign"] <- "resting & activated"

ggplot(data = data, aes(x = rCSC_resting, y = rCSC_activated, color = sign)) + 
  geom_point() + 
  geom_abline(slope = 1, intercept = 0) + 
  geom_vline(xintercept = 0, linetype = "dotted") + 
  geom_hline(yintercept = 0, linetype = "dotted") +
  geom_text(aes(label=Name),hjust=0, vjust=0) + 
   scale_color_manual("FDR", values = c("n.s" = "grey", "resting" = color_resting, "activated" = color_activated, "resting & activated" = "red" ))
```

## DegFold 

I performed the same analysis regarding the degradation rate in resting and activated lymphocytes

```{r}
degfold_resting <- "DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h"
degfold_activated <- "DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h"

load(file = "~/RMI2/gitlab/tdd/results/pvalCSC_DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h.RData")
load(file = "~/RMI2/gitlab/tdd/results/pvalCSC_DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h.RData")

suppressWarnings(rCSC_degFold_resting <- get_rCSC(df_codon_freq = df_codon_freq, index = degfold_resting))
suppressWarnings(rCSC_degFold_activated <- get_rCSC(df_codon_freq = df_codon_freq, index = degfold_activated))

names(rCSC_degFold_resting) <- names_codon_aa
barplot_resting <- barplot_rCSC(rCSC_degFold_resting, pval_resting$FDR)
barplot_resting <- barplot_resting + ggtitle(degfold_resting)

names(rCSC_degFold_activated) <- names_codon_aa
barplot_activated <- barplot_rCSC(rCSC_degFold_activated, pval_activated$FDR)
barplot_activated <- barplot_activated + ggtitle(degfold_activated)

grid.arrange(barplot_resting, barplot_activated, ncol = 1)

```

Comparaison between CSC scores in resting and activated lymhpcytes

```{r}
data <- cbind(as.data.frame(rCSC_degFold_resting),as.data.frame(rCSC_degFold_activated))
data$Name <- rownames(data)
data <- cbind(data, pval_resting$FDR, pval_activated$FDR)
data$sign <- "n.s"
data <- na.exclude(data)
data[data$`pval_resting$FDR` < 0.05 & data$`pval_activated$FDR` > 0.05, "sign"] <- "resting"
data[data$`pval_resting$FDR` > 0.05 & data$`pval_activated$FDR` < 0.05, "sign"] <- "activated"
data[data$`pval_resting$FDR` < 0.05 & data$`pval_activated$FDR` < 0.05, "sign"] <- "resting & activated"

ggplot(data = data, aes(x = rCSC_degFold_resting, y = rCSC_degFold_activated, color = sign)) + 
  geom_point() + 
  geom_abline(slope = 1, intercept = 0) + 
  geom_text(aes(label=Name),hjust=0, vjust=0) + 
  geom_vline(xintercept = 0, linetype = "dotted") + 
  geom_hline(yintercept = 0, linetype = "dotted") +
   scale_color_manual("Sign", values = c("n.s" = "grey", "resting" = color_resting, "activated" = color_activated, "resting & activated" = "red" ))

```

## Absolute Non TDD Index

```{r}
index_nonTDD_resting <- "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h"
index_nonTDD_activated <- "Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h"

load(file = "~/RMI2/gitlab/tdd/results/pvalCSC_Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h.RData")
load(file = "~/RMI2/gitlab/tdd/results/pvalCSC_Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h.RData")


suppressWarnings(rCSC_nonTDD_resting <- get_rCSC(df_codon_freq = df_codon_freq, index = index_nonTDD_resting))
suppressWarnings(rCSC_nonTDD_activated <- get_rCSC(df_codon_freq = df_codon_freq, index = index_nonTDD_activated))

names(rCSC_nonTDD_resting) <- names_codon_aa
barplot_resting <- barplot_rCSC(rCSC_nonTDD_resting, pval_resting$FDR)
barplot_resting <- barplot_resting + ggtitle(index_nonTDD_resting)

names(rCSC_nonTDD_activated) <- names_codon_aa
barplot_activated <- barplot_rCSC(rCSC_nonTDD_activated,pval_activated$FDR)
barplot_activated <- barplot_activated + ggtitle(index_nonTDD_activated)

grid.arrange(barplot_resting, barplot_activated, ncol = 1)

```

Comparaison between CSC scores in resting and activated lymhpcytes

```{r}
data <- cbind(as.data.frame(rCSC_nonTDD_resting),as.data.frame(rCSC_nonTDD_activated))
data$Name <- rownames(data)
data <- cbind(data, pval_resting$FDR, pval_activated$FDR)
data$sign <- "n.s"
data <- na.exclude(data)
data[data$`pval_resting$FDR` < 0.05 & data$`pval_activated$FDR` > 0.05, "sign"] <- "resting"
data[data$`pval_resting$FDR` > 0.05 & data$`pval_activated$FDR` < 0.05, "sign"] <- "activated"
data[data$`pval_resting$FDR` < 0.05 & data$`pval_activated$FDR` < 0.05, "sign"] <- "resting & activated"

ggplot(data = data, aes(x = rCSC_nonTDD_resting, y = rCSC_nonTDD_activated, color = sign)) + 
  geom_point() + 
  geom_abline(slope = 1, intercept = 0) + 
  geom_vline(xintercept = 0, linetype = "dotted") + 
  geom_hline(yintercept = 0, linetype = "dotted") +
  geom_text(aes(label=Name),hjust=0, vjust=0) +
  scale_color_manual("Sign", values = c("n.s" = "grey", "resting" = color_resting, "activated" = color_activated, "resting & activated" = "red" ))

```


# Looking with the GC3 manner

The work of Hia et al., 2019 in EMBO reports showed that in human, codons can be clustered into two distinct groups — codons with G or C at the third base position (GC3) and codons with either A or T at the third base position (AT3): the former stabilizing while the latter destabilizing mRNA. 

## Absolute TDD index

I try to explore the data with this method. First, I check the clustering of codon that they observe in human is also true in mouse, taking only expressed gene in resting lymphocytes. For that, I did like in the publication i.e. a PCA from the frequencies of codon in the CDS : 
```{r}
library("FactoMineR")
library("factoextra")

index_clean <- prepare_index(index_name = "Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h", filtred = TRUE)
sub <- df_codon_freq[rownames(df_codon_freq) %in% names(index_clean),]

res.pca <- PCA(sub, scale.unit = TRUE, ncp = 5, graph = FALSE)

fviz_pca_var(res.pca, col.var = "contrib",
             gradient.cols = c("blue", "yellow", "red"),
             legend.title = "Contribution")
```


```{r}
GC3_percent_resting <- calc_GC3_percent(df_codon_count = df_codon_count)
boxplot <- boxplot_opt_percent_index(opt_percent = GC3_percent_resting, index = index_clean)
grid.arrange(boxplot[[1]] + ggtitle("Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h"),
             boxplot[[2]] + ggtitle("Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h"),
             ncol = 1)


# index_clean <- prepare_index(index_name = "DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h", filtred = TRUE)
# boxplot_opt_percent_index(opt_percent = GC3_percent_resting, index = index_clean)
```
If I check in activated lymphocytes : 

```{r}
index_clean <- prepare_index(index_name = "Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h", filtred = TRUE)
sub <- df_codon_freq[rownames(df_codon_freq) %in% names(index_clean),]

res.pca <- PCA(sub, scale.unit = TRUE, ncp = 5, graph = FALSE)

fviz_pca_var(res.pca, col.var = "contrib",
             gradient.cols = c("blue", "yellow", "red"),
             legend.title = "Contribution")
```


```{r}
GC3_percent_activated <- calc_GC3_percent(df_codon_count = df_codon_count)
boxplot <- boxplot_opt_percent_index(opt_percent = GC3_percent_activated, index = index_clean)
grid.arrange(boxplot[[1]] + ggtitle("Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h"),
             boxplot[[2]] + ggtitle("Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h"),
             ncol = 1)


# index_clean <- prepare_index(index_name = "DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h", filtred = TRUE)
# boxplot_opt_percent_index(opt_percent = GC3_percent_activated, index = index_clean)
```

```{r}
p1 <- binning_correlation(var = GC3_percent_resting, index_name = "Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h", filtred = TRUE )
p1 <- p1 + xlab("GC3 percent") + 
           ylab("Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h") + 
           geom_point(color = color_resting) +
           ggtitle("Abs.TDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h")
p2 <- binning_correlation(var = GC3_percent_activated, index_name = "Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h", filtred = TRUE )
p2 <- p2 + xlab("GC3 percent") +
           ylab("Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h") +
           geom_point(color = color_activated) + 
           ggtitle("Abs.TDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h")

grid.arrange(p1, p2, ncol = 1 )

```

```{r}
p1 <- binning_correlation(var = GC3_percent_resting, index_name = "RiboDens.Lympho_Resting", filtred = FALSE )
p1 <- p1 + xlab("GC3 percent") + 
           ylab("RiboDens.Lympho_Resting") + 
           geom_point(color = color_resting) +
           ggtitle("RiboDens.Lympho_Resting")
p2 <- binning_correlation(var = GC3_percent_activated, index_name = "RiboDens.Lympho_Activated", filtred = FALSE )
p2 <- p2 + xlab("GC3 percent") +
           ylab("RiboDens.Lympho_Activated") +
           geom_point(color = color_activated) + 
           ggtitle("RiboDens.Lympho_Activated")

grid.arrange(p1, p2, ncol = 1 )

```

## DegFold
And if we look for the degradation rate at 3h : 

```{r}
index_clean <- prepare_index(index_name = "DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h", filtred = TRUE)
boxplot <- boxplot_opt_percent_index(opt_percent = GC3_percent_resting, index = index_clean)
grid.arrange(boxplot[[1]] + ggtitle("DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h"),
             boxplot[[2]] + ggtitle("DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h"),
             ncol = 1)

index_clean <- prepare_index(index_name = "DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h", filtred = TRUE)
boxplot <- boxplot_opt_percent_index(opt_percent = GC3_percent_activated, index = index_clean)
grid.arrange(boxplot[[1]] + ggtitle("DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h"),
             boxplot[[2]] + ggtitle("DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h"),
             ncol = 1)

```
```{r}
p1 <- binning_correlation(var = GC3_percent_resting, index_name = "DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h", filtred = TRUE )
p1 <- p1 + xlab("GC3 percent") + 
           ylab("DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h") + 
           geom_point(color = color_resting) +
           ggtitle("DegFold.Lympho_Resting.Trip.Ref_Trip_0h.3h")
p2 <- binning_correlation(var = GC3_percent_activated, index_name = "DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h", filtred = TRUE )
p2 <- p2 + xlab("GC3 percent") +
           ylab("DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h") +
           geom_point(color = color_activated) + 
           ggtitle("DegFold.Lympho_Activated.Trip.Ref_Trip_0h.3h")

grid.arrange(p1, p2, ncol = 1 )

```

## Absolute NonTDD index
And if we look for the nonTDD index at 3h : 

```{r}
index_clean <- prepare_index(index_name = "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h", filtred = TRUE)
boxplot <- boxplot_opt_percent_index(opt_percent = GC3_percent_resting, index = index_clean)
grid.arrange(boxplot[[1]] + ggtitle("Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h"),
             boxplot[[2]] + ggtitle("Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h"),
             ncol = 1)

index_clean <- prepare_index(index_name = "Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h", filtred = TRUE)
boxplot <- boxplot_opt_percent_index(opt_percent = GC3_percent_activated, index = index_clean)
grid.arrange(boxplot[[1]] + ggtitle("Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h"),
             boxplot[[2]] + ggtitle("Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h"),
             ncol = 1)

```

```{r}
p1 <- binning_correlation(var = GC3_percent_resting, index_name = "Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h", filtred = TRUE )
p1 <- p1 + xlab("GC3 percent") + 
           ylab("Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h") + 
           geom_point(color = color_resting) +
           ggtitle("Abs.NonTDD..Lympho_Resting.Trip_CHX.Ref_Trip_0h.3h")
p2 <- binning_correlation(var = GC3_percent_activated, index_name = "Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h", filtred = TRUE )
p2 <- p2 + xlab("GC3 percent") +
           ylab("Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h") +
           geom_point(color = color_activated) + 
           ggtitle("Abs.NonTDD..Lympho_Activated.Trip_CHX.Ref_Trip_0h.3h")

grid.arrange(p1, p2, ncol = 1 )

```

# Render 
```{r}
# rmarkdown::render("codon_association.Rmd", output_file = paste0('codon_association',Sys.time(),'.html'))
```
