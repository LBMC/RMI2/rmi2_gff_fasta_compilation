
pks <- list('ggplot2',
            'tidyverse',
            'DESeq2',
            'reshape2',
            'stringr',
            'gridExtra',
            'RColorBrewer',
            'gplots')

lapply(pks , library , character.only = T, quietly = FALSE)

theme_set(theme_bw())

RNAseqFile = "data/HTSeq_count_stats_all_libraries.csv"
RiboseqFile = "data/RiboProf_counts_stats.df"

# Differential translation ------------------------------------------------


RNAseq_data <- read.delim(file = RNAseqFile,
                          header = TRUE,
                          sep = ",",
                          row.names = 1,
                          stringsAsFactors = TRUE)

## clear data
RNAseq_data <- RNAseq_data[grep("ENSMUSG.*", rownames(RNAseq_data)), grep(".*0h_untreated_CDS$", colnames(RNAseq_data))]

rnRSd <- rownames(RNAseq_data)
RNAseq_data <- apply(RNAseq_data, 2, function(x) as.numeric(as.integer(x)))
rownames(RNAseq_data) <- rnRSd

RiboSeq_data <- read.delim(file = RiboseqFile,
                           header = TRUE,
                           sep = "\t",
                           row.names = 1,
                           stringsAsFactors = TRUE)

RiboSeq_data <- RiboSeq_data[grep("ENSMUSG.*", rownames(RiboSeq_data)), grep(".*(a|r)T._CDS$", colnames(RiboSeq_data))]
rnRSd <- rownames(RiboSeq_data)
RiboSeq_data <- apply(RiboSeq_data, 2, function(x) as.numeric(as.integer(x)))
rownames(RiboSeq_data) <- rnRSd

data <- cbind(RiboSeq_data, RNAseq_data)

for (i in 1:ncol(data)) {
  for (j in 1:nrow(data)) {
    if (is.na(data[j, i])) {
      data[j, i] <- 0
    }
  }
}

# Formatting colData
colData = as.data.frame((colnames(data)))

conds = factor(c("activated",
                 "resting",
                 'activated',
                 "resting",
                 'activated',
                 "resting",
                 "resting",
                 "resting",
                 "resting",
                 "activated",
                 "activated",
                 "activated"))

sequencing = factor(c(rep("RiboSeq", 6),
                      rep("RNAseq", 6)))
replicate = factor(c(1,
                     1,
                     2,
                     2,
                     3,
                     3,
                     3,
                     2,
                     1,
                     3,
                     2,
                     1))

colData <- cbind(colData, conds, sequencing, replicate)
colnames(colData) <- c("library", "condition", "sequencing", "replicate")

# Performing DESeq 
dds = DESeqDataSetFromMatrix(
  countData = as.matrix(data),
  colData = colData,
  design = ~ condition:sequencing + condition + replicate:sequencing
)
dds$condition <- relevel(dds$condition, "resting")
dds$sequencing <- relevel(dds$sequencing, "RNAseq")

keep <- rowSums(counts(dds)[, 1:6]) >= 10
dds <- dds[keep, ]

dds <- DESeq(dds)
res <- results(dds,
               contrast = list("conditionactivated.sequencingRiboSeq",
                               "conditionresting.sequencingRiboSeq"
                          )
        )

# Checking DESeq model
plotDispEsts(dds)

rld <- rlogTransformation(dds, blind = TRUE)
vsd <- varianceStabilizingTransformation(dds, blind = TRUE)

plotPCA(rld)

ylim <- c(-10, 10)
drawLines <- function()
  abline(h = c(-.4, .4),
         col = "dodgerblue",
         lwd = 2)
sampleDists <- dist(t(assay(rld)))
sampleDistMatrix <- as.matrix(sampleDists)
colours = colorRampPalette(rev(brewer.pal(9, "Blues")))(255)
heatmap.2(sampleDistMatrix,
          trace = "none",
          col = colours,
          cexRow = 0.8,
          cexCol = 0.8)

plotMA(res, alpha = 0.05)
summary(res)
table(res$padj < 0.05)
write.csv(res, file = "results/DESeqresults_lympho_activation_untreated.csv")


# Ribosome Profiling only -------------------------------------------------
colData_rp <- colData %>% filter(sequencing == "RiboSeq") %>% select(- sequencing)
data_rp <- data[,grep("Tmac", colnames(data))]

dds = DESeqDataSetFromMatrix(
  countData = as.matrix(data_rp),
  colData = colData_rp,
  design = ~ condition + replicate )

dds$condition <- relevel(dds$condition, "resting")
dds$sequencing <- relevel(dds$sequencing, "RNAseq")

keep <- rowSums(counts(dds)[, 1:6]) >= 10
dds <- dds[keep, ]

dds <- DESeq(dds)

res <- results(dds, c("condition", "activated", "resting"))
# Checking DESeq model
plotDispEsts(dds)

rld <- rlogTransformation(dds, blind = TRUE)
vsd <- varianceStabilizingTransformation(dds, blind = TRUE)

plotPCA(rld)

ylim <- c(-10, 10)
drawLines <- function()
  abline(h = c(-.4, .4),
         col = "dodgerblue",
         lwd = 2)
sampleDists <- dist(t(assay(rld)))
sampleDistMatrix <- as.matrix(sampleDists)
colours = colorRampPalette(rev(brewer.pal(9, "Blues")))(255)
heatmap.2(sampleDistMatrix,
          trace = "none",
          col = colours,
          cexRow = 0.8,
          cexCol = 0.8)

plotMA(res, alpha = 0.05)
summary(res)
table(res$padj < 0.05)
write.csv(res, file = "results/DESeqresults_lympho_activation_RiboProf_only.csv")

# Differential expression after 3h activation -----------------------------

filename <- "data/HTSeq_count_stats_all_libraries.csv"
data <- read.csv(filename, sep = ",")
rownames(data) <- data$Gene_id
data <- cbind(data[grep("^ENSMUSG", data$Gene_id),
                   grep("^(R|A).*untreated.*exon", colnames(data))],
              data[grep("^ENSMUSG", data$Gene_id),
                   grep("^A.*0h_Triptolide_exon", colnames(data))])

samples <- colnames(data)
words_df <- as.data.frame(matrix(ncol = length(samples), nrow = 6))
colnames(words_df) <- samples

for (i in samples) {
  words_df[, i] <- strsplit(x = i, split = "_")
}

repliate <- substr(words_df[1,], start = 2, stop = 3)
times <- unlist(words_df[4,])
conditions <- unlist(words_df[3,])
drug <- unlist(words_df[5,])

coldata <- data.frame(samples)
coldata$condition <- conditions
coldata$time <- times
coldata$replicate <- repliate
coldata$drug <- drug

# 0h vs 3h activation 
coldata3h <-
  filter(
    coldata,
    (time == "0h" & condition == "Resting" & drug == "untreated") |
    (time == "0h" & condition == "Activated" & drug == "untreated")
  )
data3h <- data[, colnames(data) %in% coldata3h$samples]

dds3h <- DESeqDataSetFromMatrix(countData = data3h,
                                colData = coldata3h,
                                design = ~ condition + replicate)
dds3h <- DESeq(dds3h)

plotDispEsts(dds3h)

vsd <- varianceStabilizingTransformation(dds3h, blind = TRUE)
sampleDists <- dist(t(assay(vsd)))
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <-
  paste(vsd$condition, vsd$replicate, sep = "-")
colnames(sampleDistMatrix) <- NULL
colors <- colorRampPalette(rev(brewer.pal(9, "Blues")))(255)
pheatmap(sampleDistMatrix,
         clustering_distance_rows = sampleDists,
         clustering_distance_cols = sampleDists,
         col = colors)

plotPCA(vsd, intgroup = c("condition", "replicate"))

# plot count for FOS
plotCounts(dds = dds3h,
           gene = grep("ENSMUSG00000021250", rownames(dds3h)))

# plot count for CD69
plotCounts(dds = dds3h,
           gene = grep("ENSMUSG00000030156", rownames(dds3h)))

res3h <- results(dds3h,
                 contrast = c("condition", "Activated", "Resting"))
summary(res3h)

write.csv(res3h, file = "results/DiffExpr_lympho_activation3h.csv")

# Differential expression after 6h activation -----------------------------

coldata6h <- filter(coldata,
                   (time == "0h" & condition == "Resting") |
                   (time == "3h" & condition == "Activated"))
data6h <- data[, colnames(data) %in% coldata6h$samples]

dds6h <- DESeqDataSetFromMatrix(countData = data6h,
                                colData = coldata6h,
                                design = ~ condition + replicate)
dds6h <- DESeq(dds6h)

plotDispEsts(dds6h)

vsd <- varianceStabilizingTransformation(dds6h, blind = TRUE)
sampleDists <- dist(t(assay(vsd)))
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <-
  paste(vsd$condition, vsd$replicate, sep = "-")
colnames(sampleDistMatrix) <- NULL
colors <- colorRampPalette(rev(brewer.pal(9, "Blues")))(255)
pheatmap(sampleDistMatrix,
         clustering_distance_rows = sampleDists,
         clustering_distance_cols = sampleDists,
         col = colors)

plotPCA(vsd, intgroup = c("condition", "replicate"))

# plot count for FOS
plotCounts(dds = dds6h,
           gene = grep("ENSMUSG00000021250", rownames(dds6h)))

# plot count for CD69
plotCounts(dds = dds6h,
           gene = grep("ENSMUSG00000030156", rownames(dds6h)))

res6h <- results(dds6h,
                 contrast = c("condition", "Activated", "Resting"))
summary(res6h)
write.csv(res6h, file = "results/DiffExpr_lympho_activation6h.csv")
