#!/usr/bin/python3

"""Generate random sequence and compare the deltaG"""
import os
import random
import numpy as np
import scipy
from datetime import datetime
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def shuffle_and_simulate(path_fasta: str,
                         n_artificial_sequences: int=2_000,
                         threads: int=20) -> None:

    assert os.path.exists(path_fasta)

    print(datetime.now().strftime("%H:%M:%S"))

    # load the sequences
    with open(path_fasta, 'r') as reader:
        content = reader.readlines()

    # Set the seed for reproducibility
    random.seed(666)

    # Generate the output file
    with open(path_fasta.replace('.fa', '_input.fasta'), 'w+') as writer:
        writer.write('')
    
    # Inject sequences and artificial ones
    for transcript in content:
        infos = transcript.rstrip().split(',')
        name = infos[0]
        sequence = infos[1]

        with open(path_fasta.replace('.fa', '_input.fasta'),
                  'a') as updater:
            
            # Add the original sequence
            updater.write(f">{name}\n")
            updater.write(f"{sequence}\n")

            for idx in range(n_artificial_sequences):
                n = len(sequence)
                # Pick A, T, G and C in random order
                list_nucleotide = ['A', 'T', 'G', 'C']
                random.shuffle(list_nucleotide)

                # Create equimolar sequence dealing with no multiplicity of 4
                n1 = int(0.25 * n)
                n2 = n - (3 * n1)

                # Artificial list
                artificial_list = np.array([list_nucleotide[0]] * n1)
                artificial_list = np.append(artificial_list,
                                            [list_nucleotide[1]] * n1)
                artificial_list = np.append(artificial_list,
                                            [list_nucleotide[2]] * n1)
                artificial_list = np.append(artificial_list,
                                            [list_nucleotide[3]] * n2)
                
                # Now shuffle the order of the bases
                np.random.shuffle(artificial_list)
                current_seq = ''.join(artificial_list)
                
                updater.write(f">{name}*{idx}\n")
                updater.write(f"{current_seq}\n")
                  
    print(datetime.now().strftime("%H:%M:%S"))

    # Destroy output file to avoid problem with RNAFold
    if os.path.exists(path_fasta.replace('.fa', '_output.fasta')):
        os.remove(path_fasta.replace('.fa', '_output.fasta'))

    # Execute RNAFold
    cmd = 'bash rnafold.sh '
    cmd += f"-i {path_fasta.replace('.fa', '_input.fasta')} "
    cmd += f"-o {path_fasta.replace('.fa', '_output.fasta')} "
    cmd += f"-j {threads}"

    os.system(cmd)
    print('Done')
    print(datetime.now().strftime("%H:%M:%S"))


def follow_dG_random(threads=30) -> None:

    # Create the final output
    file = 'length_dG.dg'
    with open(file,
              'w+') as initiator:
        initiator.write('')
    
    for n in [8, 31, 77]:
        if not os.path.exists(f"output_{n}.fasta"):
            with open('input.fasta',
                    'w+') as initiator:
                initiator.write('')

            with open('input.fasta', 'a') as updater:
                print(f"Starting {n}",
                      datetime.now().strftime("%H:%M:%S"))
                for idx in range(2_000):
                    # Pick A, T, G and C in random order
                    list_nucleotide = ['A', 'T', 'G', 'C']
                    random.shuffle(list_nucleotide)

                    # Create equimolar sequence dealing with no multiplicity
                    # of 4
                    n1 = int(0.25 * n)
                    n2 = n - (3 * n1)

                    # Artificial list
                    artificial_list = np.array([list_nucleotide[0]] * n1)
                    artificial_list = np.append(artificial_list,
                                                [list_nucleotide[1]] * n1)
                    artificial_list = np.append(artificial_list,
                                                [list_nucleotide[2]] * n1)
                    artificial_list = np.append(artificial_list,
                                                [list_nucleotide[3]] * n2)
                    
                    # Now shuffle the order of the bases
                    np.random.shuffle(artificial_list)
                    current_seq = ''.join(artificial_list)
                    
                    updater.write(f">{n}*{idx}\n")
                    updater.write(f"{current_seq}\n")

            # Destroy output file to avoid problem with RNAFold
            if os.path.exists(f'output_{n}.fasta'):
                os.remove(f'output_{n}.fasta')

            # Execute RNAFold
            cmd = 'bash rnafold.sh '
            cmd += f"-i 'input.fasta' "
            cmd += f"-o 'output_{n}.fasta' "
            cmd += f"-j {threads}"

            # Harvest the result
            os.system(cmd)
            print('Done')
            print(datetime.now().strftime("%H:%M:%S"))


def create_MFE_database():
    L = []
    MFE = []
    std = []

    for n in range(100, 10_000, 100):
        if os.path.exists(f'output_{n}.fasta'):
            print(n)
            with open(f'output_{n}.fasta',
                      'r') as reader:
                lines = reader.readlines()
            
            values = [float(line.split('(')[1].split(')')[0]) for line in lines]
            
            L.append(n)
            MFE.append(np.mean(values))
            std.append(2 * np.std(values))
    
    df = pd.DataFrame({'Length (nt)': L,
                       'MFEref': MFE,
                       '2std': std})
    fig, ax = plt.subplots()
    p = sns.regplot(data=df,
                    x='Length (nt)',
                    y='MFEref',
                    ax = ax)
    ax.errorbar(L,
                MFE,
                yerr=std,
                fmt='none',
                capsize=5,
                zorder=1,
                color='C0')

    # define y-unit to x-unit ratio
    ratio = 1.0

    # get x and y limits
    x_left, x_right = ax.get_xlim()
    y_low, y_high = ax.get_ylim()
    
    # set aspect ratio
    ax.set_aspect(abs((x_right - x_left) / (y_low - y_high) ) * ratio)

    #calculate slope and intercept of regression equation
    (slope,
     intercept,
     r,
     p,
     sterr) = scipy.stats.linregress(x=p.get_lines()[0].get_xdata(),
                                     y=p.get_lines()[0].get_ydata())

    carac = f"MFEref = {round(slope, 3)} Length + {round(intercept, 3)}\n"
    carac += f"Pearson Correlation Coefficient = {round(r, 3)}"

    plt.suptitle(carac)

    plt.show()

    df.to_csv('MFE_db.csv')


if __name__ == "__main__":

    shuffle_and_simulate('for_rnafold.fa',
                         n_artificial_sequences=2000,
                         threads=30)

    follow_dG_random()
    create_MFE_database()

    for n in [8, 31, 77]:
        if os.path.exists(f'output_{n}.fasta'):
            with open(f'output_{n}.fasta',
                        'r') as reader:
                lines = reader.readlines()
            
            values = [float(line.split('(')[1].split(')')[0]) for line in lines]
            
            print(n, np.mean(values))
