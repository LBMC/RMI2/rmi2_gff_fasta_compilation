#!/usr/bin/python3

"""Tool for fusion between the reference file of Manu and the new columns."""
import os
import pandas as pd
import numpy as np
from tqdm import tqdm
import argparse
import seaborn as sns


def merge_databases(path_to_ref: str,
                    path_to_mi_half_lives: str,
                    path_to_final: str) -> None:
    """
        Update an existing database using transcript id as reference to
        take into account gene duplication in the reference db.

    Args:
        path_to_ref (str): path to the reference database .csv
        path_to_mi_half_lives (str): path to the new value database .csv
        path_to_final (str): path of the output .csv
    """

    # First check that files and folder exist
    print('Assertion check up.')
    map(assert_path,
        [path_to_ref,
         path_to_mi_half_lives])
    
    # Load the database as pd.DataFrames
    print('Loading dataframes.')
    ref_df = pd.read_csv(path_to_ref)
    new_df = pd.read_csv(path_to_mi_half_lives)

    # Merge the reference database with the new columns
    if not os.path.exists(path_to_final):
        attribute_new_values_to_genes(ref_df,
                                      new_df,
                                      path_to_final)
    final_df = pd.read_csv(path_to_final)
    
    # Compute the MFEden
    final_df = compute_MFEden(final_df)
    final_df.to_csv(path_to_final,
                    index=False)
    print(f"Dimension of the updated database: {final_df.shape}")




def compute_MFEden(final_df: pd.DataFrame) -> pd.DataFrame:
    """
        Correct the DG calculations using the MFEden formula
        MFEden = 100 * (MFE - MFErefL) / (L - L0)

        After computions from 100bp to 9900bp using RNAfold we have a perfect
        linear behaviour between MFErefL and the length:

        MFErefL = 17.15 - (0.326 * L)

        And L0 = 8 in the reference publication.

        So it can be computed directly

        For short sequences (31 CDS to end, or 47UTR-30CDS) the MFE has been
        computed with RNAfold

        31CDS-End = -4.327
        47UTR5-30CDS = -17.244

    Args:
        final_df (pd.DataFrame): DataFrame with all values of interest

    Returns:
        pd.DataFrame: Updated dataframe
    """


    # Create dictionary to facilitate looping
    dict_MFEden = {'UTR5_DG_MFEden': ('UTR5_DG', 'Length.utr5'),
                   'CDS_DG_MFEden': ('CDS_DG', 'Length.cds'),
                   'UTR3_DG_MFEden': ('UTR3_DG', 'Length.utr3'),
                   '47UTR5-30CDS_DG_MFEden': ('47UTR5-30CDS_DG', (-17.244, 77)),
                   '31CDS-End_DG_MFEden': ('31CDS-End_DG', (-4.327, 31))}

    for key in tqdm(list(dict_MFEden.keys()),
                         desc='Correcting the RNA free energies.',
                         colour='green'):
        final_df = MFEden(final_df,
                          dict_MFEden[key][0],
                          dict_MFEden[key][1],
                          key)
    return final_df   


def MFEden(df: pd.DataFrame,
           MFE: str,
           MFErefL,
           new_col_name:'str') -> pd.DataFrame:
    """
        Compute the MFEden using acolumn or constant

    Args:
        df (pd.DataFrame): Dataframe containing the values
        MFE (str): Column with raw RNAfold values
        MFErefL (_type_): Column name or constant
        new_col_name (str): Name f the column to be created

    Returns:
        pd.DataFrame: Updated dataframe
    """    
    
    # test if we use a column or constant for MFErefL
    if isinstance(MFErefL, str):
        df[new_col_name] = round(100 * (df[MFE] -
                                        (17.15 - 0.326 * df[MFErefL])) /
                                        (df[MFErefL] - 8),
                                        3)
    
    if isinstance(MFErefL, tuple):
        df[new_col_name] = round(100 * (df[MFE] - MFErefL[0]) /
                                 (MFErefL[1] - 8),
                                 3)
    
    return df


def attribute_new_values_to_genes(ref_df: pd.DataFrame,
                                  new_df: pd.DataFrame,
                                  path_to_final: str) -> None:
    """
        Merge the two set of values using the transcript id.
        This way gene duplication is handled.
        The csv file is immediatly generated to speed up the process.
        Concatenation is much too slow.

    Args:
        ref_df (pd.DataFrame): Reference database of Manu
        new_df (pd.DataFrame): New set of values (miRNA and half-lives)
        path_to_final (str): Path to the final csv file.
    """

    # Simplify the new_df to speed up process
    wanted = ['transcript_id',
              'miRNA_canonical_sites']
    
    new_df = new_df[wanted]

    # Get all genes id (without version) associated to new values
    transcript_mi = new_df['transcript_id'].to_numpy()

    # Create dataframe for a gene that has no mi-RNA
    empty_row = {}
    for w in wanted:
        empty_row[w] = [np.nan]
    empty_df = pd.DataFrame.from_dict(empty_row)
    n_row = ref_df.shape[0]

    for row in tqdm(range(n_row),
                    desc='Adding the new columns',
                    colour='cyan'):

        # Get the reference row with its gene id (without version)
        row_df = ref_df.loc[[row]]
        transcript = row_df['transcript_id'].to_numpy()[0]

        # The transcript is associated with new values
        if transcript in transcript_mi:
            filter = new_df['transcript_id'] == transcript
            new_row_df = new_df.loc[filter]

        # No new values for this transcript
        else:
            new_row_df = empty_df.copy()
            new_row_df['transcript_id'] = [transcript]

        new_full_row_df = row_df.merge(new_row_df,
                                       on='transcript_id',
                                       how='outer')

        # Create the final file or appennd it (much faster that dataframe
        # concatenation 105it vs 5).        
        if row == 0:
            new_full_row_df.to_csv(path_to_final,
                                   index=False)
        
        else:
            new_full_row_df.to_csv(path_to_final,
                                   index=False,
                                   header=False,
                                   mode='a')



def retrieve_arguments() -> argparse.ArgumentParser.parse_args:
    """Retrieve arguments from commandline.

    Returns:
        argparse.ArgumentParser.parse_args: collection of arguments
    """ 
    # Initiate parser with description
    description = 'Compute MFEden and append the new columns to the TDD database.'
    parser = argparse.ArgumentParser(description=description)

    # Reference
    help_reference = 'Path to the reference csv.'
    parser.add_argument('reference',
                        help=help_reference,
                        default=None)

    # New
    help_new = 'Path to the new columns csv.'
    parser.add_argument('new',
                        help=help_new,
                        default=None)

    # Final
    help_final = 'Path to the final csv.'
    parser.add_argument('final',
                        help=help_final,
                        default=None)

    # Retrieve now the arguments
    arguments = parser.parse_args()

    return arguments

def assert_path(path: str) -> None:
    """
        Initial QC to check that all input files and folder are here.
        Else raise an error.

    Args:
        path (str): pathof the current file/folder to validate.
    """

    assert os.path.exists(path), \
        f"File {path} doesn't exist."


if __name__ == "__main__":
    args = retrieve_arguments()
    merge_databases(args.reference,
                    args.new,
                    args.final)
