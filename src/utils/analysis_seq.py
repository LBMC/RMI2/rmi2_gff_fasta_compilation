#!/usr/bin/python
# coding: utf-8
"""Give basic data on sequence composition."""
from __future__ import division


def ATGC(sequence):
    """Analyze the sequence."""
    A = 0
    T = 0
    G = 0
    C = 0
    N = 0

    le = len(sequence)

    if le != 0:
        A = round(100 * sequence.count('A') / le,
                  2)
        T = round(100 * sequence.count('T') / le,
                  2)
        G = round(100 * sequence.count('G') / le,
                  2)
        C = round(100 * sequence.count('C') / le,
                  2)
        N = round(100 * sequence.count('N') / le,
                  2)

    return (str(le),
            str(A),
            str(T),
            str(G),
            str(C),
            str(N))
