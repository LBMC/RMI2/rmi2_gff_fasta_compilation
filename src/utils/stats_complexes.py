#!/usr/bin/python
# coding: utf-8
"""Determine if the shift in mean of a complex is statistically significant"""
# Python libraries
from __future__ import division
import os
import numpy as np
import pandas as pd
from config import path_core_complexes, path_UNIPROT_to_ENSEMBL
from config import path_gff3, path_fa, path_Output
from config import max_cap, min_cap
from utils.time_stamp import message_time
from utils.Object_manipulator import Manipulator
from config import n_Bootsrap_prot, n_min_prot
from utils.Distributions_GO import format_bin_edges, drawDistribution
from utils.Distributions_GO import clean_name, histogram_to_file
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import random


def get_pvalues(index,
                log_file):
    """Get all required TDD indexes values and calculate the pvalues."""
    message = message_time('Starting get_pvales.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    index_of_uniprot = from_uniprot_to_index(index,
                                             input_folder,
                                             log_file)

    (indexes_of_complex,
     indexes_of_population) = from_corum_to_index(index_of_uniprot,
                                                  log_file)

    estimate_pvalues(index,
                     indexes_of_complex,
                     indexes_of_population,
                     log_file)

    message = message_time('End get_pvales.\n\n')
    print(message)
    log_file.write(message)


def estimate_pvalues(index,
                     indexes_of_complex,
                     indexes_of_population,
                     log_file):
    """Estimate pvalues from bootstrapping."""
    message = message_time('Starting estimate_pvalues.\n')
    print(message)
    log_file.write(message)

    clean_cond = clean_name(index)

    # Create the PdfPages
    pdf = PdfPages(path_Output + clean_cond + '_pval_complexes.pdf')

    # Create the .csv file for the GO_score
    csv_name = clean_cond + '_Complex_score_filtered.csv'
    open(path_Output + csv_name, 'w+').close
    csv = open(path_Output + csv_name, 'a+')
    header = 'Complex,STD,Median,p_value_STD,p_value_Median,n_prot\n'
    csv.write(header)

    # Boostraping
    message = message_time('Generating distribution')
    message + 'of the global population.\n'
    print(message)
    log_file.write(message)

    # Now that we have all TDD values we generate the probability density
    (density_TDD,
     bin_edges) = np.histogram(indexes_of_population,
                               bins=50,
                               range=(min_cap, max_cap),
                               density=True)
    # Set the max to 1 for comparison purpose
    density_TDD = density_TDD / (max(density_TDD))
    # Convert bin_edges in mean values to have the same number of values in
    # density_TDD and bins_mean
    bins_meanTDD = format_bin_edges(bin_edges)

    # Create the .csv file for the Distribution
    csv_name2 = clean_cond + '_Tot_Complex_Distribution.csv'
    open(path_Output + csv_name2, 'w+').close
    csv2 = open(path_Output + csv_name2, 'a+')
    csv2.write(histogram_to_file(bins_meanTDD, density_TDD))
    csv2.close()

    mean_TDD = np.mean(indexes_of_population)
    median_TDD = np.median(indexes_of_population)
    std_TDD = np.std(indexes_of_population)

    # The GO Analysis
    done = range(10, 1000, 10)
    complex_done = 0

    list_complex = indexes_of_complex.keys()

    # Clean the dictionary from complexes too small
    for complex in list_complex:
        if len(indexes_of_complex[complex]) < n_min_prot:
            del indexes_of_complex[complex]

    list_complex = indexes_of_complex.keys()

    seed = 666
    random.seed(seed)

    # Analyse all complexes:
    for complex in list_complex:

        # Retrieve key information for display
        local_name = complex
        score = len(indexes_of_complex[complex])

        # Create the graph
        plt.figure(figsize=(18, 6))

        # create the title and get the mean and median
        local_title = local_name + ' : ' + str(score) + ' proteins\n'
        plt.suptitle(local_title, fontsize=12)

        local_std = np.std(indexes_of_complex[complex])
        local_mean = np.mean(indexes_of_complex[complex])
        local_median = np.median(indexes_of_complex[complex])

        # First plot: DISTRIBUTION
        plt.subplot(131)
        (density_comp,
         bin_edges) = np.histogram(indexes_of_complex[complex],
                                   bins=50,
                                   range=(min_cap, max_cap),
                                   density=True)
        # Set the max to 1 for comparison purpose
        density_comp = density_comp / (max(density_comp))
        bins_meanTDDcomp = format_bin_edges(bin_edges)
        mylabel = 'Mean pop = ' + str(mean_TDD)
        mylabel += ', Median pop = ' + str(median_TDD)
        drawDistribution(bins_meanTDD,
                         density_TDD,
                         X2=bins_meanTDDcomp,
                         Y2=density_comp,
                         label=mylabel)

        # Perform the Boostraping
        boot_std = []
        boot_median = []

        for boot in range(0, n_Bootsrap_prot, 1):
            picked = []
            for choix in range(0, score, 1):
                picked.append(random.choice(indexes_of_population))
            boot_std.append(np.std(picked))
            boot_median.append(np.median(picked))

        # Now calculate Distributions
        (density_rd_std,
         bin_edges_std) = np.histogram(boot_std,
                                       bins=n_Bootsrap_prot,
                                       range=(min_cap, max_cap),
                                       density=True)
        # Set the max to 1 for comparison purpose
        density_rd_std = np.cumsum(density_rd_std)
        density_rd_std = density_rd_std / (max(density_rd_std))

        # Now we determine if we need to reverse or not
        if local_std > std_TDD:
            density_rd_std = 1 - density_rd_std

        bin_edges_std = format_bin_edges(bin_edges_std)

        (density_rd_median,
         bin_edges_median) = np.histogram(boot_median,
                                          bins=n_Bootsrap_prot,
                                          range=(min_cap, max_cap),
                                          density=True)
        # Set the max to 1 for comparison purpose
        density_rd_median = np.cumsum(density_rd_median)
        density_rd_median = density_rd_median / (max(density_rd_median))

        # Now we determine if we need to reverse or not
        if local_median > median_TDD:
            density_rd_median = 1 - density_rd_median

        bin_edges_median = format_bin_edges(bin_edges_median)

        # Get the p_value
        # First calculate the distance for all bins
        dist_to_std = bin_edges_std - local_std
        dist_to_median = bin_edges_median - local_median

        # Get the minimal distance's index
        dist_to_std = np.absolute(dist_to_std)
        dist_to_std = dist_to_std.tolist()
        index_pvalue_std = dist_to_std.index(min(dist_to_std))

        dist_to_median = np.absolute(dist_to_median)
        dist_to_median = dist_to_median.tolist()
        index_pvalue_median = dist_to_median.index(min(dist_to_median))

        # Retrieve the p_value
        pvalue_std = round(density_rd_std[index_pvalue_std], 4)
        pvalue_median = round(density_rd_median[index_pvalue_median], 4)

        # Second plot: MEAN
        plt.subplot(132)
        mylabel = 'STD = ' + str(local_std)
        mylabel += ', p-value = ' + str(pvalue_std)
        drawDistribution(bin_edges_std,
                         density_rd_std,
                         X2=[local_std, local_std],
                         Y2=[0, 1],
                         label=mylabel)

        # Second plot: MEDIAN
        plt.subplot(133)
        mylabel = 'Median = ' + str(local_median)
        mylabel += ', p-value = ' + str(pvalue_median)
        drawDistribution(bin_edges_median,
                         density_rd_median,
                         X2=[local_median, local_median],
                         Y2=[0, 1],
                         label=mylabel)

        values = ''
        values += str(local_name) + ','
        values += str(local_std - std_TDD) + ','
        values += str(local_median - median_TDD) + ','
        values += str(pvalue_std) + ','
        values += str(pvalue_median) + ','
        values += str(score) + '\n'
        csv.write(values)

        # Save the page
        plt.savefig(pdf,
                    format='pdf',
                    dpi=300)

        # Close the graph to spare memory
        plt.close()

        complex_done += 1
        if complex_done in done:
            message = message_time(str(complex_done) + ' complex analyzed.\n')
            print(message)
            log_file.write(message)

    message = message_time(str(complex_done) + ' complex analyzed.\n')
    print(message)
    log_file.write(message)
    # Close the pdf file
    pdf.close()

    # Close and save the txt file
    csv.close()

    message = message_time('End estimate_pvalues.\n\n')
    print(message)
    log_file.write(message)


def from_corum_to_index(index_of_uniprot,
                        log_file):
    """Get all complexes of Corum and obtain their composition as index."""
    message = message_time('Starting from_corum_to_index.\n')
    print(message)
    log_file.write(message)

    # Initiate listings and dictionary
    ref_uniprot_population = []
    indexes_of_population = []
    indexes_of_complex = {}

    # Open the complex database
    pd_complexes = pd.read_csv(path_core_complexes)

    # Extract only the wanted columns
    wanted_columns = ['ComplexName',
                      'subunits(UniProt IDs)',
                      'GO ID']
    pd_complexes = pd_complexes[wanted_columns]

    # get all complexes names
    complexes = pd_complexes['ComplexName'].values

    # Extract indexes for all component of all Complexes
    for complex in complexes:
        filter = pd_complexes['ComplexName'] == complex
        sub_units = pd_complexes['subunits(UniProt IDs)'][filter].values
        sub_units = sub_units[0].split(';')
        indexes_of_complex[complex] = []

        for subunit in sub_units:
            if subunit in index_of_uniprot.keys():
                sub_index = index_of_uniprot[subunit][0]
                if sub_index != 'NA':
                    indexes_of_complex[complex].append(sub_index)

                    # Update the population of indexes of proteins involved
                    # in complexes
                    if subunit not in ref_uniprot_population:
                        ref_uniprot_population.append(subunit)
                        indexes_of_population.append(sub_index)

    message = message_time('End from_corum_to_index.\n\n')
    print(message)
    log_file.write(message)

    return (indexes_of_complex, indexes_of_population)


def from_uniprot_to_index(index,
                          input_folder,
                          log_file):
    """Extract all uniprot ref from uniprot and create dictionaries."""
    message = message_time('Starting from_uniprot_to_index.\n')
    print(message)
    log_file.write(message)

    # Dictionary of uniprot refvas key and [index, gene name] as value
    index_of_uniprot = {}

    # Load the gene to transcript database
    pd_genes_trans = pd.read_csv(path_Output + 'Gene_to_Transcript.csv')
    list_gene_id = pd_genes_trans['gene_id'].values

    # Load the UNIPROT to ENSEMBL gene id
    pd_uniprot_gene = pd.read_csv(path_UNIPROT_to_ENSEMBL)
    list_uniprot_id = pd_uniprot_gene['From'].values

    # Feed dictionaries
    for uniprot in list_uniprot_id:
        filter1 = pd_uniprot_gene['From'] == uniprot
        ensembl_gene = pd_uniprot_gene['To'][filter1].values[0]

        if ensembl_gene in list_gene_id:
            filter2 = pd_genes_trans['gene_id'] == ensembl_gene
            transcript_list = pd_genes_trans['transcript_id'][filter2].values
            name = pd_genes_trans['gene_name'][filter2].values[0]

            # Get the first transcript file
            # They all have the indexes
            transcript = transcript_list[0].split(';')[0]
            f = Manipulator(input_folder + transcript + '.csv')
            value = f._getValue([index])[0]

            # We harvest only exploitable values and cap the aberrant ones
            if value != 'NA':
                value = float(value)
                if value >= max_cap:
                    value = max_cap
                elif value <= min_cap:
                    value = min_cap
                index_of_uniprot[uniprot] = [value, name]
        else:
            print(uniprot + ', ' + ensembl_gene + ' is missing.')

    message = message_time('Dictionary contains ')
    message += str(len(index_of_uniprot.keys())) + ' valid enties.\n'
    print(message)
    log_file.write(message)

    message = message_time('End from_corum_to_index.\n\n')
    print(message)
    log_file.write(message)

    return index_of_uniprot


if __name__ == "__main__":
    # execute only if run as a script
    get_pvalues()
