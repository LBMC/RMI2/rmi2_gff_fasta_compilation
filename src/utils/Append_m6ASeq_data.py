#!/usr/bin/python
# coding: utf-8
"""Append m6ASeq data to the transcripts files."""
# Python libraries
from __future__ import division
import os
import numpy as np

# Home made scripts and packages
from config import path_Output
from config import path_gff3, path_fa, path_m6ASeq
from utils.time_stamp import message_time
from utils.Object_shredder_m6ASeq import Shredder_m6ASeq
from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator

import time


def Add_m6ASeq(log_file):
    """Open the m6ASeq.tsv and add the data to the correct transcript files."""
    message = message_time('Starting Add_m6ASeq.\n')
    print(message)
    log_file.write(message)

    # 1. Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # If the specific input folder doesn't exist
    if os.path.exists(input_folder) is False:
        print('   #   ' + 'Expected output folder doesn t exist.')
        # Quit
        exit()

    # Get gene to transcripts conversion
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    db = Shredder_simple(conv_path, ',')

    # transform m6ASeq database into an object
    m6ASeq = Shredder_m6ASeq(path_m6ASeq, '\t')
    message = message_time('Starting Transfer.\n')
    print(message)
    log_file.write(message)

    # Process the m6ASeq database
    ngene, nok = treat_m6Aseq_db(m6ASeq,
                                 db,
                                 input_folder,
                                 log_file)

    message = message_time(str(ngene) + ' Genes treated.\n')
    print(message)
    log_file.write(message)

    message = message_time(str(nok) + ' Genes updated with m6A.\n')
    print(message)
    log_file.write(message)

    message = message_time('End Add_m6ASeq.\n\n')
    print(message)
    log_file.write(message)


def treat_m6Aseq_db(m6ASeq,
                    db,
                    input_folder,
                    log_file):
    """Use the m6Aseq shredder object to update transcript csv files."""
    Done = range(1000, 100000, 1000)
    ngene = 0
    nok = 0
    npeak = 0
    found = 0
    nutr5 = 0
    ncds = 0
    nutr3 = 0
    natg = 0
    nstop = 0
    for gene in m6ASeq._List_Gene_Name:
        ngene += 1
        # Extracting the data to implement in the transcript file
        newKeys = []
        dict = {}

        (n_Exp_Ends,
         Name_Exp_5Ends,
         m6A_Starts,
         m6A_Ends,
         m6A_scores,
         total_score) = m6ASeq._dict_values[gene]

        newKeys = np.append(newKeys, 'n_m6ASeq_Peaks')
        dict['n_m6ASeq_Peaks'] = str(n_Exp_Ends)

        newKeys = np.append(newKeys, 'total_m6ASeq_score')
        dict['total_m6ASeq_score'] = str(total_score)

        dict_peak = {}
        dict_score = {}

        for i in range(0, n_Exp_Ends, 1):
            prefix = Name_Exp_5Ends[i]
            newKeys = np.append(newKeys,
                                'm6ASeq_Peak_' + str(i+1))
            dict['m6ASeq_Peak_' + str(i+1)] = prefix

            newKeys = np.append(newKeys,
                                prefix + '_Start')
            dict[prefix + '_Start'] = m6A_Starts[i]

            newKeys = np.append(newKeys,
                                prefix + '_End')
            dict[prefix + '_End'] = m6A_Ends[i]

            newKeys = np.append(newKeys,
                                prefix + '_m6A_Score')
            dict[prefix + '_m6A_Score'] = m6A_scores[i]

            dict_score[i] = m6A_scores[i]
            dict_peak[i] = [int(m6A_Starts[i]), int(m6A_Ends[i])]

        # Transfer into the correct file(s)
        if gene in db._First_Col:
            nok += 1
            list_files_t = db._dict_values[gene, 'transcript_id']
            transcripts = list_files_t.split(';')
            for local_transcript in transcripts:

                newKeys = np.append(newKeys,
                                    'm6A_peaks_in_UTR5')
                dict['m6A_peaks_in_UTR5'] = 0

                newKeys = np.append(newKeys,
                                    'm6A_peaks_in_CDS')
                dict['m6A_peaks_in_CDS'] = 0

                newKeys = np.append(newKeys,
                                    'm6A_peaks_in_UTR3')
                dict['m6A_peaks_in_UTR3'] = 0

                newKeys = np.append(newKeys,
                                    'm6A_peaks_on_ATG')
                dict['m6A_peaks_on_ATG'] = 0

                newKeys = np.append(newKeys,
                                    'm6A_peaks_on_STOP')
                dict['m6A_peaks_on_STOP'] = 0

                #########
                newKeys = np.append(newKeys,
                                    'm6A_score_in_UTR5')
                dict['m6A_score_in_UTR5'] = 0

                newKeys = np.append(newKeys,
                                    'm6A_score_in_CDS')
                dict['m6A_score_in_CDS'] = 0

                newKeys = np.append(newKeys,
                                    'm6A_score_in_UTR3')
                dict['m6A_score_in_UTR3'] = 0

                newKeys = np.append(newKeys,
                                    'm6A_score_on_ATG')
                dict['m6A_score_on_ATG'] = 0

                newKeys = np.append(newKeys,
                                    'm6A_score_on_STOP')
                dict['m6A_score_on_STOP'] = 0

                # Transform the transcript file into modifiable object
                f = Manipulator(input_folder + local_transcript + '.csv')

                # Insert here the function to determine the peaks positions
                # within the different egments of the transcript
                segments_extremities = f._getValue(['UTR5_start',
                                                    'UTR5_end',
                                                    'CDS_start',
                                                    'CDS_end',
                                                    'UTR3_start',
                                                    'UTR3_end',
                                                    'trans_strand'])
                polarity = segments_extremities[6]
                UTR5raw = [int(segments_extremities[0]),
                           int(segments_extremities[1])]
                CDSraw = [int(segments_extremities[2]),
                          int(segments_extremities[3])]
                UTR3raw = [int(segments_extremities[4]),
                           int(segments_extremities[5])]

                (UTR5to_use,
                 CDSto_use,
                 UTR3to_use,
                 ATGto_use,
                 STOPto_use) = trim_segment(polarity,
                                            UTR5raw,
                                            CDSraw,
                                            UTR3raw)

                for i in range(0, n_Exp_Ends, 1):
                    npeak += 1
                    (dict,
                     inc,
                     utr5,
                     cds,
                     utr3,
                     atg,
                     stop) = where_is_this_peak(i,
                                                dict,
                                                dict_peak,
                                                dict_score,
                                                UTR5to_use,
                                                CDSto_use,
                                                UTR3to_use,
                                                ATGto_use,
                                                STOPto_use)
                    found += inc
                    nutr5 += utr5
                    ncds += cds
                    nutr3 += utr3
                    natg += atg
                    nstop += stop

                # text = input("Pause")  # Python 3

                # Update the object and the file

                for key in newKeys:
                    f._appendValue(key,
                                   dict[key])
                f._updateFile()

        else:
            message = message_time(gene + ' not in the database!.\n')
            # print(message)
            # log_file.write(message)

        if ngene in Done:
            message = message_time(str(ngene) + ' Genes treated.\n')
            print(message)
            log_file.write(message)

        if nok in Done:
            message = message_time(str(nok) + ' Genes updated with m6A.\n')
            print(message)
            print(found, '/', npeak)
            log_file.write(message)

    print(found, '/', npeak)
    print(nutr5, ncds, nutr3, natg, nstop)

    return ngene, nok


def where_is_this_peak(i,
                       dict,
                       dict_peak,
                       dict_score,
                       UTR5to_use,
                       CDSto_use,
                       UTR3to_use,
                       ATGto_use,
                       STOPto_use):
    """Determine where the peak is located."""
    peak_position = dict_peak[i]
    lcl_score = float(dict_score[i])
    # print('Analizing peak: ' + str(i), peak_position)
    attributed = 'not attributed'
    utr5 = 0
    cds = 0
    utr3 = 0
    atg = 0
    stop = 0

    if (is_it_here(peak_position, CDSto_use)):
        dict['m6A_peaks_in_CDS'] = dict['m6A_peaks_in_CDS'] + 1
        dict['m6A_score_in_CDS'] = dict['m6A_score_in_CDS'] + lcl_score
        attributed = 'yes'
        cds = 1
    else:
        if (is_it_here(peak_position, UTR5to_use)):
            dict['m6A_peaks_in_UTR5'] = dict['m6A_peaks_in_UTR5'] + 1
            dict['m6A_score_in_UTR5'] = dict['m6A_score_in_UTR5'] + lcl_score
            attributed = 'yes'
            utr5 = 1

        if (is_it_here(peak_position, UTR3to_use)):
            dict['m6A_peaks_in_UTR3'] = dict['m6A_peaks_in_UTR3'] + 1
            dict['m6A_score_in_UTR3'] = dict['m6A_score_in_UTR3'] + lcl_score
            attributed = 'yes'
            utr3 = 1
    if (is_it_here(peak_position, ATGto_use)):
        dict['m6A_peaks_on_ATG'] = dict['m6A_peaks_on_ATG'] + 1
        dict['m6A_score_on_ATG'] = dict['m6A_score_on_ATG'] + lcl_score
        atg = 1
        attributed = 'yes'
    if (is_it_here(peak_position, STOPto_use)):
        dict['m6A_peaks_on_STOP'] = dict['m6A_peaks_on_STOP'] + 1
        dict['m6A_score_on_STOP'] = dict['m6A_score_on_STOP'] + lcl_score
        stop = 1
        attributed = 'yes'
    if (attributed != 'yes'):
        # print(attributed)
        a = 0
    else:
        a = 1

    if ((cds == 1 and utr5 == 1) or
        (cds == 1 and utr3 == 1) or
        (utr5 == 1 and utr3 == 1)):
        print("warning multiple attribution")

    return (dict, a, utr5, cds, utr3, atg, stop)


def is_it_here(small_segment,
               big_segment):
    """Determine if the small segment is in the big one."""
    myRange = range(big_segment[0], big_segment[1], 1)
    if ((small_segment[0] in myRange) and (small_segment[0] in myRange)):
        res = True
    else:
        res = False
    return res


def trim_segment(polarity, UTR5, CDS, UTR3):
    """Trim the segments to later position the m6A peaks."""
    if polarity == '+':
        UTR5f = correct_order([UTR5[0], UTR5[1] + 25])
        CDSf = correct_order([CDS[0] + 25, CDS[1] - 25])
        UTR3f = correct_order([UTR3[0] - 25, UTR3[1]])
        ATG = correct_order([CDS[0] - 50, CDS[0] + 50])
        STOP = correct_order([CDS[1] + 50, CDS[1] - 50])
    else:
        UTR5f = correct_order([UTR5[0] - 25, UTR5[1]])
        CDSf = correct_order([CDS[0] + 25, CDS[1] - 25])
        UTR3f = correct_order([UTR3[0], UTR3[1] + 25])
        ATG = correct_order([CDS[1] - 50, CDS[1] + 50])
        STOP = correct_order([CDS[0] + 50, CDS[0] - 50])

    return (UTR5f, CDSf, UTR3f, ATG, STOP)


def correct_order(my_array):
    """Reorder properly the position of the segment."""
    lmin = min(my_array)
    lmax = max(my_array)
    new_array = [lmin, lmax]
    return new_array


if __name__ == "__main__":
    # execute only if run as a script
    Add_m6ASeq()
