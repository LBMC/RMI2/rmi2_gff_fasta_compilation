#!/usr/bin/python
# coding: utf-8
"""Functional object opening, getting, adding and updating transcript.csv."""
# Python libraries
from __future__ import division
import os
import numpy as np


def main():
    """Demo mode."""
    # Path of the project folder
    project_Folder = os.sep
    project_Folder += 'home.users' + os.sep
    project_Folder += 'dcluet' + os.sep
    project_Folder += 'Programmes' + os.sep
    project_Folder += 'RMI2_GFF_FASTA_compilation' + os.sep
    project_Folder += 'src' + os.sep

    # Path output
    path_Output = project_Folder + 'output' + os.sep
    path_Output += 'gencode.vM23.annotation_VS_GRCm38.p6.genome' + os.sep

    # File Name
    file_name = 'ENSMUST00000151084.csv'

    # Manipulate the file
    current_content = Manipulator(path_Output + file_name)

    # Verify the Reading
    print('Original header:')
    print(current_content._header)
    print('-')

    # Testing _updateHeader() function
    current_content._updateHeader('Here is a new entry in this file')
    print('New header:')
    print(current_content._header)
    print('-')

    # Testing the _getValue() function
    Keys = ['gene_name', 'david_input', 'peptide_seq']
    Values = current_content._getValue(Keys)
    print('Original values:')
    for i in range(0, len(Keys), 1):
        print(Keys[i] + ': \n' + Values[i])
    print('-')

    # Testing the _changeValue() function
    Keys = ['gene_name', 'peptide_seq']
    New_Values = ['Nawak-1', 'MYDAVIDWASTHERE*']
    print('New values:')
    for i in range(0, len(Keys), 1):
        current_content._changeValue(Keys[i],
                                     New_Values[i])
    Values = current_content._getValue(Keys)
    for i in range(0, len(Keys), 1):
        print(Keys[i] + ': \n' + Values[i])
    print('-')

    # Testing the _appendValue() function
    print('Adding a new entry')
    current_content._appendValue('david_input',
                                 'A new line for nothing.')
    Keys = ['david_input']
    Values = current_content._getValue(Keys)
    for i in range(0, len(Keys), 1):
        print(Keys[i] + ': \n' + Values[i])
    print('-')

    # Testing the _updateFile() function
    print('Saving changes and updates')
    current_content._updateFile()
    current_content = Manipulator(path_Output + file_name)
    print('File now contains:')
    print(current_content._header)
    Keys = ['gene_name', 'david_input', 'peptide_seq']
    Values = current_content._getValue(Keys)
    print('Original values:')
    for i in range(0, len(Keys), 1):
        print(Keys[i] + ': \n' + Values[i])
    print('-')


class Manipulator:
    """This object open and retrieve unique keys and their values into a {}.

    -The _getValue(['Key1', 'Key2',..'Keyn']) function also to extract only
    specific values.
    -The _changeValue('Key', newValue) will replace the old value by the new
    one in the dictionary.
    -The _appendValue('Key', 'Value') will append new Key and its value in the
    dictionary.
    -The _updateHeader('Message') allows to add a new message upstream to the
    current header (to keep trace of everything).
    -The _updateFile() command will overwrite the current transcript.csv file
    with the updates done in the dictionnary
    """

    def __init__(self,
                 pathCSV
                 ):
        """Construct method."""
        # Properties
        self._path = pathCSV
        self._header = ''
        self._dict_values = {}
        self._key_list = np.array([])

        # Reading the file
        self._read_file()

    def _updateFile(self):
        """Overwrite the file and save changes."""
        # Open the file for writing
        file = open(self._path, 'w')

        # Write header
        file.write(self._header + '\n')

        # Write all Values
        for key in self._key_list:
            file.write(key + ',' + str(self._dict_values[key]) + '\n')

        # Close file
        file.close

    def _appendValue(self,
                     new_key,
                     new_value):
        """Add a new key and associated value to the dictionary."""
        if new_key not in self._key_list:
            self._key_list = np.append(self._key_list, new_key)
        self._dict_values[new_key] = new_value

    def _changeValue(self,
                     key,
                     value):
        """Change the value of a specific key in the dictionnary."""
        self._dict_values[key] = value

    def _getValue(self,
                  Keys):
        """Return a list of value corresponding to the list Keys."""
        result = []
        for key in Keys:
            if key in self._dict_values:
                result.append(self._dict_values[key])
            else:
                result.append('NA')

        return result

    def _cleanHeader(self,
                     word):
        """Remove one string expression from header."""
        header = self._header
        header = header.replace(word, '')
        self._header = header

    def _updateHeader(self,
                      message):
        """Append a new comment at the begining of the header.

        !!! Message do not need a break line character nor #
        """
        self._header = '# ' + message + '\n' + self._header

    def _read_file(self):
        """Read the file and get all values."""
        # Open file and read it
        with open(self._path) as csv:
            # Read the firstline
            line = csv.readline()

            # Read all lines
            while line:
                # Decide if line belongs to header or describe a value
                if line[0] == '#':

                    # Ensure that line breaks are correctly preserve
                    if self._header == '':
                        self._header += line.replace('\n', '')
                    else:
                        self._header += '\n' + line.replace('\n', '')
                else:

                    # Remove breakline and split the line between key and value
                    fragments = line.replace('\n', '').split(',')

                    # Update Key list and Dictionary
                    self._key_list = np.append(self._key_list, fragments[0])
                    if len(fragments) == 2:
                        self._dict_values[fragments[0]] = fragments[1]
                    else:
                        self._dict_values[fragments[0]] = 'NA'
                line = csv.readline()


if __name__ == "__main__":
    # execute only if run as a script
    main()
