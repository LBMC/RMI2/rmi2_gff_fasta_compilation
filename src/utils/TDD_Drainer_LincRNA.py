#!/usr/bin/python
# coding: utf-8
"""Harvest TDD project data and perform calculations."""
# Python libraries
from __future__ import division
import os
import pandas as pd
import numpy as np
from utils.time_stamp import string_time, message_time
from utils.Object_manipulator import Manipulator
from utils.Formulas_Indexes import Calculate_Abs_TDD, Calculate_Rel_TDD
from utils.Formulas_Indexes import Calculate_Rel_nonTDD
from utils.Formulas_Indexes import Calculate_Abs_nonTDD
from utils.Formulas_Indexes import Calculate_Abs_nonDEG
from utils.Formulas_Indexes import Degradation_fold
from config import path_raw_tdd_linc
from config import path_gff3, path_fa
from config import path_Output
from config import expected_columns


# df.loc[(df['column_name'] >= A) & (df['column_name'] <= B)]

def Exploit_TDD_lincRNA(log_file,
                        cell_types,
                        cell_status,
                        transcription_inhibitors,
                        translation_inhibitors,
                        time_points,
                        replicates,
                        ref_treatment,
                        ref_time):
    """Harvest and manipulate the data."""
    message = message_time('Starting Exploit_TDD_lincRNA.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # create the line for the header of each manipulated file.
    update_header = '# ' + string_time() + ', manipulated with '
    update_header += 'Exploit_TDD\n'

    # create the path for the final output file
    output_file = path_raw_tdd_linc.replace('.csv', '_CD_python.csv')

    # Open the csv as a panda dataframe
    pd1 = pd.read_csv(path_raw_tdd_linc)

    # Get the column names and check that all required data are there
    col_names = pd1.columns.values.tolist()
    if not check_list(expected_columns,
                      col_names,
                      log_file):
        # missing columns -> abort Python
        exit()

    # Get only desired columns
    pd1 = pd1[expected_columns]

    # Convert all macro into Macro
    pd1.loc[pd1.cell == 'macro', 'cell'] = 'Macro'

    # Add the explicit inhibitors columns
    pd1 = Add_Inhibitor_columns(pd1, log_file)

    # Save the modified panda
    pd1.to_csv(output_file,
               index=False)

    # Loop to analyze all conditions
    loop_conditions(pd1,
                    cell_types,
                    cell_status,
                    transcription_inhibitors,
                    translation_inhibitors,
                    time_points,
                    replicates,
                    log_file,
                    input_folder,
                    ref_treatment,
                    ref_time)

    # End of Process
    message = message_time('End Exploit_TDD.\n\n')
    print(message)
    log_file.write(message)


def loop_conditions(pd1,
                    cell_types,
                    cell_status,
                    transcription_inhibitors,
                    translation_inhibitors,
                    time_points,
                    replicates,
                    log_file,
                    input_folder,
                    ref_treatment,
                    ref_time):
    """Calculate all indexes for all conditions."""
    for c in cell_types:
        pd2 = pd1.loc[(pd1['cell'] == c)]

        for s in cell_status:
            pd3 = pd2.loc[(pd2['Activated/Resting'] == s)]

            for inh1 in transcription_inhibitors:
                pd4 = pd3.loc[(pd3['Transcription_Inhibitor'] == inh1)]

                for inh2 in translation_inhibitors:
                    pd5 = pd4.loc[(pd4['Translation_Inhibitor'] == inh2)]

                    for t in time_points:
                        pd6 = pd5.loc[(pd5['time'] == t)]

                        cond = c + '_'
                        cond += s + '>'
                        cond += inh1 + '_'
                        cond += inh2 + '>'
                        cond += 'Ref_' + ref_treatment + '_' + ref_time + '>'
                        cond += t

                        message = message_time('Treating condition: ')
                        message += cond + '.\n'
                        print(message)
                        log_file.write

                        # Check how many rows have been selected
                        message = message_time(str(pd6.shape[0]))
                        message += ' rows will be processed.\n'
                        print(message)
                        log_file.write

                        if pd5.shape[0] > 0:
                            calculate_indexes(cond,
                                              t,
                                              pd6,
                                              pd3,
                                              replicates,
                                              log_file,
                                              input_folder,
                                              c,
                                              s,
                                              ref_treatment,
                                              ref_time,
                                              inh1,
                                              inh2)
                        else:
                            message = message_time(cond + ' is missing.')
                            message += ' Skipping.\n'
                            print(message)
                            log_file.write

                            # Skip this combination
                            break


def calculate_indexes(condition,
                      t,
                      lpd,
                      pd_Ref,   # contains all times and translation inhibitors
                      replicates,
                      log_file,
                      input_folder,
                      c,
                      s,
                      ref_inh,
                      ref_time,
                      inh1,
                      inh2):
    """Calculate all indexes for all gene for all replicates of condition."""
    message = message_time('Starting calculate_indexes for ')
    message += condition + '.\n'
    print(message)
    log_file.write(message)

    # Get the list of gene for the condition
    genes_list = np.unique(lpd['ensemblID'].values).tolist()

    size = lpd.shape[0] + 1000
    done = range(1000, size, 1000)
    n_gene_done = 0

    for g in genes_list:
        ge = g.split('.')[0]

        pd7 = lpd.loc[(lpd['ensemblID'] == g)]
        pdRef = pd_Ref.loc[(pd_Ref['ensemblID'] == g)]

        # Get list of transcripts for the gene
        list_trans = [ge]

        if len(list_trans) > 0:
            if c + '_' + s == 'Lympho_Activated':
                key_gold = 'Lympho_Activated>GOLD'
            elif c + '_' + s == 'Lympho_Resting':
                key_gold = 'Lympho_Resting>GOLD'
            elif c + '_' + s == 'Macro_Activated':
                key_gold = 'Macro_Activated>GOLD'
            elif c + '_' + s == 'Macro_Resting':
                key_gold = 'Macro_Resting>GOLD'

            absTDD = []
            absNonTDD = []
            relTDD = []
            relNonTDD = []
            absNonDeg = []
            Deg_Fold = []
            un = 'untreated'
            for r in replicates:

                pdInh1_Inh2 = pd7.loc[(pd7['replicate'] == r)]

                pdt0 = pdRef.loc[(pdRef['replicate'] == r) &
                                 (pdRef['time'] == ref_time) &
                                 (pdRef['Transcription_Inhibitor'] == ref_inh) &
                                 (pdRef['Translation_Inhibitor'] == un)]

                pdInh1 = pdRef.loc[(pdRef['replicate'] == r) &
                                   (pdRef['time'] == t) &
                                   (pdRef['Transcription_Inhibitor'] == inh1) &
                                   (pdRef['Translation_Inhibitor'] == un)]

                if (pdInh1_Inh2.shape[0] == 1 and
                    pdt0.shape[0] == 1 and
                    pdInh1.shape[0] == 1):
                    read_t_inh1_inh2 = pdInh1_Inh2['normReadsCounts'].values[0]
                    read_t_inh1 = pdInh1['normReadsCounts'].values[0]
                    read_0_inh1 = pdt0['normReadsCounts'].values[0]

                    if read_0_inh1 != 0 and (read_0_inh1 - read_t_inh1) != 0:
                        absTDD.append(Calculate_Abs_TDD(read_t_inh1_inh2,
                                                        read_t_inh1,
                                                        read_0_inh1)
                                      )

                        absNonTDD.append(Calculate_Abs_nonTDD(read_0_inh1,
                                                              read_t_inh1_inh2)
                                         )

                        relTDD.append(Calculate_Rel_TDD(read_t_inh1_inh2,
                                                        read_t_inh1,
                                                        read_0_inh1)
                                      )

                        relNonTDD.append(Calculate_Rel_nonTDD(read_0_inh1,
                                                              read_t_inh1_inh2,
                                                              read_t_inh1)
                                         )

                        absNonDeg.append(Calculate_Abs_nonDEG(read_t_inh1,
                                                              read_0_inh1)
                                         )
                        Deg_Fold.append(Degradation_fold(read_t_inh1,
                                                         read_0_inh1)
                                        )

            n_gene_done += 1

        if len(absTDD) > 0:
            # Calculate the means
            mean_absTDD = np.mean(absTDD)
            mean_relTDD = np.mean(relTDD)
            mean_absNonTDD = np.mean(absNonTDD)
            mean_relNonTDD = np.mean(relNonTDD)
            mean_absNonDeg = np.mean(absNonDeg)
            mean_Deg_Fold = np.mean(Deg_Fold)

            for local_transcript in list_trans:

                # Transform the transcript file into modifiable object
                f = Manipulator(input_folder + local_transcript + '.csv')

                # Update the object and the file
                cond_Deg_Fold = condition.replace('_' + inh2, '')
                f._appendValue('DegFold>' + cond_Deg_Fold, str(mean_Deg_Fold))
                f._appendValue('Abs(TDD)>' + condition, str(mean_absTDD))
                f._appendValue('Rel(TDD)>' + condition, str(mean_relTDD))
                f._appendValue('Abs(NonTDD)>' + condition, str(mean_absNonTDD))
                f._appendValue('Rel(NonTDD)>' + condition, str(mean_relNonTDD))
                f._appendValue('Abs(NonDEG)>' + condition, str(mean_absNonDeg))

                if key_gold not in f._key_list:
                    f._appendValue(key_gold, str(1))
                f._updateFile()

        if n_gene_done in done:
            print(message_time(str(n_gene_done) + ' lincRNA done.'))

    print(message_time(str(n_gene_done) + ' lincRNA done.'))

    message = message_time('End calculate_indexes for ')
    message += condition + '.\n'
    print(message)
    log_file.write(message)


def remove_version(genes):
    """Remove the versioning the the geneID."""
    for i in range(0, len(genes), 1):
        genes[i] = genes[i].split('.')[0]

    return genes


def Add_Inhibitor_columns(pd,
                          log_file):
    """Add Transcription_Inhibitor and the Translation_Inhibitor columns."""
    message = message_time('Starting Add_Inhibitor_columns.\n')
    print(message)
    log_file.write(message)

    # Create the new columns
    TransC = []
    TransL = []

    # Get number of Lines
    n_lines = pd.shape[0]

    # Explore all the panda and assign the values
    i = 0
    for index in range(0, n_lines, 1):
        # Get the value of the treatment column
        treat = pd['treatment'][index]

        if treat == 'Trip':
            TransC.append('Trip')
            TransL.append('untreated')

        elif treat == 'TripCHX':
            TransC.append('Trip')
            TransL.append('CHX')

        elif treat == 'TripHarr':
            TransC.append('Trip')
            TransL.append('Harr')

        elif treat == 'DRB':
            TransC.append('DRB')
            TransL.append('untreated')

        elif treat == 'DRBCHX':
            TransC.append('DRB')
            TransL.append('CHX')

        elif treat == 'DRBHarr':
            TransC.append('DRB')
            TransL.append('Harr')

        elif treat == 'untreated':
            TransC.append('untreated')
            TransL.append('untreated')

        elif treat == 'CHX':
            TransC.append('untreated')
            TransL.append('CHX')

        elif treat == 'Harr':
            TransC.append('untreated')
            TransL.append('Harr')

        else:
            print(treat)
        i += 1

    # Add the new columns
    pd['Transcription_Inhibitor'] = TransC
    pd['Translation_Inhibitor'] = TransL

    message = message_time('End Add_Inhibitor_columns.\n')
    print(message)
    log_file.write(message)

    return pd


def check_list(elements_to_have,
               list,
               log_file):
    """Validate that all elements to have are in list."""
    OK = True
    for element in elements_to_have:
        if element not in list:
            OK = False
            message = message_time(element + ' is missing.\n')
            print(message)
            log_file.write(message)

    if OK:
        message = message_time('All required data are present.\n')
        print(message)
        log_file.write(message)
    else:
        message = message_time('Aborting.\n')
        print(message)
        log_file.write(message)

    return OK


if __name__ == "__main__":
    # execute only if run as a script
    Exploit_TDD()
