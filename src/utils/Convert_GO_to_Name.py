#!/usr/bin/python
# coding: utf-8
"""Convert all GO terms from a file into csv compatible name."""
from utils.time_stamp import message_time
from utils.Object_shredder_simple import Shredder_simple
from config import path_Output

GO_Name = path_Output + 'GO_to_Phrase.tsv'


def Convert_All_GO(path_input_file, log_file):
    """Open file as string and shredder to replace all GO."""
    message = message_time('Starting to Convert GO Terms to Explicit names.\n')
    print(message)
    log_file.write(message)

    # get the GO name database
    GO_db = Shredder_simple(GO_Name, '\t')

    # Prepare Path for the output csv
    path_output_file = path_input_file.replace('.csv',
                                               '_Named.csv')

    # Open the file as raw string for modification
    print(path_input_file)
    f = open(path_input_file, 'r')
    input_string = f.read()

    # Replace all GO Terms
    for go in GO_db._First_Col:
        raw_name = GO_db._dict_values[go, 'Name']
        name = raw_name.replace(',', ';')
        input_string = input_string.replace(go, go + ' ' + name)

    # create the output file
    open(path_output_file, 'w+').close
    output_file = open(path_output_file, 'w+')
    output_file.write(input_string)
    output_file.close

    message = message_time('End Converting GO Terms to Explicit names.\n')
    print(message)
    log_file.write(message)


if __name__ == "__main__":
    # execute only if run as a script
    Convert_All_GO()
