#!/usr/bin/python
# coding: utf-8
"""Correct Tree Statistics."""
# Python libraries
import networkx as nx
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import re
from utils.time_stamp import message_time
from config import path_Arborescence
from config import path_Gene_GO
from config import path_GO_Tree
from config import path_Output
from config import plow, do_filiation
from utils.Tree_Creator import get_edges, get_family, clean_df
from utils.Object_shredder_simple import Shredder_simple


def correct_stats(path_GO_scores, colname, colindex, log_file):
    """Correct statistics within an arborescence."""
    message = message_time('Starting correct_stats.\n')
    print(message)
    log_file.write(message)

    # First, read the tree file
    GO_tree = read_tree(log_file)

    # Get the GO phrase dictionary
    phrase_of = Shredder_simple(path_Output + 'GO_to_Phrase.tsv',
                                '\t')

    # Get the GO children of dictionnary
    children_of, edges = get_family_ties(log_file)

    # Get the dictionnary of the pValues
    (dict_pval_of,
     dict_ntrans_of,
     dict_ngenes_of,
     dict_mean_of) = get_pvalues(path_GO_scores,
                                 colname,
                                 colindex,
                                 log_file)

    # Now create the tree with dataframe at each level
    stat_tree = create_tree_frame(GO_tree,
                                  dict_pval_of,
                                  dict_ntrans_of,
                                  dict_ngenes_of,
                                  dict_mean_of,
                                  log_file)

    stat_tree = adjust_pValues(stat_tree,
                               phrase_of,
                               children_of,
                               edges,
                               log_file)

    create_adjusted_pValue_file(stat_tree, path_GO_scores, log_file)

    message = message_time('End correct_stats.\n\n')
    print(message)
    log_file.write(message)


def create_adjusted_pValue_file(stat_tree, path_GO_scores, log_file):
    """Convert the stat_tree into a csv file."""
    message = message_time('Starting create_adjusted_pValue_file.\n')
    print(message)
    log_file.write(message)
    path_csv = path_GO_scores.replace('score_filtered.csv',
                                      'adjusted_pValues.csv')
    levels = stat_tree.keys()
    # Get the first DataFrame
    mydf = stat_tree[levels[0]]

    # Sequentially concatenate
    for i in range(1, len(levels), 1):
        df_toappend = stat_tree[levels[i]]
        mydf = pd.concat([mydf, df_toappend])

    # Saving as csv
    mydf.to_csv(path_csv, index=False)

    message = message_time('End create_adjusted_pValue_file.\n\n')
    print(message)
    log_file.write(message)


def adjust_pValues(stat_tree,
                   phrase_of,
                   children_of,
                   edges,
                   log_file):
    """Adjust the pValues following the reference."""
    # http://jds2019.sfds.asso.fr/program/Soumissions/subm215.pdf
    message = message_time('Starting adjust_pValues.\n')
    print(message)
    log_file.write(message)

    # Get the keys of the GO tree
    levels = stat_tree.keys()

    list_GO_OK = np.array([])
    # Perform pValue adjustement going down

    filiation = False
    children = []

    for l in levels:
        # Get the local DataFrame
        loc_df = stat_tree[l]


        if not filiation:
            # Until to have filiation we analyze everything

            children = loc_df['GO'].values

        # Prepare the dataframe to analyze only wanted GO
        # All GO not in children are stamped with To_Analyze = 0
        loc_df['To_Analyze'][~loc_df['GO'].isin(children)] = 0

        # Number of rows
        n = loc_df.shape[0]

        # Create the colonne pValue_adjusted
        col_pval = [0] * n
        loc_df['pValue_adjusted'] = col_pval

        # Directly convert the adjusted pValues to 2 if ['To_Analyze'] == 0
        # 2 means not tested at all
        loc_df['pValue_adjusted'][(loc_df['To_Analyze'] == 0)] = 2

        # test on the entire line and add the positive

        # Select on those that have to be done: ['To_Analyze'] == 1
        to_do_df = loc_df[(loc_df['To_Analyze'] == 1)]

        # Safety on length
        if to_do_df.shape[0] == 0:
            print('no children found')
            break

        m = to_do_df.shape[0]
        lim = np.arange(plow/m, plow + plow/m, plow/m).tolist()
        if len(lim) > m:
            lim = lim[:m]
        print(m, len(lim))

        # WARNING HERE IS PROBLEM WITH MACRO RESTING TEST LENGTH OF BOTH LISTS
        to_do_df['pValue'] = to_do_df['pValue'].astype(float)
        to_do_df['pValue_adjusted'] = lim
        # print(to_do_df)
        # raw_input('Before')

        # Retrieve GO that are statistically significant
        filter1 = (to_do_df['pValue'] <= to_do_df['pValue_adjusted'])
        df_OK = to_do_df[filter1]
        # print(df_OK)
        # raw_input('OK')

        # Change the adjusted pValue to 1 for those that are not validated
        filter2 = (to_do_df['pValue'] > to_do_df['pValue_adjusted'])
        to_do_df['pValue_adjusted'][filter2] = 1
        # print(to_do_df)
        # raw_input('After')

        # Transfert the adjusted pValues in the reference dataframe
        list_GO_analyzed = to_do_df['GO'].values
        for g in list_GO_analyzed:
            fG1 = (loc_df['GO'] == g)
            fG2 = (to_do_df['GO'] == g)
            loc_df['pValue_adjusted'][fG1] = to_do_df['pValue_adjusted'][fG2]

        new_OK = df_OK['GO'].values.tolist()
        list_GO_OK = np.append(list_GO_OK, new_OK)
        stat_tree[l] = loc_df

        # Prepare the next level
        if do_filiation:
            if len(new_OK) > 0 and not filiation:
                # We have hits we can start filiation process
                filiation = True

            if len(new_OK) > 0:
                # Create the listing to analyze
                for g in new_OK:
                    # We keep all children in the listing
                    children = np.append(children, children_of[g])
                children = np.unique(children).tolist()

                print('list to analyze next round:')
                print(children)

            if len(new_OK) == 0 and filiation:
                print('No more statistically relevant children.')
                break

    # QC
    list_GO_OK = list_GO_OK.tolist()
    links = pd.DataFrame()
    l_from = []
    l_to = []
    linked = []
    print('N GO = ' + str(len(list_GO_OK)))
    for e in edges:

        # Extract connections only for the key GOs
        if e[0] in list_GO_OK and e[1] in list_GO_OK:
            l_from.append(e[0])
            l_to.append(e[1])
            linked.append(e[0])
            linked.append(e[1])
            linked = np.unique(linked).tolist()

    n_not_linked = 0
    print('Not linked')
    for g in list_GO_OK:
        if g not in linked:
            n_not_linked += 1
            l_from.append(g)
            l_to.append('Alone')
            print(g + ' ' + phrase_of._dict_values[g, 'Name'])

    print('Not linked = ' + str(n_not_linked))

    links['From'] = l_from
    links['To'] = l_to

    print('Building Graph')
    # Build your graph
    G = nx.from_pandas_edgelist(links, 'From', 'To')

    # Plot it
    nx.draw(G, with_labels=True)
    plt.savefig(path_Output + 'NX_Network.png',
                dpi=300)

    log_file.write('Found GOs.\n')
    for go in list_GO_OK:
        info = go + ' ' + phrase_of._dict_values[go, 'Name']
        print(info)
        log_file.write(info + '\n')

    message = message_time('End adjust_pValues.\n\n')
    print(message)
    log_file.write(message)

    return stat_tree


def create_tree_frame(GO_tree,
                      dict_pval_of,
                      dict_ntrans_of,
                      dict_ngenes_of,
                      dict_mean_of,
                      log_file):
    """Generate a tree with all required values sorted, using pandas."""
    message = message_time('Starting create_tree_frame.\n')
    print(message)
    log_file.write(message)

    # Get the keys of the GO tree
    levels = GO_tree.keys()
    tree_df = {}

    # Construct the dataframe tree from top
    for l in levels:
        # Get all the local GOs
        list_GO = GO_tree[l]

        # Create the empty dataframe
        loc_df = pd.DataFrame()
        loc_GO = []
        loc_pvals = []
        loc_ntrans = []
        loc_ngenes = []
        loc_mean = []
        loc_todo = []

        for go in list_GO:
            if go in dict_pval_of.keys():
                loc_GO.append(go)
                loc_pvals.append(dict_pval_of[go])
                loc_ntrans.append(dict_ntrans_of[go])
                loc_ngenes.append(dict_ngenes_of[go])
                loc_mean.append(dict_mean_of[go])
                loc_todo.append(1)

        # Generate the dataframe
        loc_df['GO'] = loc_GO
        loc_df['pValue'] = loc_pvals
        loc_df['nTDD'] = loc_ntrans
        loc_df['nGenes'] = loc_ngenes
        loc_df['Mean'] = loc_mean
        loc_df['To_Analyze'] = loc_todo

        # Sort the dataframe with increasing pValues
        loc_df = loc_df.sort_values(by=['pValue'], ascending=True)

        # Feed the tree
        tree_df[l] = loc_df

    for k in tree_df.keys():
        message = message_time('Level ' + str(k) + ' contains ')
        message += str(tree_df[k].shape[0]) + ' GO.\n'
        print(message)
        log_file.write(message)

    message = message_time('End create_tree_frame.\n\n')
    print(message)
    log_file.write(message)

    return tree_df


def get_pvalues(path_file, pVal_col, colindex, log_file):
    """Extract the pValuesand the number of transcripts really in the GO."""
    message = message_time('Starting get_pvalues.\n')
    print(message)
    log_file.write(message)

    myfile = open(path_Gene_GO, 'r')
    data = myfile.read()

    # Open the csv as dataframe and clean it
    raw_df = pd.read_csv(path_file)
    raw_df = clean_df2(raw_df)

    dict_pval_of = {}
    dict_ntrans_of = {}
    dict_ngenes_of = {}
    dict_mean_of = {}
    for i in range(0, raw_df.shape[0], 1):
        vals = raw_df[['GO', pVal_col, 'n_genes', colindex]].values[i].tolist()
        loc_GO = vals[0]
        loc_pval = vals[1]
        loc_genes = vals[2]
        loc_mean = vals[3]

        # Build dictionnaries
        dict_pval_of[loc_GO] = loc_pval
        dict_ntrans_of[loc_GO] = loc_genes
        dict_mean_of[loc_GO] = loc_mean

        # Get the number of appearence of the GO in the association file
        list_appearance = [m.start() for m in re.finditer(loc_GO, data)]
        dict_ngenes_of[loc_GO] = len(list_appearance)

    n_entries1 = len(dict_pval_of.keys())
    n_entries2 = len(dict_ntrans_of.keys())
    n_entries3 = len(dict_ngenes_of.keys())

    message = message_time('Dictionary of pValues has ' + str(n_entries1))
    message += ' entries.\n'
    print(message)
    log_file.write(message)

    message = message_time('Dictionary of transcripts has ' + str(n_entries2))
    message += ' entries.\n'
    print(message)
    log_file.write(message)

    message = message_time('Dictionary of genes has ' + str(n_entries3))
    message += ' entries.\n'
    print(message)
    log_file.write(message)

    message = message_time('End get_pvalues.\n\n')
    print(message)
    log_file.write(message)

    return dict_pval_of, dict_ntrans_of, dict_ngenes_of, dict_mean_of


def get_family_ties(log_file):
    """Return the dictionary children_of to go down in the tree."""
    message = message_time('Starting get_family_ties.\n')
    print(message)
    log_file.write(message)

    # Open the csv as dataframe and clean it
    raw_df = pd.read_csv(path_Arborescence)
    raw_df = clean_df(raw_df)

    # Get the edges from the dataframe
    edges = get_edges(raw_df, log_file)

    # Get the parent and childre database.
    parent_of, children_of, parents = get_family(edges, log_file)

    n_found = len(children_of.keys())

    message = message_time(str(n_found) + ' childrehood links found.\n')
    print(message)
    log_file.write(message)

    message = message_time('End get_family_ties.\n\n')
    print(message)
    log_file.write(message)

    return children_of, edges


def read_tree(log_file):
    """Transform .csv tree into a dictionary."""
    message = message_time('Starting read_tree.\n')
    print(message)
    log_file.write(message)

    tree = {}
    level = 0
    with open(path_GO_Tree) as database:
        # Read the firstline
        line = database.readline()

        # Read all lines
        while line:
            # Remove all unwanted charaters
            line = clean_line(line)
            local_GOs = line.split(',')
            n_local_GOs = len(local_GOs)

            # QC of what is read
            message = message_time('Level ' + str(level))
            message += ' contains ' + str(n_local_GOs) + ' GOs.\n'
            print(message)
            log_file.write(message)

            # Build the tree
            tree[level] = local_GOs

            # Go to next level
            line = database.readline()
            level += 1

    message = message_time('End read_tree.\n\n')
    print(message)
    log_file.write(message)

    return tree


def clean_line(line):
    """Remove unwanted characters from line."""
    unwanted = ['"', '\n']
    for char in unwanted:
        line = line.replace(char, '')
    return line


def clean_df2(raw_df):
    """Remove lines with unwanted values."""
    raw_df = raw_df.dropna()
    raw_df = raw_df[raw_df.GO != '']
    return raw_df


if __name__ == "__main__":
    # execute only if run as a script
    correct_stats()
