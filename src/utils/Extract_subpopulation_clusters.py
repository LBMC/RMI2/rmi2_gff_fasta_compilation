#!/usr/bin/python
# coding: utf-8
"""Extract n genes from all clusters and curate the csv from Manu."""
from utils.time_stamp import string_time
from config import path_Output
from config import path_Students
import pandas as pd
import numpy as np


def extract_genes_from_clusters():
    """Get the genes from the clusters."""
    message = '   #   ' + string_time() + '   #   '
    message += 'Starting to get the genes'
    print(message)
    # create the list of clusters to analyse
    clusters = range(0, 50, 1)

    # gene wanted
    list_wanted = []

    # play with all clusters one by one
    for c in clusters:
        path_f = path_Output + 'Cluster_' + str(c) + '.csv'

        # Open the DataFrame
        pd_lcl = pd.read_csv(path_f)
        l_pd = pd_lcl.shape[0]
        if l_pd >= 10:
            n_to_keep = 10
        else:
            n_to_keep = l_pd

        # get the n_to_keep genes
        lcl_genes = pd_lcl['gene_id'].values[:(n_to_keep)]

        for i in range(0, len(lcl_genes), 1):
            lcl_genes[i] = lcl_genes[i][:18]

        # update the final list of genes
        list_wanted = np.append(list_wanted,
                                lcl_genes)

    # convert the np array into aclassic python list
    list_wanted = list_wanted.tolist()
    message = '   #   ' + string_time() + '   #   '
    message += str(len(list_wanted)) + ' gene wanted'
    print(message)

    message = '   #   ' + string_time() + '   #   '
    message += str(len(np.unique(list_wanted).tolist())) + ' gene wanted'
    print(message)

    # create the output file
    message = '   #   ' + string_time() + '   #   '
    message += 'Starting to transfer'
    print(message)

    path_final_file = path_Students.replace('.csv',
                                            '_subset_david.csv')
    f_file = open(path_final_file, 'w+')

    # Open and manipulate the file for the students
    n_gene_found = 0
    found = []
    nlines = 0
    done = range(1000, 10000, 1000)
    with open(path_Students) as database:
        # Read the firstline
        line = database.readline()

        # Read all lines
        while line:
            nlines += 1

            # Decide if line belongs to header or describe a value
            if nlines == 1:
                f_file.write(line)
            else:
                # check if the line correspond to a wanted gene
                blocks = line.split(',')
                if len(blocks) > 1:
                    gene = blocks[1].replace('"', '')[:18]
                    if gene in list_wanted:
                        n_gene_found += 1
                        if gene not in found:
                            found.append(gene)
                        f_file.write(line)
                        if n_gene_found in done:
                            message = '   #   ' + string_time() + '   #   '
                            message += str(n_gene_found)
                            message += ' lines with gene of interest'
                            print(message)
            line = database.readline()

    # close the output file
    f_file.close()
    message = '   #   ' + string_time() + '   #   '
    message += str(n_gene_found) + ' lines with gene of interest'
    print(message)

    message = '   #   ' + string_time() + '   #   '
    message += str(len(found)) + ' genes of interest found'
    print(message)

    message = '   #   ' + string_time() + '   #   '
    message += str(n_gene_found) + ' Preparing reindexing'
    print(message)

    wanted_db = pd.read_csv(path_final_file)
    del wanted_db['Unnamed: 0']

    # saving with the correct index
    wanted_db.to_csv(path_final_file.replace('.csv',
                                             '_reindexed.csv'),
                     index=True)

    message = '   #   ' + string_time() + '   #   '
    message += 'End of the process'
    print(message)


if __name__ == "__main__":
    # execute only if run as a script
    extract_genes_from_clusters()
