#!/usr/bin/python
# coding: utf-8
"""Check that Paths are OK and generate a log File."""
# Python libraries
import os
from utils.time_stamp import message_time, string_time
from utils.valid_setup import check_path
from config import project_Folder, path_Data, path_Output
from config import path_gff3, path_fa


def Initiate():
    """First check that output folder is correct."""
    message_time('Function: Initiate()')

    # 1. Creation of the time stamped log file.
    log_name = string_time() + '_CDS-Scanner-LOG.txt'
    log_file = open(path_Output + log_name, 'w+')
    log_file.write(message_time('Starting Pipeline\n\n'))
    log_file.write(message_time('Starting Initate.\n'))

    message_time('Checking paths')
    log_file.write(message_time('Checking paths\n'))
    # 2. Checking the key paths
    path_list = (project_Folder,
                 path_Data,
                 path_Output,
                 path_gff3,
                 path_fa)
    pb = check_path(path_list,
                    log_file)

    # If there is a problem in the setup
    if pb is True:
        print('   #   ' + 'Something is wrong in Setup.')
        print('   #   ' + 'Check log file for more details')
        print('   #   ' + path_Output + log_name)
        # Exiting the Program
        exit()

    # 3. Creating the specific .gff3 vs .fa output folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
    output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # If the specific output folder doesn't exist
    if os.path.exists(output_folder) is False:
        message = string_time() + ' Creating output folder.\n'
        os.mkdir(output_folder)
    else:
        message = message_time(' Using output folder.\n')
    message += output_folder + '\n\n'
    log_file.write(message)

    log_file.write(message_time('End Initate.\n'))
    return log_file


if __name__ == "__main__":
    # execute only if run as a script
    Initiate()
