#!/usr/bin/python
# coding: utf-8
"""Detect the third base of each codon."""
from utils.Object_manipulator import Manipulator
from utils.time_stamp import message_time
from config import path_gff3, path_fa, path_Output
import time
import os
import glob

def Get_C3G(log_file):
    """Analyse all transcript to establish proportion of 3rd position base."""
    message = message_time('Starting Get_C3G.\n')
    print(message)
    log_file.write(message)

    # Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    progress = range(1000, 100000, 1000)
    nFiles = 0
    for csv_file in glob.glob(input_folder + '*.csv'):
        file = os.path.basename(csv_file)
        if (file.startswith('ENSMUST')):
            # Load the content of the file
            f = Manipulator(csv_file)

            # Get the CDS
            cds = f._getValue(['CDS_seq'])[0]
            len_CDS = len(cds)
            score_A = 0
            score_T = 0
            score_C = 0
            score_G = 0
            for b in range(2, len_CDS -1 ,3):
                lcl_third = cds[b:b + 1] # substring with second index < b+1
                if (lcl_third == 'A'):
                    score_A += 1
                elif (lcl_third == 'T'):
                    score_T += 1
                elif (lcl_third == 'C'):
                    score_C += 1
                elif (lcl_third == 'G'):
                    score_G += 1
            score_A = round(100 * score_A / (len_CDS / 3), 2)
            score_T = round(100 * score_T / (len_CDS / 3), 2)
            score_C = round(100 * score_C / (len_CDS / 3), 2)
            score_G = round(100 * score_G / (len_CDS / 3), 2)

            Keys = ['perc_C3G_A',
                    'perc_C3G_T',
                    'perc_C3G_C',
                    'perc_C3G_G',
                    'perc_C3G_AT',
                    'perc_C3G_GC']
            New_Values = [str(score_A),
                          str(score_T),
                          str(score_C),
                          str(score_G),
                          str(score_A + score_T),
                          str(score_G + score_C)]
            for i in range(0, len(Keys), 1):
                f._appendValue(Keys[i], New_Values[i])

            # Update the file
            f._updateFile()
            nFiles += 1

            if nFiles in progress:
                message = message_time(str(nFiles) + '  transcripts analysed.\n')
                print(message)
                log_file.write(message)

    message = message_time('End Get_C3G.\n\n')
    print(message)
    log_file.write(message)
