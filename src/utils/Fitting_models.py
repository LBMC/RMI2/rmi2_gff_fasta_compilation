#!/usr/bin/python
# coding: utf-8
"""Fitting Models."""
from scipy.optimize import curve_fit, leastsq
from utils.time_stamp import message_time
from scipy.stats import linregress


class Model:
    """Optimize paramaters for Optimal fitting."""

    def __init__(self,
                 X,
                 Y,
                 mybounds,
                 Equation):
        """Generate the object."""
        self._equation = Equation  # Explicit equation to display in report.
        self._formulas = {}   # Explicit equation to display in report.
        self._Equations = {}  # Dictionnary of Functions.
        self._Equationsf = {}  # Dictionnary of Functions.
        self._params = {}  # Dictionnary of Parameters for each equation.
        self._bound = {}  # Dictionnary of limits for each equation.
        self._slope = 0   # slope of the linear regression
        self._intercept = 0   # intercept value for linear regression
        self._r2 = 0   # Corelation coefficient
        self._popt = []   # optimized paramters
        self._pcov = []   # The estimated covariance of popt
        self._optls = []   # Optimized parameters with least square
        self._Yfitted = []   # Predicted values

        # List of the equations

        # 0 Linear
        self._Equations['Linear'] = self._Linear
        self._Equationsf['Linear'] = self._fun_Linear
        self._params['Linear'] = ['A', 'B']
        self._bound['Linear'] = mybounds
        f0 = 'Y = A * X +  B'
        self._formulas['Linear'] = f0

        # 1 Square
        self._Equations['2nd_Order'] = self._2nd_Order
        self._Equationsf['2nd_Order'] = self._fun_2nd_Order
        self._params['2nd_Order'] = ['A', 'B', 'C']
        self._bound['2nd_Order'] = mybounds
        f1 = 'Y = A * X^2 +  B * X + C'
        self._formulas['2nd_Order'] = f1

        # 2 Cubic
        self._Equations['3rd_Order'] = self._3rd_Order
        self._Equationsf['3rd_Order'] = self._fun_3rd_Order
        self._params['3rd_Order'] = ['A', 'B', 'C', 'D']
        self._bound['3rd_Order'] = mybounds
        f2 = 'Y = A * X^3 +  B * X^2 + C * X + D'
        self._formulas['3rd_Order'] = f2

        # 3 Quadratic
        self._Equations['4th_Order'] = self._4th_Order
        self._Equationsf['4th_Order'] = self._fun_4th_Order
        self._params['4th_Order'] = ['A', 'B', 'C', 'D', 'E']
        self._bound['4h_Order'] = mybounds
        f3 = 'Y = A * X^4 +  B * X^3 + C * X^2 + D * X + E'
        self._formulas['4th_Order'] = f3

        # 1 Adjusted Hill-Langmuir
        # https://en.wikipedia.org/wiki/Hill_equation_(biochemistry)
        self._Equations['Adjusted_Hills-Langmuir'] = self._Adjusted_Hills
        self._Equationsf['Adjusted_Hills-Langmuir'] = self._fun_Adjusted_Hills
        self._params['Adjusted_Hills-Langmuir'] = ['A', 'K', 'B']
        self._bound['Adjusted_Hills-Langmuir'] = mybounds
        fl = 'Y = (A * X / ( K + X) ) - B'
        self._formulas['Adjusted_Hills-Langmuir'] = fl

        # Get parameters of the current equation.
        self._param = self._params[Equation]
        print(message_time('Launching Fitting.'))
        print(message_time(Equation))
        print(message_time(self._formulas[Equation]))

        for i in range(0, len(self._param), 1):
            txt = 'Parameter ' + self._param[i]
            txt += ', bounds (' + str(self._bound[Equation][0][i]) + ','
            txt += str(self._bound[Equation][1][i]) + ')'
            print(message_time(txt))

        # Launch fitting of the model
        self._Fit_Model(X, Y, Equation)

        # Calculated the fitted values
        self._Yfitted = self._Equations[(Equation)](X, *self._popt)

        # Calculate R2.
        self._CalculateR2(Y, self._Yfitted)

        # Optimize with
        self._Improve_Model(X, Y, Equation)

        # Calculated the fitted values
        self._Yfitted = self._Equations[(Equation)](X, *self._optls)

        # Calculate R2.
        self._CalculateR2(Y, self._Yfitted)

    def _Fit_Model(self, X, Y, Equation):
        """Fit the model for the specified equation."""
        # Get first guess.
        print('')
        print(message_time('First guess using curvefit.'))
        self._popt, self._pcov = curve_fit(self._Equations[Equation],
                                           X,
                                           Y,
                                           bounds=self._bound[Equation])

        print(message_time('Fitting done.'))
        for i in range(0, len(self._param), 1):
            txt = 'Parameter ' + self._param[i]
            txt += '= ' + str(self._popt[i])
            print(message_time(txt))

    def _Improve_Model(self,
                       X,
                       Y,
                       Equation):
        """Improve the model."""
        print('')
        print(message_time('Optimize parameters using leastsq.'))
        self._optls, cov_x = leastsq(self._Equationsf[Equation],
                                     self._popt, args=(X, Y))
        print(message_time('Fitting done.'))
        for i in range(0, len(self._param), 1):
            txt = 'Parameter ' + self._param[i]
            txt += '= ' + str(self._optls[i])
            print(message_time(txt))

    def _CalculateR2(self, Y, Yfitted):
        """Calculate R2 coefficient of correlation."""
        (self._slope,
         self._intercept,
         r_value,
         p_value,
         std_err) = linregress(Y, Yfitted)
        self._r2 = r_value**2

        print(message_time('R2 = ' + str(self._r2)))
        print(message_time('Slope = ' + str(self._slope)))
        print(message_time('Intercept = ' + str(self._intercept)))

    # =========================================================================
    def _Linear(self, X, A, B):
        """Use Linear Model."""
        return A * X + B

    def _fun_Linear(self, guess, X, Y):
        """Use Linear Model."""
        return guess[0] * X + guess[1] - Y

    # =========================================================================
    def _2nd_Order(self, X, A, B, C):
        """Use 2nd order Model."""
        return A * (X * X) + B * X + C

    def _fun_2nd_Order(self, guess, X, Y):
        """Use 2nd order Model."""
        return guess[0] * (X * X) + guess[1] * X + guess[2] - Y

    # =========================================================================
    def _3rd_Order(self, X, A, B, C, D):
        """Use 3rd order Model."""
        return A * (X * X * X) + B * (X * X) + C * X + D

    def _fun_3r_Order(self, guess, X, Y):
        """Use 3rd order Model."""
        return (guess[0] * (X * X * X) +
                guess[1] * (X * X) +
                guess[2] * X + guess[3] - Y)

    # =========================================================================
    def _4th_Order(self, X, A, B, C, D, E):
        """Use 4th order Model."""
        return (A * (X * X * X * X) +
                B * (X * X * X) +
                C * X * X +
                D * X + E)

    def _fun_4th_Order(self, guess, X, Y):
        """Use 4th order Model."""
        return (guess[0] * (X * X * X * X) +
                guess[1] * (X * X * X) +
                guess[2] * (X * X) + guess[3] * X + guess[4] - Y)

    # =========================================================================
    def _Adjusted_Hills(self, X, A, K, B):
        """Use Adjusted Hills Model."""
        return (A * X / (K + X) - B)

    def _fun_Adjusted_Hills(self, guess, X, Y):
        """Use Adjusted Hills Model."""
        return ((guess[0] * X / (guess[1] + X) - guess[2]) - Y)
