#!/usr/bin/python
# coding: utf-8
"""Test the fitting models"""
from utils.Fitting_models import Model
import numpy as np


def Test_fit():
    """Test the models."""
    X = np.asarray([0, 1, 2, 3, 4])
    Y = np.asarray([1, 3, 5, 7, 9])

    ribo_model = Model(X, Y,
                       ([-10, -10], [10, 10]),
                       'Linear')


if __name__ == "__main__":
    # execute only if run as a script
    Test_fit()
