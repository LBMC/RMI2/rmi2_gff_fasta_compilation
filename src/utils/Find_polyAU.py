#!/usr/bin/python
# coding: utf-8
"""Find all polyAU motifs for the 3'UTR."""
import re
import os
import glob
from config import path_Output, path_gff3, path_fa
from utils.time_stamp import message_time
from utils.Object_manipulator import Manipulator

motifs = ['AUUUA',
          'UUAUUUAUU',
          '[AU][AU][AU]UAUUUAU[AU][AU][AU]']

segments_to_analyze = ['UTR5',
                       'CDS',
                       'UTR3']


def Get_polyAU_Transcripts(log_file):
    """Analyse all transcript to find polyAU motifs."""
    message = message_time('Starting Get_polyAU_Transcripts.\n')
    print(message)
    log_file.write(message)

    # Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    progress = range(1000, 100000, 1000)
    nFiles = 0
    for csv_file in glob.glob(input_folder + '*.csv'):

        # Load the content of the file
        f = Manipulator(csv_file)

        # Analyse the file
        where_polyAU(f)

        # Update the file
        f._updateFile()
        nFiles += 1

        if nFiles in progress:
            message = message_time(str(nFiles) + '  transcripts analysed.\n')
            print(message)
            log_file.write(message)

    message = message_time('End Get_polyAU_Transcripts.\n\n')
    print(message)
    log_file.write(message)


def where_polyAU(manipulator):
    """Use a manipulator object and get all occurances of polyAU motifs."""
    # Check for all motifs
    for motif in motifs:

        # Initiate the total score integrating all segments
        total_score = {}
        for mot in motifs:
            # replace U by T
            motT = mot.replace('U', 'T')
            total_score[mot] = 0

            # Search in all sequences
            for segment in segments_to_analyze:

                # Get the sequence
                seq = manipulator._getValue([segment + '_seq'])[0]

                # Analyse the sequence
                if motT == '[AT][AT][AT]TATTTAT[AT][AT][AT]':
                    list_mot = [m.start() for m in re.finditer(r'[AT][AT][AT]TATTTAT[AT][AT][AT]', seq)]
                else:
                    list_mot = [m.start() for m in re.finditer(motT, seq)]

                # The score is the number of iteration
                sc = len(list_mot)

                # Update total score
                total_score[mot] = total_score[mot] + sc

                # Add keyword to the file
                Key = 'N_' + mot + '_' + segment
                manipulator._appendValue(Key, str(sc))

            # Add total keyword to the file
            Key = 'Total_' + mot
            manipulator._appendValue(Key, str(total_score[mot]))
