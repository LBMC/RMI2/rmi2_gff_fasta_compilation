#!/usr/bin/python
# coding: utf-8
"""Create the display in matplotlib."""
# Python libraries
from __future__ import division
from utils.time_stamp import message_time, string_time
from utils.Object_shredder_simple import Shredder_simple
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from config import Wgo, Hgo
from pylab import rcParams
from config import plow
from config import max_draw_go
from config import path_Gene_GO, path_Output


def Display_GO(log_file,
               GO_Gold,   # Use the score file for now (all GOs)
               distribution_file,
               arborescence_file,
               score_file,
               Xaxis='Mean',
               Yaxis='nTDD',
               Size='nTDD',
               Zcolor='pValue_adjusted',
               showtext=True,
               pvalue='pValue_adjusted'):
    """Create the Display."""
    # Create the personnal colors
    message = message_time('Starting Display_GO.\n')
    print(message)
    log_file.write(message)

    message = message_time('Create colors.\n')
    print(message)
    log_file.write(message)
    CreateMyColors()

    name_cond = score_file.replace('_GO_adjusted_pValues.csv', '')
    name_pdf = name_cond + '_GO_Network.pdf'

    message = message_time('Creating pdf file ')
    message += name_pdf
    print(message)
    log_file.write(message)

    # Create the PdfPages
    pdf = PdfPages(name_pdf)

    # Initiate graph.
    message = message_time('Prepare graph.')
    print(message)
    log_file.write(message)

    rcParams['figure.figsize'] = Wgo, Hgo
    fig, ax1 = plt.subplots()

    # Read the distribution file
    message = message_time('Read distribution file.\n')
    print(message)
    log_file.write(message)
    X_Distrib, Y_Distrib = Read_Distrib(distribution_file)

    # Draw the distribution
    message = message_time('Draw the distribution.\n')
    print(message)
    log_file.write(message)

    ax1.fill_between(X_Distrib,  Y_Distrib, color=(0.75, 0.75, 0.75))
    ax1.plot(X_Distrib,
             Y_Distrib,
             color=(0, 0, 0))
    ax1.tick_params(axis='Distribution')

    # read the score file
    message = message_time('Prepare the score GO database.\n')
    print(message)
    log_file.write(message)

    db_score = Shredder_simple(score_file, ',')

    # instantiate a second axes that shares the same x-axis
    ax2 = ax1.twinx()

    # read the arborescence file and draw it

    Draw_Arborescence(log_file,
                      GO_Gold,
                      ax2,
                      path_Gene_GO,
                      db_score,
                      Xaxis,
                      Yaxis,
                      pvalue)

    Draw_GO_Score(log_file,
                  GO_Gold,
                  ax2,
                  db_score,
                  Xaxis,
                  Yaxis,
                  Size,
                  Zcolor,
                  showtext,
                  pvalue)

    # Make ax2 in log scale
    ax2.set_yscale("log", nonposy='clip')

    # Draw the distribution
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    # plt.xlim(-1, 0.25)
    message = message_time('Display the graph.\n')
    print(message)
    log_file.write(message)

    plt.savefig(name_cond + '_GO_Network.png',
                format='png',
                dpi=300)

    plt.savefig(pdf,
                format='pdf',
                dpi=300)

    plt.close()

    message = message_time('End Display_GO.\n\n')
    print(message)
    log_file.write(message)


def Draw_GO_Score(log_file,
                  Gold_GO,
                  fig_ax,
                  db,
                  Xaxis,
                  Yaxis,
                  S,
                  C,
                  text,
                  pvalue):
    """Draw the GO at their position with the p-value color."""
    message = message_time('Trying to draw Arborescence.\n')
    print(message)
    log_file.write(message)

    ref_tsv = path_Output + 'GO_to_Phrase.tsv'
    db_go = Shredder_simple(ref_tsv, '\t')

    # Open the gold GO list
    gold = Shredder_simple(Gold_GO, ',')

    for go in db._First_Col:
        # retrieve values
        X = float(db._dict_values[go, Xaxis])
        Y = float(db._dict_values[go, Yaxis])
        s = float(db._dict_values[go, S])
        p = float(db._dict_values[go, C])

        todo = True
        if p > plow or s > max_draw_go or go not in gold._First_Col:
            todo = False
            my_color = 'no_star'
        elif p <= 0.05 and p > 0.01:
            my_color = 'one_star'
        elif p <= 0.01 and p > 0.001:
            my_color = 'two_star'
        elif p <= 0.001:
            my_color = 'three_star'
        else:
            my_color = 'green'

        if todo:
            # get name from GO_to_Phrase
            name = db_go._dict_values[go, 'Name']

            # print(go, X, Y, my_color)
            fig_ax.plot(X,
                        Y,
                        marker='o',
                        markersize=(2*np.sqrt(s)) + 10,
                        markerfacecolor=my_color,
                        markeredgewidth=0.5,
                        markeredgecolor=(0, 0, 0, 1))
            if text:
                fig_ax.text(X,
                            Y,
                            name,
                            fontsize=12,
                            # color=my_color,
                            verticalalignment='center',
                            horizontalalignment='center')


def Draw_Arborescence(log_file,
                      Gold_GO,
                      fig_ax,
                      arborescence_file,
                      GO_score_db,
                      X,
                      Y,
                      pvalue):
    """Overlay the arborescence of the GO."""
    message = message_time('Starting Draw_Arborescence.\n')
    print(message)
    log_file.write(message)
    # Open the gold GO list
    gold = Shredder_simple(Gold_GO, ',')

    links = Shredder_simple(path_Gene_GO, '\t')

    n_links = 0
    n_gene = 0
    # explore the path_Gene_GO file
    gene_done = range(100, 100000, 100)
    list_gene = links._First_Col.tolist()
    for gene in list_gene:
        GOs = links._dict_values[gene, 'GO_Term'].split(';')
        local_gold = []
        # Determine the GOs of the arborescence of one gene that are gold
        for g in GOs:
            if g in gold._First_Col:
                if float(GO_score_db._dict_values[g, 'nTDD']) < max_draw_go:
                    local_gold.append(g)
        if len(local_gold) >= 2:
            # print(len(local_gold))
            for i in range(0, len(local_gold) - 1, 1):
                Xs = []
                Ys = []
                Ps = []
                Gs = []
                # First point
                n = local_gold[i]
                Xs.append(float(GO_score_db._dict_values[n, X]))
                Ys.append(float(GO_score_db._dict_values[n, Y]))
                Ps.append(float(GO_score_db._dict_values[n, pvalue]))
                Gs.append(float(GO_score_db._dict_values[n,
                                                         'nTDD']))
                # Second point
                n = local_gold[i + 1]
                Xs.append(float(GO_score_db._dict_values[n, X]))
                Ys.append(float(GO_score_db._dict_values[n, Y]))
                Ps.append(float(GO_score_db._dict_values[n, pvalue]))
                Gs.append(float(GO_score_db._dict_values[n,
                                                         'nTDD']))

                fig_ax.plot(Xs,
                            Ys,
                            color=(0.3, 0.3, 0.3),
                            linewidth=1)
                n_links += 1
        n_gene += 1
        if n_gene in gene_done:
            message = message_time(str(n_gene) + ' go processed.\n')
            print(message)
            log_file.write(message)

    message = message_time(str(n_gene) + ' go processed.\n')
    print(message)
    log_file.write(message)

    message = message_time(str(n_links) + ' Connection drawn.\n')
    print(message)
    log_file.write(message)

    message = message_time('End Draw_Arborescence.\n\n')
    print(message)
    log_file.write(message)


def CreateMyColors():
    """Create the custom color code."""
    # Colors:
    matplotlib.colors.ColorConverter.colors['no_star'] = (0.5,
                                                          0.5,
                                                          0.5,
                                                          0.5)
    matplotlib.colors.ColorConverter.colors['one_star'] = (0.75,
                                                           0.75,
                                                           0,
                                                           0.5)
    matplotlib.colors.ColorConverter.colors['two_star'] = (0.00,
                                                           0.75,
                                                           0.75,
                                                           0.5)
    matplotlib.colors.ColorConverter.colors['three_star'] = (0.00,
                                                             0.75,
                                                             0.00,
                                                             0.5)


def Read_Distrib(path):
    """Read the Distribution file."""
    # line counter
    nlines = 0
    with open(path) as database:
        # Read the firstline
        line = database.readline()

        # Read all lines
        while line:
            nlines += 1
            line = clean_line(line)
            # Decide if line belongs to header or describe a value
            if nlines == 1:
                # Got X values
                X_distrib = np.array(line.split(','), dtype=float)

            elif nlines == 2:
                # Got Y values
                Y_distrib = np.array(line.split(','), dtype=float)
            else:
                # Quit the loop
                break
            line = database.readline()

    message = message_time(str(nlines) + ' lines processed')
    print(message)
    return X_distrib, Y_distrib


def clean_line(line):
    """Remove unwanted characters from line."""
    unwanted = ['"', '\n']
    for char in unwanted:
        line = line.replace(char, '')
    return line


if __name__ == "__main__":
    # execute only if run as a script
    Display_GO()
