#!/usr/bin/python
# coding: utf-8
"""Generate Tree."""
# Python libraries
import pandas as pd
import numpy as np
import re
from utils.time_stamp import message_time
from config import path_Arborescence
from config import path_Gene_GO
from config import path_GO_Tree


def Radagast(log_file):
    """Create a tree from a list of Child Parent relationships."""
    message = message_time('Starting Radagast.\n')
    print(message)
    log_file.write(message)

    # Open the csv as dataframe and clean it
    raw_df = pd.read_csv(path_Arborescence)
    raw_df = clean_df(raw_df)

    # Get the edges from the dataframe
    edges = get_edges(raw_df, log_file)

    # Get the parent and childre database.
    parent_of, children_of, parents = get_family(edges, log_file)

    # Get the root and GO that are only parents bu not root
    root, leaves, scores = get_root(children_of, parent_of, log_file)

    # Construct the main tree
    (princ_tree,
     children_of,
     total_found,
     leaves,
     scores) = sylvebarde(root, leaves, scores, children_of, 1, log_file)

    # Fusion of all the trees
    Fangorn = entmoot(princ_tree,
                      leaves,
                      scores,
                      children_of,
                      total_found,
                      log_file)

    # Perform QC on final tree
    Isengard(Fangorn, log_file)

    message = message_time('End Radagast.\n\n')
    print(message)
    log_file.write(message)


def Isengard(all_trees_fused, log_file):
    """Perform the QC on the forest and create the tree file."""
    message = message_time('Starting Isengard.\n')
    print(message)
    log_file.write(message)

    total_node = np.array([])
    for key in all_trees_fused.keys():
        GOs = all_trees_fused[key]

        message = message_time('Level ' + str(key))
        message += ' contains ' + str(len(GOs)) + '.\n'
        print(message)
        log_file.write(message)

        total_node = np.append(total_node, GOs)
    n_nodes = len(total_node.tolist())
    n_unique = len(np.unique(total_node).tolist())
    redundance = n_nodes - n_unique

    message = message_time('Total nodes found: ' + str(n_nodes) + '.\n')
    print(message)
    log_file.write(message)

    message = message_time('Redundant entries: ' + str(redundance) + '.\n')
    print(message)
    log_file.write(message)

    if redundance == 0:
        message = message_time('Creating Tree file.\n')
        print(message)
        log_file.write(message)

        open(path_GO_Tree, 'w+').close
        csv = open(path_GO_Tree, 'w+')

        for key in all_trees_fused.keys():
            GOs = all_trees_fused[key]
            line = ''
            for i in range(0, len(GOs) - 1, 1):
                line += GOs[i] + ','
            line += GOs[len(GOs) - 1] + '\n'
            csv.write(line)
        csv.close()

        message = message_time('Tree file Created.\n')
        print(message)
        log_file.write(message)

    message = message_time('End Isengard.\n')
    print(message)
    log_file.write(message)


def entmoot(princ_tree,
            leaves,
            scores,
            children_of,
            total_found,
            log_file):
    """Fuse all new trees to the main one."""
    message = message_time('Starting entmoot.\n')
    print(message)
    log_file.write(message)

    while len(leaves) > 0:
        maximum = max(scores)
        # Get the index of the max value and find the best root
        index = scores.index(maximum)
        root = leaves[index]

        # Remove root and its score from the leaves list
        leaves.remove(root)
        scores.remove(maximum)

        # Get the local secondary tree
        (sec_tree,
         children_of,
         total_found,
         leaves,
         scores) = sylvebarde(root, leaves, scores, children_of,
                              total_found, log_file)
        print(total_found)

        # fuse princ_tree and sec_tree
        princ_tree = graft(princ_tree, sec_tree, log_file)

    message = message_time('End entmoot.\n\n')
    print(message)
    log_file.write(message)

    return princ_tree


def graft(princ_tree, sec_tree, log_file):
    """Find the upper common hit and use it to align and merge the trees."""
    # Insure that all keys are coorectly sorted
    message = message_time('Starting graft.\n')
    print(message)
    log_file.write(message)

    princ_keys = princ_tree.keys()
    princ_keys.sort()
    sec_keys = sec_tree.keys()
    sec_keys.sort()

    # Get the upper common hit
    got_it = False
    common = ''
    lvl_in_princ = 0
    lvl_in_sec = 0
    tested = 0
    for lvl2 in sec_keys:
        print('Testing candidats in level '+str(lvl2))
        # For each level of the scondary tree we test if there exist a
        # common hit somewhere in the principal tree
        candidats = sec_tree[lvl2]
        for candidat in candidats:
            # We explore the principal tree
            tested += 1
            for lvl1 in princ_keys:
                if candidat in princ_tree[lvl1]:
                    got_it = True
                    common = candidat
                    lvl_in_princ = lvl1
                    lvl_in_sec = lvl2
                    break
            if got_it:
                break
        if got_it:
            break
    if got_it:
        message = message_time(common + ' is present in principal tree ')
        message += str(lvl_in_princ) + ' and in secondary tree '
        message += str(lvl_in_sec)
        print(message)
        log_file.write(message)

        if lvl1 != lvl2:
            princ_tree = transversal_graft(princ_tree,
                                           lvl_in_princ,
                                           sec_tree,
                                           lvl_in_sec,
                                           log_file)
        else:
            princ_tree = lateral_graft(princ_tree, sec_tree, log_file)

    else:
        message = message_time(str(tested) + ' nodes have been tested.\n')
        print(message)
        log_file.write(message)

        message = message_time('No connection found.\n')
        print(message)
        log_file.write(message)

        princ_tree = lateral_graft(princ_tree, sec_tree, log_file)

    message = message_time('End graft.\n\n')
    print(message)
    log_file.write(message)

    return princ_tree


def transversal_graft(princ_tree,
                      lvl1,
                      sec_tree,
                      lvl2,
                      log_file):
    """Merge trees that have a common node but a different levels."""
    message = message_time('Starting transversal graft.\n')
    print(message)
    log_file.write(message)

    princ_keys = princ_tree.keys()
    princ_keys.sort()
    sec_keys = sec_tree.keys()
    sec_keys.sort()

    # Determine the change in level
    delta = lvl1 - lvl2

    new_sec_keys = np.asarray(sec_keys) + delta
    new_sec_keys = new_sec_keys.tolist()

    # Create the translated 2nd tree
    new_sec_tree = {}
    for index in range(0, len(new_sec_keys), 1):
        new_sec_tree[new_sec_keys[index]] = sec_tree[sec_keys[index]]

    # Now that the second tree has been lifted we do the lateral graft.
    trans_tree = lateral_graft(princ_tree, new_sec_tree, log_file)

    message = message_time('EndStarting transversal graft.\n\n')
    print(message)
    log_file.write(message)

    return trans_tree


def lateral_graft(princ_tree, sec_tree, log_file):
    """Trees have no common points. Concatenate them laterally."""
    message = message_time('Starting lateral graft.\n')
    print(message)
    log_file.write(message)

    princ_keys = princ_tree.keys()
    princ_keys.sort()
    sec_keys = sec_tree.keys()
    sec_keys.sort()

    # Get the keys of the future tree
    max_index = max([max(princ_keys), max(sec_keys)])
    min_index = min([min(princ_keys), min(sec_keys)])
    final_keys = range(min_index,
                       max_index + 1,   # to have max index included
                       1)

    final_tree = {}
    global_listing = np.array([])
    # now concatenation
    for k in final_keys:
        # Check that the key exists in each tree
        if k in princ_keys:
            list1 = princ_tree[k]
        else:
            list1 = []

        if k in sec_keys:
            list2 = sec_tree[k]
        else:
            list2 = []

        # Track all found node to finnaly check if they are unique
        new_list = np.append(list1, list2)
        new_list = np.unique(new_list).tolist()
        global_listing = np.append(global_listing, new_list).tolist()
        final_tree[k] = new_list

    # QC
    unique_listing = np.unique(global_listing).tolist()

    message = message_time('Total merged nodes: ' + str(len(global_listing)))
    message += '.\n'
    print(message)
    log_file.write(message)

    message = message_time('Unique nodes: ' + str(len(unique_listing)))
    message += '.\n'
    print(message)
    log_file.write(message)

    message = message_time('End transversal graft.\n\n')
    print(message)
    log_file.write(message)

    return final_tree


def sylvebarde(root, leaves, scores, children_of, total_found, log_file):
    """Construct tree from the top GO (root) toward the genes."""
    message = message_time('Starting sylvebarde with' + root + '.\n')
    print(message)
    log_file.write(message)

    tree = {}   # key is the level from the root, value is the list
    level = 0
    found = np.array([])

    # Initiate tree with root.
    tree[level] = [root]
    found = np.append(found, root)

    # Generate the tree while there is parents
    while len(children_of.keys()) > 0:
        message = message_time('level ' + str(level))
        print(message)
        log_file.write(message)

        message = message_time('remaning leaves ' + str(len(leaves)))
        print(message)
        log_file.write(message)

        list_new_parents = tree[level]
        list_new_children = []

        # Get the children for each parent and create a single list
        for p in list_new_parents:
            if p in children_of.keys():
                # print(children_of[p])
                for c in children_of[p]:
                    # This loop avoid the problem to decide between flatten
                    # or not
                    # We always have a 1d array
                    if c not in found:
                        list_new_children.append(c)
                        found = np.append(found, c)
                del children_of[p]

        # If we found no new children we stop.
        if len(list_new_children) != 0:

            # Check if we can add a leaf.
            to_add = []
            for leaf in leaves:
                potential_hit = children_of[leaf]
                for pot in potential_hit:
                    if pot in list_new_children:
                        message = message_time('Found ' + pot)
                        message += ' as child of ' + leaf + '.\n'
                        print(message)
                        log_file.write(message)

                        to_add.append(leaf)

                        # Now add all the children (as one has been found)
                        for pot in potential_hit:
                            if pot not in found:
                                list_new_children.append(pot)
                                found = np.append(found, pot)

                        # Clean the dictionnaries
                        del children_of[leaf]
                        index = leaves.index(leaf)
                        leaves.remove(leaf)
                        del scores[index]
                        tree[level].append(leaf)
                        to_add.append(leaf)

                        # Exist the loop
                        break

            level += 1
            # Get unique entries
            list_new_children = np.unique(list_new_children).tolist()
            tree[level] = list_new_children

        else:
            message = message_time('No more children.\n')
            print(message)
            log_file.write(message)
            break
        total_found += len(list_new_children)

    message = message_time('Quality control.\n')
    print(message)
    log_file.write(message)

    message = message_time('Found ' + str(len(found.tolist())) + '.\n')
    print(message)
    log_file.write(message)

    message = message_time('Uniques ' + str(len(np.unique(found).tolist())))
    message += '.\n'
    print(message)
    log_file.write(message)

    message = message_time('Remaining parents ' + str(len(children_of.keys())))
    message += '.\n'
    print(message)
    log_file.write(message)

    message = message_time('End sylvebarde.\n\n')
    print(message)
    log_file.write(message)

    return (tree, children_of, total_found, leaves, scores)


def get_root(children_of,
             parent_of,
             log_file):
    """Extract the 'only parents' category and then identify the best one."""
    message = message_time('Starting get_root.\n')
    print(message)
    log_file.write(message)

    # Get root
    leaves = []
    for v in children_of.viewkeys():
        if v not in parent_of:
            leaves.append(v)

    # The best one as the one with the most hits in the association.tsv file.
    myfile = open(path_Gene_GO, 'r')
    data = myfile.read()

    # Get the number of appearence of each leaf and find the max
    scores = []
    for candidat in leaves:
        list_appearance = [m.start() for m in re.finditer(candidat, data)]
        scores.append(len(list_appearance))
    maximum = max(scores)

    # Get the index of the max value and find the best root
    index = scores.index(maximum)
    root = leaves[index]

    # Remove root from the leaves list
    leaves.remove(root)
    scores.remove(maximum)

    message = message_time('End get_root.\n\n')
    print(message)
    log_file.write(message)

    return root, leaves, scores


def get_family(edges, log_file):
    """Get the parent and children database."""
    message = message_time('Starting get_family.\n')
    print(message)
    log_file.write(message)

    parent_of = {}   # key is a node. value is its parent
    children_of = {}   # key is a node. value is its children as list
    parents = []
    done = range(1000, 100000, 1000)
    ed_done = 0
    # Analyse all edges
    for edge in edges:
        # Extract child and parent
        c, p = edge[0], edge[1]
        parent_of[c] = p
        if p in parents:
            children_of[p].append(c)
        else:
            parents.append(p)
            children_of[p] = [c]
        ed_done += 1
        if ed_done in done:
            print(str(ed_done) + ' edges analyzed.')
    print(str(ed_done) + ' edges analyzed.')

    message = message_time('End get_family.\n\n')
    print(message)
    log_file.write(message)

    return parent_of, children_of, parents


def get_edges(dataframe, log_file):
    """Extract [Child, Parent] couples list from dataframe."""
    message = message_time('Starting get_edges.\n')
    print(message)
    log_file.write(message)

    edges = []
    for i in range(0, dataframe.shape[0], 1):
        local_edge = dataframe[['Child', 'Parent']].values[i].tolist()
        edges.append(local_edge)

    message = message_time('End get_edges.\n\n')
    print(message)
    log_file.write(message)

    return edges


def clean_df(raw_df):
    """Remove lines with unwanted values."""
    raw_df = raw_df.dropna()
    raw_df = raw_df[raw_df.Child != '']
    raw_df = raw_df[raw_df.Parent != '']
    return raw_df


if __name__ == "__main__":
    # execute only if run as a script
    Radagast()
