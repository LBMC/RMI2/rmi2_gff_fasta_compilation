#!/usr/bin/python
# coding: utf-8
"""Create a heatmap graph from a csv using pandas."""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from config import path_Global
from sklearn.cluster import AgglomerativeClustering
from matplotlib.backends.backend_pdf import PdfPages
from config import path_Output
from matplotlib.pyplot import figure


colormap = 'seismic'
ncluster = 500

namepdf = 'Lympho_Activated_Normalized_Stability'

wanted = ['Norm_Reads_Lympho_Activated_Trip_1h',
          'Norm_Reads_Lympho_Activated_DRB_1h',
          'Norm_Reads_Lympho_Activated_Trip_3h',
          'Norm_Reads_Lympho_Activated_DRB_3h',
          'Norm_Reads_Lympho_Activated_TripCHX_1h',
          'Norm_Reads_Lympho_Activated_TripHarr_1h',
          'Norm_Reads_Lympho_Activated_DRBCHX_1h',
          'Norm_Reads_Lympho_Activated_DRBHarr_1h',
          'Norm_Reads_Lympho_Activated_TripCHX_3h',
          'Norm_Reads_Lympho_Activated_TripHarr_3h',
          'Norm_Reads_Lympho_Activated_DRBCHX_3h',
          'Norm_Reads_Lympho_Activated_DRBHarr_3h'
          ]

new_name = ['Trip_1h',
            'DRB_1h',
            'Trip_3h',
            'DRB_3h',
            'TripCHX_1h',
            'TripHarr_1h',
            'DRBCHX_1h',
            'DRBHarr_1h',
            'TripCHX_3h',
            'TripHarr_3h',
            'DRBCHX_3h',
            'DRBHarr_3h']

to_display = ['TripCHX_1h',
              'TripHarr_1h',
              'DRBCHX_1h',
              'DRBHarr_1h',
              'TripCHX_3h',
              'TripHarr_3h',
              'DRBCHX_3h',
              'DRBHarr_3h']

to_display2 = ['TripCHX_1h',
               'TripHarr_1h',
               ' ',
               'DRBCHX_1h',
               'DRBHarr_1h',
               ' ',
               'TripCHX_3h',
               'TripHarr_3h',
               ' ',
               'DRBCHX_3h',
               'DRBHarr_3h']

ref_col = ['TripCHX_1h', 'DRBCHX_1h']


def Bars_Clustering2():
    """Open the csv and order the desired columns as function of CV."""
    # The coefficient of variation (CV), defined as Standard deviation (SD)
    # divided by the Mean describes the variability of a sample relative to
    # its mean. Because the CV is unitless and usually expressed as a
    # percentage, it is used instead of the SD to compare the spread of data
    # sets that have different units of measurements or have the same units of
    # measurements but differs greatly in magnitude.
    # Open the csv
    mypanda = pd.read_csv(path_Global)
    figure(num=None,
           figsize=(8, 6),
           dpi=3000,
           facecolor='w',
           edgecolor='k')

    # create recipient Pdf
    pdf = PdfPages(path_Output + namepdf + '.pdf')

    # Select only wanted columns
    mypanda = mypanda[wanted]

    # Drop NaNs and zeros
    mypanda = mypanda.replace(0, np.nan)
    mypanda = mypanda.dropna()

    # Rename the columns
    for i in range(0, len(new_name), 1):
        mypanda = mypanda.rename(columns={wanted[i]: new_name[i]})

    # Caculate fold change and remove outliers values
    mypanda[' '] = mypanda[ref_col].mean()
    mypanda['TripCHX_1h'] = (mypanda['TripCHX_1h'] / mypanda['Trip_1h'])
    mypanda['TripCHX_3h'] = (mypanda['TripCHX_3h'] / mypanda['Trip_3h'])
    mypanda['TripHarr_1h'] = (mypanda['TripHarr_1h'] / mypanda['Trip_1h'])
    mypanda['TripHarr_3h'] = (mypanda['TripHarr_3h'] / mypanda['Trip_3h'])

    mypanda['DRBCHX_1h'] = (mypanda['DRBCHX_1h'] / mypanda['DRB_1h'])
    mypanda['DRBCHX_3h'] = (mypanda['DRBCHX_3h'] / mypanda['DRB_3h'])
    mypanda['DRBHarr_1h'] = (mypanda['DRBHarr_1h'] / mypanda['DRB_1h'])
    mypanda['DRBHarr_3h'] = (mypanda['DRBHarr_3h'] / mypanda['DRB_3h'])

    for i in range(0, len(to_display2), 1):
        mypanda[to_display2[i]].values[mypanda[to_display2[i]] > 5] = 5

    # Convert into an 2D array for drawing
    data = mypanda[to_display].values

    # create clusters
    hc = AgglomerativeClustering(n_clusters=ncluster,
                                 affinity='euclidean',
                                 linkage='ward')
    # save clusters for chart
    y_hc = hc.fit_predict(data)

    # reconstruct the array with all clusters
    df_clusters = pd.DataFrame(columns=to_display2)

    for c in range(0, len(to_display), 1):
        # reconstruct each column
        col = []
        for cl in range(0, ncluster, 1):
            col = np.append(col,
                            data[y_hc == cl, c].tolist(),
                            axis=0)
        df_clusters[to_display[c]] = col
    df_clusters[' '] = df_clusters[ref_col].mean()
    df_clusters['rank'] = (df_clusters[ref_col[0]] +
                           df_clusters[ref_col[1]])/2
    df_clusters = df_clusters.sort_values(by='rank', ascending=False)
    clusters2 = df_clusters[to_display2].values
    print(to_display2)
    # Draw the plot
    fig, ax = plt.subplots()
    ax.figure.set_size_inches(10, 50)
    im, cbar = heatmap(clusters2, to_display2, ax=ax,
                       cmap=colormap, cbarlabel='Fold change')
    cbar.set_clim(0, 2.0)
    plt.savefig(pdf,
                format='pdf',
                dpi=300)

    pdf.close()


def heatmap(data, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """Create a heatmap from a numpy array and two lists of labels."""
    """
    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, aspect='auto', **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


if __name__ == "__main__":
    # execute only if run as a script
    Bars_Clustering2()
