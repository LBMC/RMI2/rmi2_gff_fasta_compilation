#!/usr/bin/python
# coding: utf-8
"""Validate the quality of the transcript object."""
# Python libraries

# Home made scripts and packages
from utils.time_stamp import string_time


def QC(t   # Transcript Object
       ):
    """Check several parameters."""
    # Initialize the report
    QC_report = ''

    # Is the protein sequence OK?
    QC_report += QC_prot(t)

    # Is the transcript sequence OK?
    QC_report += QC_rna(t)

    # Update the header
    t._header = QC_report + t._header


def QC_rna(t):
    """Check RNA sequence."""
    initiation_codons = ['ATG', 'CTG']
    stop_codons = ['TAA', 'TAG', 'TGA']
    report = ''
    # Verification taht the sequence is fully known
    if 'N' in t._trans_seq:
        report += '# ' + string_time() + ','
        report += 'Transcript sequence is partial.\n'
        t._pb += 'Transcript sequence is partial.\n'
        t._Ns = '1'

    # Verification CDS codes for more than 100AA
    if int(t._CDS_le) < 200:
        report += '# ' + string_time() + ','
        report += 'CDS too short ' + t._CDS_le + '.\n'
        t._pb += 'CDS too short ' + t._CDS_le + '.\n'
        t._tooShort = str(t._CDS_le)

    # Verification Kozack sequence was found
    if t._Kozack_seq == '-':
        t._WrongKozack = 'Exact Kozack sequence not found.'

    # Validation of the initiation codon
    if t._CDS_seq[:3] not in initiation_codons:
        report += '# ' + string_time() + ','
        report += 'Wrong initiation codon ' + t._CDS_seq[:3] + '.\n'
        t._pb += 'Wrong initiation codon ' + t._CDS_seq[:3] + '.\n'
        t._noATG = '1'

    # Verification presence stop codon
    if t._CDS_seq[-3:] not in stop_codons:
        report += '# ' + string_time() + ','
        report += 'Wrong stop codon ' + t._CDS_seq[-3:] + '.\n'
        t._pb += 'Wrong stop codon ' + t._CDS_seq[-3:] + '.\n'
        t._noStop = '1'

    # Verification presence of 5'UTR
    if t._5UTR_seq == '':
        report += '# ' + string_time() + ','
        report += 'No 5 prime UTR in the database.\n'
        t._pb += 'No 5 prime UTR in the database.\n'
        t._noReported5UTR = '1'

    # Verification presence of 3'UTR
    if t._3UTR_seq == '':
        report += '# ' + string_time() + ','
        report += 'No 3 prime UTR in the database.\n'
        t._pb += 'No 3 prime UTR in the database.\n'
        t._noReported3UTR = '1'

    # Verification of donor and acceptor sites footprints
    n_intron = 0
    for intron_seq in t._Intron_seq:
        n_intron += 1

        if intron_seq[:2] != 'GT':
            message = 'Intron ' + str(n_intron) + ' ' + intron_seq[:2]
            if t._Wrong_Donor_Site == 'NA':
                t._Wrong_Donor_Site = message
            else:
                t._Wrong_Donor_Site += ';' + message


        if intron_seq[-2:] != 'AG':
            message = 'Intron ' + str(n_intron) + ' ' + intron_seq[-2:]
            if t._Wrong_Acceptor_Site == 'NA':
                t._Wrong_Acceptor_Site = message
            else:
                t._Wrong_Acceptor_Site += ';' + message

    return report


def QC_prot(t):
    """Check Protein sequence."""
    list_init = ['M', 'L']
    list_stop = ['*']
    report = ''

    # Initiation
    init = t._Prot_seq[0]
    if init not in list_init:
        report += '# ' + string_time() + ','
        report += 'Wrong AA initiator ' + init + '.\n'
        t._pb += 'Wrong AA initiator ' + init + '.\n'
        t._Wrong_Initiator_AA = '1'

    # Termination
    stop = t._Prot_seq[-1:]
    if stop not in list_stop:
        report += '# ' + string_time() + ','
        report += 'Wrong terminator ' + stop + '.\n'
        t._pb += 'Wrong terminator ' + stop + '.\n'
        t._Wrong_Terminator_AA = '1'

    # Composition
    if '-' in t._Prot_seq:
        report += '# ' + string_time() + ','
        report += 'Protein Sequence incomplete.\n'
        t._pb += 'Protein Sequence incomplete.\n'
        t._Prot_seq_Incomplete = '1'
    if '*' in t._Prot_seq[:-1]:
        report += '# ' + string_time() + ','
        report += 'In frame stop codon.\n'
        t._pb += 'In frame stop codon.\n'
        t._inFrame_stop = '1'

    return report
