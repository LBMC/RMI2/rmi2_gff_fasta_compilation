#!/bin/bash
set -e
set -u
set -o pipefail

# funtions
usage(){
  echo -e "usage : rnafolf.sh -i FILE -o OUTPUT [-j N_THREAD]\n"
  echo "-i : input file"
  echo "     FILE must be a fasta or multi-fasta file"
  echo "-o : output file"
  echo "-j : Split batch input into jobs and start processing in parallel"
  echo "     using multiple threads. A value of 0 indicates to use as many"
  echo "     parallel threads as computation cores are available.number of"
  echo "     thread for processing in parallel using multiple threads"
  echo "     default : 0"
  exit 1
}

# init variables
inputFile=""
outputFile=""
n_thread=0

while getopts 'i:o:j:h' OPTION; do
  case "$OPTION" in
    i)
      inputFile="$OPTARG"
      echo "   #                         #   Input file : $OPTARG"
      ;;

    o)
      outputFile="$OPTARG"
      echo "   #                         #   Output file : $OPTARG"
      ;;

    j)
      n_thread="$OPTARG"
      echo "   #                         #   number of thread : $OPTARG"
      ;;
    h)
      usage
      exit 0
      ;;
    ?)
      usage
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

if [ -z "$inputFile" ]
  then
    echo -e "   #   ERROR : No input file in argument\n" >&2
    usage
fi

if [ ! -f $inputFile ]
  then
    echo -e "   #   ERROR : file not found !\n" >&2
    usage
fi

if [ -z "$outputFile" ]
  then
    echo -e "   #   ERROR : No output file in argument\n" >&2
    usage
fi

if [ -f $outputFile ]
  then
    echo -e "   #   ERROR : output file already exists!\nAborting...\n" >&2
    usage
fi

# remove sequence longer than 10000 because RNAfold can not analyze it
echo 'filtering sequences longer than 10000nt...'
awk '{if($1 ~ /^>/) {
        header = $1
      } if ($1 ~ /^[ATGC]/ && length($1) < 10000) {
              print header,"\n",$1
          }
      }' $inputFile |
  sed 's/\s//g' > tmp_less10000nt.fa
echo 'done'
# the sed command serve to get one line with the transcript id and the delta G only
RNAfold --gquad --noPS --jobs=$n_thread tmp_less10000nt.fa|
  awk '{if ($1 ~ /^>/) {print $1} else {if ($3) {print $2$3} else {print $2}}}' |
  sed '$!N;s/\n//;$!N;s/\n//;' > $outputFile
#rm tmp folder
rm tmp_less10000nt.fa

exit 0
