#!/usr/bin/python
# coding: utf-8
"""Study the complexes of PPI as function of indexes TDD."""
# Python libraries
from __future__ import division
import os
import numpy as np
import pandas as pd
from config import path_Output, path_UNIPROT_to_ENSEMBL
from config import path_gff3, path_fa
from utils.time_stamp import message_time
from utils.Object_manipulator import Manipulator
from utils.Distributions_GO import clean_name
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


def distribution_complexes(log_file,
                           complex_db,
                           index):
    """Study all complexes for a specific index."""
    message = message_time('Starting distribution_complexes.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Get the dictionary converting an uniprot to index value
    index_of = from_UNIPROT_totranscript(index,
                                         input_folder,
                                         log_file)

    # Open the complex database
    pd_complexes = pd.read_csv(complex_db)

    # Extract only the wanted columns
    wanted_columns = ['ComplexName',
                      'subunits(UniProt IDs)',
                      'GO ID']
    pd_complexes = pd_complexes[wanted_columns]

    # Calculate the mean index
    pd_complexes = calculate_complex_index(pd_complexes,
                                           index_of,
                                           log_file)
    # Save the database
    clean_cond = clean_name(index)
    path = path_Output + 'Complexes_' + clean_cond + '.csv'
    pd_complexes.to_csv(path, index=False)

    # Display the database
    display_database(pd_complexes,
                     clean_cond)

    message = message_time('End distribution_complexes.\n\n')
    print(message)
    log_file.write(message)


def display_database(pd_complexes,
                     clean_cond):
    """Display the complexes mean vs std"""
    # Create pdf
    path_pdf = path_Output + 'Complexes_' + clean_cond + '.pdf'
    pdf = PdfPages(path_pdf)

    # clean the database
    pd_complexes.dropna()
    filter = pd_complexes['with_index_n_subunits'] >= 4
    pd_complexes = pd_complexes[filter]

    complexes = pd_complexes['ComplexName'].values

    fig = plt.figure(figsize=(8, 8), tight_layout=True)
    ax = fig.add_subplot(111)
    ax.plot([0, 1], [0, 1], color='r')

    for complex in complexes:
        filter1 = pd_complexes['ComplexName'] == complex
        local = pd_complexes[filter1]

        X = local['mean'].values[0]
        Y = local['std'].values[0]
        S = local['with_index_n_subunits'].values[0] * 3

        ax.scatter(X, Y, color='k', s=S, alpha=0.5)

    ax.set_xlim(xmin=0, xmax=0.5)
    ax.set_ylim(ymin=0, ymax=0.5)
    ax.set_xlabel('mean')
    ax.set_ylabel('std')
    ax.set_title(clean_cond)

    plt.savefig(pdf, format='pdf', dpi=300)
    plt.close('all')
    pdf.close()


def calculate_complex_index(pd_complexes,
                            index_of,
                            log_file):
    """Calculate the mean index and STD for all complexes."""
    message = message_time('Starting calculate_complex_index.\n')
    print(message)
    log_file.write(message)

    Means = []
    STDs = []
    n_theo = []
    n_found = []

    # get all complexes names
    complexes = pd_complexes['ComplexName'].values

    # get all complexes component and their values
    print(np.nan)
    for complex in complexes:
        filter = pd_complexes['ComplexName'] == complex
        sub_units = pd_complexes['subunits(UniProt IDs)'][filter].values
        sub_units = sub_units[0].split(';')
        loc_n_theo = len(sub_units)

        loc_values = []
        loc_n_found = 0
        for subunit in sub_units:
            if subunit in index_of.keys():
                sub_index = index_of[subunit]
                if sub_index != 'NA':
                    loc_n_found += 1
                    loc_values.append(sub_index)
                else:
                    loc_values.append(np.nan)
                    print(np.nan)

        if loc_n_found > 0:
            loc_mean = np.mean(loc_values)
            loc_std = np.std(loc_values)
        else:
            loc_mean = np.nan
            loc_std = np.nan

        Means.append(loc_mean)
        STDs.append(loc_std)
        n_theo.append(loc_n_theo)
        n_found.append(loc_n_found)

    # Add new columns to the dataframe
    pd_complexes['mean'] = Means
    pd_complexes['std'] = STDs
    pd_complexes['theorical_n_subunits'] = n_theo
    pd_complexes['with_index_n_subunits'] = n_found

    message = message_time('End calculate_complex_index.\n')
    print(message)
    log_file.write(message)

    return pd_complexes


def from_UNIPROT_totranscript(index,
                              input_folder,
                              log_file):
    """Create a dictionary of UNIPROT key with ENSEMBL transcript value."""
    message = message_time('Starting from_UNIPROT_totranscript.\n')
    print(message)
    log_file.write(message)

    dict = {}
    # Load the gene to transcript database
    pd_genes_trans = pd.read_csv(path_Output + 'Gene_to_Transcript.csv')
    list_gene_id = pd_genes_trans['gene_id'].values

    # Load the UNIPROT to ENSEMBL gene id
    pd_uniprot_gene = pd.read_csv(path_UNIPROT_to_ENSEMBL)
    list_uniprot_id = pd_uniprot_gene['From'].values

    # Feed dictionary
    for uniprot in list_uniprot_id:
        filter1 = pd_uniprot_gene['From'] == uniprot
        ensembl_gene = pd_uniprot_gene['To'][filter1].values[0]

        if ensembl_gene in list_gene_id:
            filter2 = pd_genes_trans['gene_id'] == ensembl_gene
            transcript_list = pd_genes_trans['transcript_id'][filter2].values
            # Get the first transcript file
            # They all have the indexes
            transcript = transcript_list[0].split(';')[0]
            f = Manipulator(input_folder + transcript + '.csv')
            value = f._getValue([index])[0]

            if value != 'NA':
                value = float(value)
                if value >= 1.5:
                    value = 1.5
            dict[uniprot] = value

        else:
            print(ensembl_gene + ' is missing.')

    message = message_time('Dicitionary contains ')
    message += str(len(dict.keys())) + ' enties.\n'
    print(message)
    log_file.write(message)

    message = message_time('End from_UNIPROT_totranscript.\n\n')
    print(message)
    log_file.write(message)

    return dict


if __name__ == "__main__":
    # execute only if run as a script
    distribution_complexes()
