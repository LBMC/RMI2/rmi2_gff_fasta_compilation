#!/usr/bin/python
# coding: utf-8
"""Calculate alternative 3'UTR from PASeq data."""
# Python libraries
from __future__ import division
import numpy as np

# Home made scripts and packages
from utils.time_stamp import string_time


def calculate_alt_UTR_length(manipulator):
    """Combine PASeq key and manipulator object's data."""
    # Get the orientation and number of UTR3 fragments and
    # experimental alternative ends
    # Quality control of the output was done with the transcript files:
    # ENSMUST00000130201.csv
    # ENSMUST00000045689.csv
    # ENSMUST00000081154.csv
    # ENSMUST00000185263.csv
    # ENSMUST00000190058.csv
    # ENSMUST00000146665.csv
    values = manipulator._getValue(['trans_strand',
                                    'UTR3_start',
                                    'UTR3_end',
                                    'UTR3_length',
                                    'n_UTR3_fragment',
                                    'n_Exp_UTR3_ends',
                                    'gene_name'])
    orientation = values[0]
    utr3_start = int(values[1])
    utr3_end = int(values[2])
    utr3_length = int(values[3])
    nUTR_frags = int(values[4])
    nExp_ends = int(values[5])
    gene_name = values[6]

    # Now we can get all UTR fragments informations
    theo_names = []
    theo_starts = []
    theo_ends = []
    theo_lengths = []
    for utrf in range(1, nUTR_frags + 1, 1):
        infos = ['UTR3_fragment_' + str(utrf) + '_id',
                 'UTR3_fragment_' + str(utrf) + '_start',
                 'UTR3_fragment_' + str(utrf) + '_end',
                 'UTR3_fragment_' + str(utrf) + '_len',
                 ]
        raw = manipulator._getValue(infos)
        theo_names = np.append(theo_names,
                               raw[0])
        theo_starts = np.append(theo_starts,
                                int(raw[1]))
        theo_ends = np.append(theo_ends,
                              int(raw[2]))
        theo_lengths = np.append(theo_lengths,
                                 int(raw[3]))

    # Do the same with the experimental ends from PASeq
    exp_names = []
    exp_starts = []
    exp_ends = []
    exp_lengths = []
    for new in range(1, nExp_ends + 1, 1):
        infos = ['Name_Exp_3End_' + str(new),
                 'Exp_UTR3_End_' + str(new) + '_Start',
                 'Exp_UTR3_End_' + str(new) + '_End'
                 ]
        raw2 = manipulator._getValue(infos)
        exp_names = np.append(exp_names,
                              raw2[0])
        exp_starts = np.append(exp_starts,
                               int(raw2[1]))
        exp_ends = np.append(exp_ends,
                             int(raw2[2]))

    # Now we calculate the new length depending on the orientation
    if orientation == '+':
        (exp_lengths,
         pk_status,
         pk_dis_st) = calculate_lengths_pos_strand(utr3_start,
                                                   utr3_end,
                                                   utr3_length,
                                                   nUTR_frags,
                                                   nExp_ends,
                                                   theo_names,
                                                   theo_starts,
                                                   theo_ends,
                                                   theo_lengths,
                                                   exp_names,
                                                   exp_starts,
                                                   exp_ends,
                                                   exp_lengths,
                                                   gene_name)
    elif orientation == '-':
        (exp_lengths,
         pk_status,
         pk_dis_st) = calculate_lengths_neg_strand(utr3_start,
                                                   utr3_end,
                                                   utr3_length,
                                                   nUTR_frags,
                                                   nExp_ends,
                                                   theo_names,
                                                   theo_starts,
                                                   theo_ends,
                                                   theo_lengths,
                                                   exp_names,
                                                   exp_starts,
                                                   exp_ends,
                                                   exp_lengths,
                                                   gene_name)
    else:
        message = '   #   ' + string_time() + '   #   '
        message += gene_name + ' Problem with the orientation'

    # Update the file
    for alt in range(1, nExp_ends+1, 1):
        newkey = 'Exp_UTR3_End_' + str(alt) + '_alt_Length'
        manipulator._appendValue(newkey,
                                 str(exp_lengths[alt - 1]))
        newkey = 'Exp_UTR3_End_' + str(alt) + '_Status'
        manipulator._appendValue(newkey,
                                 str(pk_status[alt - 1]))

    return manipulator, pk_status, pk_dis_st


def calculate_lengths_pos_strand(utr3_start,
                                 utr3_end,
                                 utr3_length,
                                 nUTR_frags,
                                 nExp_ends,
                                 theo_names,
                                 theo_starts,
                                 theo_ends,
                                 theo_lengths,
                                 exp_names,
                                 exp_starts,
                                 exp_ends,
                                 exp_lengths,
                                 gene_name):
    """Calculate the lengths in positive orientation."""
    status = []
    dist_from_stop = []
    for new in range(0, nExp_ends, 1):
        # Determine distance from stop
        dist = exp_ends[new] - (utr3_start - 3)
        dist_from_stop = np.append(dist_from_stop,
                                   dist)

        new_length = 0
        # Determine if the experimental end is in one of the exon
        in_exon = False

        for exon in range(0, nUTR_frags, 1):
            if (exp_ends[new] >= theo_starts[exon] and
                exp_ends[new] <= theo_ends[exon]):
                in_exon = True
                break

        # Determine if the new end is after the canonical one
        if exp_ends[new] >= utr3_end:
            # Brutaly calculate the increase in size
            new_length = (utr3_length +
                          exp_ends[new] - utr3_end)
            status = np.append(status,
                               'Downstream')

        # Determine if the new end is before the stop codon
        # the UTR length will be negative
        elif exp_ends[new] <= utr3_start:
            # Calculate the difference
            new_length = exp_ends[new] - utr3_start + 1
            status = np.append(status,
                               'Upstream')

        # Handle shorter UTR but in one exon context:
        elif (exp_ends[new] < utr3_end and
              in_exon):
            # Calculate the shrinking in size
            new_length = (utr3_length -
                          (utr3_end - exp_ends[new]))
            status = np.append(status,
                               'In UTR3 Exon')
        # Handle shorter UTR but in one intron context:
        elif (exp_ends[new] < utr3_end and
              exp_ends[new] > utr3_start and
              in_exon is False):
            # print(gene_name, ' positive intron')
            index = 0
            new_length = 0
            status = np.append(status,
                               'In UTR3 Intron')
            # Calculate the length until we arrive to the intron
            # The program starts in 5' -> 3'
            while exp_ends[new] > theo_ends[index]:
                new_length += theo_lengths[index]
                index += 1

            # Add the length from the end of the last exon to the end of the
            # new PASeq peak
            # We have to go back the the previous exon (as the current index
            # corresponds to an exon after the PASeq peak)
            new_length += exp_ends[new] - theo_ends[index - 1]

        # Update the list of lengths
        exp_lengths = np.append(exp_lengths,
                                new_length)
    # Give back all the lengths
    return (exp_lengths, status, dist_from_stop)


def calculate_lengths_neg_strand(utr3_start,
                                 utr3_end,
                                 utr3_length,
                                 nUTR_frags,
                                 nExp_ends,
                                 theo_names,
                                 theo_starts,
                                 theo_ends,
                                 theo_lengths,
                                 exp_names,
                                 exp_starts,
                                 exp_ends,
                                 exp_lengths,
                                 gene_name):
    """Calculate the lengths in negative orientation."""
    status = []
    dist_from_stop = []
    for new in range(0, nExp_ends, 1):
        # Determine distance from stop
        dist = utr3_start + 3 - exp_starts[new]
        dist_from_stop = np.append(dist_from_stop,
                                   dist)
        new_length = 0
        # Determine if the experimental end is in one of the exon
        in_exon = False
        for exon in range(0, nUTR_frags, 1):
            if (exp_starts[new] >= theo_starts[exon] and
                exp_starts[new] <= theo_ends[exon]):
                in_exon = True
                break

        # Determine if the new end is after the canonical one
        if exp_starts[new] <= utr3_start:
            # Brutaly calculate the difference
            new_length = (utr3_length +
                          (utr3_start - exp_starts[new]))
            status = np.append(status,
                               'Downstream')

        # Determine if the new end is before the stop codon
        # the UTR length will be negative
        elif exp_starts[new] >= utr3_end:
            # Calculate the difference
            new_length = utr3_end - exp_starts[new] + 1
            status = np.append(status,
                               'Upstream')
        # Handle shorter UTR but in one exon context:
        elif exp_starts[new] > utr3_start and in_exon:
            # Calculate the shrinking in size
            new_length = (utr3_length -
                          (exp_starts[new] - utr3_start))
            status = np.append(status,
                               'In UTR3 Exon')
        # Handle shorter UTR but in one intron context:
        elif (exp_starts[new] > utr3_start and
              exp_starts[new] < utr3_end and
              in_exon is False):
            # print(gene_name, ' negative intron')
            index = nUTR_frags - 1
            new_length = 0
            status = np.append(status,
                               'In UTR3 Intron')
            # Calculate the length until we arrive to the intron
            # The program starts in 3' -> 5'
            while exp_starts[new] < theo_starts[index]:
                new_length += theo_lengths[index]
                index -= 1

            # Add the length from the end of the last exon to the end of the
            # new PASeq peak
            # We have to go back the the previous exon (as the current index
            # corresponds to an exon after the PASeq peak)
            new_length += theo_starts[index + 1] - exp_starts[new]

        # Update the list of lengths
        exp_lengths = np.append(exp_lengths,
                                new_length)
    # Give back all the lengths
    return (exp_lengths, status, dist_from_stop)
