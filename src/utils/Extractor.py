#!/usr/bin/python
# coding: utf-8
"""Extract data from transcript.csv files and generate a bilan.csv."""
# Python libraries
import os
import glob

# Homemade utilities
from config import data_to_extract
from config import path_Output
from config import path_gff3, path_fa, path_Values
from utils.Object_manipulator import Manipulator
from utils.time_stamp import message_time, string_time
import pandas as pd
import numpy as np

List_at_least_One = ['Lympho_Activated>GOLD',
                     'Lympho_Resting>GOLD',
                     'Macro_Activated>GOLD',
                     'Macro_Resting>GOLD']
Expt_at_least_One = ['1', '1', '1', '1']


def Extract_Infos(log_file):
    """Perform the extraction."""
    message = message_time('Home version 13:31.\n')
    log_file.write(message)
    print(message)

    message = message_time('Starting Extract_Infos.\n')
    log_file.write(message)
    print(message)
    # print('Data to extract:')
    # print(data_to_extract)
    # 1. Checking the specific .gff3 vs .fa output folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # 2 Create the time stamped output file
    file_name = string_time() + '_Subset_Data.csv'
    subset_file = open(path_Output + file_name, 'w+')
    message = message_time('Output file created\n')
    print(message)
    log_file.write(message)

    # 3 Generate the header
    header = format_list_csv(data_to_extract)

    # Transfer header to the file
    subset_file.write(header)

    # 4 Collect all data for all files with at least a GOLD value
    message = message_time('Starting extraction.\n')
    print(message)
    log_file.write(message)

    manipulated_files = 0
    total = 0
    done = range(100, 100000, 100)
    done2 = range(1000, 100000, 1000)
    for csv_file in glob.glob(output_folder + '*.csv'):
        f = Manipulator(csv_file)
        total += 1
        # Extract data only from stamped files
        values_to_check = f._getValue(List_at_least_One)
        to_extract = False
        for i in range(0, len(List_at_least_One), 1):
            if values_to_check[i] == Expt_at_least_One[i]:
                to_extract = True
                # At least one is OK -> to extract
                break
        if total in done2:
            message = message_time(str(total))
            message += ' files have been tested.\n'
            print(message)
            log_file.write(message)

        if to_extract:
            manipulated_files += 1
            # Extract the values
            current_values = f._getValue(data_to_extract)

            # transfer into the csv
            line = format_list_csv(current_values)
            subset_file.write(line)

            if manipulated_files in done:
                message = message_time(str(manipulated_files))
                message += ' files have been drained.\n'
                print(message)
                log_file.write(message)

    message = message_time(str(manipulated_files))
    message += ' files have been drained.\n'
    print(message)
    log_file.write(message)
    message = message_time(str(total))
    message += ' files have been processed.\n\n'
    print(message)
    log_file.write(message)
    subset_file.close

    Quality_Extraction(log_file,
                       file_name)

    message = message_time('Transferring File to data folder.\n')
    log_file.write(message)
    print(message)

    # Transfert the file in Data folder
    os.rename(path_Output + file_name, path_Values)

    message = message_time('End of Extract_Infos.\n\n')
    log_file.write(message)
    print(message)


def Quality_Extraction(log_file,
                       file_name):
    """Verify that the output file has only unique entries."""
    # 5 Perform the quality control
    message = message_time('Performing Extraction quality control:\n')
    print(message)
    log_file.write(message)

    # Open the file
    mypanda = pd.read_csv(path_Output + file_name)
    print(mypanda.shape)
    # remove columns containing only NaNs
    mypanda = mypanda.dropna(axis = 1, how = 'all')
    print(mypanda.shape)

    message = message_time('')
    message += 'Database contains '
    message += str(mypanda.shape[0]) + ' entries.\n'
    print(message)
    log_file.write(message)

    genes = mypanda['gene_id'].values
    unique_entries = np.unique(genes)
    message = message_time('')
    message += str(unique_entries.shape[0])
    message += ' unique entries.\n'
    print(message)
    log_file.write(message)
    mypanda.to_csv(file_name)


def format_list_csv(list):
    """Transform list into a csv line."""
    line = ''
    for item in list:
        # Position correctly the exact number of ','
        if line == '':
            line += item
        else:
            line += ',' + item
    # Finish header with line break
    line += '\n'
    return line


if __name__ == "__main__":
    # execute only if run as a script
    Extract_Infos()
