#!/usr/bin/python
# coding: utf-8
"""Transform the CORUM db to UNIPROT reference .txt list."""
# Python libraries
from __future__ import division
import numpy as np
import pandas as pd

paths = ['coreComplexes.csv', 'allComplexes.csv']


def Extract_UNIPROT():
    """Get all UNIPROT ID."""
    # txt file to submit at https://www.uniprot.org/uploadlists/
    list_UNIPROT = []
    for path in paths:
        # read the csv and extract only the mouse complexes
        df = pd.read_csv(path, sep='\t')
        print('Raw dataframe', df.shape[0])

        # Get Only physical interaction
        filter_interaction = df['Experimental System Type'] == 'Mouse'
        df = df[filter_interaction]

        # Save the sub-databse into a csv
        path_out = path.replace('.csv', '_mouse.csv')
        df.to_csv(path_out, index=False)

        # Get all entries in 'subunits(UniProt IDs)'
        complexed_prot = df['subunits(UniProt IDs)'].values
        for complex in complexed_prot:
            isolated_prot = complex.split(';')
            list_UNIPROT = np.append(list_UNIPROT, isolated_prot).tolist()

    # keep only unique entries and sort
    list_UNIPROT = np.unique(list_UNIPROT).tolist()
    list_UNIPROT.sort()
    print(len(list_UNIPROT))

    # create the txt
    f = open('UNIPROT_complexed_proteins.txt', 'w+')
    for p in list_UNIPROT:
        f.write(p + '\n')
    f.close()


if __name__ == "__main__":
    # execute only if run as a script
    Extract_UNIPROT()
