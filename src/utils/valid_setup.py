#!/usr/bin/python
# coding: utf-8
"""Verify that paths are ok."""
import os
from utils.time_stamp import message_time


def check_path(pathlist,
               logfile,
               wanteDebug=True):
    """Verify if all paths exist."""
    # Generate Initial message for the log file
    message = message_time('Starting check_path.\n')
    logfile.write(message)

    # Initialize the variable
    problem = False

    for path in pathlist:
        # Specific message for a path
        message = '     Searching for: ' + path + ':\n'
        message += '     ' + str(os.path.exists(path)) + '\n'
        logfile.write(message)

        # Generate a warning if a file or path is missing/wrong
        if os.path.exists(path) is False:
            problem = True

    # Generate Final message for the log file
    message = message_time('End Check_path.\n\n')
    logfile.write(message)

    return problem
