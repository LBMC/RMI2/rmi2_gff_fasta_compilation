#!/usr/bin/python
# coding: utf-8
"""Calculate Kozack scores."""
import math


def frequency_score(Kozack_seq):
    """Calculate the frequency score using Solene approach.

    I have included a normalization at 100% to know what the value means.
    """
    # Create the dictionnary containing the frequency of the nucleotides
    # from -5 to +5
    dict_freq = {}
    #                                         A  T  G
    #                 -5   -4   -3   -2   -1  0  +1 +2  +3   +4   +5
    dict_freq['A'] = [0.1, 0.2, 1.3, 0.2, 0.2, 0, 0, 0, 0.4, 0.2, 0.1]
    dict_freq['T'] = [0.11, 0.1, 0.2, 0.1, 0.15, 0, 0, 0, 0.25, 0.1, 0.15]
    dict_freq['C'] = [0.13, 0.3, 0.25, 0.3, 0.75, 0, 0, 0, 0.2, 0.35, 0.15]
    dict_freq['G'] = [0.12, 0.2, 1, 0.2, 0.45, 0, 0, 0, 0.8, 0.15, 0.2]

    # Best and lowest score for normalization
    # 0.13 + 0.3 + 1.3 + 0.3 + 0.75 + 0.8 + 0.35 + 0.2
    # 0.1 + 0.1 + 0.2 + 0.1 + 0.15 + 0.2 + 0.1 + 0.1
    best_score = 4.13
    low_score = 1.05

    # Calculating raw score
    score = 0
    for n in range(0, len(Kozack_seq), 1):
        #                  <--letter-->  Position in kozack
        score += dict_freq[Kozack_seq[n]][n]
    # Normalizing
    score = round(100 * (score - low_score) / (best_score - low_score),
                  2)

    return str(score)


def efficiency_score_mono(Kozack_seq):
    """Calculate the efficiency score based on Noderer et al 2014."""
    # Create the dictionnary containing the efficiency of the nucleotides
    # from -5 to +5
    # Noderer et al 2014
    dict_eff = {}
    #                                             A  T  G
    #                 -5    -4    -3    -2    -1  +1 +2 +3   +4    +5  +6
    dict_eff['A'] = [1.03, 0.91, 0.71, 0.99, 0.90, 0, 0, 0, 1.06, 0.93, 0]
    dict_eff['T'] = [1.04, 1.08, 0.92, 1.08, 1.00, 0, 0, 0, 0.91, 1.09, 0]
    dict_eff['C'] = [0.94, 1.07, 1.24, 1.05, 1.08, 0, 0, 0, 0.98, 0.99, 0]
    dict_eff['G'] = [0.99, 0.94, 1.13, 0.88, 1.02, 0, 0, 0, 1.04, 0.99, 0]

    # Best and lowest score for normalization
    best_score = math.exp(1.04 + 1.08 + 1.24 + 1.08 + 1.08 + 1.06 + 1.09)
    low_score = math.exp(0.94 + 0.91 + 0.71 + 0.88 + 0.90 + 0.91 + 0.93)
    # Calculating raw score
    score = 0
    for n in range(0, len(Kozack_seq), 1):
        #                  <--letter-->  Position in kozack
        score += dict_eff[Kozack_seq[n]][n]
    # Normalizing
    score = math.exp(score)
    score = round(100 * (score - low_score) / (best_score - low_score),
                  2)

    return str(score)


def efficiency_score_di(Kozack_seq):
    """Calculate the efficiency score based on Noderer et al 2014."""
    # Create the dictionnary containing the efficiency of the nucleotides
    # from -5 to +5
    # Noderer et al 2014
    dict_eff = {}
    #                                             A  T  G
    #                 -5    -4    -3    -2    -1  +1 +2 +3   +4    +5  +6
    dict_eff['A'] = [1.03, 0.91, 0.71, 0.99, 0.90, 0, 0, 0, 1.06, 0.93, 0]
    dict_eff['T'] = [1.04, 1.08, 0.92, 1.08, 1.00, 0, 0, 0, 0.91, 1.09, 0]
    dict_eff['C'] = [0.94, 1.07, 1.24, 1.05, 1.08, 0, 0, 0, 0.98, 0.99, 0]
    dict_eff['G'] = [0.99, 0.94, 1.13, 0.88, 1.02, 0, 0, 0, 1.04, 0.99, 0]

    # Calculating raw score
    score = 0
    for n in range(0, len(Kozack_seq), 1):
        #                  <--letter-->  Position in kozack
        score += dict_eff[Kozack_seq[n]][n]
    # Normalizing

    dic_eff_di = {}
    # correction_factor is defined as sigma all combi dinucleotide/16 = 1
    # Positions -4/-3
    correct_factor = 1.218
    dic_eff_di[1, 'T', 'T'] = 0.62 * correct_factor
    dic_eff_di[1, 'T', 'C'] = 0.73 * correct_factor
    dic_eff_di[1, 'T', 'A'] = 0.75 * correct_factor
    dic_eff_di[1, 'T', 'G'] = 0.62 * correct_factor

    dic_eff_di[1, 'C', 'T'] = 0.73 * correct_factor
    dic_eff_di[1, 'C', 'C'] = 0.80 * correct_factor
    dic_eff_di[1, 'C', 'A'] = 0.82 * correct_factor
    dic_eff_di[1, 'C', 'G'] = 0.74 * correct_factor

    dic_eff_di[1, 'A', 'T'] = 0.92 * correct_factor
    dic_eff_di[1, 'A', 'C'] = 0.94 * correct_factor
    dic_eff_di[1, 'A', 'A'] = 0.94 * correct_factor
    dic_eff_di[1, 'A', 'G'] = 0.93 * correct_factor

    dic_eff_di[1, 'G', 'T'] = 0.85 * correct_factor
    dic_eff_di[1, 'G', 'C'] = 0.92 * correct_factor
    dic_eff_di[1, 'G', 'A'] = 0.92 * correct_factor
    dic_eff_di[1, 'G', 'G'] = 0.90 * correct_factor

    # Positions -3/-2
    correct_factor = 1.227
    dic_eff_di[2, 'T', 'T'] = 0.66 * correct_factor
    dic_eff_di[2, 'T', 'C'] = 0.75 * correct_factor
    dic_eff_di[2, 'T', 'A'] = 0.71 * correct_factor
    dic_eff_di[2, 'T', 'G'] = 0.57 * correct_factor

    dic_eff_di[2, 'C', 'T'] = 0.77 * correct_factor
    dic_eff_di[2, 'C', 'C'] = 0.81 * correct_factor
    dic_eff_di[2, 'C', 'A'] = 0.82 * correct_factor
    dic_eff_di[2, 'C', 'G'] = 0.67 * correct_factor

    dic_eff_di[2, 'A', 'T'] = 0.92 * correct_factor
    dic_eff_di[2, 'A', 'C'] = 0.92 * correct_factor
    dic_eff_di[2, 'A', 'A'] = 0.93 * correct_factor
    dic_eff_di[2, 'A', 'G'] = 0.92 * correct_factor

    dic_eff_di[2, 'G', 'T'] = 0.88 * correct_factor
    dic_eff_di[2, 'G', 'C'] = 0.92 * correct_factor
    dic_eff_di[2, 'G', 'A'] = 0.91 * correct_factor
    dic_eff_di[2, 'G', 'G'] = 0.88 * correct_factor

    # Positions +4/+5
    correct_factor = 1.289
    dic_eff_di[8, 'T', 'T'] = 0.85 * correct_factor
    dic_eff_di[8, 'T', 'C'] = 0.82 * correct_factor
    dic_eff_di[8, 'T', 'A'] = 0.88 * correct_factor
    dic_eff_di[8, 'T', 'G'] = 0.82 * correct_factor

    dic_eff_di[8, 'C', 'T'] = 0.78 * correct_factor
    dic_eff_di[8, 'C', 'C'] = 0.73 * correct_factor
    dic_eff_di[8, 'C', 'A'] = 0.87 * correct_factor
    dic_eff_di[8, 'C', 'G'] = 0.78 * correct_factor

    dic_eff_di[8, 'A', 'T'] = 0.81 * correct_factor
    dic_eff_di[8, 'A', 'C'] = 0.91 * correct_factor
    dic_eff_di[8, 'A', 'A'] = 0.81 * correct_factor
    dic_eff_di[8, 'A', 'G'] = 0.78 * correct_factor

    dic_eff_di[8, 'G', 'T'] = 0.78 * correct_factor
    dic_eff_di[8, 'G', 'C'] = 1.02 * correct_factor
    dic_eff_di[8, 'G', 'A'] = 0.77 * correct_factor
    dic_eff_di[8, 'G', 'G'] = 0.90 * correct_factor

    for n in [1, 2, 8]:
        score += dic_eff_di[n,
                            Kozack_seq[n],
                            Kozack_seq[n + 1]]

    # Best and lowest score for normalization
    best_score = math.exp(1.04 + 1.08 + 1.24 + 1.08 + 1.08 + 1.06 + 1.09 +
                          1.218 * 0.94 +
                          1.227 * 0.93 +
                          1.289 * 1.02)
    low_score = math.exp(0.94 + 0.91 + 0.71 + 0.88 + 0.90 + 0.91 + 0.93 +
                         1.218 * 0.62 +
                         1.227 * 0.57 +
                         1.289 * 0.73)

    # Normalizing
    score = math.exp(score)
    score = round(100 * (score - low_score) / (best_score - low_score),
                  2)

    return str(score)
