#!/usr/bin/python
# coding: utf-8
"""Analyse all genes to attribute explicit GO terms."""
# Python libraries
from __future__ import division

# Home made scripts and packages
from config import path_Arborescence
from config import path_GO_annotated
from utils.time_stamp import message_time


def Arborescence_obtainer(log_file):
    """Generate a csv file GO to GO connection."""
    message = message_time('Starting Arborescence_obtainer.\n')
    print(message)
    log_file.write(message)

    # Create the converter file
    open(path_Arborescence, 'w+').close
    convf = open(path_Arborescence, 'w+')
    header = 'Child,Parent\n'
    convf.write(header)

    message = message_time('Arborescence file created.\n')
    print(message)
    log_file.write(message)

    # Initiate key variables
    done = range(1000, 100000, 1000)
    nGO = 0
    GO_line = ''
    myGO = ''
    myParent = ''

    # Open the .obo file
    with open(path_GO_annotated) as database:
        # Read the firstline
        line = database.readline()

        # Read all lines
        while line:

            # Find a new GO term
            line = clean_line(line)
            if line.startswith('id:'):
                myGO = line.split('id: ')[1]

            # Find a Parent is_a
            elif 'is_a:' in line:
                # get the GO and description
                myParent = line.split('is_a:')[1]
                # remove description
                myParent = myParent.split('!')[0]
                # remove spaces
                myParent = myParent.replace(' ', '')
                if 'GO:' in myParent and len(myParent) == 10:
                    GO_line = myGO + ','
                    GO_line += myParent + '\n'
                    convf.write(GO_line)
                    nGO += 1
                else:
                    print('Recognition problem: ' + myParent)

            # Find a Parent part_of
            elif 'part_of' in line:
                # get the GO and description
                myParent = line.split('part_of')[1]
                # remove description
                myParent = myParent.split('!')[0]
                # remove spaces
                myParent = myParent.replace(' ', '')
                if 'GO:' in myParent and len(myParent) == 10:
                    GO_line = myGO + ','
                    GO_line += myParent + '\n'
                    convf.write(GO_line)
                    nGO += 1
                else:
                    print('Recognition problem: ' + myParent)

            if nGO in done:
                message = message_time(str(nGO) + ' GO relations .\n')
                print(message)
                log_file.write(message)

                # Reset the values
                GO_line = ''
                myGO = ''
                myParent = ''

            # read new line
            line = database.readline()

    message = message_time(str(nGO) + ' GO relations .\n')
    print(message)
    log_file.write(message)

    convf.close

    message = message_time('End Arborescence_obtainer.\n\n')
    print(message)
    log_file.write(message)


def clean_line(line):
    """Remove unwanted characters from line."""
    unwanted = ['"', '\n']
    for char in unwanted:
        line = line.replace(char, '')
    return line


if __name__ == "__main__":
    # execute only if run as a script
    Arborescence_obtainer()
