#!/usr/bin/python
# coding: utf-8
"""Analyzes the CDS sequence for stretches of a codon list ."""
# Python libraries
from __future__ import division


def getStretches(manipulator_object,
                 list_codons,
                 minimal_size):
    """Get the sequence, launch the analysis and update the object."""
    sequence = manipulator_object._getValue(['CDS_seq'])[0]

    # Create the head of the keys
    head_key = 'Stretches'
    for codon in list_codons:
        head_key += '_' + codon

    keys = [head_key + '_score',
            head_key + '_pondarated_score',
            head_key + '_number',
            head_key + '_position_length']

    values_list = detectStretches(sequence,
                                  list_codons,
                                  minimal_size)

    for i in range(0, len(keys), 1):
        current_Key = keys[i]
        value = values_list[i]

        if current_Key in manipulator_object._key_list:
            manipulator_object._changeValue(current_Key,
                                            value)
        else:
            manipulator_object._appendValue(current_Key,
                                            value)


def detectStretches(sequence,
                    codon_To_Search,
                    minimal_size):
    """Detect stretches of specific codons."""
    # Initiate scores and numbers
    score = 0
    local_score = 0
    local_length = 0
    number_stretches = 0
    new_stretch = 0
    position_bio = 0
    position_length = ''

    # Analyses the sequence
    for position in range(0, len(sequence), 3):
        codon = sequence[position:position + 3]

        # If codon is interesting
        if codon in codon_To_Search:
            # increment values
            local_length += 1

            # Stretch if at least 2
            if local_length == minimal_size:
                new_stretch = 1
                # Position of the starting codon
                position_bio = position - 3 + 1
            else:
                new_stretch = 0

            # If it is a new stretch we increment the number_stretches
            if new_stretch == 1:
                number_stretches += 1

        # We calculate score only when stretch is over and reset values
        elif (codon not in codon_To_Search) and (local_length >= minimal_size):
            local_score = 2**local_length
            score += local_score
            if position_length != '':
                position_length += ' '
            position_length += str(position_bio) + '_'
            position_length += str(local_length)

            # Reset the values
            local_score = 0
            position_bio = 0
            local_length = 0

        # The codon is not ok
        else:
            # Reset the values
            local_score = 0
            position_bio = 0
            local_length = 0
    # Correct score with Length
    ponderated_score = str(3 * score / len(sequence))
    score = str(score)
    number_stretches = str(number_stretches)

    return [score,
            ponderated_score,
            number_stretches,
            position_length]
