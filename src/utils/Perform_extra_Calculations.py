#!/usr/bin/python
# coding: utf-8
"""Perform extra calulation and update .csv files."""
# Python libraries
from __future__ import division
import os
import glob
# Homemade utilities
from config import path_Output, value_stamp, minimal_size
from config import path_gff3, path_fa
from utils.Object_manipulator import Manipulator
from utils.time_stamp import message_time, string_time
from utils.Composition_AA import AA
from utils.Streches_detection import getStretches


def Extra_Calculations(log_file):
    """Pipeline for calculation."""
    # The analyse will be performed on all (#) or Gold for TDD (value_stamp)
    # file by checking in the header
    message = message_time('Starting Extra_Calculations.\n')
    print(message)
    log_file.write(message)
    subset = 'QC Validated'

    # 1. Checking the specific .gff3 vs .fa output folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    message = message_time('Extra Calculations on ' + subset + '.\n')
    print(message)
    log_file.write(message)

    nfile = 0
    done = range(1000, 100000, 1000)
    for csv_file in glob.glob(output_folder + '*.csv'):
        f = Manipulator(csv_file)

        QC = f._getValue(['QC_passed'])[0]

        if QC == '1':

            # Perform the Analysis
            perform_calculations(f)

            # Save the file updated
            f._updateFile()
            nfile += 1
            if nfile in done:
                message = message_time(str(nfile) + ' Files treated.\n')
                print(message)
                log_file.write(message)

    message = message_time(str(nfile) + ' Files treated.\n')
    print(message)
    log_file.write(message)

    message = message_time('Extra calculations are over.\n\n')
    print(message)
    log_file.write(message)


def perform_calculations(manipulator_object):
    """Perform the calculations for each file."""
    # AA analysis
    head = string_time() + ', Analysis of AA composition'
    manipulator_object._updateHeader(head)
    AA(manipulator_object)

    # Stretches analysis
    combo_AA = ['LYS', 'ASP', 'GLU',
                'LYS-ASP', 'LYS-GLU', 'ASP-GLU',
                'LYS-ASP-GLU']
    dict_combo_codons = {}
    dict_combo_codons['LYS'] = ['AAA', 'AAG']
    dict_combo_codons['ASP'] = ['GAC', 'GAT']
    dict_combo_codons['GLU'] = ['GAA', 'GAG']
    dict_combo_codons['LYS-ASP'] = ['AAA', 'AAG', 'GAC', 'GAT']
    dict_combo_codons['LYS-GLU'] = ['AAA', 'AAG', 'GAA', 'GAG']
    dict_combo_codons['ASP-GLU'] = ['GAC', 'GAT', 'GAA', 'GAG']
    dict_combo_codons['LYS-ASP-GLU'] = ['AAA', 'AAG',
                                        'GAC', 'GAT',
                                        'GAA', 'GAG']

    for AAs in combo_AA:
        # Update the header
        head = string_time() + ', Get stretches for codons '
        head += AAs
        manipulator_object._updateHeader(head)

        # Perform the analysis
        getStretches(manipulator_object,
                     dict_combo_codons[AAs],
                     minimal_size)


if __name__ == "__main__":
    # execute only if run as a script
    Extra_Calculations()
