#!/usr/bin/python
# coding: utf-8
"""Add introns to the correct files."""
# Python libraries
from Bio.Seq import Seq
import numpy as np
# Home made scripts and packages
from utils.analysis_seq import ATGC


def add_intron_Juliana(intron,
                       transcript,
                       fasta,
                       wanteDebug=True):
    """Append the current intron to its parent transcript object."""
    # Determine how many introns are already given
    n_Introns = transcript._nIntrons

    # Give intron name
    intron_id = 'intron_' + str(n_Introns + 1)

    # Get sequence properties
    intron_seq = intron.sequence(fasta,
                                 use_strand=False)
    if intron.strand == '-':
        intron_seq = Seq(intron_seq)
        intron_seq = str(intron_seq.reverse_complement())

    (le,
     A,
     T,
     G,
     C,
     N) = ATGC(intron_seq)

    # Determine if the intron is in the 5UTR or 3UTR
    # First we get the borders if they exist...
    if transcript._5UTR_start != '':
        lim_5UTR = (int(transcript._5UTR_start),
                    int(transcript._5UTR_end))
        lim_5UTR = (min(lim_5UTR), max(lim_5UTR))
        if int(intron.start) > lim_5UTR[0] and int(intron.start) < lim_5UTR[1]:
            transcript.Introns_5UTR_id += intron_id + ';'

    if transcript._3UTR_start != '':
        lim_3UTR = (int(transcript._3UTR_start),
                    int(transcript._3UTR_end))
        lim_3UTR = (min(lim_3UTR), max(lim_3UTR))
        if int(intron.start) > lim_3UTR[0] and int(intron.start) < lim_3UTR[1]:
            transcript.Introns_3UTR_id += intron_id + ';'

    # Update transcript
    transcript._nIntrons += 1
    transcript._Intron_id = np.append(transcript._Intron_id,
                                      intron_id)
    transcript._Intron_start = np.append(transcript._Intron_start,
                                         str(intron.start))
    transcript._Intron_end = np.append(transcript._Intron_end,
                                       str(intron.end))
    transcript._Intron_seq = np.append(transcript._Intron_seq,
                                       str(intron_seq))
    transcript._Intron_le = np.append(transcript._Intron_le,
                                      le)
    transcript._Intron_A = np.append(transcript._Intron_A,
                                     A)
    transcript._Intron_T = np.append(transcript._Intron_T,
                                     T)
    transcript._Intron_G = np.append(transcript._Intron_G,
                                     G)
    transcript._Intron_C = np.append(transcript._Intron_C,
                                     C)
    transcript._Intron_N = np.append(transcript._Intron_N,
                                     N)
    return transcript
