#!/usr/bin/python
# coding: utf-8
"""Append G quadruplex data to the transcripts files."""
# Python libraries
from __future__ import division
import os

# Home made scripts and packages
from config import path_Output
from config import path_gff3, path_fa
from config import path_4G_UTR5, path_4G_CDS, path_4G_UTR3
from config import threshold_Cg_div_Cc
from utils.time_stamp import string_time
from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator


def Add_4G():
    """Open the 4G .csv files and add the data to the correct transcript."""
    print('   #   ' + string_time() + '   #   Harvesting m6ASeq data')
    # 1. Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # If the specific input folder doesn't exist
    if os.path.exists(input_folder) is False:
        print('   #   ' + 'Expected output folder doesn t exist.')
        # Quit
        exit()

    # Initiate the key list and dictionnary
    list_Tot = []
    dict_Tot = {}

    list_4G_files = [path_4G_UTR5,
                     path_4G_CDS,
                     path_4G_UTR3]
    list_Keys = ['n4G_UTR5',
                 'n4G_CDS',
                 'n4G_UTR3']

    # Loop on the 4G.csv files
    for i in range(0, len(list_4G_files), 1):
        print('   #   ' + string_time() + '   #   Processing ' + list_4G_files[i])
        list_transcript = []
        dict_transcript = {}

        # Open the current file and convert into object shredder
        db = Shredder_simple(list_4G_files[i], ',')
        # print(db._2nd_Keys)
        # Loop in the lines using the Index keyword
        for index in db._First_Col:
            score = db._dict_values[index, 'Cg/Cc']
            if score != '#DIV/0!':
                score = float(score)

            # We use only valid values
            if score > threshold_Cg_div_Cc:
                # Obtain the transcript ID
                ID = db._dict_values[index, 'gene_id'].split('|')[1]

                # Update the dictionnary
                if ID in list_transcript:
                    dict_transcript[ID] = dict_transcript[ID] + 1
                else:
                    # Initiate the counting
                    list_transcript.append(ID)
                    dict_transcript[ID] = 1

                # Update the total dictionarry
                if ID in list_Tot:
                    dict_Tot[ID] = dict_Tot[ID] + 1
                else:
                    # Initiate the counting
                    list_Tot.append(ID)
                    dict_Tot[ID] = 1

        # Saving the current data
        print('   #   ' + string_time() + '   #   Transferring.')
        for transcript in list_transcript:
            # Check that the file exists
            if os.path.isfile(input_folder + transcript + '.csv'):
                f = Manipulator(input_folder + transcript + '.csv')
                f._appendValue(list_Keys[i],
                               str(dict_transcript[transcript]))
                f._updateFile()
            else:
                print(transcript + ' is missing!')

    # Saving the current data
    print('   #   ' + string_time() + '   #   Transferring.')
    for transcript in list_Tot:
        # Check that the file exists
        if os.path.isfile(input_folder + transcript + '.csv'):
            f = Manipulator(input_folder + transcript + '.csv')
            f._appendValue('n4G_total',
                           str(dict_Tot[transcript]))
            f._updateFile()
        else:
            print(transcript + ' is missing!')


if __name__ == "__main__":
    # execute only if run as a script
    Add_4G()
