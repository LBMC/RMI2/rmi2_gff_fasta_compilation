macro "3D_movie"{
    setBatchMode(true);
    // Zero frame
    zero_F = 256

    // x y zero
    zero = 15

    // Choose the txt file
    path_data = File.openDialog("Data File");

    // Open the file as string
    raw_values = File.openAsString(path_data);

    // Generate le line array
    lines = split(raw_values, '\n');

    // Create the recipient images
    newImage("Red", "8-bit black", 270, 270, 501);
    newImage("Green", "8-bit black", 270, 270, 501);
    newImage("Blue", "8-bit black", 270, 270, 501);

    initiate_Red(256, 501)
    initiate_Green(254, 258, zero)
    initiate_Blue(254, 258, zero)

    // Draw points
    for (l = 1; l < lines.length; l ++){
        showProgress(l/lines.length);
        // Get the values
        stringsV = split(lines[l], ',');
        TDD = parseFloat(stringsV[0]);
        RD = stringsV[1];
        UTR = stringsV[2];

        pb = 0;
        if (RD == 'NA' || RD == 'Inf'){
            pb = 1;
        }

        if (UTR == 'NA' || UTR == 'Inf'){
            pb = 1;
        }

        if (pb !=1) {
            draw_point(TDD, parseFloat(RD), parseFloat(UTR), zero);
        }
    }


    // Fuse all channels
    run("Merge Channels...", "c1=Red c2=Green c3=Blue keep");
    saveAs("Tiff", path_data + "_3D.tif");

function draw_point(Z, X, Y, zero){
    OK = 1;
    if (Z < -1 || Z > 1){
        OK = 0;
    }

    if (X < 0 || X > 2){
        OK = 0;
    }

    if (Y < 0 || Y > 7000){
        OK = 0;
    }

    if (OK == 1){
        calZ = round(255 * Z);
        calX = round(255 * X / 2);
        calY = round(255 * Y / 7000);

        if (calZ < 0) {
            colZ = 100;
            colX = 100;
            colY = 100;
        }else{
            colZ = calZ;
            colX = calX;
            colY = calY;
        }

        // We take into account that we span -1 to 1 using frame numbers from 1
        // to 501
        calZ += 255;

        // Now we draw each point in 3D

        // First Color
        selectWindow("Red");
        setForegroundColor(colZ, colZ, colZ);
        if ((calZ - 2)>=1 && (calZ - 2)<=501){
            setSlice(calZ - 2);
            makeOval(calX + zero, calY + zero, 1, 1);
            run("Fill", "slice");
        }
        if ((calZ - 1)>=1 && (calZ - 1)<=501){
            setSlice(calZ - 1);
            makeOval(calX + zero - 1, calY + zero -1, 3, 3);
            run("Fill", "slice");
        }
        if ((calZ)>=1 && (calZ)<=501){
            setSlice(calZ);
            makeOval(calX + zero - 2, calY + zero -2, 5, 5);
            run("Fill", "slice");
        }
        if ((calZ + 1)>=1 && (calZ + 1)<=501){
            setSlice(calZ + 1);
            makeOval(calX + zero - 1, calY + zero -1, 3, 3);
            run("Fill", "slice");
        }
        if ((calZ + 2)>=1 && (calZ + 2)<=501){
            setSlice(calZ + 2);
            makeOval(calX + zero, calY + zero, 1, 1);
            run("Fill", "slice");
        }

        // Second Color
        selectWindow("Green");
        setForegroundColor(colX, colX, colX);
        if ((calZ - 2)>=1 && (calZ - 2)<=501){
            setSlice(calZ - 2);
            makeOval(calX + zero, calY + zero, 1, 1);
            run("Fill", "slice");
        }
        if ((calZ - 1)>=1 && (calZ - 1)<=501){
            setSlice(calZ - 1);
            makeOval(calX + zero - 1, calY + zero -1, 3, 3);
            run("Fill", "slice");
        }
        if ((calZ)>=1 && (calZ)<=501){
            setSlice(calZ);
            makeOval(calX + zero - 2, calY + zero -2, 5, 5);
            run("Fill", "slice");
        }
        if ((calZ + 1)>=1 && (calZ + 1)<=501){
            setSlice(calZ + 1);
            makeOval(calX + zero - 1, calY + zero -1, 3, 3);
            run("Fill", "slice");
        }
        if ((calZ + 2)>=1 && (calZ + 2)<=501){
            setSlice(calZ + 2);
            makeOval(calX + zero, calY + zero, 1, 1);
            run("Fill", "slice");
        }

        // Third Color
        selectWindow("Blue");
        setForegroundColor(colY, colY, colY);
        if ((calZ - 2)>=1 && (calZ - 2)<=501){
            setSlice(calZ - 2);
            makeOval(calX + zero, calY + zero, 1, 1);
            run("Fill", "slice");
        }
        if ((calZ - 1)>=1 && (calZ - 1)<=501){
            setSlice(calZ - 1);
            makeOval(calX + zero - 1, calY + zero -1, 3, 3);
            run("Fill", "slice");
        }
        if ((calZ)>=1 && (calZ)<=501){
            setSlice(calZ);
            makeOval(calX + zero - 2, calY + zero -2, 5, 5);
            run("Fill", "slice");
        }
        if ((calZ + 1)>=1 && (calZ + 1)<=501){
            setSlice(calZ + 1);
            makeOval(calX + zero - 1, calY + zero -1, 3, 3);
            run("Fill", "slice");
        }
        if ((calZ + 2)>=1 && (calZ + 2)<=501){
            setSlice(calZ + 2);
            makeOval(calX + zero, calY + zero, 1, 1);
            run("Fill", "slice");
        }
    }


}


function initiate_Red(start_frame, end_frame){
    selectWindow("Red")
    // Grey value
    GV = -1;
    for (s = start_frame; s <= end_frame; s++){
        setSlice(s);
        GV += 1;
        setForegroundColor(GV, GV, GV);
        makeRectangle(0, 0, 5, 5);
        run("Fill", "slice");
    }
}

function initiate_Green(start_frame, end_frame, zero){
    selectWindow("Green")
    // Grey value
    for (GV = 0; GV <= 255; GV++){
        for (s = start_frame; s <= end_frame; s++){
            setSlice(s);
            setForegroundColor(GV, GV, GV);
            makeRectangle(zero + GV, 0, 1, 5);
            run("Fill", "slice");
        }
    }
}

function initiate_Blue(start_frame, end_frame, zero){
    selectWindow("Blue")
    // Grey value
    for (GV = 0; GV <= 255; GV++){
        for (s = start_frame; s <= end_frame; s++){
            setSlice(s);
            setForegroundColor(GV, GV, GV);
            makeRectangle(0, zero + GV, 5, 1);
            run("Fill", "slice");
        }
    }
}


}// End of the macro
