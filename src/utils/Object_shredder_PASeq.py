#!/usr/bin/python
# coding: utf-8
"""Object dedicated to read PASeq results and update files."""
# Python libraries
from __future__ import division
from utils.time_stamp import string_time
import numpy as np


class Shredder_PASeq:
    """This object is generated from a specific database.

    It resume all lines and their properties.
    The PASeq values are transmitted to a PASeq object to facilitate the
    data transfert to the transcript .csv files
    """

    def __init__(self,
                 path,
                 separator
                 ):
        """Construct method."""
        self._separator = separator
        self._path = path
        self._dict_values = {}
        self._List_Gene_Name = []

        # The dictionary will created like this:
        # self._dict_values['Transcript_ID', '2nd_Key']
        # 2nd_Keys are the header column names

        # Reading the file
        print('   #   ' + string_time() + '   #   Shredding csv database')
        self._read_file()

    def _read_file(self):
        """Read the file and feed the dictionary."""
        # line counter
        nlines = 0

        # initialize all varaibles
        (n_Exp_Ends,
         Name_Exp_3Ends,
         PA_IDs,
         PA_Starts,
         PA_Ends,
         PA_sitesCombined,
         PA_siteCount,
         PA_A1,
         PA_A1_Tot,
         PA_A2,
         PA_A2_Tot,
         PA_A3,
         PA_A3_Tot,
         PA_m1,
         PA_m1_Tot,
         PA_m2,
         PA_m2_Tot,
         PA_m3,
         PA_m3_Tot,
         PA_R1,
         PA_R1_Tot,
         PA_R2,
         PA_R2_Tot,
         PA_R3,
         PA_R3_Tot,
         PA_i1,
         PA_i1_Tot,
         PA_i2,
         PA_i2_Tot,
         PA_i3,
         PA_i3_Tot) = self._new_Gene()

        line_dict = {}

        with open(self._path) as database:
            # Read the firstline
            line = database.readline()

            # Read all lines
            while line:
                nlines += 1
                line = self._clean_line(line)

                """# debug mode
                if len(self._List_Gene_Name) == 2:
                    break"""

                # Decide if line belongs to header or describe a value
                if nlines == 1:
                    print('   #   ' + string_time() + '   #   Reading header')
                    # feed the header
                    self._header = line.split(self._separator)
                    # Initialize the Curr_Gene
                    Curr_Gene = ''

                else:
                    # Remove breakline and split the line between key and value
                    frags = line.split(self._separator)

                    # Get Gene Name from fragment 19 and
                    # Determine if it is a new valid Gene
                    if Curr_Gene not in (frags[19], ''):
                        # Update the dictionnary for the last entry
                        self._dict_values[Curr_Gene] = (n_Exp_Ends,
                                                        Name_Exp_3Ends,
                                                        PA_IDs,
                                                        PA_Starts,
                                                        PA_Ends,
                                                        PA_sitesCombined,
                                                        PA_siteCount,
                                                        PA_A1,
                                                        PA_A1_Tot,
                                                        PA_A2,
                                                        PA_A2_Tot,
                                                        PA_A3,
                                                        PA_A3_Tot,
                                                        PA_m1,
                                                        PA_m1_Tot,
                                                        PA_m2,
                                                        PA_m2_Tot,
                                                        PA_m3,
                                                        PA_m3_Tot,
                                                        PA_R1,
                                                        PA_R1_Tot,
                                                        PA_R2,
                                                        PA_R2_Tot,
                                                        PA_R3,
                                                        PA_R3_Tot,
                                                        PA_i1,
                                                        PA_i1_Tot,
                                                        PA_i2,
                                                        PA_i2_Tot,
                                                        PA_i3,
                                                        PA_i3_Tot)

                        # Update list name
                        if frags[19] in self._List_Gene_Name:
                            message = '   #   ' + string_time() + '   #   '
                            message += frags[19] + ' is Duplicated'
                        self._List_Gene_Name = np.append(self._List_Gene_Name,
                                                         frags[19])
                        # Update Curr_Gene
                        Curr_Gene = frags[19]

                        # Reset variables
                        (n_Exp_Ends,
                         Name_Exp_3Ends,
                         PA_IDs,
                         PA_Starts,
                         PA_Ends,
                         PA_sitesCombined,
                         PA_siteCount,
                         PA_A1,
                         PA_A1_Tot,
                         PA_A2,
                         PA_A2_Tot,
                         PA_A3,
                         PA_A3_Tot,
                         PA_m1,
                         PA_m1_Tot,
                         PA_m2,
                         PA_m2_Tot,
                         PA_m3,
                         PA_m3_Tot,
                         PA_R1,
                         PA_R1_Tot,
                         PA_R2,
                         PA_R2_Tot,
                         PA_R3,
                         PA_R3_Tot,
                         PA_i1,
                         PA_i1_Tot,
                         PA_i2,
                         PA_i2_Tot,
                         PA_i3,
                         PA_i3_Tot) = self._new_Gene()

                        # Retrieve values from the line and store them in
                        # line_dict = {}

                        line_dict = {}
                        for key in range(0, len(self._header), 1):
                            line_dict[self._header[key]] = frags[key]

                        # Update variables
                        (n_Exp_Ends,
                         Name_Exp_3Ends,
                         PA_IDs,
                         PA_Starts,
                         PA_Ends,
                         PA_sitesCombined,
                         PA_siteCount,
                         PA_A1,
                         PA_A1_Tot,
                         PA_A2,
                         PA_A2_Tot,
                         PA_A3,
                         PA_A3_Tot,
                         PA_m1,
                         PA_m1_Tot,
                         PA_m2,
                         PA_m2_Tot,
                         PA_m3,
                         PA_m3_Tot,
                         PA_R1,
                         PA_R1_Tot,
                         PA_R2,
                         PA_R2_Tot,
                         PA_R3,
                         PA_R3_Tot,
                         PA_i1,
                         PA_i1_Tot,
                         PA_i2,
                         PA_i2_Tot,
                         PA_i3,
                         PA_i3_Tot) = self._update_Gene(line_dict,
                                                        n_Exp_Ends,
                                                        Name_Exp_3Ends,
                                                        PA_IDs,
                                                        PA_Starts,
                                                        PA_Ends,
                                                        PA_sitesCombined,
                                                        PA_siteCount,
                                                        PA_A1,
                                                        PA_A1_Tot,
                                                        PA_A2,
                                                        PA_A2_Tot,
                                                        PA_A3,
                                                        PA_A3_Tot,
                                                        PA_m1,
                                                        PA_m1_Tot,
                                                        PA_m2,
                                                        PA_m2_Tot,
                                                        PA_m3,
                                                        PA_m3_Tot,
                                                        PA_R1,
                                                        PA_R1_Tot,
                                                        PA_R2,
                                                        PA_R2_Tot,
                                                        PA_R3,
                                                        PA_R3_Tot,
                                                        PA_i1,
                                                        PA_i1_Tot,
                                                        PA_i2,
                                                        PA_i2_Tot,
                                                        PA_i3,
                                                        PA_i3_Tot)

                    elif Curr_Gene == '':
                        # Create the first Gene
                        self._List_Gene_Name = np.append(self._List_Gene_Name,
                                                         frags[19])
                        # Update Curr_Gene
                        Curr_Gene = frags[19]

                        # Reset variables
                        (n_Exp_Ends,
                         Name_Exp_3Ends,
                         PA_IDs,
                         PA_Starts,
                         PA_Ends,
                         PA_sitesCombined,
                         PA_siteCount,
                         PA_A1,
                         PA_A1_Tot,
                         PA_A2,
                         PA_A2_Tot,
                         PA_A3,
                         PA_A3_Tot,
                         PA_m1,
                         PA_m1_Tot,
                         PA_m2,
                         PA_m2_Tot,
                         PA_m3,
                         PA_m3_Tot,
                         PA_R1,
                         PA_R1_Tot,
                         PA_R2,
                         PA_R2_Tot,
                         PA_R3,
                         PA_R3_Tot,
                         PA_i1,
                         PA_i1_Tot,
                         PA_i2,
                         PA_i2_Tot,
                         PA_i3,
                         PA_i3_Tot) = self._new_Gene()

                        # Retrieve values from the line and store them in
                        # line_dict = {}

                        line_dict = {}
                        for key in range(0, len(self._header), 1):
                            line_dict[self._header[key]] = frags[key]

                        # Update variables
                        # Update variables
                        (n_Exp_Ends,
                         Name_Exp_3Ends,
                         PA_IDs,
                         PA_Starts,
                         PA_Ends,
                         PA_sitesCombined,
                         PA_siteCount,
                         PA_A1,
                         PA_A1_Tot,
                         PA_A2,
                         PA_A2_Tot,
                         PA_A3,
                         PA_A3_Tot,
                         PA_m1,
                         PA_m1_Tot,
                         PA_m2,
                         PA_m2_Tot,
                         PA_m3,
                         PA_m3_Tot,
                         PA_R1,
                         PA_R1_Tot,
                         PA_R2,
                         PA_R2_Tot,
                         PA_R3,
                         PA_R3_Tot,
                         PA_i1,
                         PA_i1_Tot,
                         PA_i2,
                         PA_i2_Tot,
                         PA_i3,
                         PA_i3_Tot) = self._update_Gene(line_dict,
                                                        n_Exp_Ends,
                                                        Name_Exp_3Ends,
                                                        PA_IDs,
                                                        PA_Starts,
                                                        PA_Ends,
                                                        PA_sitesCombined,
                                                        PA_siteCount,
                                                        PA_A1,
                                                        PA_A1_Tot,
                                                        PA_A2,
                                                        PA_A2_Tot,
                                                        PA_A3,
                                                        PA_A3_Tot,
                                                        PA_m1,
                                                        PA_m1_Tot,
                                                        PA_m2,
                                                        PA_m2_Tot,
                                                        PA_m3,
                                                        PA_m3_Tot,
                                                        PA_R1,
                                                        PA_R1_Tot,
                                                        PA_R2,
                                                        PA_R2_Tot,
                                                        PA_R3,
                                                        PA_R3_Tot,
                                                        PA_i1,
                                                        PA_i1_Tot,
                                                        PA_i2,
                                                        PA_i2_Tot,
                                                        PA_i3,
                                                        PA_i3_Tot)
                    elif Curr_Gene == frags[19]:
                        # Update the current gene

                        # Retrieve values from the line and store them in
                        # line_dict = {}

                        line_dict = {}
                        for key in range(0, len(self._header), 1):
                            line_dict[self._header[key]] = frags[key]

                        # Update variables
                        # Update variables
                        (n_Exp_Ends,
                         Name_Exp_3Ends,
                         PA_IDs,
                         PA_Starts,
                         PA_Ends,
                         PA_sitesCombined,
                         PA_siteCount,
                         PA_A1,
                         PA_A1_Tot,
                         PA_A2,
                         PA_A2_Tot,
                         PA_A3,
                         PA_A3_Tot,
                         PA_m1,
                         PA_m1_Tot,
                         PA_m2,
                         PA_m2_Tot,
                         PA_m3,
                         PA_m3_Tot,
                         PA_R1,
                         PA_R1_Tot,
                         PA_R2,
                         PA_R2_Tot,
                         PA_R3,
                         PA_R3_Tot,
                         PA_i1,
                         PA_i1_Tot,
                         PA_i2,
                         PA_i2_Tot,
                         PA_i3,
                         PA_i3_Tot) = self._update_Gene(line_dict,
                                                        n_Exp_Ends,
                                                        Name_Exp_3Ends,
                                                        PA_IDs,
                                                        PA_Starts,
                                                        PA_Ends,
                                                        PA_sitesCombined,
                                                        PA_siteCount,
                                                        PA_A1,
                                                        PA_A1_Tot,
                                                        PA_A2,
                                                        PA_A2_Tot,
                                                        PA_A3,
                                                        PA_A3_Tot,
                                                        PA_m1,
                                                        PA_m1_Tot,
                                                        PA_m2,
                                                        PA_m2_Tot,
                                                        PA_m3,
                                                        PA_m3_Tot,
                                                        PA_R1,
                                                        PA_R1_Tot,
                                                        PA_R2,
                                                        PA_R2_Tot,
                                                        PA_R3,
                                                        PA_R3_Tot,
                                                        PA_i1,
                                                        PA_i1_Tot,
                                                        PA_i2,
                                                        PA_i2_Tot,
                                                        PA_i3,
                                                        PA_i3_Tot)

                # read new line
                line = database.readline()

        # Update the dictionnary for the last entry
        self._dict_values[Curr_Gene] = (n_Exp_Ends,
                                        Name_Exp_3Ends,
                                        PA_IDs,
                                        PA_Starts,
                                        PA_Ends,
                                        PA_sitesCombined,
                                        PA_siteCount,
                                        PA_A1,
                                        PA_A1_Tot,
                                        PA_A2,
                                        PA_A2_Tot,
                                        PA_A3,
                                        PA_A3_Tot,
                                        PA_m1,
                                        PA_m1_Tot,
                                        PA_m2,
                                        PA_m2_Tot,
                                        PA_m3,
                                        PA_m3_Tot,
                                        PA_R1,
                                        PA_R1_Tot,
                                        PA_R2,
                                        PA_R2_Tot,
                                        PA_R3,
                                        PA_R3_Tot,
                                        PA_i1,
                                        PA_i1_Tot,
                                        PA_i2,
                                        PA_i2_Tot,
                                        PA_i3,
                                        PA_i3_Tot
                                        )
        print('   #   ' + string_time() + '   #   Table has: ' + str(nlines))

    def _clean_line(self, line):
        """Remove unwanted characters from line."""
        unwanted = ['"', '\n']
        for char in unwanted:
            line = line.replace(char, '')
        return line

    def _new_Gene(self):
        """Reset all key variables for a new gene."""
        n_Exp_Ends = 0
        Name_Exp_3Ends = []
        PA_IDs = []
        PA_Starts = []
        PA_Ends = []
        PA_sitesCombined = []
        PA_siteCount = []
        PA_A1 = []
        PA_A2 = []
        PA_A3 = []
        PA_m1 = []
        PA_m2 = []
        PA_m3 = []
        PA_R1 = []
        PA_R2 = []
        PA_R3 = []
        PA_i1 = []
        PA_i2 = []
        PA_i3 = []
        PA_A1_Tot = 0
        PA_A2_Tot = 0
        PA_A3_Tot = 0
        PA_m1_Tot = 0
        PA_m2_Tot = 0
        PA_m3_Tot = 0
        PA_R1_Tot = 0
        PA_R2_Tot = 0
        PA_R3_Tot = 0
        PA_i1_Tot = 0
        PA_i2_Tot = 0
        PA_i3_Tot = 0

        return (n_Exp_Ends,
                Name_Exp_3Ends,
                PA_IDs,
                PA_Starts,
                PA_Ends,
                PA_sitesCombined,
                PA_siteCount,
                PA_A1,
                PA_A1_Tot,
                PA_A2,
                PA_A2_Tot,
                PA_A3,
                PA_A3_Tot,
                PA_m1,
                PA_m1_Tot,
                PA_m2,
                PA_m2_Tot,
                PA_m3,
                PA_m3_Tot,
                PA_R1,
                PA_R1_Tot,
                PA_R2,
                PA_R2_Tot,
                PA_R3,
                PA_R3_Tot,
                PA_i1,
                PA_i1_Tot,
                PA_i2,
                PA_i2_Tot,
                PA_i3,
                PA_i3_Tot
                )

    def _update_Gene(self,
                     myDict,
                     n_Exp_Ends,
                     Name_Exp_3Ends,
                     PA_IDs,
                     PA_Starts,
                     PA_Ends,
                     PA_sitesCombined,
                     PA_siteCount,
                     PA_A1,
                     PA_A1_Tot,
                     PA_A2,
                     PA_A2_Tot,
                     PA_A3,
                     PA_A3_Tot,
                     PA_m1,
                     PA_m1_Tot,
                     PA_m2,
                     PA_m2_Tot,
                     PA_m3,
                     PA_m3_Tot,
                     PA_R1,
                     PA_R1_Tot,
                     PA_R2,
                     PA_R2_Tot,
                     PA_R3,
                     PA_R3_Tot,
                     PA_i1,
                     PA_i1_Tot,
                     PA_i2,
                     PA_i2_Tot,
                     PA_i3,
                     PA_i3_Tot
                     ):
        """Update all key variables for a new gene."""
        n_Exp_Ends += 1

        PA_A1_Tot += int(myDict['Activated_CD4_T_cells_A1'])
        PA_A2_Tot += int(myDict['Activated_CD4_T_cells_A2'])
        PA_A3_Tot += int(myDict['Activated_CD4_T_cells_A3'])
        PA_m1_Tot += int(myDict['LPS_stimulated_macrophages_m1'])
        PA_m2_Tot += int(myDict['LPS_stimulated_macrophages_m2'])
        PA_m3_Tot += int(myDict['LPS_stimulated_macrophages_m3'])
        PA_R1_Tot += int(myDict['Resting_CD4_T_cells_R1'])
        PA_R2_Tot += int(myDict['Resting_CD4_T_cells_R2'])
        PA_R3_Tot += int(myDict['Resting_CD4_T_cells_R3'])
        PA_i1_Tot += int(myDict['unstimulated_macrophages_i1'])
        PA_i2_Tot += int(myDict['unstimulated_macrophages_i2'])
        PA_i3_Tot += int(myDict['unstimulated_macrophages_i3'])
        Name_Exp_3Ends = np.append(Name_Exp_3Ends,
                                   'Exp_UTR3_End_' + str(n_Exp_Ends))
        PA_IDs = np.append(PA_IDs, myDict['PA_id'])
        PA_Starts = np.append(PA_Starts, myDict['start'])
        PA_Ends = np.append(PA_Ends, myDict['end'])
        PA_sitesCombined = np.append(PA_sitesCombined, myDict['sitesCombined'])
        PA_siteCount = np.append(PA_siteCount, myDict['sitesCount'])
        PA_A1 = np.append(PA_A1, myDict['Activated_CD4_T_cells_A1'])
        PA_A2 = np.append(PA_A2, myDict['Activated_CD4_T_cells_A2'])
        PA_A3 = np.append(PA_A3, myDict['Activated_CD4_T_cells_A3'])
        PA_m1 = np.append(PA_m1, myDict['LPS_stimulated_macrophages_m1'])
        PA_m2 = np.append(PA_m2, myDict['LPS_stimulated_macrophages_m2'])
        PA_m3 = np.append(PA_m3, myDict['LPS_stimulated_macrophages_m3'])
        PA_R1 = np.append(PA_R1, myDict['Resting_CD4_T_cells_R1'])
        PA_R2 = np.append(PA_R2, myDict['Resting_CD4_T_cells_R2'])
        PA_R3 = np.append(PA_R3, myDict['Resting_CD4_T_cells_R3'])
        PA_i1 = np.append(PA_i1, myDict['unstimulated_macrophages_i1'])
        PA_i2 = np.append(PA_i2, myDict['unstimulated_macrophages_i2'])
        PA_i3 = np.append(PA_i3, myDict['unstimulated_macrophages_i3'])

        return (n_Exp_Ends,
                Name_Exp_3Ends,
                PA_IDs,
                PA_Starts,
                PA_Ends,
                PA_sitesCombined,
                PA_siteCount,
                PA_A1,
                PA_A1_Tot,
                PA_A2,
                PA_A2_Tot,
                PA_A3,
                PA_A3_Tot,
                PA_m1,
                PA_m1_Tot,
                PA_m2,
                PA_m2_Tot,
                PA_m3,
                PA_m3_Tot,
                PA_R1,
                PA_R1_Tot,
                PA_R2,
                PA_R2_Tot,
                PA_R3,
                PA_R3_Tot,
                PA_i1,
                PA_i1_Tot,
                PA_i2,
                PA_i2_Tot,
                PA_i3,
                PA_i3_Tot
                )
