#!/usr/bin/python
# coding: utf-8
"""Harvest TDD project data and perform calculations."""
# Python libraries
from __future__ import division


def Calculate_Rel_TDD(read_t_inh1_inh2,
                      read_t_inh1,
                      read_0_inh1):
    """Calculate relative TDD index."""
    tdd = ((read_t_inh1_inh2 - read_t_inh1) /
           (read_0_inh1 - read_t_inh1))

    return tdd


def Calculate_Abs_TDD(read_t_inh1_inh2,
                      read_t_inh1,
                      read_0_inh1):
    """Calculate absolute TDD index."""
    tdd = ((read_t_inh1_inh2 - read_t_inh1) /
           (read_0_inh1))

    return tdd


def Calculate_Rel_nonTDD(read_0_inh1,
                         read_t_inh1_inh2,
                         read_t_inh1):
    """Calculate relative nonTDD index."""
    nontdd = ((read_0_inh1 - read_t_inh1_inh2) /
              (read_0_inh1 - read_t_inh1))

    return nontdd


def Calculate_Abs_nonTDD(read_0_inh1,
                         read_t_inh1_inh2
                         ):
    """Calculate absolute nonTDD index."""
    nontdd = ((read_0_inh1 - read_t_inh1_inh2) /
              (read_0_inh1))

    return nontdd


def Calculate_Abs_nonDEG(read_t_inh1,
                         read_0_inh1
                         ):
    """Calculate absolute non Degradation index."""
    nondeg = read_t_inh1 / read_0_inh1

    return nondeg


def Degradation_fold(read_t,
                     read_0):
    """Calculate the fold of degradation."""
    # Use with t0 untreated or treated with Trip or DRB
    deg_fold = 1 - (read_t / read_0)

    return deg_fold
