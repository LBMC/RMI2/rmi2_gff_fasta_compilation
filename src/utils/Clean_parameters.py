#!/usr/bin/python
# coding: utf-8
"""Extract data from Emmanuel's database to merge with output .csv files."""
# Python libraries
import os
import glob
# Homemade utilities
from config import path_Output
from config import path_gff3, path_fa
from utils.Object_manipulator import Manipulator
from utils.time_stamp import string_time


def Cleaner():
    """Clean parameters that can be wrong before recalculation."""
    print('   #   ' + string_time() + '   #   Starting cleaning')
    keys = ['n4G_UTR5',
            'n4G_CDS',
            'n4G_UTR3',
            'n4G_total']

    # 1. Checking the specific .gff3 vs .fa output folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # If the specific output folder doesn't exist
    if os.path.exists(output_folder) is False:
        print('   #   ' + 'Expected input folder doesn t exist.')
        # Quit
        exit()

    manipulated_files = 0
    done = range(1000, 100000, 1000)
    for csv_file in glob.glob(output_folder + '*.csv'):
        f = Manipulator(csv_file)

        # Extract data only from stamped files
        manipulated_files += 1

        for key in keys:
            f._appendValue(key, 'NA')
        f._updateFile()
        if manipulated_files in done:
            message = '   #   ' + string_time() + '   #   '
            message += str(manipulated_files) + ' files have been cleaned'
            print(message)

    message = '   #   ' + string_time() + '   #   '
    message += str(manipulated_files) + ' files have been cleaned\n'
    print(message)


if __name__ == "__main__":
    # execute only if run as a script
    Cleaner()
