#!/usr/bin/python
# coding: utf-8
"""Create the csv file for the lincRNA."""
# Python libraries
from __future__ import division
import pandas as pd
import os
import numpy as np
from utils.time_stamp import message_time, string_time
from config import path_raw_tdd_linc
from config import path_gff3, path_fa
from config import path_Output


def create_files(log_file):
    """Create a csv file for all LincRNA."""
    message = message_time('Starting create_files.\n')
    print(message)
    log_file.write(message)

    # Output folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
    output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Database to gene_id -> gene_name conversion
    db_gene_id = pd.read_csv(path_Output + 'Gene_to_Transcript.csv')

    # TDD reads datababase
    db_tdd_linc = pd.read_csv(path_raw_tdd_linc)

    # get all unique gene_id of the lincRNA.
    lincRNAs = np.unique(db_tdd_linc['ensemblID'].values).tolist()
    message = message_time(str(len(lincRNAs)) + ' lincRNA will be created.\n')
    print(message)
    log_file.write(message)

    n_linc = 0
    done = range(1000, 100000, 1000)
    for linc in lincRNAs:
        # Path of the file to be created
        path_local_linc = output_folder + linc + '.csv'
        open(path_local_linc, 'w+').close()
        f = open(path_local_linc, 'w+')

        # create the line for the header of each manipulated file.
        header = '# ' + string_time() + ', created with '
        header += 'create_files\n'
        f.write(header)

        # gene_id
        gene_id = 'gene_id,' + linc + '\n'
        f.write(gene_id)

        # gene_name
        filter = db_gene_id['gene_id'] == linc
        name = db_gene_id['gene_name'][filter].values
        if len(name) == 0:
            name = 'None'
        else:
            name = name[0]
        gene_name = 'gene_name,' + name + '\n'
        f.write(gene_name)

        # Stamp the file with lincRNA.
        f.write('lincRNA,1\n')

        f.close()
        n_linc += 1

        if n_linc in done:
            message = message_time(str(n_linc) + ' files created.\n')
            print(message)
            log_file.write(message)

    message = message_time('End create_files.\n\n')
    print(message)
    log_file.write(message)
