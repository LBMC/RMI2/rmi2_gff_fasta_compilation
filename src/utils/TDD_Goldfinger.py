#!/usr/bin/python
# coding: utf-8
"""Attribute a TDD score to identify Gold transcript for all conditions."""
# Python libraries
import os
import numpy as np
import pandas as pd

from config import path_Output, path_gff3, path_fa
from config import list_QC_failed
from utils.time_stamp import message_time, string_time
from utils.Object_manipulator import Manipulator


def Pointer(log_file):
    """For each gene test all transcripts files."""
    message = message_time('Starting Pointer.\n')
    print(message)
    log_file.write(message)

    message = message_time('version ' + 'home 1.1' + ' 10:53\n.')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Open the gene to transcript_csv as dataframe
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    pd_genes_trans = pd.read_csv(conv_path)
    genes_list = np.unique(pd_genes_trans['gene_id'].values).tolist()

    # Loop to analyze all conditions
    cell_types = ['Lympho', 'Macro']
    cell_status = ['Activated', 'Resting']
    # As we initial filter permits all index or none we look just for Abs(TDD)
    prefix = 'Abs(TDD)>'
    transcription_inhibitors = ['Trip', 'DRB']
    translation_inhibitors = ['CHX', 'Harr']
    time_points = ['3h', '1h']

    # Create a dictionnary with all possible abs TDD values for
    # a specific cell type and status
    dict_to_check = {}
    (dict_to_check,
     keys_cond) = initiate_dictionary(dict_to_check,
                                      cell_types,
                                      cell_status,
                                      prefix,
                                      transcription_inhibitors,
                                      translation_inhibitors,
                                      time_points)

    # Create the file to list the rejected genes
    path_rejected = path_Output + string_time() + '_GoldFinger_rejected.csv'
    myCSV = open(path_rejected, 'w+')

    # Add header to file
    h = format_list_csv_line(list_QC_failed)
    myCSV.write(h)

    for c in cell_types:
        for s in cell_status:

            cond = c + '_'
            cond += s + '>'

            if cond not in keys_cond:
                # Condition not present -> bad architecture
                message = message_time('Condition ' + cond)
                message += ' is not supposed to exist.\n'
                print(message)
                break

            # Add condition to csv file
            myCSV.write(cond + '\n')

            message = message_time('attributing best transcript ')
            message += 'for ' + cond + '.\n'
            print(message)
            log_file.write(message)

            gene_done = 0
            done = range(1000,
                         100000,
                         1000)
            n_gold = 0
            n_unique_transcript = 0
            n_cannot_be_used = 0
            n_QC = 0
            n_PA = 0
            n_appris = 0
            n_length = 0
            n_non_resolve = 0

            for gene in genes_list:
                (n_gold,
                 n_unique_transcript,
                 n_cannot_be_used,
                 n_QC,
                 n_PA,
                 n_appris,
                 n_length,
                 n_non_resolve,
                 myCSV) = treat(gene,
                                pd_genes_trans,
                                cond,
                                dict_to_check,
                                input_folder,
                                n_gold,
                                n_unique_transcript,
                                n_cannot_be_used,
                                n_QC,
                                n_PA,
                                n_appris,
                                n_length,
                                n_non_resolve,
                                myCSV)
                gene_done += 1

                if gene_done in done:
                    message = message_time(str(gene_done))
                    message += ' genes treated.\n'
                    print(message)
                    log_file.write(message)

                    message = message_time(str(n_gold) + ' Gold.\n')
                    print(message)
                    log_file.write(message)


            message = message_time(str(gene_done))
            message += ' genes treated.\n'
            print(message)
            log_file.write(message)

            message = message_time(str(n_gold) + ' Gold.\n')
            print(message)
            log_file.write(message)

            message = message_time(str(n_unique_transcript))
            message += ' genes with unique transcript.\n'
            print(message)
            log_file.write(message)

            message = message_time(str(n_QC))
            message += ' genes requiring QC_validated filter.\n'
            print(message)
            log_file.write(message)

            message = message_time(str(n_PA))
            message += ' genes requiring pASeq filter.\n'
            print(message)
            log_file.write(message)

            message = message_time(str(n_appris))
            message += ' genes requiring n_appris filter.\n'
            print(message)
            log_file.write(message)

            message = message_time(str(n_length))
            message += ' genes requiring transcript length filter.\n'
            print(message)
            log_file.write(message)

            message = message_time(str(n_non_resolve))
            message += ' genes for which we cannot resolve trancript.'
            message += ' Arbitrary attribution to first one.\n'
            print(message)
            log_file.write(message)

            message = message_time(str(n_cannot_be_used))
            message += ' genes with no valid trancript.\n'
            print(message)
            log_file.write(message)

    myCSV.close()

    message = message_time('End Pointer.\n\n')
    print(message)
    log_file.write(message)


def format_list_csv_line(mylist):
    """Format a list content into a csv file line."""
    line = ''

    # Only the n-1 first elements are taken to add ','
    for element in range(0, len(mylist) - 1, 1):
        line += mylist[element] + ','

    # add last element
    line += mylist[len(mylist) - 1] + '\n'

    return line

def treat(gene,
          pd_genes_trans,
          cond,
          dict_to_check,
          input_folder,
          n_gold,
          n_unique_transcript,
          n_cannot_be_used,
          n_QC,
          n_PA,
          n_appris,
          n_length,
          n_non_resolve,
          myCSV):
    """Check if the gene has data for cond an attribute the best transcript."""
    # Get all transcripts for the current gene
    pd_trans = pd_genes_trans.loc[(pd_genes_trans['gene_id'] == gene)]
    list_trans = pd_trans['transcript_id'].values[0].split(';')

    # print(gene)

    # number of trated transcripts
    ntrans = 0

    new_keyword = cond + 'GOLD'

    le = len(list_trans)

    valids = []
    paSEQ = []
    length_transcripts = []
    lvl_appris = []
    key_pASeq_to_search = ('Score_PASeq_' +
                           cond.replace('>', ''))
    OK = False

    for t in list_trans:

        ntrans += 1
        # Open the file as a manipulator object
        f = Manipulator(input_folder + t + '.csv')

        # As all transcripts have already the same data
        # check in the first one if the wanted infos are present
        if ntrans == 1:

            values = f._getValue(dict_to_check[cond])
            if only(values, 'NA'):

                # Not an interesting gene for this condition
                break
            else:
                OK = True

        # Now try to find the best transcript
        QC = f._dict_values['QC_passed']
        if QC == '1' and le == 1:

            f._appendValue(new_keyword,
                           '1')
            f._updateFile()
            n_gold += 1
            n_unique_transcript += 1

            # Now abort to check another gene
            break

        v, p, lt, a = is_valid_stringent(f,
                                         key_pASeq_to_search)
        valids.append(v)
        paSEQ.append(p)
        length_transcripts.append(lt)
        lvl_appris.append(a)

    # More than one transcript and TDD values
    if le > 1 and OK:

        # Generate a dataframe
        df = pd.DataFrame()
        # print(len(list_trans))
        # print(len(valids))
        # print(len(paSEQ))
        # print(len(length_transcripts))
        df['transcript'] = list_trans
        df['Valid'] = valids
        df['paSEQ'] = paSEQ
        df['Length.transcript'] = length_transcripts
        df['appris'] = lvl_appris

        # print('Gene with several transcripts')
        # print(df)
        # raw_input('')

        # First filter on QC_validated
        pd_fltr1 = filter_df(df,
                             'Valid',
                             1)
        n_trans_QC = pd_fltr1.shape[0]

        # print('After filter_one')
        # print(pd_fltr1)
        # raw_input('')

        if  n_trans_QC == 1:
            # take transcript name
            transcript = pd_fltr1['transcript'].values[0]

            # print('Gold transcript')
            # print(transcript)
            # print('')

            udpate_transcript_file(transcript,
                                   input_folder,
                                   new_keyword,
                                   '1')
            n_gold += 1
            n_QC += 1

        elif n_trans_QC == 0:

            # No QC validated transcript for this gene
            # -> No use for the project
            n_cannot_be_used += 1

            # Add infos of all the transcripts to rejected file
            for t2 in list_trans:
                # Open the file as a manipulator object
                f2 = Manipulator(input_folder + t2 + '.csv')

                header = f2._header

                # Get values concerning QC_validation
                values = f2._getValue(list_QC_failed)
                """
                if 'In frame stop codon.' in header:
                    values[4] = '1'
                    print('In frame stop codon.')
                """

                # Format the value for the csv
                formated = format_list_csv_line(values)

                # Append to the rejected file
                myCSV.write(formated)


        elif n_trans_QC > 1:
            # QC is not enough to filter we should get QC == 1 and
            # the best pASeq score

            # Get the best pASeq Score

            scores = pd_fltr1['paSEQ'].values
            best_paSEQ = max(scores)

            # print('score PaSEQ')
            # print(scores)
            # print(best_paSEQ)

            # Get transcripts with only this value
            pd_fltr2 = filter_df(pd_fltr1,
                                 'paSEQ',
                                 best_paSEQ)

            n_trans_PA = pd_fltr2.shape[0]

            # print('After filter_two')
            # print(pd_fltr2)
            # raw_input('')

            if n_trans_PA == 1:
                # We found a single hit
                # take transcript name
                transcript = pd_fltr1['transcript'].values[0]

                # print('Gold transcript')
                # print(transcript)
                # print('')

                udpate_transcript_file(transcript,
                                       input_folder,
                                       new_keyword,
                                       '1')
                n_gold += 1
                n_PA += 1

            elif n_trans_PA > 1:
                # QC & PASeq are not enough to filter
                # use appris_level to filter Now

                # Get the best appris_level

                scores = pd_fltr2['appris'].values
                best_appris = min(scores)

                # print('Appris level')
                # print(scores)
                # print(best_appris)

                # Get transcripts with only this value
                pd_fltr3 = filter_df(pd_fltr2,
                                     'appris',
                                     best_appris)

                n_trans_appris = pd_fltr3.shape[0]

                # print('After filter_three')
                # print(pd_fltr3)
                # raw_input('')

                if n_trans_appris == 1:
                    # We found a single hit
                    # take transcript name
                    transcript = pd_fltr3['transcript'].values[0]

                    # print('Gold transcript')
                    # print(transcript)
                    # print('')

                    udpate_transcript_file(transcript,
                                           input_folder,
                                           new_keyword,
                                           '1')
                    n_gold += 1
                    n_appris += 1

                elif n_trans_appris >= 1:
                    # we need another filter -> length
                    # Get the longer transcript

                    scores = pd_fltr3['Length.transcript'].values
                    best_length = max(scores)

                    # print('Length.transcript')
                    # print(scores)
                    # print(best_length)

                    # Get transcripts with only this value
                    pd_fltr4 = filter_df(pd_fltr3,
                                         'Length.transcript',
                                         best_length)

                    n_trans_length = pd_fltr4.shape[0]

                    # print('After filter_four')
                    # print(pd_fltr4)
                    # raw_input('')

                    if n_trans_length == 1:
                        # We found a single hit
                        # take transcript name
                        transcript = pd_fltr4['transcript'].values[0]

                        # print('Gold transcript')
                        # print(transcript)
                        # print('')

                        udpate_transcript_file(transcript,
                                               input_folder,
                                               new_keyword,
                                               '1')
                        n_gold += 1
                        n_length += 1

                    else:
                        # print(gene + ' Cannot resolve')
                        # print(pd_fltr4)
                        # raw_input('')
                        transcript = pd_fltr4['transcript'].values[0]

                        # print('Gold transcript')
                        # print(transcript)
                        # print('')

                        udpate_transcript_file(transcript,
                                               input_folder,
                                               new_keyword,
                                               '1')

                        udpate_transcript_file(transcript,
                                               input_folder,
                                               'Trancript_duplication',
                                               '1')
                        n_gold += 1
                        n_non_resolve += 1

    return (n_gold,
            n_unique_transcript,
            n_cannot_be_used,
            n_QC,
            n_PA,
            n_appris,
            n_length,
            n_non_resolve,
            myCSV)


def filter_df(input_df,
              column,
              value):
    """Filter a dataframe on a specific value/column combination."""
    pd_output = input_df.loc[(input_df[column] == value)].copy()
    return pd_output


def is_valid_stringent(transcript,
                       key_PASeq):
    """Check if a Transcript file is valid with stringent parameters."""
    paseq = 0
    lt = 0

    # Get extra parameters
    if transcript._dict_values['QC_passed'] == '1':
        valid_QC = 1
    else:
        valid_QC = 0

    appris = transcript._dict_values['appris_level']
    if appris not in ['1', '2', '3', '4', '5']:
        appris = 1000
    else:
        appris = float(appris)


    if key_PASeq in transcript._key_list:
        paseq = transcript._dict_values[key_PASeq]
        paseq = float(paseq)

    if 'trans_length' in transcript._key_list:
        lt = transcript._dict_values['trans_length']
        lt = float(lt)

    return valid_QC, paseq, lt, appris


def udpate_transcript_file(transcript,
                           input_folder,
                           new_keyword,
                           value):
    """Update the hit transcript file."""
    f = Manipulator(input_folder + transcript + '.csv')
    f._appendValue(new_keyword,
                   '1')
    f._updateFile()


def initiate_dictionary(dict_to_check,
                        cell_types,
                        cell_status,
                        prefix,
                        transcription_inhibitors,
                        translation_inhibitors,
                        time_points):
    """Ceate a dictionnary with all possible abs TDD values."""
    # Generate all keyword
    keys_cond = []
    for c in cell_types:
        for s in cell_status:
            key = c + '_' + s + '>'
            keys_cond.append(key)

    # feed the dictionary
    for k in keys_cond:
        list_of_Index = []
        for inh1 in transcription_inhibitors:
            for inh2 in translation_inhibitors:
                for t in time_points:
                    cond = prefix
                    cond += k
                    cond += inh1 + '_' + inh2 + '>Ref_' + inh1 + '_0h>'
                    cond += t

                    # Feed the list of all index for one cell type/status
                    list_of_Index.append(cond)
        # Feed the dictionnary
        dict_to_check[k] = list_of_Index

    return (dict_to_check, keys_cond)


def only(mylist,
         value):
    """Determine if all elements in the list are equal to value."""
    res = False
    for i in mylist:
        if i != value:
            res = False
            break
        else:
            res = True
    return res


if __name__ == "__main__":
    # execute only if run as a script
    Pointer()
