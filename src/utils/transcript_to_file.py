#!/usr/bin/python
# coding: utf-8
"""Convert a transcript object into a csv file."""
# Python libraries
import numpy as np


def create_file(t,   # transcript object
                path_csv):
    """Create a csv file and write in it the key values of the transcript."""
    # Create and open the file:
    myCSV = open(path_csv, 'w+')

    # Add header
    myCSV.write(t._header)

    # Ensure we have no empty cell in the later table
    if t.Introns_3UTR_id == '':
        t.Introns_3UTR_id = np.nan
    if t.Introns_5UTR_id == '':
        t.Introns_5UTR_id = np.nan

    # Add general infos
    content = 'gene_id,' + t._gene_id + '\n'
    content += 'gene_name,' + t._gene_name + '\n'
    content += 'chr,' + t._chrom + '\n'
    content += 'trans_strand,' + t._trans_strand + '\n'
    content += 'appris_level,' + t._trans_level + '\n'
    content += 'QC_passed,' + str(t._QC) + '\n'

    content += 'In_frame_Stop_codon,' + t._inFrame_stop + '\n'
    content += 'N_in_sequence,' + t._Ns + '\n'
    content += 'CDS_too_Short,' + t._tooShort + '\n'
    content += 'Wrong_Kozack_Environment,' + t._WrongKozack + '\n'
    content += 'No_correct_Initiator_Codon,' + t._noATG + '\n'
    content += 'No_correct_Initiator_AA,' + t._Wrong_Initiator_AA + '\n'
    content += 'No_Stop_Codon,' + t._noStop + '\n'
    content += 'No_correct_Terminator_AA,' + t._Wrong_Terminator_AA + '\n'
    content += 'No_Reported_5UTR,' + t._noReported5UTR + '\n'
    content += 'No_Reported_3UTR,' + t._noReported3UTR + '\n'
    content += 'Wrong_Intron_Donor_Site,' + t._Wrong_Donor_Site + '\n'
    content += 'Wrong_Intron_Acceptor_Site,' + t._Wrong_Acceptor_Site + '\n'
    content += 'Part_of_Protein_sequence_unknown,' + t._Prot_seq_Incomplete + '\n'

    content += 'peptide_seq,' + t._Prot_seq + '\n'

    # Add transcrip infos
    content += 'transcript_id,' + t._trans_id + '\n'
    content += 'trans_start,' + str(t._trans_start) + '\n'
    content += 'trans_end,' + str(t._trans_end) + '\n'
    content += 'trans_length,' + t._trans_le + '\n'
    content += 'trans_seq,' + t._trans_seq + '\n'
    content += 'trans_A,' + t._trans_A + '\n'
    content += 'trans_T,' + t._trans_T + '\n'
    content += 'trans_G,' + t._trans_G + '\n'
    content += 'trans_C,' + t._trans_C + '\n'
    content += 'trans_N,' + t._trans_N + '\n'

    # Add mRNA infos
    content += 'mRNA_start,' + str(t._mRNA_start) + '\n'
    content += 'mRNA_end,' + str(t._mRNA_end) + '\n'
    content += 'mRNA_length,' + t._mRNA_le + '\n'
    content += 'mRNA_seq,' + t._mRNA_seq + '\n'
    content += 'mRNA_A,' + t._mRNA_A + '\n'
    content += 'mRNA_T,' + t._mRNA_T + '\n'
    content += 'mRNA_G,' + t._mRNA_G + '\n'
    content += 'mRNA_C,' + t._mRNA_C + '\n'
    content += 'mRNA_N,' + t._mRNA_N + '\n'

    # Add 5'UTR infos
    content += 'UTR5_start,' + t._5UTR_start + '\n'
    content += 'UTR5_end,' + t._5UTR_end + '\n'
    content += 'UTR5_length,' + t._5UTR_le + '\n'
    content += 'UTR5_seq,' + t._5UTR_seq + '\n'
    content += 'UTR5_A,' + t._5UTR_A + '\n'
    content += 'UTR5_T,' + t._5UTR_T + '\n'
    content += 'UTR5_G,' + t._5UTR_G + '\n'
    content += 'UTR5_C,' + t._5UTR_C + '\n'
    content += 'UTR5_N,' + t._5UTR_N + '\n'
    content += 'UTR5_50,' + t._UTR5_50 + '\n'

    # Add CDS infos
    content += 'CDS_start,' + t._CDS_start + '\n'
    content += 'Kozack_seq,' + t._Kozack_seq + '\n'
    content += 'Kozack_score_frequency,' + str(t._Kozack_sc_freq) + '\n'
    content += 'Kozack_score_efficiency,' + str(t._Kozack_sc_eff) + '\n'
    content += 'Kozack_score_dinucleotide,' + str(t._Kozack_sc_di) + '\n'
    content += 'CDS_end,' + t._CDS_end + '\n'
    content += 'CDS_length,' + t._CDS_le + '\n'
    content += 'CDS_seq,' + t._CDS_seq + '\n'
    content += 'CDS_A,' + t._CDS_A + '\n'
    content += 'CDS_T,' + t._CDS_T + '\n'
    content += 'CDS_G,' + t._CDS_G + '\n'
    content += 'CDS_C,' + t._CDS_C + '\n'
    content += 'CDS_N,' + t._CDS_N + '\n'

    # Add 3'UTR infos
    content += 'UTR3_start,' + t._3UTR_start + '\n'
    content += 'UTR3_end,' + t._3UTR_end + '\n'
    content += 'UTR3_length,' + t._3UTR_le + '\n'
    content += 'UTR3_seq,' + t._3UTR_seq + '\n'
    content += 'UTR3_A,' + t._3UTR_A + '\n'
    content += 'UTR3_T,' + t._3UTR_T + '\n'
    content += 'UTR3_G,' + t._3UTR_G + '\n'
    content += 'UTR3_C,' + t._3UTR_C + '\n'
    content += 'UTR3_N,' + t._3UTR_N + '\n'

    # 5'UTR fragments
    content += 'n_UTR5_fragment,' + str(t._n5UTRf) + '\n'
    content += 'n_introns_in_UTR5,' + str(t.nIntrons_5UTR) + '\n'
    content += 'introns_in_UTR5_id,' + str(t.Introns_5UTR_id) + '\n'
    for i in range(0, t._5UTRf_id.size, 1):
        content += 'UTR5_fragment_' + str(i+1) + '_id,' + t._5UTRf_id[i] + '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_start,' + t._5UTRf_start[i]
        content += '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_end,' + t._5UTRf_end[i]
        content += '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_len,' + t._5UTRf_le[i]
        content += '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_seq,' + t._5UTRf_seq[i]
        content += '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_A,' + t._5UTRf_A[i] + '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_T,' + t._5UTRf_T[i] + '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_G,' + t._5UTRf_G[i] + '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_C,' + t._5UTRf_C[i] + '\n'
        content += 'UTR5_fragment_' + str(i+1) + '_N,' + t._5UTRf_N[i] + '\n'

    # CDS fragments
    content += 'n_CDS_fragments,' + str(t._nCDSf) + '\n'
    for i in range(0, t._CDSf_id.size, 1):
        content += 'CDS_fragment_id,' + t._CDSf_id[i] + '\n'
        content += 'CDS_fragment_start,' + t._CDSf_start[i] + '\n'
        content += 'CDS_fragment_end,' + t._CDSf_end[i] + '\n'
        content += 'CDS_fragment_len,' + t._CDSf_le[i] + '\n'
        content += 'CDS_fragment_seq,' + t._CDSf_seq[i] + '\n'
        content += 'CDS_fragment_A,' + t._CDSf_A[i] + '\n'
        content += 'CDS_fragment_T,' + t._CDSf_T[i] + '\n'
        content += 'CDS_fragment_G,' + t._CDSf_G[i] + '\n'
        content += 'CDS_fragment_C,' + t._CDSf_C[i] + '\n'
        content += 'CDS_fragment_N,' + t._CDSf_N[i] + '\n'

    # 3'UTR fragments
    content += 'n_UTR3_fragment,' + str(t._n3UTRf) + '\n'
    content += 'n_introns_in_UTR3,' + str(t.nIntrons_3UTR) + '\n'
    content += 'introns_in_UTR3_id,' + str(t.Introns_3UTR_id) + '\n'
    for i in range(0, t._3UTRf_id.size, 1):
        content += 'UTR3_fragment_' + str(i+1) + '_id,' + t._3UTRf_id[i] + '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_start,' + t._3UTRf_start[i]
        content += '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_end,' + t._3UTRf_end[i]
        content += '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_len,' + t._3UTRf_le[i]
        content += '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_seq,' + t._3UTRf_seq[i]
        content += '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_A,' + t._3UTRf_A[i] + '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_T,' + t._3UTRf_T[i] + '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_G,' + t._3UTRf_G[i] + '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_C,' + t._3UTRf_C[i] + '\n'
        content += 'UTR3_fragment_' + str(i+1) + '_N,' + t._3UTRf_N[i] + '\n'

    # Add Exons infos
    content += 'n_exon,' + str(t._nExons) + '\n'

    for i in range(0, t._Exon_id.size, 1):
        content += 'exon_' + str(i+1) + '_id,' + t._Exon_id[i] + '\n'
        content += 'exon_' + str(i+1) + '_start,' + t._Exon_start[i] + '\n'
        content += 'exon_' + str(i+1) + '_end,' + t._Exon_end[i] + '\n'
        content += 'exon_' + str(i+1) + '_len,' + t._Exon_le[i] + '\n'
        content += 'exon_' + str(i+1) + '_seq,' + t._Exon_seq[i] + '\n'
        content += 'exon_' + str(i+1) + '_A,' + t._Exon_A[i] + '\n'
        content += 'exon_' + str(i+1) + '_T,' + t._Exon_T[i] + '\n'
        content += 'exon_' + str(i+1) + '_G,' + t._Exon_G[i] + '\n'
        content += 'exon_' + str(i+1) + '_C,' + t._Exon_C[i] + '\n'
        content += 'exon_' + str(i+1) + '_N,' + t._Exon_N[i] + '\n'

    # Add Introns infos
    content += 'n_intron,' + str(t._nIntrons) + '\n'

    for i in range(0, t._Intron_id.size, 1):
        content += 'intron_' + str(i+1) + '_id,' + t._Intron_id[i] + '\n'
        content += 'intron_' + str(i+1) + '_start,' + t._Intron_start[i] + '\n'
        content += 'intron_' + str(i+1) + '_end,' + t._Intron_end[i] + '\n'
        content += 'intron_' + str(i+1) + '_len,' + t._Intron_le[i] + '\n'
        content += 'intron_' + str(i+1) + '_seq,' + t._Intron_seq[i] + '\n'
        content += 'intron_' + str(i+1) + '_A,' + t._Intron_A[i] + '\n'
        content += 'intron_' + str(i+1) + '_T,' + t._Intron_T[i] + '\n'
        content += 'intron_' + str(i+1) + '_G,' + t._Intron_G[i] + '\n'
        content += 'intron_' + str(i+1) + '_C,' + t._Intron_C[i] + '\n'
        content += 'intron_' + str(i+1) + '_N,' + t._Intron_N[i] + '\n'

    myCSV.write(content)
    del t
