#!/usr/bin/python
# coding: utf-8
"""Object dedicated to read m6ASeq results and update files."""
# Python libraries
from __future__ import division
from utils.time_stamp import string_time
import numpy as np


class Shredder_m6ASeq:
    """This object is generated from a specific database.

    It resume all lines and their properties.
    The PASeq values are transmitted to a PASeq object to facilitate the
    data transfert to the transcript .csv files
    """

    def __init__(self,
                 path,
                 separator
                 ):
        """Construct method."""
        self._separator = separator
        self._path = path
        self._dict_values = {}
        self._List_Gene_Name = []

        # The dictionary will created like this:
        # self._dict_values['Transcript_ID', '2nd_Key']
        # 2nd_Keys are the header column names

        # Reading the file
        print('   #   ' + string_time() + '   #   Shredding csv database')
        self._read_file()

    def _read_file(self):
        """Read the file and feed the dictionary."""
        # line counter
        nlines = 0

        # initialize all variables
        (n_Exp_Ends,
         Name_Exp_5Ends,
         m6A_Starts,
         m6A_Ends,
         m6A_scores,
         total_score) = self._new_Gene()

        line_dict = {}

        with open(self._path) as database:
            # Read the firstline
            line = database.readline()

            # Read all lines
            while line:
                nlines += 1
                line = self._clean_line(line)

                """# debug mode
                if len(self._List_Gene_Name) == 2:
                    break"""

                # Decide if line belongs to header or describe a value
                if nlines == 1:
                    print('   #   ' + string_time() + '   #   Reading header')
                    # feed the header
                    self._header = line.split(self._separator)
                    # Initialize the Curr_Gene
                    Curr_Gene = ''

                else:
                    # Remove breakline and split the line between key and value
                    frags = line.split(self._separator)

                    # Get Gene Name from fragment 19 and
                    # Determine if it is a new valid Gene
                    gene_of_line = frags[0].split(';')[1]
                    if Curr_Gene not in (gene_of_line, ''):
                        # Update the dictionnary for the last entry
                        self._dict_values[Curr_Gene] = (n_Exp_Ends,
                                                        Name_Exp_5Ends,
                                                        m6A_Starts,
                                                        m6A_Ends,
                                                        m6A_scores,
                                                        total_score)

                        # Update list name
                        if gene_of_line in self._List_Gene_Name:
                            message = '   #   ' + string_time() + '   #   '
                            message += gene_of_line + ' is Duplicated'
                        self._List_Gene_Name = np.append(self._List_Gene_Name,
                                                         gene_of_line)
                        # Update Curr_Gene
                        Curr_Gene = gene_of_line

                        # Reset variables
                        (n_Exp_Ends,
                         Name_Exp_5Ends,
                         m6A_Starts,
                         m6A_Ends,
                         m6A_scores,
                         total_score) = self._new_Gene()

                        # Retrieve values from the line and store them in
                        # line_dict = {}

                        line_dict = {}
                        for key in range(0, len(self._header), 1):
                            line_dict[self._header[key]] = frags[key]

                        # Update variables
                        (n_Exp_Ends,
                         Name_Exp_5Ends,
                         m6A_Starts,
                         m6A_Ends,
                         m6A_scores,
                         total_score) = self._update_Gene(line_dict,
                                                          n_Exp_Ends,
                                                          Name_Exp_5Ends,
                                                          m6A_Starts,
                                                          m6A_Ends,
                                                          m6A_scores,
                                                          total_score)

                    elif Curr_Gene == '':
                        # Create the first Gene
                        gene_of_line = frags[0].split(';')[1]
                        self._List_Gene_Name = np.append(self._List_Gene_Name,
                                                         gene_of_line)
                        # Update Curr_Gene
                        Curr_Gene = gene_of_line

                        # Reset variables
                        (n_Exp_Ends,
                         Name_Exp_5Ends,
                         m6A_Starts,
                         m6A_Ends,
                         m6A_scores,
                         total_score) = self._new_Gene()

                        # Retrieve values from the line and store them in
                        # line_dict = {}

                        line_dict = {}
                        for key in range(0, len(self._header), 1):
                            line_dict[self._header[key]] = frags[key]

                        # Update variables
                        (n_Exp_Ends,
                         Name_Exp_5Ends,
                         m6A_Starts,
                         m6A_Ends,
                         m6A_scores,
                         total_score) = self._update_Gene(line_dict,
                                                          n_Exp_Ends,
                                                          Name_Exp_5Ends,
                                                          m6A_Starts,
                                                          m6A_Ends,
                                                          m6A_scores,
                                                          total_score)
                    elif Curr_Gene == gene_of_line:
                        # Update the current gene

                        # Retrieve values from the line and store them in
                        # line_dict = {}

                        line_dict = {}
                        for key in range(0, len(self._header), 1):
                            line_dict[self._header[key]] = frags[key]

                        # Update variables
                        (n_Exp_Ends,
                         Name_Exp_5Ends,
                         m6A_Starts,
                         m6A_Ends,
                         m6A_scores,
                         total_score) = self._update_Gene(line_dict,
                                                          n_Exp_Ends,
                                                          Name_Exp_5Ends,
                                                          m6A_Starts,
                                                          m6A_Ends,
                                                          m6A_scores,
                                                          total_score)
                # read new line
                line = database.readline()

        # Update the dictionnary for the last entry
        self._dict_values[Curr_Gene] = (n_Exp_Ends,
                                        Name_Exp_5Ends,
                                        m6A_Starts,
                                        m6A_Ends,
                                        m6A_scores,
                                        total_score)

        print('   #   ' + string_time() + '   #   Table has: ' + str(nlines))

    def _clean_line(self, line):
        """Remove unwanted characters from line."""
        unwanted = ['"', '\n']
        for char in unwanted:
            line = line.replace(char, '')
        return line

    def _new_Gene(self):
        """Reset all key variables for a new gene."""
        n_Exp_Ends = 0
        Name_Exp_5Ends = []
        m6A_Starts = []
        m6A_Ends = []
        m6A_scores = []
        total_score = 0

        return (n_Exp_Ends,
                Name_Exp_5Ends,
                m6A_Starts,
                m6A_Ends,
                m6A_scores,
                total_score)

    def _update_Gene(self,
                     myDict,
                     n_Exp_Ends,
                     Name_Exp_5Ends,
                     m6A_Starts,
                     m6A_Ends,
                     m6A_scores,
                     total_score
                     ):
        """Update all key variables for a new gene."""
        n_Exp_Ends += 1
        Name_Exp_5Ends = np.append(Name_Exp_5Ends,
                                   'm6ASeq_Peak_' + str(n_Exp_Ends))
        m6A_Starts = np.append(m6A_Starts,
                               myDict['peakStart'])
        m6A_Ends = np.append(m6A_Ends,
                             myDict['peakEnd'])
        m6A_scores = np.append(m6A_scores,
                               myDict['score'])
        total_score += float(myDict['score'])

        return (n_Exp_Ends,
                Name_Exp_5Ends,
                m6A_Starts,
                m6A_Ends,
                m6A_scores,
                total_score)
