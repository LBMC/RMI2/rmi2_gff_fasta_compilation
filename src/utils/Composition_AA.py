#!/usr/bin/python
# coding: utf-8
"""Analyze a peptide sequence and return the Amino Acid Composition."""
# Python libraries
from __future__ import division


def AA(manipulator_object):
    """Perform the analysis."""
    # List keys
    list_keys = ['ALA', 'ARG', 'ASN', 'ASP', 'CYS',
                 'GLU', 'GLN', 'GLY', 'HIS', 'ILE',
                 'LEU', 'LYS', 'MET', 'PHE', 'PRO',
                 'SER', 'THR', 'TRP', 'TYR', 'VAL']
    list_AA = ['A', 'R', 'N', 'D', 'C',
               'E', 'Q', 'G', 'H', 'I',
               'L', 'K', 'M', 'F', 'P',
               'S', 'T', 'W', 'Y', 'V']

    # Get the sequence
    sequence = manipulator_object._getValue(['peptide_seq'])[0]
    len_seq = len(sequence) - 1

    # Analyse the sequence without counting stop codon
    for aa in range(0, len(list_AA), 1):
        current_Key = 'percent_' + list_keys[aa] + '_' + list_AA[aa]
        percent = str(round(100 * sequence.count(list_AA[aa]) / len_seq, 2))

        # Add or correct the value in the manipulator object
        if current_Key in manipulator_object._key_list:
            manipulator_object._changeValue(current_Key,
                                            percent)
        else:
            manipulator_object._appendValue(current_Key,
                                            percent)
