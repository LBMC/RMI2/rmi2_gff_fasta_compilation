#! /usr/bin/python3
# coding: utf-8
"""Get codon frequency for all ORF."""

# Python libraries
import os
import glob
from collections import Counter
import pandas as pd
import numpy as np
from multiprocess import Pool
import time

# Homemade utilities
from config import data_to_extract
from config import path_Output
from config import path_gff3, path_fa, path_Values
from utils.Object_manipulator import Manipulator
from utils.time_stamp import message_time, string_time



class add_codon_frequency_to_csvs:

    def __init__(self) -> None:
    
        # Checking the specific .gff3 vs .fa output folder
        gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
        fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
        output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep
        
        self.go_folder = '/home/dcluet/Bureau/GOs/'
     
        self.done = list(range(100, 100000, 100))
        self.done2 = list(range(1000, 100000, 1000))
        
        self.dict_codons = self._create_dict_codons()
        self.keys = list(self.dict_codons.keys())
        
        list_columns = list(np.append(self.keys,
                                      ['Ribosomes Density',
                                       "3' UTR length",
                                       'TDD value',
                                       'TID value']))
        
        self.header = self._make_csv_line(list_columns)
        print(self.header)
        list_csv = glob.glob(output_folder + '*.csv')
        self.manipulated_files = 0
        self.total = 0
                
        with Pool(5) as pool:
            pool.map(self._analyze_csv, list_csv)
        
        message = message_time(str(self.manipulated_files))
        message += f' files have been manipulated  {len(list_csv)}.\n'
        print(message)
        
        
        message = message_time(str(self.total))
        message += ' files have been tested.\n'
        print(message)

    def _analyze_csv(self,
                     csv_file: str) -> None:
        """Analyze codon frequency in the current transcript csv file and add 
            these infos into the GO csv files

        Args:
            csv_file (str): Path to the transcript csv file
        """        
        # Create a blank copy of the codon dictionary
        codons = self.dict_codons.copy()

        f = Manipulator(csv_file)
        self.total += 1
        # Extract data only from stamped files
        values = f._getValue(['gene_name',
                              'CDS_seq',
                              'GO_Term',
                              'appris_level',
                              'QC_passed',
                              'RiboDens>Lympho_Resting',
                              'UTR3_length',
                              'Abs(TDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h',
                              'Abs(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h'])
        codons['gene_name'] = values[0]
        CDS = values[1]
        list_GOs = values[2].split(';')
        appris = values[3]
        qc = values[4]
        ribodens = clean_value(values[5])
        utr3 = clean_value(values[6])
        tdd = clean_value(values[7])
        tid = clean_value(values[8])
        
        if CDS != 'NA':
            new_values = []
            codons = self._get_frequency(CDS,
                                            codons)
            # Add the new values in the file and update it
            for key in self.keys:
                new_values.append(codons[key])
                if key != 'gene_name':
                    f._appendValue(key, codons[key])
            f._updateFile()
            self.manipulated_files += 1
            
            # Add to GO csv files only if appris level is 1 and QC passed
            if (str(appris) == '1') and (str(qc) == '1'):
                list_values = list(np.append(new_values,
                                             [ribodens,
                                              utr3,
                                              tdd,
                                              tid]))
                line = self._make_csv_line(list_values)
                for GO in list_GOs:             
                    file_go = os.path.join(self.go_folder,
                                        f"{GO}_codons_freq.csv")
                    
                    # Create and update go csv file
                    if not os.path.exists(file_go):
                        with open(file_go, 'a+') as writer:
                            writer.write(self.header)
                            
                    with open(file_go, 'a') as writer:
                        writer.write(line)
                        
        if self.manipulated_files in self.done:
            self.done.remove(self.manipulated_files)
            message = message_time(str(self.manipulated_files))
            message += ' files have been manipulated.\n'
            print(message)
        
        
        if self.total in self.done2:
            self.done2.remove(self.total)
            message = message_time(str(self.total))
            message += ' files have been tested.\n'
            print(message)
            


    def _make_csv_line(self,
                       values: list) -> str:
        """Generate a csv formated line from list

        Args:
            values (list): list of values (mixed types)

        Returns:
            str: csv formated line with newline break
        """
        res = ''
        for idx in range(0, len(values) - 1, 1):
            res += f"{values[idx]},"
        res += f"{values[len(values) - 1]}\n"
        
        return res
        

    def _get_frequency(self,
                       CDS: str,
                       codons: dict) -> dict:
        """Analyze CDS and return codon -> frequency dictionary

        Args:
            CDS (str): Coding sequence
            codons (dict): codon -> nan dictionary

        Returns:
            dict: codon -> frequency
        """
        list_codons = []
        
        length_CDS = len(CDS) 
        length_prot = len(CDS) / 3
        
        # Get all codons
        for codon in range(0, length_CDS, 3):
            list_codons.append(CDS[codon: codon + 3])
        
        # gat fast counts
        counts = Counter(list_codons)

        
        # update dictionary with frequency
        for codon in codons:
            if codon != 'gene_name':
                codons[codon] = round(counts.get(codon, 0) / length_prot,
                                    5)
        
        return codons

    def _create_dict_codons(self) -> dict:
        """Generate a codon -> 0 frequency dictionary

        Returns:
            dict: 3 letters codon -> 0 value dict
        """
        nucleotides = ['A', 'T', 'G', 'C']
        dict_codons = {}
        dict_codons['gene_name'] = np.nan
        for first in nucleotides:
            for second in nucleotides:
                for third in nucleotides:
                    dict_codons[first + second + third] = 0
        return dict_codons
    
def clean_value(value: float) -> float:
    """Clean Values from NA or Inf

    Args:
        value (float): value possibly being NA or Inf

    Returns:
        float: value or np.nan
    """    
    if value in ['NA', 'Inf']:
        value = np.nan
    
    return value