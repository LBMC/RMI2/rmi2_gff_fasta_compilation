#!/usr/bin/python
# coding: utf-8
"""Main pipeline script."""
# Python libraries
import os
import time
import numpy as np

# Home made scripts and packages
from config import debug
from config import path_gff3, path_db, path_fa, path_Output
from utils.time_stamp import string_time, message_time
from utils.create_database import create_db
from utils.Object_transcript import Transcript
from utils.transcript_to_file import create_file
from utils.quality_control_transcript import QC
from utils.get_intron import add_intron


def CDS_scanner_Juliana(log_file):
    """Execute main script."""
    print(message_time('Parsing CDS'))
    log_file.write(message_time('Starting CDS_scanner.\n'))

    # Creating the database
    database = create_db(path_gff3,
                         path_db,
                         log_file)

    # Accessing to all CDS and generate a transcript object.
    log_file.write(message_time('Starting exploring the database for CDS\n'))

    # Creating the Transcript objects
    # The generator has to be done again (consumed by the sum)
    message_time('Analysing all the CDS:')
    log_file.write(message_time('Analysing all the CDS:\n'))

    # Create _header for all files
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
    output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep
    header = '# ' + string_time() + ', created from '
    header += gff3_name + '_VS_' + fasta_name + '\n'

    # Initialisation of the dictonnary of transcripts
    dict_Transcripts = {}

    # Progress list
    progress = np.arange(10000, 1000000, 10000).tolist()

    # Find all coding transcript
    dict_Transcripts = Find_CDS(database,
                                dict_Transcripts,
                                header,
                                output_folder,
                                progress,
                                log_file)

    # Performing Quality Control
    Quality_Control(dict_Transcripts,
                    output_folder,
                    log_file,
                    progress)

    # Delete dictionnary
    del(dict_Transcripts)

    # Inform the user that analysis is over
    print(message_time('CDS_scanner is over.\n'))
    log_file.write(message_time('End CDS_scanner.\n\n'))


def Quality_Control(dict_Transcripts,
                    output_folder,
                    log_file,
                    progress):
    """Perform basic quality control on all CDS."""
    failed_QC = 0
    message = message_time('Starting Quality Control.\n')
    log_file.write(message)
    print(message)

    index = 0
    for key in dict_Transcripts:
        current_t = dict_Transcripts[key]

        # Perform Quality quality control
        QC(current_t)

        # Create file only if QC is OK
        if current_t._pb != '':
            failed_QC += 1
            current_t._QC = str(0)
        else:
            current_t._QC = str(1)

        # Generate the output file
        create_file(current_t,
                    output_folder + current_t._trans_id[:18] + '.csv')
        index += 1
        if index in progress:
            message = message_time(str(index) + ' Transcript files created.\n')
            print(message)
            log_file.write(message)

        del current_t

    message = message_time(str(index) + ' Transcript files created.\n')
    print(message)
    log_file.write(message)

    # Closing the log_file
    message = '\n' + str(index) + ' single CDS were analysed.\n'
    message += str(failed_QC) + ' failed QC.\n\n'
    message += message_time('End of the CDS quality control.\n')
    log_file.write(message)


def Add_Introns(database,
                dict_Transcripts,
                progress,
                log_file):
    """Add Introns to each transcript object."""
    message = message_time('Starting adding introns.\n')
    log_file.write(message)
    print(message)

    index = 0
    for intron in database.create_introns():
        trans_name = intron.attributes['transcript_id'][0][:18]
        if trans_name in dict_Transcripts:
            local_transcript = dict_Transcripts[trans_name]

            dict_Transcripts[trans_name] = add_intron(intron,
                                                      local_transcript,
                                                      path_fa,
                                                      wanteDebug=debug)
            index += 1
            if index in progress:
                message = message_time(str(index) + ' Introns added.\n')
                print(message)
                log_file.write(message)

    message = message_time(str(index) + ' Introns added.\n')
    print(message)
    log_file.write(message)

    message = message_time('End of introns incorporation.\n')
    log_file.write(message)


def Find_CDS(database,
             dict_Transcripts,
             header,
             output_folder,
             progress,
             log_file):
    """Find all coding sequences and associate them to a transcript object."""
    index = 0
    for cds in database.features_of_type('CDS',
                                         order_by='start'):
        # CDS are fragmented in the database.
        # To avoid re-calculation, we check if the associated transcript
        # has already be done.
        time.sleep(0)
        trans_name = cds.attributes['transcript_id'][0][:18]

        if trans_name not in dict_Transcripts:
            index += 1
            # print(str(index))
            dict_Transcripts[trans_name] = Transcript(header,
                                                      cds,
                                                      database,
                                                      path_fa,
                                                      output_folder,
                                                      debug)
            if index in progress:
                message = message_time('')
                message += str(index) + ' Transcript objects created.\n'
                print(message)
                log_file.write(message)

    message = message_time('')
    message += str(index) + ' Transcript objects created.\n'
    print(message)
    log_file.write(message)

    return dict_Transcripts


if __name__ == "__main__":
    # execute only if run as a script
    CDS_scanner()
