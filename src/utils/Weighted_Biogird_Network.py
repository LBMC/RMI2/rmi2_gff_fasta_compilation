#!/usr/bin/python
# coding: utf-8
"""Extract all usable interactions partner to create a cytoscape ntwk."""
# Python libraries
from __future__ import division
import os
import numpy as np
import pandas as pd
from config import path_Output, path_UNIPROT_to_ENSEMBL, path_UNIPROT_to_ENTREZ
from config import path_gff3, path_fa, path_Values
from utils.time_stamp import message_time
from utils.Object_manipulator import Manipulator
from utils.Distributions_GO import clean_name
from config import max_cap, min_cap
from config import path_Interactions


def create_weighted_network(index,
                            log_file):
    """Get all required infos concerning all curated biogrid interactions."""
    message = message_time('Starting create_weighted_network.\n')
    print(message)
    log_file.write(message)

    # Input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Get the mean of the transcripts population with capping values:
    # (min_cap, max_cap)
    mymean = get_mean(index, log_file)

    # Get the dictionary converting an uniprot to index value or gene_name
    (index_of,
     gene_name_of) = from_UNIPROT_to_transcript(index,
                                                input_folder,
                                                mymean,
                                                log_file)

    # Get the dictionary converting an entrez with
    # uniprot reference [0]
    # gene name [1]
    # delta index from mean [2]
    # Only for valid values (dropna in from_UNIPROT_to_transcript)
    (uniprot_of,
     entrez_of) = from_ENTREZ_to_UNIPROT(log_file,
                                         index_of,
                                         gene_name_of)

    # Prepare the csv file
    cytoscape_header = 'Source,Interaction,Target,TDD1,Type,Edge\n'
    csv = cytoscape_header
    csv += 'LOW' + ',,,' + str(min_cap) + ',complex,' + str(200) + '\n'
    csv += 'HIGH' + ',,,' + str(max_cap) + ',complex,' + str(200) + '\n'

    # Create the csv file for cytoscape with weighted interactions
    make_network(uniprot_of,
                 entrez_of,
                 csv,
                 log_file)

    message = message_time('End create_weighted_network.\n\n')
    print(message)
    log_file.write(message)


def make_network(uniprot_of,
                 entrez_of,
                 csv,
                 log_file):
    """Extract all proteins involved in interactions AND having an index."""
    message = message_time('Starting make_network.\n')
    print(message)
    log_file.write(message)

    # load the curated biogrid database
    ppi_pd = pd.read_csv(path_Interactions)

    # Reduce the complexity
    ppi_pd = ppi_pd[['Entrez Gene Interactor A',
                     'Entrez Gene Interactor B']]

    # Get a list of unique protein enties
    unique_prots = []
    unique_prots = np.append(unique_prots,
                             ppi_pd['Entrez Gene Interactor A'].values)
    unique_prots = np.append(unique_prots,
                             ppi_pd['Entrez Gene Interactor B'].values)
    unique_prots = np.unique(unique_prots).tolist()

    # Extract only entries that have an index value
    valid_prots = uniprot_of.keys()
    exploitable = []
    for prot in unique_prots:
        if prot in valid_prots:
            exploitable.append(prot)

    # Simplify the panda dataframe by keeping only rows with
    # Proteins having an index
    filterA = ppi_pd['Entrez Gene Interactor A'].isin(exploitable)
    filterB = ppi_pd['Entrez Gene Interactor B'].isin(exploitable)
    ppi_filterA = ppi_pd[filterA]
    ppi_validated = ppi_filterA[filterB]

    # List of proteins involved in a dual validated interaction
    exploitable_2 = []
    exploitable_2 = np.append(exploitable_2,
                              ppi_validated['Entrez Gene Interactor A'].values)
    exploitable_2 = np.append(exploitable_2,
                              ppi_validated['Entrez Gene Interactor B'].values)
    exploitable_2 = np.unique(exploitable_2).tolist()

    # Create the connection to LOW or HIGH.
    # This ensure to have at least one time each protein in the First
    # Column for the display using the TDD delta rendering.
    for prot in exploitable_2:
        lcl = str(uniprot_of[prot][1]).upper() + ','
        delta = uniprot_of[prot][2]
        lcl += 'direct' + ','
        if delta > 0:
            lcl += 'HIGH,'
        elif delta < 0:
            lcl += 'LOW,'
        lcl += str(delta) + ',protein,' + str(round(1000*delta)) + '\n'
        # transfert to the temporary csv variable
        csv += lcl

    # Now for each interaction the interaction column is feeded with the
    # std of the 2 delta. More homogeneous -> closer
    n_rows = ppi_validated.shape[0]
    print(n_rows)
    for row in range(0, n_rows, 1):
        lcl_df = ppi_validated.iloc[row, :]
        A = lcl_df['Entrez Gene Interactor A']
        B = lcl_df['Entrez Gene Interactor B']
        name_A = str(uniprot_of[A][1]).upper()
        delta_A = uniprot_of[A][2]
        name_B = str(uniprot_of[B][1]).upper()
        delta_B = uniprot_of[B][2]
        mean_AB = abs(np.std([delta_A, delta_B]))
        lcl = name_A + ',' + 'direct' + ',' + name_B + ','
        lcl += str(delta_A) + ',protein,' + str(round(1000*mean_AB)) + '\n'
        csv += lcl

    # Create the csv file
    path = path_Output + 'Weighted_Cytoscape.csv'
    f = open(path, 'w+')
    f.write(csv)
    f.close()

    message = message_time('End make_network.\n\n')
    print(message)
    log_file.write(message)


def from_ENTREZ_to_UNIPROT(log_file,
                           index_of,
                           gene_name_of):
    """Generate a dict of entrez entries with uniprot reference as value."""
    message = message_time('Starting from_ENTREZ_to_UNIPROT.\n')
    print(message)
    log_file.write(message)

    dict = {}
    entrez_of = {}

    # load the entrez to uniprot database
    pd_entrez = pd.read_csv(path_UNIPROT_to_ENTREZ)
    list_entrez_id = pd_entrez['ENTREZ'].values

    # as an entrez gene can have various isoform proteins (some duplications)
    # get only unique entries
    list_entrez_id = np.unique(list_entrez_id).tolist()
    print(str(len(list_entrez_id)) + ' entries.')
    # Feed dictionaries
    n_missing = 0
    for entrez in list_entrez_id:
        filter1 = pd_entrez['ENTREZ'] == entrez
        # we check possible unprot of an entry
        uniprots = pd_entrez['UNIPROT'][filter1].values
        found = False
        for uniprot in uniprots:
            if uniprot in index_of.keys():
                dict[entrez] = (uniprot,
                                gene_name_of[uniprot],
                                index_of[uniprot])
                entrez_of[uniprot] = entrez
                found = True
                break

        if not found:
            n_missing += 1
            print(str(entrez) + ' is missing')
    print(str(n_missing) + ' missing correspondances.')

    message = message_time('End from_ENTREZ_to_UNIPROT.\n\n')
    print(message)
    log_file.write(message)

    return dict, entrez_of


def from_UNIPROT_to_transcript(index,
                               input_folder,
                               mymean,
                               log_file):
    """Create a dictionary of UNIPROT key with ENSEMBL transcript value."""
    message = message_time('Starting from_UNIPROT_to_transcript.\n')
    print(message)
    log_file.write(message)

    dict = {}
    dict_genename = {}
    # Load the gene to transcript database
    pd_genes_trans = pd.read_csv(path_Output + 'Gene_to_Transcript.csv')
    list_gene_id = pd_genes_trans['gene_id'].values

    # Load the UNIPROT to ENSEMBL gene id
    pd_uniprot_gene = pd.read_csv(path_UNIPROT_to_ENSEMBL)
    list_uniprot_id = pd_uniprot_gene['From'].values

    # Feed dictionaries
    for uniprot in list_uniprot_id:
        filter1 = pd_uniprot_gene['From'] == uniprot
        ensembl_gene = pd_uniprot_gene['To'][filter1].values[0]

        if ensembl_gene in list_gene_id:
            filter2 = pd_genes_trans['gene_id'] == ensembl_gene
            transcript_list = pd_genes_trans['transcript_id'][filter2].values
            name = pd_genes_trans['gene_name'][filter2].values[0]

            # Get the first transcript file
            # They all have the indexes
            transcript = transcript_list[0].split(';')[0]
            f = Manipulator(input_folder + transcript + '.csv')
            value = f._getValue([index])[0]

            # Feed the capped values in dictionary only for non NA values
            if value != 'NA':
                value = float(value)
                if value >= max_cap:
                    value = max_cap
                elif value <= min_cap:
                    value = min_cap
                value = value - mymean
                dict[uniprot] = value
                dict_genename[uniprot] = name

        else:
            print(uniprot + ', ' + ensembl_gene + ' is missing.')

    message = message_time('Dictionary contains ')
    message += str(len(dict.keys())) + ' enties.\n'
    print(message)
    log_file.write(message)

    message = message_time('End from_UNIPROT_to_transcript.\n\n')
    print(message)
    log_file.write(message)

    return (dict, dict_genename)


def get_mean(index,
             log_file):
    """Get the mean value for the specific index to calculate delta."""
    message = message_time('Starting get_mean.\n')
    print(message)
    log_file.write(message)

    # Open the database values as a shredder dictionnary
    db = pd.read_csv(path_Values)

    # Initialize all required variables
    db = db[index]

    # Drop all 'NA'
    db.dropna(inplace=True)

    # Harvest valid values
    raw_TDD = db.values

    message = message_time('Impact of capping:\n')
    mean_before = str(np.mean(raw_TDD))
    message += 'Mean before capping: ' + mean_before + '\n'

    # Apply capping
    cap_TDD = np.where(raw_TDD < min_cap,   # Condition
                       min_cap,   # Value if true
                       raw_TDD   # Value if false
                       )
    cap_TDD = np.where(raw_TDD > max_cap,   # Cndition
                       max_cap,   # Value if true
                       raw_TDD   # Value if false
                       )

    # Calculate the wanted mean
    wanted_mean = np.mean(cap_TDD)
    mean_after = str(wanted_mean)
    message += 'Mean after capping: ' + mean_after + '\n'
    print(message)
    log_file.write(message)

    message = message_time('End get_mean.\n\n')
    print(message)
    log_file.write(message)

    return wanted_mean
