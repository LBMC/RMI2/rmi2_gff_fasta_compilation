#!/usr/bin/python
# coding: utf-8
"""Generate a database from a .gff3 file."""
# Python packages
import os
import gffutils

# House made scripts and packages
from utils.time_stamp import string_time


def create_db(path_GFF,
              path_database,
              logfile,
              wanteDebug=True):
    """Create the database if not already generated."""
    # Generate Initial message for the log file
    message = string_time()
    message += ' Starting create_db:\n'
    logfile.write(message)

    if os.path.exists(path_database) is False:
        # If database doesn't exists
        message = '     Creating the database.\n'
        logfile.write(message)
        print('Starting creation of the database.')
        print('Process can take more than 10 hours.')

        # Create the Database
        db = gffutils.create_db(path_GFF,
                                dbfn=path_database,
                                force=True,
                                keep_order=True,
                                merge_strategy='merge',
                                sort_attribute_values=True)
        print('Database created.')

    else:
        # The database already exists
        message = '     Loading the database.\n'
        logfile.write(message)
        db = gffutils.FeatureDB(path_database)

    # Generate Final message for the log file
    message = string_time()
    message += ' create_db finished its job.\n\n'
    logfile.write(message)

    # Return the database
    return db
