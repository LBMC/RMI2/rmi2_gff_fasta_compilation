#!/usr/bin/python
# coding: utf-8
"""Importing Ribidensities from Emmanuel."""
import os
from config import path_Ribodens
from config import path_Output
from config import path_gff3, path_fa
from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator
from utils.time_stamp import message_time


def Add_RiboDens(log_file):
    """Add Ribodensity calculated by Emmanuel."""
    # Naive T cells: aax0194_Hwang_Table_S4.csv
    # Activated T cells: aax0194_Hwang_Table_S5.csv
    # Hal-life: aax0194_Hwang_Table_S3.csv

    message = message_time('Starting Add_RiboDens.\n')
    print(message)
    log_file.write(message)

    # Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Get gene_id to transcripts conversion
    list_gene_id, dict_ricci, db_gene_ricci = get_Ricci_id()

    # Get the Ribodens

    message = message_time('Getting the RiboDens.\n')
    print(message)
    log_file.write(message)

    db_Ribodens = Shredder_simple(path_Ribodens, ',')

    # create the list of the new key_words
    keys = []
    for i in range(1, len(db_Ribodens._2nd_Keys), 1):
        keys.append(db_Ribodens._2nd_Keys[i])

    n_transcripts = 0
    done = range(1000, 100000, 1000)

    for gene in db_Ribodens._First_Col:

        transcripts = []
        values = []
        # Get the values of the Ribodens
        for key in keys:
            values.append(db_Ribodens._dict_values[gene,
                                                   key])
        # Transfer to all transcript files
        gene_id = gene.split('.')[0]
        if gene_id in dict_ricci.keys():
            transcripts = dict_ricci[gene_id]

        for local_transcript in transcripts:
            n_transcripts += 1
            # Transform the transcript file into modifiable object
            f = Manipulator(input_folder + local_transcript + '.csv')

            # Update the object and the file
            for k in range(0, len(keys), 1):
                f._appendValue(keys[k],
                               values[k])
            f._updateFile()

            if n_transcripts in done:
                message = message_time(str(n_transcripts))
                message += ' files updated.\n'
                print(message)
                log_file.write(message)

    message = message_time(str(n_transcripts))
    message += ' files updated.\n'
    print(message)
    log_file.write(message)

    message = message_time('End Add_RiboDens.\n\n')
    print(message)
    log_file.write(message)


def get_Ricci_id():
    """Extract gene_id to transcript connection."""
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    db_gene_ricci = Shredder_simple(conv_path, ',')
    dict_ricci = {}
    list_gene_id = []
    for name in db_gene_ricci._First_Col:
        gene_id = db_gene_ricci._dict_values[name, 'gene_id']
        transcripts = db_gene_ricci._dict_values[name,
                                                 'transcript_id'].split(';')

        list_gene_id.append(gene_id)
        dict_ricci[gene_id] = transcripts

    return list_gene_id, dict_ricci, db_gene_ricci


if __name__ == "__main__":
    # execute only if run as a script
    Add_RiboDens()
