#!/usr/bin/python
# coding: utf-8
"""Associate target genes to microRNA from miRTarBase."""
import os
from config import path_Output, path_miTarget, path_gff3, path_fa
import pandas as pd
from utils.time_stamp import message_time
from utils.Object_manipulator import Manipulator


def Associate_miRNA(log_file):
    """Attribute the miRNA targeting a gene to all transcripts."""
    message = message_time('Starting Associate_miRNA.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Open the gene to transcript_csv as dataframe
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    pd_genes_trans = pd.read_csv(conv_path)

    # simplify the database with only the wanted columns
    pd_genes_trans = pd_genes_trans[['gene_name', 'transcript_id']]

    # Get list gene names
    our_genes = pd_genes_trans['gene_name'].values

    # Open the miRTarBase file
    pd_miR = pd.read_csv(path_miTarget)

    # Get list gene names
    shoot_targets(log_file,
                  our_genes,
                  pd_genes_trans,
                  pd_miR,
                  input_folder)

    message = message_time('End Associate_miRNA.\n\n')
    print(message)
    log_file.write(message)


def shoot_targets(log_file,
                  our_genes,
                  pd_gt,
                  pd_miR,
                  input_folder):
    """Attribute to all transcripts of a gene the targeting miRNA."""
    done = range(1000, 100000, 1000)
    n_gene = 0
    n_OK = 0
    for gene in our_genes:
        # Get the microRNA
        pd_miRNAs = pd_miR.loc[pd_miR['Target Gene'] == gene]

        if pd_miRNAs.shape[0] > 0:

            miRNAs = pd_miRNAs['miRNA'].values

            N_miRNA = len(miRNAs)
            miRNA_names = ''
            for i in range(0, N_miRNA - 1, 1):
                miRNA_names += miRNAs[i] + ';'
            miRNA_names += miRNAs[N_miRNA - 1]

            # Get all the transcripts
            pd_loc = pd_gt.loc[pd_gt['gene_name'] == gene]
            transcripts = pd_loc['transcript_id'].values[0]
            transcripts = transcripts.split(';')

            for trans in transcripts:
                f = Manipulator(input_folder + trans + '.csv')
                f._appendValue('N_miRNA', str(N_miRNA))
                f._appendValue('miRNA_names', miRNA_names)
                f._updateFile()

            n_OK += 1
            if n_OK in done:
                message = message_time(str(n_OK) + ' genes OK.\n')
                print(message)
                log_file.write(message)

        n_gene += 1

    message = message_time(str(n_OK) + ' genes OK.\n')
    print(message)
    log_file.write(message)

    message = message_time(str(n_gene) + ' genes done.\n')
    print(message)
    log_file.write(message)
