#!/usr/bin/python
# coding: utf-8
"""Importing data from Hwang et al 2020."""
import os
from config import path_Hwang_S3, path_Hwang_S4, path_Hwang_S5
from config import path_Output
from config import path_gff3, path_fa
from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator
from utils.time_stamp import message_time


def Add_Hwang(log_file):
    """Add polyA tail length and transcript half life from Hwang 2020."""
    # Naive T cells: aax0194_Hwang_Table_S4.csv
    # Activated T cells: aax0194_Hwang_Table_S5.csv
    # Hal-life: aax0194_Hwang_Table_S3.csv

    message = message_time('Starting Add_Hwang.\n')
    print(message)
    log_file.write(message)

    # Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Get gene_id to transcripts conversion
    list_gene_id, dict_ricci, db_gene_ricci = get_Ricci_id()

    # As the half-life of the transcripts is in NM_XXXX format and gene name
    # The program will first collect all ENSEMBLE gene_id for all gene_name
    # in the resting and activated condictions
    gene_name_to_ID = {}

    # Get the Naive T cells polyA length

    message = message_time('Getting the Resting TCD4 polyA length.\n')
    print(message)
    log_file.write(message)

    prefix1 = 'Hwang>Lympho_Resting>polyA_length_'
    db_rest_pA = Shredder_simple(path_Hwang_S4, ',')

    # Feed the gene_name to gene_id dictionnary
    gene_name_to_ID = create_link(db_rest_pA,
                                  db_rest_pA._2nd_Keys[1],   # gene name
                                  gene_name_to_ID)

    # Feed the transcript files with the data
    feed_files(db_rest_pA,
               dict_ricci,
               list_gene_id,
               prefix1,
               log_file,
               input_folder)


    # Get the Activated T cells polyA length
    message = message_time('Getting the Activated TCD4 polyA length.\n')
    print(message)
    log_file.write(message)

    prefix2 = 'Hwang>Lympho_Activated>polyA_length_'
    db_act_pA = Shredder_simple(path_Hwang_S5, ',')

    # Feed the gene_name to gene_id dictionnary
    gene_name_to_ID = create_link(db_act_pA,
                                  db_act_pA._2nd_Keys[1],   # gene name
                                  gene_name_to_ID)

    # Feed the transcript files with the data
    feed_files(db_act_pA,
               dict_ricci,
               list_gene_id,
               prefix2,
               log_file,
               input_folder)

    # Now add the values of half-life for each gene
    message = message_time('Getting mRNA half-life.\n')
    print(message)
    log_file.write(message)

    prefix3 = 'Hwang>transcript_half-life'
    db_half_life = Shredder_simple(path_Hwang_S3, ',')

    ok = 0
    absent = 0
    for gene_name in db_half_life._First_Col:
        # Get all transcripts
        transcripts = []
        if gene_name in db_gene_ricci._First_Col:
            ok += 1
            gene_id = db_gene_ricci._dict_values[gene_name,
                                                 'gene_id']
            transcripts = dict_ricci[gene_id]

        elif gene_name in gene_name_to_ID.keys():
            ok += 1
            gene_id = gene_name_to_ID[gene_name].split('.')[0]
            if gene_id in list_gene_id:
                transcripts = dict_ricci[gene_id]

        else:
            absent += 1

        for transcript in transcripts:
            # Transform the transcript file into modifiable object
            f = Manipulator(input_folder + transcript + '.csv')

            # Update the object and the file
            f._appendValue(prefix3,
                           db_half_life._dict_values[gene_name,
                                                     db_half_life._2nd_Keys[1]
                                                     ])
            f._updateFile()


    message = message_time(str(ok) + ' gene names can be used.\n')
    print(message)
    log_file.write(message)

    message = message_time(str(absent) + ' gene names cannot be used.\n')
    print(message)
    log_file.write(message)

    message = message_time('End Add_Hwang.\n\n')
    print(message)
    log_file.write(message)


def feed_files(db,
               dict_ricci,
               list_id,
               prefix,
               log_file,
               input_folder):
    """Get polyA length values and attribute to each transcripts files."""
    # harvest keys
    initial_keys = []
    keys = []

    # Rename the keys to have explicit keyword in our files
    for index in range(2, len(db._2nd_Keys), 1):
        new_key = rename_key(db._2nd_Keys[index],
                             ':',
                             0,
                             prefix)
        initial_keys.append(db._2nd_Keys[index])
        keys.append(new_key)

    n_gene = 0
    n_gene_OK = 0

    for gene in db._First_Col:

        n_gene += 1
        # Remove the extension to fit with our
        gene_no_v = gene.split('.')[0]

        # harvest values
        values = []
        for k in range(0, len(initial_keys), 1):
            val = db._dict_values[gene, initial_keys[k]]
            if val == '':
                val = 'NA'

            values.append(val)

        if gene_no_v in list_id:
            n_gene_OK += 1
            transcripts = dict_ricci[gene_no_v]

            for local_transcript in transcripts:

                # Transform the transcript file into modifiable object
                f = Manipulator(input_folder + local_transcript + '.csv')

                # Update the object and the file
                for k in range(0, len(keys), 1):
                    f._appendValue(keys[k],
                                   values[k])
                f._updateFile()

        # feed all files
        # for local_transcript in transcripts:

    message = message_time(str(n_gene) + ' genes processed.\n')
    print(message)
    log_file.write(message)

    message = message_time(str(n_gene_OK) + ' genes in common.\n')
    print(message)
    log_file.write(message)


def rename_key(key_value,
               separator,
               position_after_split,
               prefix):
    """Give a new name to key."""
    to_harvest = key_value.split(separator)[position_after_split]
    new_name = prefix + to_harvest

    return new_name

def create_link(db,
                what_I_want,
                dict):
    """Feed a dictionary with link between two columns."""
    for gene_id in db._First_Col:
        gene_name = db._dict_values[gene_id, what_I_want]
        dict[gene_name] = gene_id

    return dict

def get_Ricci_id():
    """Extract gene_id to transcript connection."""
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    db_gene_ricci = Shredder_simple(conv_path, ',')
    dict_ricci = {}
    list_gene_id = []
    for name in db_gene_ricci._First_Col:
        gene_id = db_gene_ricci._dict_values[name, 'gene_id']
        transcripts = db_gene_ricci._dict_values[name,
                                                 'transcript_id'].split(';')

        list_gene_id.append(gene_id)
        dict_ricci[gene_id] = transcripts

    return list_gene_id, dict_ricci, db_gene_ricci


if __name__ == "__main__":
    # execute only if run as a script
    Add_Hwang()
