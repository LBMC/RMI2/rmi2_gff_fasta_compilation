#!/usr/bin/python
# coding: utf-8
"""Core object of the project analysing CDS and creating file."""
# Python libraries
from __future__ import division
from utils.time_stamp import string_time
import numpy as np


class Shredder_complex:
    """This object is generated from a specific .csv database.

    It resume all lines and their properties.
    """

    def __init__(self,
                 path,
                 separator,
                 ref,   # index of the column with the reference key ('gene')
                 to_combine,   # List of index of columns to use to create
                               # Experimental condition
                 to_get        # index of the column to extract
                 ):
        """Construct method."""
        self._separator = separator
        self._path = path
        self._ref = ref
        self._get = to_get
        self._cond_elmnts = to_combine
        self._conditions = []
        self._dict_key_in_cond = {}
        self._dict_values = {}
        self._First_Col = []
        # The dictionary will created like this:
        # self._dict_values['Transcript_ID', '2nd_Key']
        # 2nd_Keys are the header column names

        # Reading the file
        print('   #   ' + string_time() + '   #   Shredding csv database')
        self._read_file()

    def _read_file(self):
        """Read the file and feed the dictionary."""
        # line counter
        nlines = 0
        Done = range(10000, 10000000, 10000)
        with open(self._path) as database:
            # Read the firstline
            line = database.readline()

            # Read all lines
            while line:
                nlines += 1
                line = self._clean_line(line)
                # Decide if line belongs to header or describe a value
                if nlines != 1:
                    # Remove breakline and split the line between key and value
                    frags = line.split(self._separator)
                    # Create the Condition key word
                    local_condition = ''
                    size = len(self._cond_elmnts)
                    for i in range(0, size - 1, 1):
                        local_condition += frags[self._cond_elmnts[i]]
                        local_condition += '_'
                    local_condition += frags[self._cond_elmnts[size - 1]]

                    # If we find a new condition
                    if local_condition not in self._conditions:
                        # inform user
                        message = '   #   ' + string_time() + '   #   '
                        message += local_condition
                        print(message)
                        # update the list of conditions
                        self._conditions.append(local_condition)
                        self._dict_key_in_cond[local_condition] = []

                    # get the key value (gene id...)
                    key1 = frags[self._ref]
                    if key1 not in self._First_Col:
                        self._First_Col.append(key1)

                    # Determine if it is the first encounter of this key
                    # for this condition
                    val = float(frags[self._get])
                    if key1 not in self._dict_key_in_cond[local_condition]:
                        self._dict_key_in_cond[local_condition].append(key1)
                        self._dict_values[key1,
                                          local_condition] = [val]
                    else:
                        self._dict_values[key1,
                                          local_condition].append(val)

                if nlines in Done:
                    message = '   #   ' + string_time() + '   #   '
                    message += str(nlines) + ' lines processed '
                    message += local_condition + ' '
                    print(message)

                line = database.readline()
        message = '   #   ' + string_time() + '   #   '
        message += str(nlines) + ' lines processed'
        print(message)
        print('   #   ' + string_time() + '   #   Table has: ' + str(nlines))
        print('   #   ' + string_time() + '   #   Starting calculating means')

        test = 0
        for c in self._conditions:
            for g in self._dict_key_in_cond[c]:
                if test == 0:
                    print(self._dict_values[g, c])
                self._dict_values[g, c] = np.mean(self._dict_values[g, c])
                if test == 0:
                    print(self._dict_values[g, c])
                    test += 1
        print('   #   ' + string_time() + '   #   Database ready')
        print('')

    def _clean_line(self, line):
        """Remove unwanted characters from line."""
        unwanted = ['"', '\n']
        for char in unwanted:
            line = line.replace(char, '')
        return line
