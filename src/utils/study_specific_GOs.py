#!/usr/bin/python
# coding: utf-8
"""Merge different statistic outputs as function of relevance."""
# MORPHEUS will do the clustering
# https://software.broadinstitute.org/morpheus/
# Python libraries
from __future__ import division

# Perso Library
from config import path_Output, Conditions_GO_to_merge, Condition_Merge
from config import number_stat_conditions, max_cutoff, min_cutoff
from config import go_key_words
from utils.Object_shredder_simple import Shredder_simple
from utils.time_stamp import message_time

# Parameters
GO_Name = path_Output + 'GO_to_Phrase.tsv'
suffix = 'at_least_one_stars'
if number_stat_conditions != 'All':
    suffix = str(number_stat_conditions) + '_is_enough'

pvalue_slice_max = 0.05
pvalue_slice_min = -0.1

pval = 'pValue_adjusted'
score = 'Mean'

Conditions_Clean = []
for c in Conditions_GO_to_merge:
    c = c.replace('>', '_')
    c = c.replace('(', '')
    c = c.replace(')', '')
    # print(c)
    Conditions_Clean.append(c)


def Merge_specific_scores(log_file):
    """Merge all GO with specific pvalues in all sets."""
    # Create the merged csv file
    message = message_time('Starting Merge_specific_scores.\n')
    print(message)
    log_file.write(message)

    # Get GO Names
    GO_db = Shredder_simple(GO_Name, '\t')

    # Get a list of valid GOs
    list_spe_go = get_specific_go(GO_db)
    print(list_spe_go)
    n_ok = str(len(list_spe_go))
    message = message_time(n_ok + ' GOs found with at least one keyword')
    print(message)
    log_file.write(message)

    message = message_time('Creating output_file.\n')
    print(message)
    log_file.write(message)

    # print(Conditions_GO_to_merge)
    # print(Conditions_Clean)

    csv_name = Condition_Merge + '_' + suffix + '.csv'
    open(path_Output + csv_name, 'w+').close
    csv = open(path_Output + csv_name, 'a+')

    header = 'GO'
    for c in Conditions_GO_to_merge:
        header += ',' + c
    header += '\n'
    csv.write(header)

    # Initiate the dictionnary that will contain the shredder Object
    message = message_time('Retrieving data.\n')
    print(message)
    log_file.write(message)

    values = {}
    for c in Conditions_Clean:
        p = path_Output + c + '_GO_adjusted_pValues.csv'
        values[c] = Shredder_simple(p, ',')

    # Do all GO using first set as reference
    message = message_time('Start merging.\n')
    print(message)
    log_file.write(message)

    done = range(100, 20000, 100)
    go_done = 0
    for go in values[Conditions_Clean[0]]._First_Col:
        if go in list_spe_go:
            # check that it is present in all the other sets
            always_present = True
            for c in Conditions_Clean:
                if go not in values[c]._First_Col:
                    # One false is enough to stop
                    always_present = False

            to_write = ''
            if always_present:
                to_write = getValues(go,
                                     values,
                                     GO_db)
            if to_write != '':
                go_done += 1
                csv.write(to_write)

                if go_done in done:
                    message = message_time(str(go_done) + ' GO merged.\n')
                    print(message)
                    log_file.write(message)

    # terminate the job
    message = message_time(str(go_done) + ' GO merged.\n')
    print(message)
    log_file.write(message)
    csv.close

    message = message_time('End Merge_specific_scores.\n\n')
    print(message)
    log_file.write(message)


def get_specific_go(GO_db):
    """Get all GOs that contain specific Keywords."""
    list_ok = []
    for go in GO_db._First_Col:
        name = GO_db._dict_values[go, 'Name']
        for keyword in go_key_words:
            if keyword in name:
                list_ok.append(go)
                break

    return list_ok


def getValues(myGO,
              dict,
              GO_db):
    """Validate if the GO has desired pvalues and return median."""
    if myGO in GO_db._First_Col:
        raw_name = GO_db._dict_values[myGO, 'Name']
        name = raw_name.replace(',', ';')
        myGOf = myGO + ' ' + name
    # Get pvalues
    pValues = []
    results = []
    stats = []
    size = []
    res = ''
    todo = True
    for c in Conditions_Clean:
        pValues.append(float(dict[c]._dict_values[myGO, pval]))
        results.append(dict[c]._dict_values[myGO, score])
        n = float(dict[c]._dict_values[myGO, 'nTDD'])
        if n > max_cutoff or n < min_cutoff:
            todo = False
            break
        size.append(n)

    if todo:
        # Check that all are below pvalue_max
        # and 1 at least above pvalue_min
        spacer = ''
        pref = '['
        for p in pValues:
            if p > 0.05:
                stats.append(0)
                spacer += '      '
                pref += '0'
            elif p <= 0.05 and p > 0.01:
                stats.append(1)
                spacer += '   '
                pref += '1'
            elif p <= 0.01 and p > 0.001:
                stats.append(2)
                spacer += '  '
                pref += '2'
            elif p <= 0.001:
                stats.append(3)
                pref += '3'
        pref += ']'

        # Get the number of statistical Conditions
        stat_valid = len(stats) - stats.count(0)

        # Determine if OK
        if number_stat_conditions == 'All':
            minOK = len(stats)
        else:
            minOK = int(number_stat_conditions)

        if stat_valid >= minOK:
            # generate the Results
            res = pref + ' ' + myGOf + ','
            for r in range(0, len(results)-1, 1):
                res += results[r] + ','
            res += results[len(results)-1] + '\n'

    # return '' if fails
    return res


if __name__ == "__main__":
    # execute only if run as a script
    Merge_specific_scores()
