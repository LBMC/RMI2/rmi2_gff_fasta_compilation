#!/usr/bin/python
# coding: utf-8
"""Script that generate the .fa and files for DeltaG Calculation."""
# Python libraries
import os
import glob
import shutil
import numpy as np

# Home made scripts and packages
from config import appris_limit_DG, threads
from config import path_Output
from config import path_gff3, path_fa
from utils.time_stamp import message_time, string_time
from utils.Object_manipulator import Manipulator
from utils.Free_Energy_and_GC_merger import Transfer_DG


def Export_DG(log_file):
    """Prepare the Fasta file for the Delta G calculation."""
    message = message_time('Starting Export_DG.\n')
    print(message)
    log_file.write(message)

    # Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Create the fasta file
    time_FP = string_time()
    fa_name = time_FP + '_' + gff3_name + '_VS_' + fasta_name
    fa_name += '_DeltaG.fa'
    fa_file = open(path_Output + fa_name, 'w+')
    dg_name = fa_name.replace('.fa', '.dg')

    message = message_time('Fasta file created.\n')
    print(message)
    log_file.write(message)

    # Feed the fasta file
    nFiles = feed_fasta(input_folder,
                        fa_file,
                        log_file)

    message = message_time(str(nFiles) + ' Transcript files processed.\n')
    print(message)
    log_file.write(message)

    # Close the fasta file
    fa_file.close
    shutil.copy2(path_Output + fa_name,
                 path_Output + 'Temp_DeltaG.fa')

    message = message_time('Launching RNAfold.\n')
    print(message)
    log_file.write(message)

    # Generate the command line
    cmd = 'bash src/utils/rnafold.sh '
    cmd += '-i src/output/Temp_DeltaG.fa '
    cmd += '-o src/output/Temp.dg '
    cmd += '-j ' + str(threads)

    print(cmd)
    os.system(cmd)
    print('Done')

    # Reorganize properly the files
    os.rename(path_Output + 'Temp.dg',
              path_Output + dg_name)
    os.remove(path_Output + 'Temp_DeltaG.fa')

    # perform transfer
    Transfer_DG(path_Output + dg_name,
                log_file)

    message = message_time(str(nFiles) + 'End Export_DG.\n\n')
    print(message)
    log_file.write(message)


def feed_fasta(input_folder,
               fa_file,
               log_file):
    """Collect all sequence to run rnafold."""
    # Explore all files and feed the fasta Files
    progress = np.arange(1000, 100000, 1000).tolist()
    nFiles = 0
    for csv_file in glob.glob(input_folder + '*.csv'):

        # Load the content of the file
        f = Manipulator(csv_file)

        # Filter on QC passed files and appris level (set in config.py)
        filter1 = f._getValue(['QC_passed'])[0]
        filter2 = f._getValue(['appris_level'])[0]
        if filter2 == '':
            filter2 = 1000

        if filter1 == '1' and int(filter2) <= appris_limit_DG:
            # We found a correct file
            nFiles += 1
            [transcript_id,
             gene_name,
             UTR5_seq,
             CDS_seq,
             UTR3_seq] = f._getValue(['transcript_id',
                                      'gene_name',
                                      'UTR5_seq',
                                      'CDS_seq',
                                      'UTR3_seq'])
            head = '>' + transcript_id + '_' + gene_name

            sequences = ''
            sequences += head + '_UTR5' + '\n'
            sequences += UTR5_seq + '\n'
            sequences += head + '_CDS' + '\n'
            sequences += CDS_seq + '\n'
            sequences += head + '_UTR3' + '\n'
            sequences += UTR3_seq + '\n'

            if len(UTR5_seq) >= 1 and len(CDS_seq) >= 30:
                if len(UTR5_seq) >= 47:
                    A_seq = UTR5_seq[-47:] + CDS_seq[0:30]
                else:
                    A_seq = UTR5_seq + CDS_seq[0:30]
                B_seq = CDS_seq[30:]
                sequences += head + '_47UTR5-30CDS' + '\n'
                sequences += A_seq + '\n'
                sequences += head + '_31CDS-End' + '\n'
                sequences += B_seq + '\n'

            # Write in the File
            fa_file.write(sequences)
            if nFiles in progress:
                message = message_time(str(nFiles))
                message += ' Transcript files processed.\n'
                print(message)
                log_file.write(message)

    return nFiles


if __name__ == "__main__":
    # execute only if run as a script
    Export_DG()
