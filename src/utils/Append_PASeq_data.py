#!/usr/bin/python
# coding: utf-8
"""Append PASeq data to the transcripts files."""
# Python libraries
from __future__ import division
import os
import numpy as np

# Home made scripts and packages
from config import path_Output
from config import path_gff3, path_fa, path_PASeq
from utils.time_stamp import string_time, message_time
from utils.Object_shredder_PASeq import Shredder_PASeq
from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator
from utils.PASeq_to_UTR_length import calculate_alt_UTR_length


def Add_PASeq(log_file):
    """Open the PASeq file and add the data to the correct transcript files."""
    message = message_time('Starting Add_PASeq.\n')
    print(message)
    log_file.write(message)
    # 1. Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Get gene to transcripts conversion
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    db = Shredder_simple(conv_path, ',')

    # transform PASeq database into an object
    PASeq = Shredder_PASeq(path_PASeq, '\t')
    print('   #   ' + string_time() + '   #   Starting Transfer')
    Done = range(1000, 100000, 1000)
    ngene = 0
    nok = 0

    # Get all gne from PASEQ data
    n_peaks = 0
    n_in_exon = 0
    n_in_intron = 0
    n_down = 0
    n_up_down = 0
    n_up = 0

    for gene in PASeq._List_Gene_Name:

        (n_Exp_Ends,
         newKeys,
         dict,
         ngene) = get_PASeq(gene, ngene, PASeq)

        # Transfer into the correct file(s)
        if gene in db._First_Col:
            (n_in_exon,
             n_in_intron,
             n_down,
             n_up_down,
             n_up,
             n_peaks) = update_transcript_files(gene,
                                                db,
                                                n_Exp_Ends,
                                                newKeys,
                                                dict,
                                                input_folder,
                                                log_file,
                                                n_in_exon,
                                                n_in_intron,
                                                n_down,
                                                n_up_down,
                                                n_up,
                                                n_peaks)
            nok += 1

        else:
            message = '   #   ' + string_time() + '   #   '
            message += gene + ' not in the database!'
        if ngene in Done:
            message = message_time(str(ngene) + ' Genes treated.\n')
            print(message)
            log_file.write(message)
        if nok in Done:
            message = message_time(str(nok) + ' Genes with PASeq.\n')
            print(message)
            log_file.write(message)

    message = message_time(str(ngene) + ' Genes treated.\n')
    print(message)
    log_file.write(message)

    message = message_time(str(nok) + ' Genes with PASeq.\n')
    print(message)
    log_file.write(message)

    message = message_time(str(n_peaks) + ' PASeq peaks treated.\n')
    print(message)
    log_file.write(message)

    total_attributed = n_in_exon + n_in_intron + n_down + n_up_down + n_up

    message = message_time(str(total_attributed))
    message += ' PASeq peaks atttributed.\n'
    print(message)
    log_file.write(message)

    message = message_time(str(n_in_exon))
    message += ' PASeq peaks atttributed to exon.\n'
    print(message)
    log_file.write(message)

    message = message_time(str(n_in_intron))
    message += ' PASeq peaks atttributed to intron.\n'
    print(message)
    log_file.write(message)

    message = message_time(str(n_down))
    message += ' PASeq peaks constitantly Downstream.\n'
    print(message)
    log_file.write(message)

    message = message_time(str(n_up_down))
    message += ' PASeq peaks in Upstream/Downstream situation.\n'
    print(message)
    log_file.write(message)

    message = message_time(str(n_up))
    message += ' PASeq peaks constitantly Upstream'
    message += ' (Detected not Attributed).\n'
    print(message)
    log_file.write(message)

    message = message_time('End Add_PASeq.\n\n')
    print(message)
    log_file.write(message)


def update_transcript_files(gene,
                            db,
                            n_Exp_Ends,
                            newKeys,
                            dict,
                            input_folder,
                            log_file,
                            n_in_exon,
                            n_in_intron,
                            n_down,
                            n_up_down,
                            n_up,
                            n_peaks):
    """Transfer PASeq values to all concerned transcript files."""
    list_files_t = db._dict_values[gene, 'transcript_id']
    transcripts = list_files_t.split(';')

    # Dictionary containing the raw status of peaks in each transcript
    dict_status_pk = {}
    dict_dist_pk = {}
    can_be_attributed = []
    list_t_pk = []
    dict_t_pk = {}

    for pk in range(1, n_Exp_Ends + 1, 1):
        dict_status_pk[pk] = []
        dict_dist_pk[pk] = []
        can_be_attributed = np.append(can_be_attributed,
                                      'False')

    for local_transcript in transcripts:

        # Transform the transcript file into modifiable object
        f = Manipulator(input_folder + local_transcript + '.csv')

        # Update the object and the file
        for key in newKeys:
            f._appendValue(key,
                           dict[key])
        f, peaks_status, dist_from_stop = calculate_alt_UTR_length(f)
        # Harvest the status on the peaks and transfer in the Dictionary
        for pk in range(1, n_Exp_Ends + 1, 1):
            dict_status_pk[pk] = np.append(dict_status_pk[pk],
                                           peaks_status[pk - 1])
            dict_dist_pk[pk] = np.append(dict_dist_pk[pk],
                                         dist_from_stop[pk - 1])
        f._updateFile()

    # Attibution of the peaks
    # print(gene + ' has ' + str(n_Exp_Ends) + ' PASeq peaks')
    # print(transcripts)
    for pk in range(1, n_Exp_Ends + 1, 1):
        n_peaks += 1
        # print('Peak ' + str(pk))
        # print(dict_status_pk[pk])
        if 'In UTR3 Exon' in dict_status_pk[pk]:
            n_in_exon += 1
            can_be_attributed[pk - 1] = 'Exon'
            # Determine the correct transcripts
            lt = np.where(dict_status_pk[pk] == 'In UTR3 Exon')[0]
            # Transform in a list of transcript name
            names_t = []
            for t_index in lt:
                names_t = np.append(names_t,
                                    transcripts[t_index])
            # print('Exon', names_t)
            # Now we assign the peak to the transcripts
            for name in names_t:
                # First occurance of a transcript
                if name not in list_t_pk:
                    list_t_pk = np.append(list_t_pk,
                                          name)
                    dict_t_pk[name] = [pk]
                else:
                    dict_t_pk[name] = np.append(dict_t_pk[name],
                                                pk)

        elif 'In UTR3 Intron' in dict_status_pk[pk]:
            n_in_intron += 1
            can_be_attributed[pk - 1] = 'Intron'
            # Determine the correct transcripts
            lt = np.where(dict_status_pk[pk] == 'In UTR3 Intron')[0]
            # Transform in a list of transcript name
            names_t = []
            for t_index in lt:
                names_t = np.append(names_t,
                                    transcripts[t_index])
            # print('Intron', names_t)
            # Now we assign the peak to the transcripts
            for name in names_t:
                # First occurance of a transcript
                if name not in list_t_pk:
                    list_t_pk = np.append(list_t_pk,
                                          name)
                    dict_t_pk[name] = [pk]
                else:
                    dict_t_pk[name] = np.append(dict_t_pk[name],
                                                pk)
        elif only(dict_status_pk[pk], 'Downstream'):
            n_down += 1
            can_be_attributed[pk - 1] = 'Longer'
            # Determine the correct transcripts
            # by determing the lowest distance from the stop codon
            minimum = min(dict_dist_pk[pk])
            lt = np.where(dict_dist_pk[pk] == minimum)[0]
            # Transform in a list of transcript name
            names_t = []
            for t_index in lt:
                names_t = np.append(names_t,
                                    transcripts[t_index])
            # print('Intron', names_t)
            # Now we assign the peak to the transcripts
            for name in names_t:
                # First occurance of a transcript
                if name not in list_t_pk:
                    list_t_pk = np.append(list_t_pk,
                                          name)
                    dict_t_pk[name] = [pk]
                else:
                    dict_t_pk[name] = np.append(dict_t_pk[name],
                                                pk)
        # If Only Downstream or Upstream
        elif (only_or(dict_status_pk[pk],
                     ['Downstream', 'Upstream']) and
              not only(dict_status_pk[pk], 'Upstream') and
              not only(dict_status_pk[pk], 'Downstream')):
            n_up_down += 1
            can_be_attributed[pk - 1] = 'Longer'
            # in order to facilitate the attribution to a transcript
            # the negative values are transformed into 1 000 000 000
            list_distances = dict_dist_pk[pk].tolist()
            for d in range(0, len(list_distances), 1):
                if list_distances[d] < 0:
                    list_distances[d] = 1000000000
            # Determine the correct transcripts
            # by determing the lowest distance from the stop codon
            minimum = min(list_distances)
            lt = np.where(dict_dist_pk[pk] == minimum)[0]
            names_t = []
            # Transform in a list of transcript name
            for t_index in lt:
                names_t = np.append(names_t,
                                    transcripts[t_index])
            # print('Intron', names_t)
            # Now we assign the peak to the transcripts
            for name in names_t:
                # First occurance of a transcript
                if name not in list_t_pk:
                    list_t_pk = np.append(list_t_pk,
                                          name)
                    dict_t_pk[name] = [pk]
                else:
                    dict_t_pk[name] = np.append(dict_t_pk[name],
                                                pk)

        elif only(dict_status_pk[pk], 'Upstream'):
            n_up += 1

    # print('Final Attribution')
    # print(can_be_attributed)
    for trans in list_t_pk:
        # print(trans, dict_t_pk[trans])
        calculate_new_length(trans,
                             dict_t_pk[trans],
                             input_folder)
    # transmit to main code
    return n_in_exon, n_in_intron, n_down, n_up_down, n_up, n_peaks


def only_or(mylist,
            value_list):
    """Determine if all elements in the list are equal to value."""
    res = False
    for i in mylist:
        if i not in value_list:
            res = False
            break
        else:
            res = True
    return res


def get_PASeq(gene, ngene, PASeq):
    """Get all PASeq info for a gene."""
    ngene += 1
    # Extracting the data to implement in the transcript file
    newKeys = []
    dict = {}

    (n_Exp_Ends, Name_Exp_3Ends, PA_IDs,
     PA_Starts, PA_Ends, PA_sitesCombined, PA_siteCount,
     PA_A1, PA_A1_Tot, PA_A2, PA_A2_Tot, PA_A3, PA_A3_Tot,
     PA_m1, PA_m1_Tot, PA_m2, PA_m2_Tot, PA_m3, PA_m3_Tot,
     PA_R1, PA_R1_Tot, PA_R2, PA_R2_Tot, PA_R3, PA_R3_Tot,
     PA_i1, PA_i1_Tot, PA_i2, PA_i2_Tot, PA_i3, PA_i3_Tot
     ) = PASeq._dict_values[gene]

    newKeys = np.append(newKeys, 'n_Exp_UTR3_ends')
    dict['n_Exp_UTR3_ends'] = str(n_Exp_Ends)

    for i in range(0, n_Exp_Ends, 1):
        prefix = Name_Exp_3Ends[i]
        newKeys = np.append(newKeys,
                            'Name_Exp_3End_' + str(i+1))
        dict['Name_Exp_3End_' + str(i+1)] = prefix
        newKeys = np.append(newKeys,
                            prefix + '_PA_ID')
        dict[prefix + '_PA_ID'] = PA_IDs[i]
        newKeys = np.append(newKeys,
                            prefix + '_Start')
        dict[prefix + '_Start'] = PA_Starts[i]
        newKeys = np.append(newKeys,
                            prefix + '_End')
        dict[prefix + '_End'] = PA_Ends[i]
        newKeys = np.append(newKeys,
                            prefix + '_Combined_Sites')
        dict[prefix + '_Combined_Sites'] = PA_sitesCombined[i]
        newKeys = np.append(newKeys,
                            prefix + '_Total_Counts')
        dict[prefix + '_Total_Counts'] = PA_siteCount[i]
        newKeys = np.append(newKeys,
                            prefix + '_Activated_CD4_T_cells_A1')
        dict[prefix + '_Activated_CD4_T_cells_A1'] = PA_A1[i]
        newKeys = np.append(newKeys,
                            prefix + '_Activated_CD4_T_cells_A1_Perc')
        score = percent_to_string(int(PA_A1[i]), PA_A1_Tot, 2)
        dict[prefix + '_Activated_CD4_T_cells_A1_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_Activated_CD4_T_cells_A2')
        dict[prefix + '_Activated_CD4_T_cells_A2'] = PA_A2[i]
        newKeys = np.append(newKeys,
                            prefix + '_Activated_CD4_T_cells_A2_Perc')
        score = percent_to_string(int(PA_A2[i]), PA_A2_Tot, 2)
        dict[prefix + '_Activated_CD4_T_cells_A2_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_Activated_CD4_T_cells_A3')
        dict[prefix + '_Activated_CD4_T_cells_A3'] = PA_A3[i]
        newKeys = np.append(newKeys,
                            prefix + '_Activated_CD4_T_cells_A3_Perc')
        score = percent_to_string(int(PA_A3[i]), PA_A3_Tot, 2)
        dict[prefix + '_Activated_CD4_T_cells_A3_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_LPS_Stimulated_Macrophages_m1')
        dict[prefix + '_LPS_Stimulated_Macrophages_m1'] = PA_m1[i]
        newKeys = np.append(newKeys,
                            prefix + '_LPS_Stimulated_Macrophages_m1_Perc')
        score = percent_to_string(int(PA_m1[i]), PA_m1_Tot, 2)
        dict[prefix + '_LPS_Stimulated_Macrophages_m1_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_LPS_Stimulated_Macrophages_m2')
        dict[prefix + '_LPS_Stimulated_Macrophages_m2'] = PA_m2[i]
        newKeys = np.append(newKeys,
                            prefix + '_LPS_Stimulated_Macrophages_m2_Perc')
        score = percent_to_string(int(PA_m2[i]), PA_m2_Tot, 2)
        dict[prefix + '_LPS_Stimulated_Macrophages_m2_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_LPS_Stimulated_Macrophages_m3')
        dict[prefix + '_LPS_Stimulated_Macrophages_m3'] = PA_m3[i]
        newKeys = np.append(newKeys,
                            prefix + '_LPS_Stimulated_Macrophages_m3_Perc')
        score = percent_to_string(int(PA_m3[i]), PA_m3_Tot, 2)
        dict[prefix + '_LPS_Stimulated_Macrophages_m3_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_Resting_CD4_T_cells_R1')
        dict[prefix + '_Resting_CD4_T_cells_R1'] = PA_R1[i]
        newKeys = np.append(newKeys,
                            prefix + '_Resting_CD4_T_cells_R1_Perc')
        score = percent_to_string(int(PA_R1[i]), PA_R1_Tot, 2)
        dict[prefix + '_Resting_CD4_T_cells_R1_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_Resting_CD4_T_cells_R2')
        dict[prefix + '_Resting_CD4_T_cells_R2'] = PA_R2[i]
        newKeys = np.append(newKeys,
                            prefix + '_Resting_CD4_T_cells_R2_Perc')
        score = percent_to_string(int(PA_R2[i]), PA_R2_Tot, 2)
        dict[prefix + '_Resting_CD4_T_cells_R2_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_Resting_CD4_T_cells_R3')
        dict[prefix + '_Resting_CD4_T_cells_R3'] = PA_R3[i]
        newKeys = np.append(newKeys,
                            prefix + '_Resting_CD4_T_cells_R3_Perc')
        score = percent_to_string(int(PA_R3[i]), PA_R3_Tot, 2)
        dict[prefix + '_Resting_CD4_T_cells_R3_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_Unstimulated_Macrophages_i1')
        dict[prefix + '_Unstimulated_Macrophages_i1'] = PA_i1[i]
        newKeys = np.append(newKeys,
                            prefix + '_Unstimulated_Macrophages_i1_Perc')
        score = percent_to_string(int(PA_i1[i]), PA_i1_Tot, 2)
        dict[prefix + '_Unstimulated_Macrophages_i1_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_Unstimulated_Macrophages_i2')
        dict[prefix + '_Unstimulated_Macrophages_i2'] = PA_i2[i]
        newKeys = np.append(newKeys,
                            prefix + '_Unstimulated_Macrophages_i2_Perc')
        score = percent_to_string(int(PA_i2[i]), PA_i2_Tot, 2)
        dict[prefix + '_Unstimulated_Macrophages_i2_Perc'] = score
        newKeys = np.append(newKeys,
                            prefix + '_Unstimulated_Macrophages_i3')
        dict[prefix + '_Unstimulated_Macrophages_i3'] = PA_i3[i]
        newKeys = np.append(newKeys,
                            prefix + '_Unstimulated_Macrophages_i3_Perc')
        score = percent_to_string(int(PA_i3[i]), PA_i3_Tot, 2)
        dict[prefix + '_Unstimulated_Macrophages_i3_Perc'] = score

    return n_Exp_Ends, newKeys, dict, ngene


def only(mylist,
         value):
    """Determine if all elements in the list are equal to value."""
    res = False
    for i in mylist:
        if i != value:
            res = False
            break
        else:
            res = True
    return res


def calculate_new_length(transcript_name,
                         peaks,
                         input_folder):
    """Calculate the mean length of the experimental UTR3 in the conditions."""
    # Transform the transcript file into modifiable object
    # print(transcript_name)

    f = Manipulator(input_folder + transcript_name + '.csv')
    dict_vals = {}

    retained = ''

    mean_ls = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    total = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    list_out = ['Exp_UTR3_Activated_CD4_T_cells_A1_Mean_l',
                'Exp_UTR3_Activated_CD4_T_cells_A2_Mean_l',
                'Exp_UTR3_Activated_CD4_T_cells_A3_Mean_l',
                'Exp_UTR3_LPS_Stimulated_Macrophages_m1_Mean_l',
                'Exp_UTR3_LPS_Stimulated_Macrophages_m2_Mean_l',
                'Exp_UTR3_LPS_Stimulated_Macrophages_m3_Mean_l',
                'Exp_UTR3_Resting_CD4_T_cells_R1_Mean_l',
                'Exp_UTR3_Resting_CD4_T_cells_R2_Mean_l',
                'Exp_UTR3_Resting_CD4_T_cells_R3_Mean_l',
                'Exp_UTR3_Unstimulated_Macrophages_i1_Mean_l',
                'Exp_UTR3_Unstimulated_Macrophages_i2_Mean_l',
                'Exp_UTR3_Unstimulated_Macrophages_i3_Mean_l']

    # Retrieve the key values for each peak
    for peak in peaks:
        prefix = 'Exp_UTR3_End_' + str(peak)
        vals = f._getValue([prefix + '_Activated_CD4_T_cells_A1_Perc',
                            prefix + '_Activated_CD4_T_cells_A2_Perc',
                            prefix + '_Activated_CD4_T_cells_A3_Perc',
                            prefix + '_LPS_Stimulated_Macrophages_m1_Perc',
                            prefix + '_LPS_Stimulated_Macrophages_m2_Perc',
                            prefix + '_LPS_Stimulated_Macrophages_m3_Perc',
                            prefix + '_Resting_CD4_T_cells_R1_Perc',
                            prefix + '_Resting_CD4_T_cells_R2_Perc',
                            prefix + '_Resting_CD4_T_cells_R3_Perc',
                            prefix + '_Unstimulated_Macrophages_i1_Perc',
                            prefix + '_Unstimulated_Macrophages_i2_Perc',
                            prefix + '_Unstimulated_Macrophages_i3_Perc',
                            prefix + '_alt_Length'])
        # Convert NA as 0
        # vals = [x if x != 'NA' else '0' for x in vals]
        """print(vals)
        if transcript_name == 'ENSMUST00000054551':
            raw_input("Press Enter to continue...")"""
        dict_vals[peak] = vals

        # Generate the field containing the selected peaks
        if retained == '':
            retained += str(peak)
        else:
            retained += ';' + str(peak)

        # Calculate the total contribution of all peaks for each conditions
        for i in range(0, 12, 1):
            total[i] += float(vals[i])

    # Add the field containing the retained PASeq peak
    newkey = 'Exp_UTR3_Ends_validated'
    f._appendValue(newkey, retained)

    # Calculate the mean lengths pondarated by the contribution in each
    # experimental

    # Calculate the sum with weights
    for peak in peaks:
        for i in range(0, 12, 1):
            mean_ls[i] += round(float(dict_vals[peak][i]) *
                                float(dict_vals[peak][12]),
                                2)

    # total weights for each replicate of all condition
    for i in range(0, 12, 1):
        if total[i] != 0:
            mean_ls[i] = round(mean_ls[i] / total[i])
        else:
            mean_ls[i] = 0

        newkey = list_out[i]
        f._appendValue(newkey,
                       str(mean_ls[i]))

    # Calculate the mean experimental length for all conditions
    # Get all desired values

    [LA1, LA2, LA3,
     MA1, MA2, MA3,
     LR1, LR2, LR3,
     MR1, MR2, MR3] = f._getValue(list_out)

    f._appendValue('Exp_UTR3_Lympho_Activated_Mean',
                   str((float(LA1) +
                        float(LA2) +
                        float(LA3)) / 3))

    f._appendValue('Score_PASeq_Lympho_Activated',
                   str(round((float(total[0]) +
                              float(total[1]) +
                              float(total[2])) / 3,
                             2)
                       )
                   )

    f._appendValue('Exp_UTR3_Macro_Activated_Mean',
                   str((float(MA1) +
                        float(MA2) +
                        float(MA3)) / 3))

    f._appendValue('Score_PASeq_Macro_Activated',
                   str(round((float(total[3]) +
                              float(total[4]) +
                              float(total[5])) / 3,
                             2)
                       )
                   )

    f._appendValue('Exp_UTR3_Lympho_Resting_Mean',
                   str((float(LR1) +
                        float(LR2) +
                        float(LR3)) / 3))

    f._appendValue('Score_PASeq_Lympho_Resting',
                   str(round((float(total[6]) +
                              float(total[7]) +
                              float(total[8])) / 3,
                             2)
                       )
                   )

    f._appendValue('Exp_UTR3_Macro_Resting_Mean',
                   str((float(MR1) +
                        float(MR2) +
                        float(MR3)) / 3))

    f._appendValue('Score_PASeq_Macro_Resting',
                   str(round((float(total[9]) +
                              float(total[10]) +
                              float(total[11])) / 3,
                             2)
                       )
                   )

    # Update and save file
    f._updateFile()


def percent_to_string(numerator,
                      denominator,
                      decimals):
    """Perform percentage division if possible and return a string."""
    score = 0
    if denominator == 0 and numerator == 0:
        # Impossible division (no data)
        score = '0'
    else:
        score = str(round(100 * numerator / denominator,
                          decimals))
    return score


if __name__ == "__main__":
    # execute only if run as a script
    Add_PASeq()
