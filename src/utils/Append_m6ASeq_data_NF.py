#!/usr/bin/python
# coding: utf-8
"""Append m6ASeq data to the transcripts files."""
# Python libraries
from __future__ import division
import os
import numpy as np
import pandas as pd

# Home made scripts and packages
from config import path_Output
from config import path_gff3, path_fa, path_m6ASeq_NF
from utils.time_stamp import message_time
from utils.Object_shredder_m6ASeq import Shredder_m6ASeq
from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator

import time


def Add_m6ASeq_NF(log_file):
    """Open the m6ASeq.tsv and add the data to the correct transcript files."""
    message = message_time('Starting Add_m6ASeq.\n')
    print(message)
    log_file.write(message)

    # 1. Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # If the specific input folder doesn't exist
    if os.path.exists(input_folder) is False:
        print('   #   ' + 'Expected output folder doesn t exist.')
        # Quit
        exit()

    # transform m6ASeq database into a df object
    df_m6ASeq = pd.read_csv(path_m6ASeq_NF)
    message = message_time('Starting Transfer.\n')
    print(message)
    log_file.write(message)

    # Process the m6ASeq database
    ntranscripts, nok = treat_m6Aseq_db(df_m6ASeq,
                                        input_folder,
                                        log_file)

    message = message_time(str(ntranscripts) + ' Transcripts treated.\n')
    print(message)
    log_file.write(message)

    message = message_time(str(nok) + ' Transcripts updated with m6A.\n')
    print(message)
    log_file.write(message)

    message = message_time('End Add_m6ASeq.\n\n')
    print(message)
    log_file.write(message)


def treat_m6Aseq_db(df_m6ASeq,
                    input_folder,
                    log_file):
    """Use the m6Aseq df object to update transcript csv files."""
    ntranscripts, nok = 0, 0
    Done = range(1000, 100000, 1000)

    # Get unique transcripts Name
    transcripts = df_m6ASeq['Transcript'].unique()

    # analyze each transcript
    for local_transcript in transcripts:
        ntranscripts += 1
        path = input_folder + local_transcript + '.csv'
        if (os.path.isfile(path) == False):
            print(local_transcript + '.csv is missing.')
        else:
            nok += 1
            f = Manipulator(path)
            newKeys, dict_m6A = initiate_dict()
            df = df_m6ASeq.loc[df_m6ASeq['Transcript'] == local_transcript]
            localisations = df['Domain'].value_counts()
            loc = localisations.keys()
            if '5UTR' in loc:
                dict_m6A['m6A_peak_utr5_NF'] = localisations['5UTR']

            if 'CDS' in loc:
                dict_m6A['m6A_peak_CDS_NF'] = localisations['CDS']

            if 'exon' in loc:
                dict_m6A['m6A_peak_CDS_NF'] = (dict_m6A['m6A_peak_CDS_NF'] +
                                               localisations['exon'])

            if 'intron' in loc:
                dict_m6A['m6A_peak_intron_NF'] = localisations['intron']

            if '3UTR' in loc:
                dict_m6A['m6A_peak_utr3_NF'] = localisations['3UTR']

            for key in newKeys:
                f._appendValue(key,
                               dict_m6A[key])
            f._updateFile()


        if ntranscripts in Done:
            message = message_time(str(ntranscripts) + ' transcripts treated.\n')
            print(message)
            log_file.write(message)

    return ntranscripts, nok

def initiate_dict():
    """Generate a dictionary with 0 values for all keys"""
    newKeys = ['m6A_peak_utr5_NF',
               'm6A_peak_CDS_NF',
               'm6A_peak_intron_NF',
               'm6A_peak_utr3_NF']
    dict = {}
    for key in newKeys:
        dict[key] = 0

    return newKeys, dict


if __name__ == "__main__":
    # execute only if run as a script
    Add_m6ASeq()
