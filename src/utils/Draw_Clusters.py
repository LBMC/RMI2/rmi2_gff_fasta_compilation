#!/usr/bin/python
# coding: utf-8
"""Create a heatmap graph from a csv using pandas."""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from config import path_Global
from sklearn.cluster import AgglomerativeClustering
from matplotlib.backends.backend_pdf import PdfPages
from config import path_Output
from matplotlib.pyplot import figure
from utils.time_stamp import message_time


colormap = 'seismic'
ncluster = 50

namepdf = 'Lympho_Activated_CLUSTERS_' + str(ncluster)

wanted = ['Norm_Reads_Lympho_Activated_Trip_0h',
          'Norm_Reads_Lympho_Activated_Trip_1h',
          'Norm_Reads_Lympho_Activated_DRB_1h',
          'Norm_Reads_Lympho_Activated_Trip_3h',
          'Norm_Reads_Lympho_Activated_DRB_3h',
          'Norm_Reads_Lympho_Activated_TripCHX_1h',
          'Norm_Reads_Lympho_Activated_TripHarr_1h',
          'Norm_Reads_Lympho_Activated_DRBCHX_1h',
          'Norm_Reads_Lympho_Activated_DRBHarr_1h',
          'Norm_Reads_Lympho_Activated_TripCHX_3h',
          'Norm_Reads_Lympho_Activated_TripHarr_3h',
          'Norm_Reads_Lympho_Activated_DRBCHX_3h',
          'Norm_Reads_Lympho_Activated_DRBHarr_3h',
          'gene_id',
          'gene_name',
          'transcript_id'
          ]

new_name = ['0h',
            'Trip_1h',
            'DRB_1h',
            'Trip_3h',
            'DRB_3h',
            'TripCHX_1h',
            'TripHarr_1h',
            'DRBCHX_1h',
            'DRBHarr_1h',
            'TripCHX_3h',
            'TripHarr_3h',
            'DRBCHX_3h',
            'DRBHarr_3h']

to_display = ['DRB_1h',
              'Trip_1h',
              'DRB_3h',
              'Trip_3h',
              'DRBCHX_1h',
              'TripCHX_1h',
              'DRBHarr_1h',
              'TripHarr_1h',
              'DRBCHX_3h',
              'TripCHX_3h',
              'DRBHarr_3h',
              'TripHarr_3h']

to_display2 = ['Trip_1h',
               'DRB_1h',
               'Trip_3h',
               'DRB_3h',
               ' ',
               'Trip_1h',
               'TripCHX_1h',
               'TripHarr_1h',
               ' ',
               'DRB_1h',
               'DRBCHX_1h',
               'DRBHarr_1h',
               ' ',
               'Trip_3h',
               'TripCHX_3h',
               'TripHarr_3h',
               ' ',
               'DRB_3h',
               'DRBCHX_3h',
               'DRBHarr_3h']

to_cluster = ['DRB_1h',
              'Trip_1h',
              'DRB_3h',
              'Trip_3h',
              'DRBCHX_1h',
              'TripCHX_1h',
              'DRBHarr_1h',
              'TripHarr_1h',
              'DRBCHX_3h',
              'TripCHX_3h',
              'DRBHarr_3h',
              'TripHarr_3h']


ref_col = ['Trip_1h', 'DRB_1h']


def Bars_Clustering(log_file):
    """Open the csv and order the desired columns as function of CV."""
    # The coefficient of variation (CV), defined as Standard deviation (SD)
    # divided by the Mean describes the variability of a sample relative to
    # its mean. Because the CV is unitless and usually expressed as a
    # percentage, it is used instead of the SD to compare the spread of data
    # sets that have different units of measurements or have the same units of
    # measurements but differs greatly in magnitude.
    # Open the csv
    message = message_time('Starting Bars_Clustering\n')
    log_file.write(message)
    mypanda = pd.read_csv(path_Global)
    figure(num=None,
           figsize=(8, 6),
           dpi=3000,
           facecolor='w',
           edgecolor='k')

    # create recipient Pdf
    pdf = PdfPages(path_Output + namepdf + '.pdf')

    # Select only wanted columns
    mypanda = mypanda[wanted]
    mypanda = mypanda.sort_values(by='gene_id', ascending=False)
    message = message_time('Database has ' + str(mypanda.shape[0]))
    message += ' entries.\n'
    print(message)
    log_file.write(message)

    genes = mypanda['gene_id'].values

    unique = np.unique(genes)
    message = message_time('Database has ' + str(len(unique)))
    message += ' unique entries.\n'
    print(message)
    log_file.write(message)

    # Drop NaNs and zeros
    mypanda = mypanda.replace(0, np.nan)
    mypanda = mypanda.dropna()

    # Rename the columns
    for i in range(0, len(new_name), 1):
        mypanda = mypanda.rename(columns={wanted[i]: new_name[i]})

    # Caculate fold change and remove outliers values
    for i in range(0, len(to_display), 1):
        mypanda[to_display[i]] = (mypanda[to_display[i]] /
                                  mypanda[new_name[0]])
        mypanda[to_display[i]].values[mypanda[to_display[i]] > 1.2] = 1.2

    # Order the panda using CV column
    mypanda = mypanda.sort_values(by=new_name[0], ascending=True)

    # Convert into an 2D array for drawing
    data = mypanda[to_cluster].values

    # create clusters
    hc = AgglomerativeClustering(n_clusters=ncluster,
                                 affinity='euclidean',
                                 linkage='ward')
    # save clusters for chart
    y_hc = hc.fit_predict(data)

    # Draw the cluster
    message = message_time('Drawing Clusters.\n')
    print(message)
    log_file.write(message)

    index = 0
    for clust in range(0, ncluster, 1):
        index += 1
        df_one_cluster = pd.DataFrame(columns=to_cluster)
        for co in range(0, len(to_cluster), 1):
            df_one_cluster[to_cluster[co]] = data[y_hc == clust, co].tolist()
        # Find the name of all members of the cluster
        # number of row
        col_id = []
        n_row = df_one_cluster.shape[0]

        for row in range(0, n_row, 1):
            loc_val = df_one_cluster[to_cluster].values[row]
            loc_df = mypanda.copy()
            # Look for the unique line of interest
            for i in range(0, len(to_cluster), 1):
                loc_df = loc_df.loc[(loc_df[to_cluster[i]] == loc_val[i])]
                if loc_df.shape[0] == 1:
                    break
            id = loc_df['gene_id'].values[0]

            # Update the col array
            col_id = np.append(col_id, id)

        # Add the column 'gene_id' to the current cluster dataframe
        df_one_cluster['gene_id'] = col_id

        # Save the cluster dataframe as csv
        df_one_cluster.to_csv(path_Output + 'Cluster_' + str(clust) + '.csv',
                              index=False)

        values = df_one_cluster[to_cluster].values
        draw_cluster_trajectory(to_cluster, values, index, pdf)

    # reconstruct the array with all clusters
    df_clusters = pd.DataFrame(columns=to_cluster)
    for c in range(0, len(to_cluster), 1):
        # reconstruct each column
        col = []
        for cl in range(0, ncluster, 1):
            col = np.append(col,
                            data[y_hc == cl, c].tolist(),
                            axis=0)
        df_clusters[to_cluster[c]] = col
    df_clusters[' '] = df_clusters[ref_col].mean()
    df_clusters['rank'] = (df_clusters[ref_col[0]] +
                           df_clusters[ref_col[1]])/2
    df_clusters = df_clusters.sort_values(by='rank', ascending=False)
    clusters = df_clusters[to_display].values
    clusters2 = df_clusters[to_display2].values

    # Draw the plot
    fig, ax = plt.subplots()
    ax.figure.set_size_inches(10, 50)
    im, cbar = heatmap(clusters, to_display, ax=ax,
                       cmap=colormap, cbarlabel='Fold change')

    plt.savefig(pdf,
                format='pdf',
                dpi=300)

    # Draw the plot
    fig, ax = plt.subplots()
    ax.figure.set_size_inches(10, 50)
    im, cbar = heatmap(clusters2, to_display2, ax=ax,
                       cmap=colormap, cbarlabel='Fold change')
    plt.savefig(pdf,
                format='pdf',
                dpi=300)

    pdf.close()
    message = message_time('End Bars_Clustering.\n\n')
    print(message)
    log_file.write(message)


def draw_cluster_trajectory(X,
                            Ys,
                            cluster_number,
                            pdf):
    """Draw cluster trajectory with SD."""
    n_genes = Ys.shape[0]
    titre = 'Cluster number ' + str(cluster_number) + ', '
    titre += str(n_genes) + ' gene'
    if n_genes > 1:
        titre += 's'

    # Figures format
    Dimension = (8, 4)  # inches.
    Bottom = 0.15  # proportion (0-1).
    Left = 0.15  # proportion (0-1).

    W = 0.375  # Width (0-1) of the figure
    H = 0.75  # Height (0-1) of the figure

    mean = np.mean(Ys, axis=0)
    std = np.std(Ys, axis=0)
    min = mean - 2 * std
    max = mean + 2 * std

    fig = plt.figure(figsize=Dimension)
    ax = fig.add_axes([Left,
                       Bottom,
                       W,
                       H
                       ])
    plt.suptitle(titre)

    # Fill between the min and max std
    plt.fill_between(X, min, max, color=(0, 0.75, 1, 0.25))
    """
    # Draw the molecules
    # Trip
    plt.fill_between([0, 5], [1.35, 1.35], color=(0, 1, 0, 0.25))
    # DRB
    plt.fill_between([6, 11], [1.35, 1.35], color=(0, 0.5, 0, 0.25))
    # CHX
    plt.fill_between([2, 3], [1.25, 1.25], color=(1, 0, 0, 0.25))
    plt.fill_between([8, 9], [1.25, 1.25], color=(1, 0, 0, 0.25))
    # HARR
    plt.fill_between([4, 7], [1.25, 1.25], color=(0.5, 0, 0, 0.25))
    """
    # Draw the STD curves
    plt.plot(X, min, color=(0, 0, 1))
    plt.plot(X, max, color=(0, 0, 1))

    # Draw all genes curves
    for g in range(0, n_genes, 1):
        plt.plot(X, Ys[g], color=(1, 0, 0, 0.25))

    # Draw the Mean
    plt.plot(X, mean, color=(0, 0, 0))
    plt.setp(ax.get_xticklabels(),
             rotation=30, ha='right',
             rotation_mode='anchor',
             fontsize=8)

    plt.ylim(ymin=0, ymax=1.35)
    plt.savefig(pdf,
                format='pdf',
                dpi=300)
    plt.close()


def heatmap(data, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """Create a heatmap from a numpy array and two lists of labels."""
    """
    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, aspect='auto', **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


if __name__ == "__main__":
    # execute only if run as a script
    Bars_Clustering()
