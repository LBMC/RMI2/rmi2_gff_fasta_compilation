#!/usr/bin/python
# coding: utf-8
"""Analyse all genes to attribute explicit GO terms."""
# Python libraries
# Home made scripts and packages
from config import path_Output
from config import path_GO_annotated
from utils.time_stamp import message_time


def GO_obtainer(log_file):
    """Generate a csv file GO to Phrase."""
    message = message_time('Starting GO_obtainer.\n')
    print(message)
    log_file.write(message)

    # Create the converter file
    conv_name = 'GO_to_Phrase.tsv'
    open(path_Output + conv_name, 'w+').close
    convf = open(path_Output + conv_name, 'w+')
    header = 'GO_id\tName\tNamespace\n'
    convf.write(header)
    message = message_time('GO_to_Phrase.tsv file created.\n')
    print(message)
    log_file.write(message)

    # Initiate key variables
    done = range(1000, 100000, 1000)
    nGO = 0
    GO_line = ''
    myGO = ''
    myName = ''
    myNameSpace = ''

    # Open the .obo file
    with open(path_GO_annotated) as database:
        # Read the firstline
        line = database.readline()

        # Read all lines
        while line:

            # Find a new GO term
            line = clean_line(line)
            if line.startswith('id:'):
                nGO += 1
                myGO = line.split('id: ')[1]
            elif line.startswith('name:'):
                myName = line.split('name: ')[1]
            elif line.startswith('namespace:'):
                myNameSpace = line.split('namespace: ')[1]
            elif myGO != '' and myName != '' and myNameSpace != '':
                # We have all what we need
                if nGO in done:
                    message = message_time(str(nGO) + ' GO terms acquired.\n')
                    print(message)
                    log_file.write(message)

                GO_line = myGO + '\t'
                GO_line += myName + '\t'
                GO_line += myNameSpace + '\n'
                convf.write(GO_line)

                # Reset the values
                GO_line = ''
                myGO = ''
                myName = ''
                myNameSpace = ''

            # read new line
            line = database.readline()

    # Update last entry
    convf.write(GO_line)

    message = message_time(str(nGO) + ' GO terms acquired.\n')
    print(message)
    log_file.write(message)

    message = message_time('End GO_obtainer.\n\n')
    print(message)
    log_file.write(message)


def clean_line(line):
    """Remove unwanted characters from line."""
    unwanted = ['"', '\n']
    for char in unwanted:
        line = line.replace(char, '')
    return line


if __name__ == "__main__":
    # execute only if run as a script
    GO_obtainer()
