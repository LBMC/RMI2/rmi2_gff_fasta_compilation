#!/usr/bin/python
# coding: utf-8
"""Core object of the project analysing CDS and creating file."""
# Python libraries
from __future__ import division
from utils.time_stamp import string_time
import numpy as np


class Shredder:
    """This object is generated from a specific .csv database.

    It resume all lines and their properties.
    """

    def __init__(self,
                 path,
                 separator
                 ):
        """Construct method."""
        self._separator = separator
        self._path = path
        self._2nd_Keys = []
        self._dict_values = {}
        self._trans_IDs = np.array([])
        # The dictionary will created like this:
        # self._dict_values['Transcript_ID', '2nd_Key']
        # 2nd_Keys are the header column names

        # Reading the file
        print('   #   ' + string_time() + '   #   Shredding csv database')
        self._read_file()

    def _read_file(self):
        """Read the file and feed the dictionary."""
        # line counter
        nlines = 0
        with open(self._path) as database:
            # Read the firstline
            line = database.readline()

            # Read all lines
            while line:
                nlines += 1
                line = self._clean_line(line)
                # Decide if line belongs to header or describe a value
                if nlines == 1:
                    # feed the header
                    self._header = line.split(self._separator)
                    # print(self._header)
                else:
                    # Remove breakline and split the line between key and value
                    frags = line.split(self._separator)

                    # transcript ID is in frags[1]
                    self._trans_IDs = np.append(self._trans_IDs, frags[1])
                    for key in range(0, len(self._header), 1):
                        self._dict_values[frags[1],
                                          self._header[key]] = frags[key]
                line = database.readline()
        print('   #   ' + string_time() + '   #   Table has: ' + str(nlines))

    def _clean_line(self, line):
        """Remove unwanted characters from line."""
        unwanted = ['"', '\n']
        for char in unwanted:
            line = line.replace(char, '')
        return line
