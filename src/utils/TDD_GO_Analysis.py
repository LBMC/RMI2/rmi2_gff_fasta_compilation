#!/usr/bin/python
# coding: utf-8
"""Perform TDD distribution and GO analysis."""
import matplotlib
import os
from config import path_Values
from config import path_Output
from config import path_gff3, path_fa
from utils.time_stamp import string_time, message_time
from config import Dimension, H, W, Left, Bottom
from config import Cells, Time, Treatment, Indexes
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd


def Analyse_GO(log_file):
    """Determine hits and distribution for GO and compare to global pop."""
    message = message_time('Starting Analyse_GO.\n')
    print(message)
    log_file.write(message)

    # 1. Checking the specific .gff3 vs .fa input folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Open the database values as a shredder dictionnary
    db = pd.read_csv(path_Values)
    header = db.columns.values.tolist()

    # Dictionnary of values to get for each conditions
    Dict_Keys = {}

    # Feed the dictionary and create the conditions list.
    (Conditions,
     Dict_Keys) = feed_Dict(Dict_Keys,
                            Cells,
                            Time,
                            Treatment,
                            Indexes)

    # Analyse all TDD values for each Condition
    for condition in Conditions:
        Explore_TDD(condition,
                    Dict_Keys,
                    db,
                    log_file)

    message = message_time('End Analyse_GO.\n\n')
    print(message)
    log_file.write(message)


def feed_Dict(Dict,
              Cells,
              Time,
              Treatment,
              Indexes):
    """Create the dictionnary of keyword for each condition."""
    Conditions = []
    for index in Indexes:
        for cell in Cells:
            for treat in Treatment:
                for time in Time:
                    condition = index + '>'
                    condition += cell + '>'
                    condition += treat + '>'
                    condition += time

                    Conditions.append(condition)

                    Key_OK_to_go = cell + '>GOLD'
                    Dict[condition] = Key_OK_to_go

    return (Conditions, Dict)


def Explore_TDD(condition,
                Dict_Keys,
                database,
                log_file):
    """Analyse the TDD as a fonction of the condition."""
    message = message_time('Treating ' + condition + '.\n')
    print(message)
    log_file.write(message)

    nom_pdf = condition.replace('>', '_')
    nom_pdf = nom_pdf.replace('(', '')
    nom_pdf = nom_pdf.replace(')', '')

    # Create the PdfPages
    pdf = PdfPages(path_Output + nom_pdf + '_TDD_GO.pdf')

    # Initialize all required variables
    raw_TDD = []
    list_GO = []
    TDD_GO = {}
    hit_GO = {}

    # Get only wanted values: cond>GOLD = 1
    col = Dict_Keys[condition]

    # Simplify the database
    # 1 Get only the usefull column
    df_simple = database[['transcript_id',
                          col,
                          condition,
                          'GO_Name']]

    # 2 Get only the useful lines
    df_simple = df_simple.loc[df_simple[col] == 1]
    df_simple = df_simple.dropna()

    # 3 Get all unique transcript id
    transcript_IDs = np.unique(df_simple['transcript_id'].values)
    transcript_IDs = transcript_IDs.tolist()

    done = range(1000, len(transcript_IDs) + 1000, 1000)

    n_trans = 0
    # Loop in all transcripts
    for trans in transcript_IDs:

        # Get only the values corresponding to the transcript
        df1 = df_simple.loc[df_simple['transcript_id'] == trans]
        loc_vals = df1.values[0]

        tdd = float(loc_vals[2])
        if tdd > 1:
            tdd = 1.2

        # Update the TDD index values list
        raw_TDD = np.append(raw_TDD,
                            tdd)
        # Retrieve all GO
        current_GO = loc_vals[3]
        current_GO = current_GO.split(';')

        # Create or update entry for all the gene GO Names
        for go in current_GO:
            if go in list_GO:
                hit_GO[go] = hit_GO[go] + 1
                TDD_GO[go] = np.append(TDD_GO[go],
                                       float(tdd))
            elif go != '':
                list_GO.append(go)
                hit_GO[go] = 1
                TDD_GO[go] = [float(tdd)]

        n_trans += 1
        if n_trans in done:
            message = message_time(str(n_trans) + ' Transcripts done.\n')
            print(message)
            log_file.write(message)

    # Order all GO
    best_GO, best_hits = find_best_GO(list_GO, hit_GO)

    message = message_time('Generating distributions.\n')
    print(message)
    log_file.write(message)

    # Now that we have all TDD values we generate the probability density
    (density_TDD,
     bin_edges) = np.histogram(raw_TDD,
                               bins=50,
                               range=(-1, 1),
                               density=True)
    # Convert bin_edges in mean values to have the same number of values in
    # density_TDD and bins_mean
    bins_meanTDD = format_bin_edges(bin_edges)

    drawDistribution(bins_meanTDD,
                     density_TDD,
                     condition,
                     pdf)

    # Perform graph for the 20 most represented GO
    done = range(1000, 100000, 1000)
    n_go = 0

    for bestgo in range(0, len(best_GO), 1):
        nom = best_GO[bestgo]
        if hit_GO[nom] > 10:

            nom = best_GO[bestgo]
            score = best_hits[bestgo]
            (density_go,
             bin_edges) = np.histogram(TDD_GO[nom],
                                       bins=50,
                                       range=(-1, 1),
                                       density=True)

            bins_meanTDDgo = format_bin_edges(bin_edges)
            titre = condition + ' ' + str(score) + '\n' + nom
            drawDistribution(bins_meanTDD,
                             density_TDD,
                             titre,
                             pdf,
                             X2=bins_meanTDDgo,
                             Y2=density_go)
            n_go += 1

            if n_go in done:
                message = message_time(str(n_go) + ' GO done.\n')
                print(message)
                log_file.write(message)
    # Close the pdf file
    pdf.close()


def drawDistribution(X1,
                     Y1,
                     title,
                     pdf,
                     X2=[],
                     Y2=[]):
    """Draw the distribution of TDD and GO."""
    # create plot
    fig = plt.figure(figsize=Dimension)
    fig.add_axes([Left,
                  Bottom,
                  W,
                  H
                  ])

    plt.plot(X1,
             Y1,
             color=(0, 0, 0),
             linewidth=1)
    if len(X2) != 0 and len(Y2) != 0:
        plt.plot(X2,
                 Y2,
                 color=(1, 0.5, 0),
                 linewidth=2)
    plt.suptitle(title)
    plt.savefig(pdf,
                format='pdf',
                dpi=300)

    # Close the figure to spare memory
    plt.close()


def find_best_GO(list_GO,
                 hit_GO):
    """Find the GO with the most entries."""
    # Find the 20 best
    ranked_hits = []
    ranked_names = []

    for iteration in range(0, len(hit_GO), 1):
        top = 0
        candidat = ''
        for go in list_GO:
            if hit_GO[go] > top:
                top = hit_GO[go]
                candidat = go
        ranked_hits.append(top)
        ranked_names.append(candidat)

        # remove name already in the list
        list_GO.remove(candidat)

    return ranked_names, ranked_hits


def format_bin_edges(bin_edges):
    """Calculate the mean value for the bin edges."""
    values = []
    for i in range(0, len(bin_edges)-1, 1):
        m = round((bin_edges[i] + bin_edges[i+1])/2,
                  2)
        values = np.append(values, m)
    return values


if __name__ == "__main__":
    # execute only if run as a script
    Analyse_GO()
