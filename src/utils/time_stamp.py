#!/usr/bin/python
# coding: utf-8
"""Generate a time stamp string."""
# Python libraries
from datetime import datetime


def string_time():
    """Convert time into a string time stamp."""
    # Get current time value
    myTime_now = datetime.now()

    # Create the stamp
    stamp = str(myTime_now.year) + '-'
    stamp += digit(str(myTime_now.month)) + '-'
    stamp += digit(str(myTime_now.day)) + '_'
    stamp += digit(str(myTime_now.hour)) + '-'
    stamp += digit(str(myTime_now.minute)) + '-'
    stamp += digit(str(myTime_now.second))

    # Return the stamp
    return stamp


def message_time(msg):
    """Combine a string_time and a message."""
    txt = '   #   ' + string_time() + '   #   ' + msg
    return txt


def digit(myString):
    """Add 0 to single digit time/date value."""
    if len(myString) == 1:
        myString = '0' + myString
    return myString
