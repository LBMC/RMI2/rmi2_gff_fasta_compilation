#!/usr/bin/python
# coding: utf-8
"""Script that generate the .bed, .fa and .tev files for Ribosome Profiling."""
# Python libraries
import os
import glob
import numpy as np
# Home made scripts and packages
from config import appris_limit
from config import path_Output
from config import path_gff3, path_fa
from utils.time_stamp import message_time, string_time
from utils.Object_manipulator import Manipulator


def Export_Ribo_Profil(log_file):
    """Explore the transcript.csv collection and generate the files.

    In order to be stringent only the transcript passing the QC are used.
    """
    message = message_time('Starting Export_Ribo_Profil.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Creation of the time stamped log and ouptut files.
    time_FP = string_time()

    log_file.write(message_time('Starting Export.\n'))

    # fasta file
    fa_name = time_FP + '_' + gff3_name + '_VS_' + fasta_name + '.fa'
    fa_file = open(path_Output + fa_name, 'w+')
    log_file.write(message_time('Fasta file created.\n'))

    # tsv file
    tsv_name = time_FP + '_' + gff3_name + '_VS_' + fasta_name + '.tsv'
    tsv_file = open(path_Output + tsv_name, 'w+')
    log_file.write(message_time('Tsv file created.\n'))

    # bed file
    bed_name = time_FP + '_' + gff3_name + '_VS_' + fasta_name + '.bed'
    bed_file = open(path_Output + bed_name, 'w+')
    log_file.write(message_time('Bed file created.\n'))

    # Explore all the transcript.csv files
    log_file.write(message_time('Starting scanning.\n'))

    nFiles = explore_transcripts(input_folder,
                                 fa_file, tsv_file, bed_file,
                                 log_file)

    message = message_time(str(nFiles) + ' Transcript files processed.\n')
    print(message)

    # Close all files
    fa_file.close
    tsv_file.close
    bed_file.close

    message = message_time('End Export_Ribo_Profil.\n\n')
    print(message)
    log_file.write(message)


def explore_transcripts(input_folder,
                        fa_file, tsv_file, bed_file,
                        log_file):
    """Explore all csv file to extract data."""
    progress = np.arange(5000, 1000000, 5000).tolist()
    nFiles = 0
    for csv_file in glob.glob(input_folder + '*.csv'):

        # Load the content of the file
        f = Manipulator(csv_file)

        # Filter on QC passed files and appris level (set in config.py)
        filter1 = f._getValue(['QC_passed'])[0]
        filter2 = f._getValue(['appris_level'])[0]
        if filter2 == '':
            filter2 = 1000

        if filter1 == '1' and int(filter2) <= appris_limit:
            nFiles += 1
            export(f, fa_file, tsv_file, bed_file)
            if nFiles in progress:
                message = message_time(str(nFiles))
                message += ' Transcript files processed.\n'
                print(message)
                log_file.write(message)

    message = message_time(str(nFiles) + ' Transcript files processed.\n')
    log_file.write(message)

    return nFiles


def export(f,
           fa_file,
           tsv_file,
           bed_file):
    """Get, process and export data from a csv file for Ribosome Profiling."""
    # Initialisation of the strings to write in the files
    fa_string = '>'
    tsv_string = ''
    bed_string = ''

    # Create the common header
    [transcript_id,
     gene_name,
     mRNA_seq,
     mRNA_length,
     UTR5_seq,
     UTR5_length,
     CDS_seq,
     CDS_length,
     UTR3_seq,
     UTR3_length] = f._getValue(['transcript_id',
                                 'gene_name',
                                 'mRNA_seq',
                                 'mRNA_length',
                                 'UTR5_seq',
                                 'UTR5_length',
                                 'CDS_seq',
                                 'CDS_length',
                                 'UTR3_seq',
                                 'UTR3_length'])

    common_header = transcript_id + '_' + gene_name

    # Get sequences and properties
    mRNA_length = int(mRNA_length)
    UTR5_length = int(UTR5_length)
    UTR5_start = 0
    UTR5_end = UTR5_start + UTR5_length

    CDS_length = int(CDS_length)
    CDS_start = mRNA_seq.find(CDS_seq)
    CDS_end = CDS_start + CDS_length

    UTR3_length = int(UTR3_length)
    UTR3_start = CDS_end
    UTR3_end = UTR3_start + UTR3_length

    # Some verifications
    if UTR5_start != 0:
        print(common_header)
        print('Problem with 5UTR start:' + str(UTR5_start))
        print(str(0))
    if UTR5_end != CDS_start:
        print(common_header)
        print('Problem with 5UTR end:' + str(UTR5_end))
        print(str(CDS_start))
    if UTR3_start != CDS_end:
        print(common_header)
        print('Problem with 3UTR start:' + str(UTR3_start))
        print(str(CDS_end))
    if UTR3_end != mRNA_length:
        print(common_header)
        print('Problem with 3UTR end:' + str(UTR3_end))
        print(str(mRNA_length))

    # Update Files
    fa_string += common_header + '\n'
    fa_string += mRNA_seq + '\n'
    fa_file.write(fa_string)

    bed_string += common_header + '\t'
    bed_string += str(UTR5_start) + '\t' + str(UTR5_end) + '\t'
    bed_string += 'UTR5' + '\t' + '0' + '\t' + '+' + '\n'

    bed_string += common_header + '\t'
    bed_string += str(CDS_start) + '\t' + str(CDS_end) + '\t'
    bed_string += 'CDS' + '\t' + '0' + '\t' + '+' + '\n'

    bed_string += common_header + '\t'
    bed_string += str(UTR3_start) + '\t' + str(UTR3_end) + '\t'
    bed_string += 'UTR3' + '\t' + '0' + '\t' + '+' + '\n'
    bed_file.write(bed_string)

    tsv_string += common_header + '\t'
    tsv_string += str(mRNA_length) + '\n'
    tsv_file.write(tsv_string)


if __name__ == "__main__":
    # execute only if run as a script
    Export_Ribo_Profil()
