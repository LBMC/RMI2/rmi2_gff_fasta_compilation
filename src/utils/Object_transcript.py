#!/usr/bin/python
# coding: utf-8
"""Core object of the project analysing CDS and creating file."""
# Python libraries
from __future__ import division
from Bio.Seq import Seq
import numpy as np
# import time
# Home made scripts and packages
from utils.analysis_seq import ATGC
from utils.cds_translate import translate
from utils.Kozack_scores import frequency_score
from utils.Kozack_scores import efficiency_score_mono
from utils.Kozack_scores import efficiency_score_di


class Transcript:
    """This object is generated from a specific CDS.

    Using the inheritance within the sqlite3 database created by gffutils:
    -We first get the parent transcript and associated sequence in the fasta
    file (genomic sequence), and other features.
    -We then go back down to the level of the CDS, exons, start and stop codon.
    -We generate the ORF and the introns.
    -Some specific analysis are performed.

    A unique file is created/overwriten depending on the overwrite variable
    """

    def __init__(self,
                 header,
                 cds,
                 database,
                 path_fasta,
                 output_folder,
                 debug
                 ):
        """Construct method."""
        self._header = header
        self._pb = ''
        self._QC = str(0)

        # Errors
        self._inFrame_stop = 'NA'
        self._Ns = 'NA'
        self._tooShort = 'NA'
        self._WrongKozack = 'NA'
        self._noATG = 'NA'
        self._Wrong_Initiator_AA = 'NA'
        self._noStop = 'NA'
        self._Wrong_Terminator_AA = 'NA'
        self._noReported5UTR = 'NA'
        self._noReported3UTR = 'NA'
        self._Wrong_Donor_Site = 'NA'
        self._Wrong_Acceptor_Site = 'NA'
        self._Prot_seq_Incomplete = 'NA'

        # Transcript parameters
        self._gene_id = ''
        self._gene_name = ''
        self._chrom = ''
        self._trans_id = ''
        self._trans_level = ''
        self._trans_strand = ''
        self._trans_start = ''
        self._trans_end = ''
        self._trans_seq = ''
        self._trans_le = ''
        self._trans_A = ''
        self._trans_T = ''
        self._trans_G = ''
        self._trans_C = ''
        self._trans_N = ''

        # mRNA parameters
        self._mRNA_seq = ''
        self._mRNA_start = ''
        self._mRNA_end = ''
        self._mRNA_seq = ''
        self._mRNA_le = ''
        self._mRNA_A = ''
        self._mRNA_T = ''
        self._mRNA_G = ''
        self._mRNA_C = ''
        self._mRNA_N = ''

        # 5' UTR parameters
        self.nIntrons_5UTR = 0
        self.Introns_5UTR_id = ''
        self._5UTR_start = ''
        self._5UTR_end = ''
        self._5UTR_le = ''
        self._5UTR_seq = ''
        self._UTR5_50 = ''
        self._5UTR_A = ''
        self._5UTR_T = ''
        self._5UTR_G = ''
        self._5UTR_C = ''
        self._5UTR_N = ''
        self._n5UTRf = ''
        self._5UTRf_id = np.array([])
        self._5UTRf_start = np.array([])
        self._5UTRf_end = np.array([])
        self._5UTRf_le = np.array([])
        self._5UTRf_seq = np.array([])
        self._5UTRf_A = np.array([])
        self._5UTRf_T = np.array([])
        self._5UTRf_G = np.array([])
        self._5UTRf_C = np.array([])
        self._5UTRf_N = np.array([])

        # 3' UTR parameters
        self.nIntrons_3UTR = 0
        self.Introns_3UTR_id = ''
        self._3UTR_start = ''
        self._3UTR_end = ''
        self._3UTR_le = ''
        self._3UTR_seq = ''
        self._3UTR_A = ''
        self._3UTR_T = ''
        self._3UTR_G = ''
        self._3UTR_C = ''
        self._3UTR_N = ''
        self._n3UTRf = ''
        self._3UTRf_id = np.array([])
        self._3UTRf_start = np.array([])
        self._3UTRf_end = np.array([])
        self._3UTRf_le = np.array([])
        self._3UTRf_seq = np.array([])
        self._3UTRf_A = np.array([])
        self._3UTRf_T = np.array([])
        self._3UTRf_G = np.array([])
        self._3UTRf_C = np.array([])
        self._3UTRf_N = np.array([])

        # CDS parameters
        self._CDS_seq = ''
        self._CDS_start = ''
        self._CDS_end = ''
        self._CDS_seq = ''
        self._CDS_len = ''
        self._CDS_A = ''
        self._CDS_T = ''
        self._CDS_G = ''
        self._CDS_C = ''
        self._CDS_N = ''
        self._nCDSf = ''
        self._CDSf_id = np.array([])
        self._CDSf_start = np.array([])
        self._CDSf_end = np.array([])
        self._CDSf_n = np.array([])
        self._CDSf_le = np.array([])
        self._CDSf_seq = np.array([])
        self._Prot_seq = np.array([])
        self._CDSf_le = np.array([])
        self._CDSf_A = np.array([])
        self._CDSf_T = np.array([])
        self._CDSf_G = np.array([])
        self._CDSf_C = np.array([])
        self._CDSf_N = np.array([])

        # Kozack paramaters
        self._Kozack_seq = '-'
        self._Kozack_sc_freq = str(-1)
        self._Kozack_sc_eff = str(-1)
        self._Kozack_sc_di = str(-1)

        # exons parameters
        self._nExons = ''
        self._Exon_id = np.array([])
        self._Exon_le = np.array([])
        self._Exon_start = np.array([])
        self._Exon_end = np.array([])
        self._Exon_seq = np.array([])
        self._Exon_A = np.array([])
        self._Exon_T = np.array([])
        self._Exon_G = np.array([])
        self._Exon_C = np.array([])
        self._Exon_N = np.array([])

        # introns parameters
        self._nIntrons = 0
        self._Intron_id = np.array([])
        self._Intron_start = np.array([])
        self._Intron_end = np.array([])
        self._Intron_le = np.array([])
        self._Intron_seq = np.array([])
        self._Intron_A = np.array([])
        self._Intron_T = np.array([])
        self._Intron_G = np.array([])
        self._Intron_C = np.array([])
        self._Intron_N = np.array([])

        # Create the transcript with all parameters feeded
        self._getTranscript(cds,
                            database,
                            path_fasta)

    def _getKozack(self):
        """Get the kozack sequence if relevant."""
        initiation_codons = ['ATG', 'CTG']
        start_codon = self._CDS_seq[:3]
        # First check it is woorth doing calculation
        # Average MW of AA = 110 Da
        # Some ORF code for 8 kDa peptides ~ 70 AA -> 210
        if start_codon in initiation_codons and int(self._CDS_le) > 200:
            n_nucl = 120
            # Take the 120 first nucleotides to find the initiation codon in
            # the mRNA seq (initially it was 40... not enough)
            primer = self._CDS_seq[:n_nucl]

            # Validate that it is enough (Debug)
            c = self._mRNA_seq.count(primer)
            if c >= 2:
                print(str(n_nucl) + ' is not enough for ' + self._trans_id)

            # Find the position of the init codon
            position_init = self._mRNA_seq.find(primer)

            # Validate that we have enough nucleotides upstream
            if position_init >= 5:
                start_K = position_init - 5
                # We need up to + 5 only but substring is upper index excluded
                stop_K = position_init + 6
                self._Kozack_seq = self._mRNA_seq[start_K:stop_K]
                self._Kozack_sc_freq = frequency_score(self._Kozack_seq)
                self._Kozack_sc_eff = efficiency_score_mono(self._Kozack_seq)
                self._Kozack_sc_di = efficiency_score_di(self._Kozack_seq)

    def _getSplitted(self,
                     trans,
                     type,
                     database,
                     path_fasta):
        """Get the child type and infos for a splitted sequence."""
        # Initialisation of the paramaters
        nChild = 0
        start = str(0)
        end = str(0)
        child_id = np.array([])
        child_seq = np.array([])
        child_le = np.array([])
        child_A = np.array([])
        child_T = np.array([])
        child_G = np.array([])
        child_C = np.array([])
        child_N = np.array([])
        child_s = np.array([])
        child_e = np.array([])
        concatenated_seq = ''

        for child in database.children(trans,
                                       featuretype=type,
                                       order_by='start'):
            # Obtain start for the concatenated sequence
            if nChild == 0:
                start = str(child.start)

            # A new Child fragment has been found
            nChild += 1
            end = str(child.end)
            child_id = np.append(child_id, child.id)
            seq = child.sequence(path_fasta,
                                 use_strand=False)
            concatenated_seq += seq
            if self._trans_strand == '-':
                seq = Seq(seq)
                seq = str(seq.reverse_complement())

            # Analyse the sequence
            (le, A, T, G, C, N) = ATGC(seq)

            # Update arrays
            child_seq = np.append(child_seq, seq)
            child_le = np.append(child_le, str(le))
            child_A = np.append(child_A, str(A))
            child_T = np.append(child_T, str(T))
            child_G = np.append(child_G, str(G))
            child_C = np.append(child_C, str(C))
            child_N = np.append(child_N, str(N))
            child_s = np.append(child_s, str(child.start))
            child_e = np.append(child_e, str(child.end))

        # Process mRNA sequence
        if self._trans_strand == '-':
            concatenated_seq = Seq(concatenated_seq)
            concatenated_seq = str(concatenated_seq.reverse_complement())
        (concat_seq_le, concat_seq_A,
         concat_seq_T, concat_seq_G,
         concat_seq_C, concat_seq_N) = ATGC(concatenated_seq)

        return (start, end, concat_seq_le, concatenated_seq,
                concat_seq_A, concat_seq_T, concat_seq_G, concat_seq_C,
                concat_seq_N, nChild, child_id, child_s, child_e, child_le,
                child_seq, child_A, child_T, child_G, child_C, child_N)

    def _getTranscript(self,
                       cds,
                       database,
                       path_fasta):
        """Get the parent transcript of a CDS."""
        nTranscript = 0
        for trans in database.parents(cds,
                                      featuretype='transcript',
                                      order_by='start'):

            # Verifying only one Transcript per CDS
            nTranscript += 1
            self.trans = trans

            # 2019/11/26 Due to correct inheritance in database, only one
            # transcript for each each. I let this here in case of later
            # problems with other gff3 files
            if nTranscript >= 2:
                print('Warning several transcripts for CDS :' + cds.id)
            trans_seq = trans.sequence(path_fasta,
                                       use_strand=False)

            if trans.strand == '-':
                trans_seq = Seq(trans_seq)
                trans_seq = str(trans_seq.reverse_complement())

            # Get Attributes
            appris = str(-1)
            if 'tag' in trans.attributes:
                for att in trans.attributes['tag']:
                    if att.count('appris_principal_') == 1:
                        appris = att.replace('appris_principal_', '')
                        self._trans_level = str(appris)
            self._gene_id = trans.attributes['gene_id'][0]
            self._gene_name = trans.attributes['gene_name'][0]
            self._trans_id = trans.id
            self._chrom = trans.chrom
            self._trans_strand = trans.strand
            self._trans_start = trans.start
            self._trans_end = trans.end
            self._trans_seq = trans_seq

            # Get sequence properties
            (self._trans_le, self._trans_A, self._trans_T,
             self._trans_G, self._trans_C, self._trans_N) = ATGC(trans_seq)

            # Find CDS
            (self._CDS_start, self._CDS_end, self._CDS_le, self._CDS_seq,
             self._CDS_A, self._CDS_T, self._CDS_G, self._CDS_C, self._CDS_N,
             self._nCDSf, self._CDSf_id, self._CDSf_start, self._CDSf_end,
             self._CDSf_le, self._CDSf_seq, self._CDSf_A, self._CDSf_T,
             self._CDSf_G, self._CDSf_C,
             self._CDSf_N) = self._getSplitted(trans,
                                               'CDS',
                                               database,
                                               path_fasta)
            # Translate CDS
            self._Prot_seq = translate(self._CDS_seq)

            # Find Exons
            (self._mRNA_start, self._mRNA_end, self._mRNA_le, self._mRNA_seq,
             self._mRNA_A, self._mRNA_T, self._mRNA_G, self._mRNA_C,
             self._mRNA_N, self._nExons, self._Exon_id, self._Exon_start,
             self._Exon_end, self._Exon_le, self._Exon_seq,
             self._Exon_A, self._Exon_T, self._Exon_G, self._Exon_C,
             self._Exon_N) = self._getSplitted(trans,
                                               'exon',
                                               database,
                                               path_fasta)

            # Get the Kozack context
            # The mRNA is required to get the nucleotides before initatiation
            # codon
            self._getKozack()

            # Find five_prime_UTR
            (self._5UTR_start, self._5UTR_end, self._5UTR_le, self._5UTR_seq,
             self._5UTR_A, self._5UTR_T, self._5UTR_G, self._5UTR_C,
             self._5UTR_N, self._n5UTRf, self._5UTRf_id, self._5UTRf_start,
             self._5UTRf_end, self._5UTRf_le, self._5UTRf_seq,
             self._5UTRf_A, self._5UTRf_T, self._5UTRf_G, self._5UTRf_C,
             self._5UTRf_N) = self._getSplitted(trans,
                                                'five_prime_UTR',
                                                database,
                                                path_fasta)

            if len(self._5UTR_seq) >= 50:
                self._UTR5_50 = self._5UTR_seq[-50:]
            else:
                self._UTR5_50 = 'NA'

            if self._n5UTRf > 0:
                self.nIntrons_5UTR = self._n5UTRf - 1
            else:
                self.nIntrons_5UTR = 0

            # Find three_prime_UTR
            (self._3UTR_start, self._3UTR_end, self._3UTR_le, self._3UTR_seq,
             self._3UTR_A, self._3UTR_T, self._3UTR_G, self._3UTR_C,
             self._3UTR_N, self._n3UTRf, self._3UTRf_id, self._3UTRf_start,
             self._3UTRf_end, self._3UTRf_le, self._3UTRf_seq,
             self._3UTRf_A, self._3UTRf_T, self._3UTRf_G, self._3UTRf_C,
             self._3UTRf_N) = self._getSplitted(trans,
                                                'three_prime_UTR',
                                                database,
                                                path_fasta)

            if self._n3UTRf > 0:
                self.nIntrons_3UTR = self._n3UTRf - 1
            else:
                self.nIntrons_3UTR = 0
