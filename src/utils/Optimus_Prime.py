#!/usr/bin/python
# coding: utf-8
"""Code from Sample et al 2019."""
# https://www.nature.com/articles/s41587-019-0164-5

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import seaborn as sns
from sklearn import preprocessing
import keras


pd.set_option("display.max_colwidth", 100)
sns.set(style="ticks", color_codes=True)

np.random.seed(1337)

# Plotting settings
plt.rcParams['font.weight'] = 'normal'
plt.rcParams['axes.labelweight'] = 'normal'
plt.rcParams['axes.labelpad'] = 5
plt.rcParams['axes.linewidth'] = 2.5
plt.rcParams['xtick.labelsize'] = 20
plt.rcParams['ytick.labelsize'] = 20
plt.rcParams['axes.labelsize'] = 20
plt.rcParams['figure.dpi'] = 200
plt.rcParams['lines.linewidth'] = 7
plt.rcParams['legend.markerscale'] = 1.5
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['xtick.major.size'] = 8
plt.rcParams['ytick.major.size'] = 8
plt.rcParams['xtick.minor.size'] = 8
plt.rcParams['ytick.minor.size'] = 8
plt.rcParams['xtick.minor.width'] = 3
plt.rcParams['ytick.minor.width'] = 3
plt.rcParams['xtick.major.width'] = 2.5
plt.rcParams['ytick.major.width'] = 2.5
plt.rcParams['axes.facecolor'] = 'white'
plt.rcParams['figure.autolayout'] = True
plt.rcParams['mathtext.default'] = 'regular'
plt.rcParams['xtick.color'] = 'black'
plt.rcParams['ytick.color'] = 'black'
plt.rcParams['axes.labelcolor'] = "black"
plt.rcParams['axes.edgecolor'] = 'black'


def test_data(df, model, test_seq, obs_col, output_col='pred'):
    """Take the one hot score and apply the model."""
    scaler = preprocessing.StandardScaler()
    scaler.fit(df[obs_col].values.reshape(-1, 1))
    # df.loc[:,'obs_stab'] = test_df['stab_df']
    predictions = model.predict(test_seq).reshape(-1)
    df.loc[:, output_col] = scaler.inverse_transform(predictions)
    return df


def one_hot_encode(df, col='utr', seq_len=50):
    """Generate the one hot score."""
    # Dictionary returning one-hot encoding of nucleotides.
    nuc_d = {'a': [1, 0, 0, 0],
             'c': [0, 1, 0, 0],
             'g': [0, 0, 1, 0],
             't': [0, 0, 0, 1],
             'n': [0, 0, 0, 0]}

    # Creat empty matrix.
    vectors = np.empty([len(df), seq_len, 4])

    # Iterate through UTRs and one-hot encode
    for i, seq in enumerate(df[col].str[:seq_len]):
        seq = seq.lower()
        a = np.array([nuc_d[x] for x in seq])
        vectors[i] = a
    return vectors


def r2(x, y):
    """Calculate the correlation R2 value."""
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    return r_value**2


def Optimus(pathCSV,
            colutr,
            colname,
            pathModel='src/data/retrained_main_MRL_model.hdf5'):
    """Calculate the prediction of Mean ribosome Load from 5UTR."""
    colpred = colname+'_pred'
    # Open the csv file
    df = pd.read_csv(pathCSV)
    df = df[['transcript_id', colutr, colname]]

    # Drop NaN
    df = df.dropna()
    # Drop inf
    df = df[df[colname] != np.inf]
    # Drop all sequences < 50
    df = df[df[colutr].apply(lambda x: len(x) == 50)]

    # calculate the one hot score
    oh_seqs = one_hot_encode(df=df, col=colutr, seq_len=50)

    # Apply the model
    retrained_model = keras.models.load_model(pathModel)
    df = test_data(df=df,
                   model=retrained_model,
                   test_seq=oh_seqs,
                   obs_col=colname,
                   output_col=colpred)

    r_squared = r2(df[colname], df[colpred])
    all_norm_obs = df[colname]
    all_norm_pred = df[colpred]

    c2 = sns.color_palette("hls", 8)[5]

    f, ax = plt.subplots()
    f.set_size_inches((5, 5))
    ax.scatter(all_norm_obs,
               all_norm_pred,
               alpha=0.5,
               color=c2,
               s=2)

    ax.set_xlabel('Observed MRL', size=20)
    ax.set_ylabel('Predicted MRL', size=20)
    ax.text(x=.71,
            y=0.03,
            s='r$^2$: ' + str(round(r_squared, 3)),
            transform=ax.transAxes,
            size=16)
    ax.set_xlim((0, 5))
    ax.set_ylim((-20, 20))
    leg = ax.legend(loc='upper left',
                    fontsize=16,
                    handletextpad=-0.2,
                    markerscale=6)
    for lh in leg.legendHandles:
        lh.set_alpha(1)
    sns.despine()
    plt.show()


if __name__ == "__main__":
    # execute only if run as a script
    Optimus()
