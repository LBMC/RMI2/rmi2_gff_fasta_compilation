#!/usr/bin/python
# coding: utf-8
"""Transfer the GO term to all transcript of a gene."""
# Python libraries
from __future__ import division
import os

# Home made scripts and packages
from config import path_Output
from config import path_gff3, path_fa, path_Gene_GO
from utils.time_stamp import message_time
from utils.Object_shredder_GO import Shredder_GO
from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator


def Add_GO(log_file):
    """Open the GO_to_Phrase.tsv and add the data to the transcript files."""
    message = message_time('Starting Add_GO.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep
    path_GO = path_Output + 'GO_to_Phrase.tsv'

    # Get gene to transcripts conversion
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    db = Shredder_simple(conv_path, ',')

    # Get gene to GO conversion
    gene_go_db = Shredder_simple(path_Gene_GO, '\t')

    # transform GO database into an object
    GOES = Shredder_GO(path_GO, '\t')
    message = message_time('Starting transfer.\n')
    print(message)
    log_file.write(message)

    Done = range(1000, 100000, 1000)
    ngene = 0
    pb = 0
    for gene in db._First_Col:
        ngene += 1
        # get all GO terms for the current gene
        if gene in gene_go_db._First_Col:
            raw_GO = gene_go_db._dict_values[gene, 'GO_Term']
            list_GO = raw_GO.split(';')

            list_name = ''
            list_namespace = ''

            for go in list_GO:
                list_name += GOES._dict_values[go][0] + ';'
                list_namespace += GOES._dict_values[go][1] + ';'

            # Clean names and namespace from ','
            list_name = list_name.replace(',', '.')
            list_namespace = list_namespace.replace(',', '.')

            # Get the list of the transcripts associated to the gene
            list_files_t = db._dict_values[gene, 'transcript_id']
            transcripts = list_files_t.split(';')
            for local_transcript in transcripts:

                # Transform the transcript file into modifiable object
                f = Manipulator(input_folder + local_transcript + '.csv')

                # Update the object and the file
                f._appendValue('GO_Term', raw_GO)
                f._appendValue('GO_Name', list_name)
                f._appendValue('GO_NameSpace', list_namespace)
                f._updateFile()
        else:
            pb += 1
            # Get the list of the transcripts associated to the gene
            list_files_t = db._dict_values[gene, 'transcript_id']
            transcripts = list_files_t.split(';')
            for local_transcript in transcripts:

                # Transform the transcript file into modifiable object
                f = Manipulator(input_folder + local_transcript + '.csv')

                # Update the object and the file
                f._appendValue('GO_Term', 'NA')
                f._appendValue('GO_Name', 'NA')
                f._appendValue('GO_NameSpace', 'NA')
                f._updateFile()

        if ngene in Done:
            message = message_time(str(ngene) + ' Genes treated.\n')
            print(message)
            log_file.write(message)

    message = message_time(str(ngene) + ' Genes treated.\n')
    print(message)
    log_file.write(message)

    message = message_time('End Add_GO.\n\n')
    print(message)
    log_file.write(message)


if __name__ == "__main__":
    # execute only if run as a script
    Add_GO()
