#!/usr/bin/python
# coding: utf-8
"""Extract Free Energy values and append to transcript.csv.

The percent of GC is harvested from the transcript.csv file and used to
generate an explicit table.
"""
import os

# Homemade utilities
from config import path_Output
from config import path_gff3, path_fa
from utils.Object_manipulator import Manipulator
from utils.time_stamp import message_time, string_time


def Transfer_DG(path_free_energy,
                log_file):
    """Core function of the program."""
    message = message_time('Starting Transfer_DG.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
    output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Create the time stamped output file
    file_name = string_time() + '_Free_Energy.csv'
    subset_file = open(path_Output + file_name, 'w+')
    message = message_time('Output file created.\n')
    print(message)
    log_file.write(message)

    # Generate the header
    values_list = ('transcript_id',
                   'gene_name',
                   'type',
                   'DG',
                   'percent(GC)')
    header = format_list_csv(values_list)

    # Transfer header to the file
    subset_file.write(header)

    feed_csv_file(path_free_energy,
                  output_folder,
                  subset_file,
                  log_file)

    message = message_time('DG Transfer is over.\n\n')
    print(message)
    log_file.write(message)


def feed_csv_file(path_free_energy,
                  output_folder,
                  subset_file,
                  log_file):
    """Extract the DG values and feed the csv file."""
    """Example of a complete line within the RNAfold output file:

    >ENSMUST00000000001.4_Gnai3_CDS(-305.70)

    """
    # 4 read the file and do QC on each line before any manipulation
    # line counter
    nlines = 0
    linesOK = 0
    with open(path_free_energy) as database:
        # Read the firstline
        line = database.readline()

        # Read all lines
        while line:
            nlines += 1

            # Determine if line is complte
            if line.count(')') == 1:
                linesOK += 1

                # >ENSMUST00000000001.4_Gnai3_CDS(-305.70)\n
                line = line.replace('\n', '')
                # >ENSMUST00000000001.4_Gnai3_CDS(-305.70)
                line = line.replace('>', '')
                # ENSMUST00000000001.4_Gnai3_CDS(-305.70)
                line = line.replace('(', '_')
                # ENSMUST00000000001.4_Gnai3_CDS_-305.70)
                line = line.replace(')', '')
                # ENSMUST00000000001.4_Gnai3_CDS_-305.70
                infos = line.split('_')
                # (ENSMUST00000000001.4, Gnai3, CDS, -305.70)
                transcriptID = infos[0]
                geneName = infos[1]
                type = infos[2]
                DG = infos[3]

                treat_transcript(transcriptID,
                                 output_folder,
                                 DG,
                                 geneName,
                                 type,
                                 subset_file)

            line = database.readline()

    subset_file.close()

    message = message_time('Table has: ' + str(nlines))
    print(message)
    log_file.write(message)

    message = message_time('Lines OK: ' + str(linesOK))
    print(message)
    log_file.write(message)


def treat_transcript(transcriptID,
                     output_folder,
                     DG,
                     geneName,
                     type,
                     subset_file):
    """Add DG values to the transcript files."""
    nomfichier = transcriptID.split('.')[0]
    pathFichier = output_folder + nomfichier + '.csv'
    if os.path.isfile(pathFichier):
        # print(pathFichier)
        current_transcript = Manipulator(pathFichier)

        if type in ('CDS', 'UTR5', 'UTR3'):
            if type == 'CDS':
                values_t = ['CDS_G', 'CDS_C']
                newkey = 'CDS_DG'
            elif type == 'UTR5':
                values_t = ['UTR5_G', 'UTR5_C']
                newkey = 'UTR5_DG'
            elif type == 'UTR3':
                values_t = ['UTR3_G', 'UTR3_C']
                newkey = 'UTR3_DG'

            [g, c] = current_transcript._getValue(values_t)
            percent_GC = str(round(float(g) + float(c),
                                   2)
                             )

        if type == '47UTR5-30CDS':
            newkey = '47UTR5-30CDS_DG'
            percent_GC = 'NA'

        if type == '31CDS-End':
            newkey = '31CDS-End_DG'
            percent_GC = 'NA'

        current_transcript._appendValue(newkey,
                                        DG)
        current_transcript._updateFile()

        list_to_csv = (transcriptID,
                       geneName,
                       type,
                       DG,
                       percent_GC)
        to_append = format_list_csv(list_to_csv)
        subset_file.write(to_append)


def format_list_csv(list):
    """Transform list into a csv line."""
    line = ''
    for item in list:
        # Position correctly the exact number of ','
        if line == '':
            line += str(item)
        else:
            line += ',' + str(item)
    # Finish header with line break
    line += '\n'
    return line


if __name__ == "__main__":
    # execute only if run as a script
    Transfer_DG()
