#!/usr/bin/python
# coding: utf-8
"""Study the complexes of PPI as function of indexes TDD."""
# Python libraries
from __future__ import division
import os
import numpy as np
import pandas as pd
from config import path_Output, path_UNIPROT_to_ENSEMBL, path_UNIPROT_to_ENTREZ
from config import path_gff3, path_fa, path_Values
from utils.time_stamp import message_time
from utils.Object_manipulator import Manipulator
from utils.Distributions_GO import clean_name
from config import max_cap, min_cap


def distribution_complexes(log_file,
                           complex_db,
                           complex_list,
                           core_node,
                           index,
                           path_Interactions):
    """Study all complexes for a specific index."""
    message = message_time('Starting distribution_complexes.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    mymean = get_mean(index, log_file)

    cytoscape_header = 'Source,Interaction,Target,TDD1,Type\n'
    current_csv = cytoscape_header
    current_csv += core_node[0] + ',,,NA,' + core_node[1] + '\n'

    # Get the dictionary converting an uniprot to index value or gene_name
    (index_of,
     gene_name_of) = from_UNIPROT_to_transcript(index,
                                                input_folder,
                                                mymean,
                                                log_file)
    (uniprot_of,
     entrez_of) = from_ENTREZ_to_UNIPROT(log_file,
                                         index_of,
                                         gene_name_of)

    # Open the complex database
    pd_complexes = pd.read_csv(complex_db)

    # Extract only the wanted columns
    wanted_columns = ['ComplexID',
                      'ComplexName',
                      'subunits(UniProt IDs)',
                      'GO ID']
    pd_complexes = pd_complexes[wanted_columns]

    # Calculate the mean index
    (pd_complexes,
     updated_csv,
     dict_Uniprot_index) = calculate_complex_index(pd_complexes,
                                                   index_of,
                                                   gene_name_of,
                                                   complex_list,
                                                   current_csv,
                                                   core_node,
                                                   log_file)

    # Add First corona of physical interactions
    updated_csv = get_physical_interaction(path_Interactions,
                                           dict_Uniprot_index,
                                           uniprot_of,
                                           entrez_of,
                                           updated_csv,
                                           log_file)

    # Save the database
    clean_cond = clean_name(index)
    path = path_Output + 'Complexes_' + clean_cond + '.csv'
    pd_complexes.to_csv(path, index=False)

    path2 = path_Output + 'Cytoscape_' + clean_cond + '.csv'
    f = open(path2, 'w+')
    f.write(updated_csv)
    f.close()

    message = message_time('End distribution_complexes.\n\n')
    print(message)
    log_file.write(message)


def get_mean(index,
             log_file):
    """Get the mean value for the specific index to calculate delta."""
    message = message_time('Starting get_mean.\n')
    print(message)
    log_file.write(message)

    # Open the database values as a shredder dictionnary
    db = pd.read_csv(path_Values)

    # Initialize all required variables
    db = db[index]
    db.dropna(inplace=True)
    raw_TDD = db.values
    wanted_mean = np.mean(raw_TDD)

    message = message_time('End get_mean.\n\n')
    print(message)
    log_file.write(message)

    return wanted_mean


def get_physical_interaction(path_Interactions,
                             dict_Uniprot_index,
                             uniprot_of,
                             entrez_of,
                             updated_csv,
                             log_file):
    """Extract physical interactions and add them to the interactome csv."""
    message = message_time('Starting get_physical_interaction.\n')
    print(message)
    log_file.write(message)

    # list of protein of interest
    known = dict_Uniprot_index.keys()
    print(str(len(known)) + ' proteins to study.')

    # convert into an entrez list
    seeked_entrez = []
    for k in known:
        if k in entrez_of.keys():
            seeked_entrez.append(entrez_of[k])
    print(str(len(seeked_entrez)) + ' proteins in the interaction database.')

    # load the curated biogrid database
    ppi_pd = pd.read_csv(path_Interactions)

    know_Uniprot = uniprot_of.keys()

    # Reduce the complexity
    ppi_pd = ppi_pd[['Entrez Gene Interactor A',
                     'Entrez Gene Interactor B']]
    for entrez in seeked_entrez:
        filter = ((ppi_pd['Entrez Gene Interactor A'] == entrez) |
                  (ppi_pd['Entrez Gene Interactor B'] == entrez)
                  )
        loc_ppis = ppi_pd[filter]
        n_ppis = loc_ppis.shape[0]
        for interaction in range(0, n_ppis, 1):
            A = loc_ppis.iloc[interaction, 0]
            B = loc_ppis.iloc[interaction, 1]
            if A in know_Uniprot and B in know_Uniprot:
                if A != entrez:
                    msg = str(uniprot_of[A][1]).upper() + ',direct,'
                    msg += str(uniprot_of[B][1]).upper() + ','
                    msg += str(uniprot_of[A][2]) + ',protein\n'
                else:
                    msg = str(uniprot_of[B][1]).upper() + ',direct,'
                    msg += str(uniprot_of[A][1]).upper() + ','
                    msg += str(uniprot_of[B][2]) + ',protein\n'

                updated_csv += msg

    message = message_time('End get_physical_interaction.\n\n')
    print(message)
    log_file.write(message)

    return updated_csv


def calculate_complex_index(pd_complexes,
                            index_of,
                            gene_name_of,
                            complex_list,
                            current_csv,
                            core_node,
                            log_file):
    """Calculate the mean index and STD for all complexes."""
    message = message_time('Starting calculate_complex_index.\n')
    print(message)
    log_file.write(message)

    dict_Uniprot_index = {}
    Means = []
    STDs = []
    n_theo = []
    n_found = []

    # get all complexes names
    print(complex_list)
    filterWanted = pd_complexes['ComplexID'].isin(complex_list)
    pd_complexes = pd_complexes[filterWanted]
    print(pd_complexes.shape[0])
    complexes = pd_complexes['ComplexName'].values

    # get all complexes component and their values
    for complex in complexes:
        print(complex)

        filter = pd_complexes['ComplexName'] == complex
        sub_units = pd_complexes['subunits(UniProt IDs)'][filter].values
        sub_units = sub_units[0].split(';')
        loc_n_theo = len(sub_units)

        loc_values = []
        loc_n_found = 0
        for subunit in sub_units:
            if subunit in index_of.keys():
                sub_index = index_of[subunit]
                dict_Uniprot_index[subunit] = sub_index
                current_csv += str(gene_name_of[subunit]).upper() + ',belong,'
                current_csv += complex + ',' + str(sub_index) + ',protein\n'

                if sub_index != 'NA':
                    loc_n_found += 1
                    loc_values.append(sub_index)
                else:
                    loc_values.append(np.nan)
                    print(np.nan)

        if loc_n_found > 0:
            loc_mean = np.nanmean(loc_values)
            loc_std = np.std(loc_values)
        else:
            loc_mean = np.nan
            loc_std = np.nan

        Means.append(loc_mean)
        STDs.append(loc_std)
        n_theo.append(loc_n_theo)
        n_found.append(loc_n_found)

        current_csv += complex + ',direct,' + core_node[0] + ','
        current_csv += str(loc_mean) + ',complex\n'

    # Add new columns to the dataframe
    pd_complexes['mean'] = Means
    pd_complexes['std'] = STDs
    pd_complexes['theorical_n_subunits'] = n_theo
    pd_complexes['with_index_n_subunits'] = n_found

    message = message_time('End calculate_complex_index.\n')
    print(message)
    log_file.write(message)

    return (pd_complexes, current_csv, dict_Uniprot_index)


def from_ENTREZ_to_UNIPROT(log_file,
                           index_of,
                           gene_name_of):
    """Generate a dict of entrez entries with uniprot reference as value."""
    message = message_time('Starting from_ENTREZ_to_UNIPROT.\n')
    print(message)
    log_file.write(message)

    dict = {}
    entrez_of = {}

    # load the entrez to uniprot database
    pd_entrez = pd.read_csv(path_UNIPROT_to_ENTREZ)
    list_entrez_id = pd_entrez['ENTREZ'].values

    # as an entrez gene can have various isoform proteins (some duplications)
    # get only unique entries
    list_entrez_id = np.unique(list_entrez_id).tolist()
    print(str(len(list_entrez_id)) + ' entries.')
    # Feed dictionaries
    n_missing = 0
    for entrez in list_entrez_id:
        filter1 = pd_entrez['ENTREZ'] == entrez
        # we check possible unprot of an entry
        uniprots = pd_entrez['UNIPROT'][filter1].values
        found = False
        for uniprot in uniprots:
            if uniprot in index_of.keys():
                dict[entrez] = (uniprot,
                                gene_name_of[uniprot],
                                index_of[uniprot])
                entrez_of[uniprot] = entrez
                found = True
                break

        if not found:
            n_missing += 1
            print(str(entrez) + ' is missing')
    print(str(n_missing) + ' missing correspondances.')

    message = message_time('End from_ENTREZ_to_UNIPROT.\n\n')
    print(message)
    log_file.write(message)

    return dict, entrez_of


def from_UNIPROT_to_transcript(index,
                               input_folder,
                               mymean,
                               log_file):
    """Create a dictionary of UNIPROT key with ENSEMBL transcript value."""
    message = message_time('Starting from_UNIPROT_to_transcript.\n')
    print(message)
    log_file.write(message)

    dict = {}
    dict_genename = {}
    # Load the gene to transcript database
    pd_genes_trans = pd.read_csv(path_Output + 'Gene_to_Transcript.csv')
    list_gene_id = pd_genes_trans['gene_id'].values

    # Load the UNIPROT to ENSEMBL gene id
    pd_uniprot_gene = pd.read_csv(path_UNIPROT_to_ENSEMBL)
    list_uniprot_id = pd_uniprot_gene['From'].values

    # Feed dictionaries
    for uniprot in list_uniprot_id:
        filter1 = pd_uniprot_gene['From'] == uniprot
        ensembl_gene = pd_uniprot_gene['To'][filter1].values[0]

        if ensembl_gene in list_gene_id:
            filter2 = pd_genes_trans['gene_id'] == ensembl_gene
            transcript_list = pd_genes_trans['transcript_id'][filter2].values
            name = pd_genes_trans['gene_name'][filter2].values[0]
            dict_genename[uniprot] = name

            # Get the first transcript file
            # They all have the indexes
            transcript = transcript_list[0].split(';')[0]
            f = Manipulator(input_folder + transcript + '.csv')
            value = f._getValue([index])[0]

            if value != 'NA':
                value = float(value)
                if value >= max_cap:
                    value = max_cap
                elif value <= min_cap:
                    value = min_cap
                value = value - mymean
            dict[uniprot] = value

        else:
            print(uniprot + ', ' + ensembl_gene + ' is missing.')

    message = message_time('Dictionary contains ')
    message += str(len(dict.keys())) + ' enties.\n'
    print(message)
    log_file.write(message)

    message = message_time('End from_UNIPROT_to_transcript.\n\n')
    print(message)
    log_file.write(message)

    return (dict, dict_genename)


if __name__ == "__main__":
    # execute only if run as a script
    distribution_complexes()
