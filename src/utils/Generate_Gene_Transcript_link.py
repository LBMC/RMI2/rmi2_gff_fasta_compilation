#!/usr/bin/python
# coding: utf-8
"""Analyse all transcripts file to get the corresponding gene."""
# Python libraries
import os
import glob
import numpy as np
# Home made scripts and packages
from config import path_Output
from config import path_gff3, path_fa
from utils.time_stamp import message_time
from utils.Object_manipulator import Manipulator


def Transcript_Gene_Converter(log_file):
    """Generate a csv file for transcript file to genename convertion."""
    message = message_time('Starting Transcript_Gene_Converter.\n')
    print(message)
    log_file.write(message)

    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Create the converter file
    conv_name = 'Gene_to_Transcript.csv'
    open(path_Output + conv_name, 'w+').close
    convf = open(path_Output + conv_name, 'w+')
    message = message_time('Gene_to_Transcript.csv File created.\n')
    print(message)
    log_file.write(message)

    # Initiate key variables
    genes = []
    dict_Transcripts = {}
    dict_ID = {}
    done = range(10000, 100000, 10000)
    # For every file in the folder the gene name is obtained.
    list_transcript = glob.glob(input_folder + '*.csv')
    tested_files = 0
    for csv_file in list_transcript:
        tested_files += 1
        # Load the content of the file
        f = Manipulator(csv_file)
        Keys = ['gene_name', 'gene_id', 'transcript_id']
        values = f._getValue(Keys)
        gene = values[0]
        gene_id = values[1].split('.')[0]
        transcript = values[2].split('.')[0]

        # detection of duplicates
        if gene not in genes:
            genes = np.append(genes, gene)
            dict_Transcripts[gene] = [transcript]
            dict_ID[gene] = gene_id
        else:
            dict_Transcripts[gene] = np.append(dict_Transcripts[gene],
                                               transcript)
        if tested_files in done:
            message = message_time(str(tested_files) + ' Files checked.\n')
            print(message)
            log_file.write(message)

    message = message_time(str(tested_files) + ' Files checked.\n')
    print(message)
    log_file.write(message)

    header = 'gene_name,gene_id,transcript_id\n'
    convf.write(header)

    for g in genes:
        line = g + ',' + dict_ID[g] + ','
        index = 0
        for t in dict_Transcripts[g]:
            if index == 0:
                line += t
            else:
                line += ';' + t
            index += 1
        line += '\n'
        convf.write(line)
    convf.close

    message = message_time('End Transcript_Gene_Converter.\n\n')
    print(message)
    log_file.write(message)


if __name__ == "__main__":
    # execute only if run as a script
    Transcript_Gene_Converter()
