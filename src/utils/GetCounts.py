#!/usr/bin/python
# coding: utf-8
"""Extract the count value for all replicates and all condition."""
import os
from utils.time_stamp import string_time
from config import path_counts
from config import path_Output
from config import path_gff3, path_fa
from utils.Object_shredder_complex import Shredder_complex
from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator


prefix = 'Norm_Reads_'


def Get_Normalized_Counts():
    """Get the counts from the csv for all conditions."""
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]
    input_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep
    print('   #   ' + string_time() + '   #   Starting Counts extraction')
    db = Shredder_complex(path_counts,
                          ',',
                          1,
                          [4, 5, 6, 7],
                          3)
    for c in db._conditions:
        print(prefix + c)

    # Get gene to transcripts conversion
    print('   #   ' + string_time() + '   #   Geting Gene to Transcripts')
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    db2 = Shredder_simple(conv_path, ',')
    # Get the gene_id and associated transcripts
    List_ID = []
    dict_transcript = {}

    for g1 in db2._First_Col:
        id = db2._dict_values[g1, 'gene_id']
        List_ID.append(id)
        dict_transcript[id] = db2._dict_values[g1, 'transcript_id']

    min = 1000000
    max = 0

    for c in db._conditions:
        print('   #   ' + string_time() + '   #   ' + c)
        n_genes = 0
        n_files = 0
        n_missing = 0

        for g in db._dict_key_in_cond[c]:
            gene = g.split('.')[0]
            value = db._dict_values[g, c]
            if value < min:
                min = value
            if value > max:
                max = value

            value = str(value)

            # Transfer into the correct file(s)
            if gene in List_ID:
                list_files_t = dict_transcript[gene]
                transcripts = list_files_t.split(';')

                for local_transcript in transcripts:

                    # Transform the transcript file into modifiable object
                    f = Manipulator(input_folder + local_transcript + '.csv')

                    # Update the object and the file
                    f._appendValue(prefix + c, value)
                    f._updateFile()
                    n_files += 1
                n_genes += 1
            else:
                # print(gene + ' is missing!')
                n_missing += 1
        message = '   #   ' + string_time() + '   #      '
        message += str(n_genes) + ' genes processed'
        print(message)

        message = '   #   ' + string_time() + '   #      '
        message += str(n_missing) + ' genes missing'
        print(message)

        message = '   #   ' + string_time() + '   #      '
        message += str(n_files) + ' files updated'
        print(message)

    print('min ' + str(min))
    print('max ' + str(max))


if __name__ == "__main__":
    # execute only if run as a script
    Get_Normalized_Counts()
