#!/usr/bin/python
# coding: utf-8
"""Perform TDD distribution and GO analysis."""
# Python libraries
from __future__ import division
from config import path_Values
from config import path_Output, filtreted_of
from config import n_Bootsrap, min_gene, max_draw_go, max_cap, min_cap
from utils.study_specific_GOs import get_specific_go
from utils.Object_shredder_simple import Shredder_simple
from utils.time_stamp import message_time
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from config import key_filter
import numpy as np
import random
import pandas as pd


def Analyse_GO_by_keywords(condition,
                           log_file):
    """Regroup all GO containing at least on element of go_key_words."""
    message = message_time('Starting Analyse_GO_by_keywords.\n')
    print(message)
    log_file.write(message)

    GO_Name = path_Output + 'GO_to_Phrase.tsv'
    # Get GO Names
    GO_db = Shredder_simple(GO_Name, '\t')

    # Get a list of valid GOs
    list_spe_go = get_specific_go(GO_db)

    message = message_time('Analysing ' + condition + '.\n')
    print(message)
    log_file.write(message)

    # Open the database values as a shredder dictionnary
    db = pd.read_csv(path_Values)

    # Get the gene filtred
    list_key = ['Lympho_Resting',
                'Lympho_Activated',
                'Macro_Resting',
                'Macro_Activated']

    for key in list_key:
        if key in condition:
            path_genes_filtred = filtreted_of[key]
    df_OK = pd.read_csv(path_genes_filtred)
    genes_OK = df_OK['gene_id'].values

    # Analyse all TDD values in Condition
    Explore_TDD_go_selectd(condition,
                           db,
                           genes_OK,
                           list_spe_go,
                           log_file)

    message = message_time('End Analyse_GO_by_keywords.\n\n')
    print(message)
    log_file.write(message)


def Explore_TDD_go_selectd(condition,
                           database,
                           genes_OK,
                           list_spe_go,
                           log_file):
    """Get all relevant values for the global population and wanted GOs."""
    clean_cond = clean_name(condition)
    # Create the PdfPages
    pdf = PdfPages(path_Output + clean_cond + '_selected_GO.pdf')

    # Initialize all required variables
    raw_TDD = []
    TDD_GO = []

    # Reduce the dataframe to only the wanted columns
    database = database[['gene_id',
                         condition,
                         'GO_Term',
                         'GO_Name']]

    # Remove the extension in the gene_id
    ids = database['gene_id'].values
    clean_ids = []
    for i in ids:
        clean_ids.append(i.split('.')[0])

    database['gene_id'] = clean_ids

    # Select only rows corresponding to the wanted genes
    filter = database['gene_id'].isin(genes_OK)
    database = database[filter]

    # Remove all lines with NA
    database = database.dropna()

    genes = database['gene_id'].values

    # Loop in all genes
    for gene in genes:
        lc_db = database.loc[database['gene_id'] == gene]

        tdd = lc_db[condition].values[0]

        # Update the TDD index values list
        tdd = float(tdd)
        if tdd >= max_cap:
            tdd = max_cap
        elif tdd <= min_cap:
            tdd = min_cap
        raw_TDD.append(tdd)

        # Retrieve all GO and their name
        current_GO = lc_db['GO_Term'].values[0]
        current_GO = current_GO.split(';')
        for go in current_GO:
            if go in list_spe_go:
                TDD_GO.append(tdd)
                break
    # Now that we have all TDD values we generate the probability density
    (density_TDD,
     bin_edges) = np.histogram(raw_TDD,
                               bins=50,
                               range=(min_cap, max_cap),
                               density=True)
    # Set the max to 1 for comparison purpose
    density_TDD = density_TDD / (max(density_TDD))
    # Convert bin_edges in mean values to have the same number of values in
    # density_TDD and bins_mean
    bins_meanTDD = format_bin_edges(bin_edges)

    # Now that we have all TDD values we generate the probability density
    (density_GO,
     bin_edges) = np.histogram(TDD_GO,
                               bins=50,
                               range=(min_cap, max_cap),
                               density=True)
    # Set the max to 1 for comparison purpose
    density_GO = density_GO / (max(density_GO))
    # Convert bin_edges in mean values to have the same number of values in
    # density_TDD and bins_mean
    bins_meanGO = format_bin_edges(bin_edges)

    # Create the graph
    plt.figure(figsize=(18, 6))

    # create the title and get the mean and median
    local_title = 'Selected GOs: '
    local_title += str(len(TDD_GO)) + ' transcripts.'
    plt.suptitle(local_title, fontsize=12)

    # First plot: DISTRIBUTION
    plt.subplot(111)
    drawDistribution(bins_meanTDD,
                     density_TDD,
                     X2=bins_meanGO,
                     Y2=density_GO,
                     label=key_filter)

    # Save the page
    plt.savefig(pdf,
                format='pdf',
                dpi=300)

    # Close the graph to spare memory
    plt.close()

    # Close the pdf file
    pdf.close()


def Analyse_GO(Condition,
               log_file):
    """Perform the GO Analysis."""
    message = message_time('Starting Analyse_GO.\n')
    print(message)
    log_file.write(message)

    message = message_time('Analysing ' + Condition + '.\n')
    print(message)
    log_file.write(message)

    # Open the database values as a shredder dictionnary
    db = pd.read_csv(path_Values)

    # Get the gene filtred
    list_key = ['Lympho_Resting',
                'Lympho_Activated',
                'Macro_Resting',
                'Macro_Activated']

    for key in list_key:
        if key in Condition:
            path_genes_filtred = filtreted_of[key]
    df_OK = pd.read_csv(path_genes_filtred)
    genes_OK = df_OK['gene_id'].values

    # Analyse all TDD values in Condition
    Explore_TDD(Condition, db, genes_OK, log_file)

    message = message_time('End Analyse_GO.\n\n')
    print(message)
    log_file.write(message)


def clean_name(nom):
    """Remove unwanted characters."""
    nom = nom.replace('>', '_')
    nom = nom.replace('(', '')
    nom = nom.replace(')', '')
    return nom


def Explore_TDD(condition,
                database,
                genes_OK,
                log_file):
    """Analyse the TDD as a fonction of the condition."""
    message = message_time('Starting Explore_TDD.\n')
    print(message)
    log_file.write(message)

    # Create the .csv file for the GO_score
    clean_cond = clean_name(condition)
    csv_name = clean_cond + '_GO_score_filtered.csv'
    open(path_Output + csv_name, 'w+').close
    csv = open(path_Output + csv_name, 'a+')
    header = 'GO,Mean,Median,p_value_Mean,p_value_Median,n_genes\n'
    csv.write(header)

    # Create the PdfPages
    pdf = PdfPages(path_Output + clean_cond + '_TDD_GO.pdf')

    # Initialize all required variables
    raw_TDD = []
    list_GO = []
    TDD_GO = {}
    hit_GO = {}
    name_GO = {}

    # Reduce the dataframe to only the wanted columns
    database = database[['gene_id',
                         condition,
                         'GO_Term',
                         'GO_Name']]

    # Remove the extension in the gene_id
    ids = database['gene_id'].values
    clean_ids = []
    for i in ids:
        clean_ids.append(i.split('.')[0])

    database['gene_id'] = clean_ids

    # Select only rows corresponding to the wanted genes
    filter = database['gene_id'].isin(genes_OK)
    database = database[filter]

    # Remove all lines with NA
    database = database.dropna()

    genes = database['gene_id'].values

    # Loop in all genes
    for gene in genes:
        lc_db = database.loc[database['gene_id'] == gene]

        tdd = lc_db[condition].values[0]

        # Update the TDD index values list
        tdd = float(tdd)
        if tdd >= max_cap:
            tdd = max_cap
        elif tdd <= min_cap:
            tdd = min_cap
        raw_TDD.append(tdd)

        # Retrieve all GO and their name
        current_GO = lc_db['GO_Term'].values[0]
        current_GO = current_GO.split(';')

        current_name = lc_db['GO_Name'].values[0]
        current_name = current_name.split(';')

        # Create or update entry for all the gene GO Names
        for i in range(0, len(current_GO), 1):
            go = current_GO[i]

            if go in list_GO:
                hit_GO[go] = hit_GO[go] + 1
                TDD_GO[go].append(tdd)
            elif go != '':
                list_GO.append(go)
                hit_GO[go] = 1
                TDD_GO[go] = [tdd]
                name_GO[go] = current_name[i]

    # Order all GO
    best_GO, best_hits = find_best_GO(list_GO, hit_GO)

    # Boostraping

    message = message_time('Generating distribution')
    message + 'of the global population.\n'
    print(message)
    log_file.write(message)

    # Now that we have all TDD values we generate the probability density
    (density_TDD,
     bin_edges) = np.histogram(raw_TDD,
                               bins=50,
                               range=(min_cap, max_cap),
                               density=True)
    # Set the max to 1 for comparison purpose
    density_TDD = density_TDD / (max(density_TDD))
    # Convert bin_edges in mean values to have the same number of values in
    # density_TDD and bins_mean
    bins_meanTDD = format_bin_edges(bin_edges)

    # Create the .csv file for the Distribution
    csv_name2 = clean_cond + '_Tot_Distribution.csv'
    open(path_Output + csv_name2, 'w+').close
    csv2 = open(path_Output + csv_name2, 'a+')
    csv2.write(histogram_to_file(bins_meanTDD, density_TDD))
    csv2.close()

    mean_TDD = np.mean(raw_TDD)
    median_TDD = np.median(raw_TDD)

    # The GO Analysis
    done = range(100, 20000, 100)
    go_done = 0

    seed = 666
    random.seed(seed)

    # for bestgo in range(0, len(best_GO), 1):
    for bestgo in range(len(best_GO) - 1, -1, -1):
        # Retrieve key information for display
        local_term = best_GO[bestgo]
        local_name = name_GO[local_term]
        score = best_hits[bestgo]

        if score <= min_gene or score > max_draw_go:
            go_done += 1
            if go_done in done:
                message = message_time(str(go_done) + ' GO analyzed.\n')
                print(message)
                log_file.write(message)
        else:
            # Create the graph
            plt.figure(figsize=(18, 6))

            # create the title and get the mean and median
            local_title = local_term + ' : ' + str(score) + ' genes\n'
            local_title += local_name
            plt.suptitle(local_title, fontsize=12)

            local_mean = np.mean(TDD_GO[local_term])
            local_median = np.median(TDD_GO[local_term])

            # First plot: DISTRIBUTION
            plt.subplot(131)
            (density_go,
             bin_edges) = np.histogram(TDD_GO[local_term],
                                       bins=50,
                                       range=(min_cap, max_cap),
                                       density=True)
            # Set the max to 1 for comparison purpose
            density_go = density_go / (max(density_go))
            bins_meanTDDgo = format_bin_edges(bin_edges)
            mylabel = 'Mean pop = ' + str(mean_TDD)
            mylabel += ', Median pop = ' + str(median_TDD)
            drawDistribution(bins_meanTDD,
                             density_TDD,
                             X2=bins_meanTDDgo,
                             Y2=density_go,
                             label=mylabel)

            # Perform the Boostraping
            boot_mean = []
            boot_median = []

            for boot in range(0, n_Bootsrap, 1):
                picked = []
                for choix in range(0, score, 1):
                    picked.append(random.choice(raw_TDD))
                boot_mean.append(np.mean(picked))
                boot_median.append(np.median(picked))

            # Now calculate Distributions
            (density_rd_mean,
             bin_edges_mean) = np.histogram(boot_mean,
                                            bins=n_Bootsrap,
                                            range=(-1, max_cap),
                                            density=True)
            # Set the max to 1 for comparison purpose
            density_rd_mean = np.cumsum(density_rd_mean)
            density_rd_mean = density_rd_mean / (max(density_rd_mean))

            # Now we determine if we need to reverse or not
            if local_mean > mean_TDD:
                density_rd_mean = 1 - density_rd_mean

            bin_edges_mean = format_bin_edges(bin_edges_mean)

            (density_rd_median,
             bin_edges_median) = np.histogram(boot_median,
                                              bins=n_Bootsrap,
                                              range=(-1, max_cap),
                                              density=True)
            # Set the max to 1 for comparison purpose
            density_rd_median = np.cumsum(density_rd_median)
            density_rd_median = density_rd_median / (max(density_rd_median))

            # Now we determine if we need to reverse or not
            if local_median > median_TDD:
                density_rd_median = 1 - density_rd_median

            bin_edges_median = format_bin_edges(bin_edges_median)

            # Get the p_value
            # First calculate the distance for all bins
            dist_to_mean = bin_edges_mean - local_mean
            dist_to_median = bin_edges_median - local_median

            # Get the minimal distance's index
            dist_to_mean = np.absolute(dist_to_mean)
            dist_to_mean = dist_to_mean.tolist()
            index_pvalue_mean = dist_to_mean.index(min(dist_to_mean))

            dist_to_median = np.absolute(dist_to_median)
            dist_to_median = dist_to_median.tolist()
            index_pvalue_median = dist_to_median.index(min(dist_to_median))

            # Retrieve the p_value
            pvalue_mean = round(density_rd_mean[index_pvalue_mean], 4)
            pvalue_median = round(density_rd_median[index_pvalue_median], 4)

            # Second plot: MEAN
            plt.subplot(132)
            mylabel = 'Mean = ' + str(local_mean)
            mylabel += ', p-value = ' + str(pvalue_mean)
            drawDistribution(bin_edges_mean,
                             density_rd_mean,
                             X2=[local_mean, local_mean],
                             Y2=[0, 1],
                             label=mylabel)

            # Second plot: MEDIAN
            plt.subplot(133)
            mylabel = 'Median = ' + str(local_median)
            mylabel += ', p-value = ' + str(pvalue_median)
            drawDistribution(bin_edges_median,
                             density_rd_median,
                             X2=[local_median, local_median],
                             Y2=[0, 1],
                             label=mylabel)

            values = ''
            values += str(local_term) + ','
            values += str(local_mean - mean_TDD) + ','
            values += str(local_median - median_TDD) + ','
            values += str(pvalue_mean) + ','
            values += str(pvalue_median) + ','
            values += str(score) + '\n'
            csv.write(values)

            # Save the page
            plt.savefig(pdf,
                        format='pdf',
                        dpi=300)

            # Close the graph to spare memory
            plt.close()

            go_done += 1
            if go_done in done:
                message = message_time(str(go_done) + ' GO analyzed.\n')
                print(message)
                log_file.write(message)

    message = message_time(str(go_done) + ' GO analyzed.\n')
    print(message)
    log_file.write(message)
    # Close the pdf file
    pdf.close()

    # Close and save the txt file
    csv.close()

    message = message_time('End Explore_TDD.\n\n')
    print(message)
    log_file.write(message)


def histogram_to_file(Xarray, Yarray):
    """Generate the output for the distribution file."""
    lineX = ''
    lineY = ''
    for i in range(0, len(Xarray), 1):
        if i == (len(Xarray) - 1):
            lineX += str(Xarray[i]) + '\n'
            lineY += str(Yarray[i]) + '\n'
        else:
            lineX += str(Xarray[i]) + ','
            lineY += str(Yarray[i]) + ','

    return (lineX + lineY)


def drawDistribution(X1,
                     Y1,
                     X2=[],
                     Y2=[],
                     label=''):
    """Draw the distribution of TDD and GO."""
    plt.plot(X1,
             Y1,
             color=(0, 0, 0),
             linewidth=1)
    if len(X2) != 0 and len(Y2) != 0:
        plt.plot(X2,
                 Y2,
                 color=(1, 0.5, 0),
                 linewidth=2)
        plt.xlabel(label)


def find_best_GO(list_GO,
                 hit_GO):
    """Find the GO with the most entries."""
    # Find the 20 best
    ranked_hits = []
    ranked_names = []

    for iteration in range(0, len(hit_GO), 1):
        top = 0
        candidat = ''
        for go in list_GO:
            if hit_GO[go] > top:
                top = hit_GO[go]
                candidat = go
        ranked_hits.append(top)
        ranked_names.append(candidat)

        # remove name already in the list
        list_GO.remove(candidat)

    return ranked_names, ranked_hits


def format_bin_edges(bin_edges):
    """Calculate the mean value for the bin edges."""
    values = []
    for i in range(0, len(bin_edges)-1, 1):
        m = round((bin_edges[i] + bin_edges[i+1])/2,
                  2)
        values = np.append(values, m)
    return values


if __name__ == "__main__":
    # execute only if run as a script
    Analyse_GO()
