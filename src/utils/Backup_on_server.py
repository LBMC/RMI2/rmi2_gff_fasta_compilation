#!/usr/bin/python
"""Backup the result of the Analysis on Biodata."""
import os
from config import path_bckp
from config import path_Output
from utils.time_stamp import message_time


def backup(log_file):
    """Perform the backup."""
    message = message_time('Starting Backup\n')
    print(message)
    log_file.write(message)

    # Main command
    RS = 'rsync -va '
    bashCommand = RS + path_Output + ' ' + path_bckp
    os.system(bashCommand)

    message = message_time('Backup Over\n\n')
    log_file.write(message)
    print(message)
