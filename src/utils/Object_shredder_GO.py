#!/usr/bin/python
# coding: utf-8
"""Object dedicated to read m6ASeq results and update files."""
# Python libraries
from __future__ import division
from utils.time_stamp import string_time
import numpy as np


class Shredder_GO:
    """This object is generated from a specific database.

    It resume all lines and their properties.
    The PASeq values are transmitted to a PASeq object to facilitate the
    data transfert to the transcript .csv files
    """

    def __init__(self,
                 path,
                 separator
                 ):
        """Construct method."""
        self._separator = separator
        self._path = path
        self._dict_values = {}
        self._List_GO = []

        # The dictionary will created like this:
        # self._dict_values['Transcript_ID', '2nd_Key']
        # 2nd_Keys are the header column names

        # Reading the file
        print('   #   ' + string_time() + '   #   Shredding GO tsv database')
        self._read_file()

    def _read_file(self):
        """Read the file and feed the dictionary."""
        # initialize all variables
        nlines = 0
        with open(self._path) as database:
            # Read the firstline
            line = database.readline()

            # Read all lines
            while line:
                nlines += 1
                line = self._clean_line(line)

                # Decide if line belongs to header or describe a value
                if nlines == 1:
                    print('   #   ' + string_time() + '   #   Reading header')

                else:
                    # Remove breakline and split the line between key and value
                    frags = line.split(self._separator)

                    # Get the GO term, name and namespace
                    GO = frags[0]
                    name = frags[1]
                    namespace = frags[2]

                    self._List_GO = np.append(self._List_GO, GO)
                    self._dict_values[GO] = [name, namespace]

                # read new line
                line = database.readline()

        print('   #   ' + string_time() + '   #   Table has: ' + str(nlines))

    def _clean_line(self, line):
        """Remove unwanted characters from line."""
        unwanted = ['"', '\n']
        for char in unwanted:
            line = line.replace(char, '')
        return line

    def _new_Gene(self):
        """Reset all key variables for a new gene."""
        n_Exp_Ends = 0
        Name_Exp_5Ends = []
        m6A_Starts = []
        m6A_Ends = []
        m6A_scores = []
        total_score = 0

        return (n_Exp_Ends,
                Name_Exp_5Ends,
                m6A_Starts,
                m6A_Ends,
                m6A_scores,
                total_score)

    def _update_Gene(self,
                     myDict,
                     n_Exp_Ends,
                     Name_Exp_5Ends,
                     m6A_Starts,
                     m6A_Ends,
                     m6A_scores,
                     total_score
                     ):
        """Update all key variables for a new gene."""
        n_Exp_Ends += 1
        Name_Exp_5Ends = np.append(Name_Exp_5Ends,
                                   'm6ASeq_Peak_' + str(n_Exp_Ends))
        m6A_Starts = np.append(m6A_Starts,
                               myDict['peakStart'])
        m6A_Ends = np.append(m6A_Ends,
                             myDict['peakEnd'])
        m6A_scores = np.append(m6A_scores,
                               myDict['score'])
        total_score += round(float(myDict['score']),
                             3)

        return (n_Exp_Ends,
                Name_Exp_5Ends,
                m6A_Starts,
                m6A_Ends,
                m6A_scores,
                total_score)
