#!/usr/bin/python
# coding: utf-8
"""Convert a CDS into a protein sequence."""
# Python libraries

# Dictionary of codons:
AA = {}
AA['AAA'] = 'K'
AA['AAC'] = 'N'
AA['AAG'] = 'K'
AA['AAT'] = 'N'
AA['ACA'] = 'T'
AA['ACC'] = 'T'
AA['ACG'] = 'T'
AA['ACT'] = 'T'
AA['AGA'] = 'R'
AA['AGC'] = 'S'
AA['AGG'] = 'R'
AA['AGT'] = 'S'
AA['ATA'] = 'I'
AA['ATC'] = 'I'
AA['ATG'] = 'M'
AA['ATT'] = 'I'
AA['CAA'] = 'Q'
AA['CAC'] = 'H'
AA['CAG'] = 'Q'
AA['CAT'] = 'H'
AA['CCA'] = 'P'
AA['CCC'] = 'P'
AA['CCG'] = 'P'
AA['CCT'] = 'P'
AA['CGA'] = 'R'
AA['CGC'] = 'R'
AA['CGG'] = 'R'
AA['CGT'] = 'R'
AA['CTA'] = 'L'
AA['CTC'] = 'L'
AA['CTG'] = 'L'
AA['CTT'] = 'L'
AA['GAA'] = 'E'
AA['GAC'] = 'D'
AA['GAG'] = 'E'
AA['GAT'] = 'D'
AA['GCA'] = 'A'
AA['GCC'] = 'A'
AA['GCG'] = 'A'
AA['GCT'] = 'A'
AA['GGA'] = 'G'
AA['GGC'] = 'G'
AA['GGG'] = 'G'
AA['GGT'] = 'G'
AA['GTA'] = 'V'
AA['GTC'] = 'V'
AA['GTG'] = 'V'
AA['GTT'] = 'V'
AA['TAA'] = '*'
AA['TAC'] = 'Y'
AA['TAG'] = '*'
AA['TAT'] = 'Y'
AA['TCA'] = 'S'
AA['TCC'] = 'S'
AA['TCG'] = 'S'
AA['TCT'] = 'S'
AA['TGA'] = '*'
AA['TGC'] = 'C'
AA['TGG'] = 'W'
AA['TGT'] = 'C'
AA['TTA'] = 'L'
AA['TTC'] = 'F'
AA['TTG'] = 'L'
AA['TTT'] = 'F'


def translate(sequence):
    """Convert nucleic acid sequence into amino acid sequence."""
    # Initialize peptide
    peptide = ''
    # Analyse the sequence
    for position in range(0, len(sequence), 3):
        codon = sequence[position:position + 3]
        if codon in AA:
            peptide += AA[codon]
        else:
            peptide += '-'
    return peptide
