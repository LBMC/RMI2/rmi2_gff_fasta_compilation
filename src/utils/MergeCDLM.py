#!/usr/bin/python
# coding: utf-8
"""Extract data from Emmanuel's database to merge with output .csv files."""
# Python libraries
import os
import glob
import numpy as np
import pandas as pd
# Homemade utilities
from config import path_Output, value_stamp
from config import path_gff3, path_fa, path_tdd
from config import separator, suffix
from config import parameters_to_drain

from utils.Object_shredder_simple import Shredder_simple
from utils.Object_manipulator import Manipulator
from utils.time_stamp import message_time, string_time


def Merger(log_file):
    """Pipeline for the merging."""
    message = message_time('Starting Merger.\n')
    print(message)
    log_file.write(message)
    parameters = parameters_to_drain

    # 1. Checking the specific .gff3 vs .fa output folder
    gff3_name = os.path.splitext(os.path.basename(path_gff3))[0]
    fasta_name = os.path.splitext(os.path.basename(path_fa))[0]

    output_folder = path_Output + gff3_name + '_VS_' + fasta_name + os.sep

    # Load the database
    db = Shredder_simple(path_tdd, separator)

    # Get gene_id to transcripts conversion
    list_gene_id, dict_ricci, db_gene_ricci = get_Ricci_id()

    # Drain Emmanuel's database within the transcript.csv file
    # print(suffix)
    drain_TDD(db,
              dict_ricci,
              output_folder,
              log_file,
              parameters,
              suffix)

    message = message_time('End Merger.\n\n')
    print(message)
    log_file.write(message)


def drain_TDD(db,
              dict_ricci,
              output_folder,
              log_file,
              parameters,
              suffix):
    """Transfer Emmanuel's Key parameters in the .csv files."""
    message = message_time('Starting Transfer.\n')
    print(message)
    log_file.write(message)
    n_files = 0
    done = range(1000, 100000, 1000)
    for t in db._First_Col:

        # We don't need the version
        t = t.split('.')[0]

        current_path = output_folder + t + '.csv'

        if os.path.exists(current_path) is True:

            # Open the .csv for manipulation
            file = Manipulator(current_path)
            # Collect Emmanuel TDD infos and transfert to .csv
            # Only for gold one (remove NaN files)
            for item in parameters:
                file._appendValue(item + suffix,
                                  db._dict_values[t,
                                                  item])
            # Update csv file
            file._updateFile()
            n_files += 1

            if n_files in done:
                message = message_time(str(n_files) + ' Files modified.\n')
                print(message)
                log_file.write(message)

    message = message_time(str(n_files) + ' Files modified.\n')
    print(message)
    log_file.write(message)

    message = message_time('End Transfer.\n')
    print(message)
    log_file.write(message)


def get_Ricci_id():
    """Extract gene_id to transcript connection."""
    conv_name = 'Gene_to_Transcript.csv'
    conv_path = path_Output + conv_name
    db_gene_ricci = Shredder_simple(conv_path, ',')
    dict_ricci = {}
    list_gene_id = []
    for name in db_gene_ricci._First_Col:

        gene_id = db_gene_ricci._dict_values[name, 'gene_id']
        transcripts = db_gene_ricci._dict_values[name,
                                                 'transcript_id'].split(';')

        list_gene_id.append(gene_id)
        dict_ricci[gene_id] = transcripts

    return list_gene_id, dict_ricci, db_gene_ricci


def move_useless(output_folder,
                 not_gold_folder):
    """Move csv files that do not have gold stamp in their header."""
    # Search for csv files
    print('Moving unwanted files')
    for csv_file in glob.glob(output_folder + '*.csv'):
        f = Manipulator(csv_file)

        # Move files that do not have been stamped with Gold
        if f._header.count('Gold for TDD') == 0:
            # kill link to the file
            del f
            # move file
            file_name = os.path.basename(csv_file)
            os.rename(csv_file, not_gold_folder + file_name)


if __name__ == "__main__":
    # execute only if run as a script
    Merger()
