#!/usr/bin/python
# coding: utf-8
"""Configuration file with options."""
import os

# Main options
overwrite = True
debug = False
appris_limit = 1
appris_limit_DG = 1000
separator = ','   # for multiple rel TDD indexes files, else ','
suffix = ''
value_stamp = 'Gold for TDD'
minimal_size = 3
threads = 30   # Number of threads allocated to RNAfold


# Path of the project folder
project_Folder = os.getcwd() + os.sep

# Path of the Data
path_Data = project_Folder + 'data' + os.sep

# Path output
path_Output = project_Folder + 'output' + os.sep

# Path of the GFF file
# TDD
path_gff3 = path_Data + 'gencode.vM23.annotation.gff3'

# Path of the sqlite database (gffutils)
# TDD
path_db = path_Data + 'gencode.vM23.annotation.db'

# Path of fasta genome file
# TDD
path_fa = path_Data + 'GRCm38.p6.genome.fa'

# Path to Emmanuel's database
# path_tdd = path_Data + 'database_20200213.csv'
path_tdd = path_Data + 'features_length_ensembl.csv'
path_raw_tdd = path_Data + 'dbNormCountsAll_exon.csv'
path_raw_tdd_linc = path_Data + 'dbNormCountsOnly_lincRNA_exon.csv'

# Path to normalized counts
path_counts = path_Data + 'normalized_counts_all.csv'

# Path to global_database
path_Global = path_Data + 'Global_Database.csv'

# Path to free energy calculation output
path_free_energy = path_Data + 'RNAfold_2019-12-5_15-54-13'
path_free_energy += '_gencode.vM23.annotation_VS_GRCm38.p6.genome'
path_free_energy += '_DeltaG_uniq.dg'

# Path to PASeq database
path_PASeq = path_Data + 'PASeq_mergedPeaks_annotated_Count.tsv'

# path to database students
path_Students = path_Data + 'dbNormCountsAll_exon.csv'

# Path to m6ASeq database
path_m6ASeq = path_Data + 'm6A_quantification.tsv'
path_m6ASeq_NF = path_Data + 'm6A_NF_rank_merged_allpeaks_anno_curated.csv'

# Path to GO terms
path_GO_annotated = path_Data + 'go-basic.obo'

# Path Gene to GO association
path_Gene_GO = path_Data + 'association.tsv'

# Paths to 4G data
path_4G_UTR5 = path_Data + '128_MM_results_4G_UTR5.csv'
path_4G_CDS = path_Data + '128_MM_results_4G_CDS.csv'
path_4G_UTR3 = path_Data + '128_MM_results_4G_UTR3.csv'
threshold_Cg_div_Cc = 3.05

# Path to Hwang 2020 values
path_Hwang_S3 = path_Data + 'aax0194_Hwang_Table_S3.csv'
path_Hwang_S4 = path_Data + 'aax0194_Hwang_Table_S4.csv'
path_Hwang_S5 = path_Data + 'aax0194_Hwang_Table_S5.csv'

# Path to RiboDens
path_Ribodens = path_Data + 'RiboDensity.csv'

# Path target microRNA
path_miTarget = path_Data + 'miRTarBase_8_mmu.csv'

# Current database
path_Values = path_Data + 'current_subset.csv'

# GO interconnections
path_Arborescence = path_Data + 'GO_Arborescence.csv'
path_GO_Tree = path_Data + 'GO_tree.csv'

filtreted_of = {}
LR = path_Data + 'filtered_genes_Lympho_Resting.csv'
LA = path_Data + 'filtered_genes_Lympho_Activated.csv'
MR = path_Data + 'filtered_genes_Macro_Resting.csv'
MA = path_Data + 'filtered_genes_Macro_Activated.csv'
filtreted_of['Lympho_Resting'] = LR
filtreted_of['Lympho_Activated'] = LA
filtreted_of['Macro_Resting'] = MR
filtreted_of['Macro_Activated'] = MA





data_to_extract = ['transcript_id',
                   'gene_id',
                   'gene_name',
                   'lincRNA',
                   'Lympho_Activated>GOLD',
                   'Lympho_Resting>GOLD',
                   'Macro_Activated>GOLD',
                   'Macro_Resting>GOLD',
                   'Length.utr5',
                   'UTR5_length',
                   'UTR5_G',
                   'UTR5_C',
                   'Length.cds',
                   'CDS_length',
                   'CDS_G',
                   'CDS_C',
                   'Length.utr3',
                   'UTR3_length',
                   'UTR3_G',
                   'UTR3_C',
                   'trans_length',
                   'perc_C3G_AT',
                   'perc_C3G_GC',
                   'perc_C3G_A',
                   'perc_C3G_T',
                   'perc_C3G_C',
                   'perc_C3G_G',
                   'QC_passed',
                   'appris_level',
                   'n_m6ASeq_Peaks',
                   'total_m6ASeq_score',
                   'm6A_peaks_in_UTR5',
                   'm6A_peaks_in_CDS',
                   'm6A_peaks_in_UTR3',
                   'm6A_peaks_on_ATG',
                   'm6A_peaks_on_STOP',
                   'm6A_score_in_UTR5',
                   'm6A_score_in_CDS',
                   'm6A_score_in_UTR3',
                   'm6A_score_on_ATG',
                   'm6A_score_on_STOP',
                   'm6A_peak_utr5_NF',
                   'm6A_peak_CDS_NF',
                   'm6A_peak_intron_NF',
                   'm6A_peak_utr3_NF',
                   'n_Exp_UTR3_ends',
                   'Exp_UTR3_Ends_validated',
                   'Exp_UTR3_Lympho_Activated_Mean',
                   'Score_PASeq_Lympho_Activated',
                   'Exp_UTR3_Macro_Activated_Mean',
                   'Score_PASeq_Macro_Activated',
                   'Exp_UTR3_Lympho_Resting_Mean',
                   'Score_PASeq_Lympho_Resting',
                   'Exp_UTR3_Macro_Resting_Mean',
                   'Score_PASeq_Macro_Resting',
                   'CDS_DG',
                   'UTR5_DG',
                   'UTR3_DG',
                   '47UTR5-30CDS_DG',
                   '31CDS-End_DG',
                   'UTR5_50',
                   'Kozack_score_frequency',
                   'Kozack_score_efficiency',
                   'Kozack_score_dinucleotide',
                   'n_introns_in_UTR5',
                   'n_CDS_fragments',
                   'n_introns_in_UTR3',
                   'n_exon',
                   'n_intron',
                   'RiboDens>Lympho_Resting',
                   'RiboDens>Lympho_Activated',
                   'RiboDens>Macro_Resting',
                   'RiboDens>Macro_Activated',
                   # polyAU
                   'Total_AUUUA',
                   'N_AUUUA_UTR5',
                   'N_AUUUA_CDS',
                   'N_AUUUA_UTR3',
                   'Total_UUAUUUAUU',
                   'N_UUAUUUAUU_UTR5',
                   'N_UUAUUUAUU_CDS',
                   'N_UUAUUUAUU_UTR3',
                   'Total_[AU][AU][AU]UAUUUAU[AU][AU][AU]',
                   'N_[AU][AU][AU]UAUUUAU[AU][AU][AU]_UTR5',
                   'N_[AU][AU][AU]UAUUUAU[AU][AU][AU]_CDS',
                   'N_[AU][AU][AU]UAUUUAU[AU][AU][AU]_UTR3',
                   # microRNA
                   'N_miRNA',
                   'miRNA_names',
                   # Degradation Fold
                   'DegFold>Lympho_Activated>Trip>Ref_Trip_0h>3h',
                   'DegFold>Lympho_Activated>Trip>Ref_untreated_0h>3h',
                   'DegFold>Lympho_Activated>Trip>Ref_Trip_0h>1h',
                   'DegFold>Lympho_Activated>Trip>Ref_untreated_0h>1h',
                   'DegFold>Lympho_Activated>DRB>Ref_DRB_0h>3h',
                   'DegFold>Lympho_Activated>DRB>Ref_DRB_0h>1h',
                   'DegFold>Lympho_Resting>Trip>Ref_Trip_0h>3h',
                   'DegFold>Lympho_Resting>Trip>Ref_untreated_0h>3h',
                   'DegFold>Lympho_Resting>Trip>Ref_Trip_0h>1h',
                   'DegFold>Lympho_Resting>Trip>Ref_untreated_0h>1h',
                   'DegFold>Macro_Activated>Trip>Ref_Trip_0h>3h',
                   'DegFold>Macro_Resting>Trip>Ref_Trip_0h>3h',
                   # Absolute TDD index
                   'Abs(TDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(TDD)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>3h',
                   'Abs(TDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>1h',
                   'Abs(TDD)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>1h',
                   'Abs(TDD)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(TDD)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>3h',
                   'Abs(TDD)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>1h',
                   'Abs(TDD)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>1h',
                   'Abs(TDD)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>3h',
                   'Abs(TDD)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>1h',
                   'Abs(TDD)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>3h',
                   'Abs(TDD)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>1h',
                   'Abs(TDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(TDD)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>3h',
                   'Abs(TDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>1h',
                   'Abs(TDD)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>1h',
                   'Abs(TDD)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(TDD)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>3h',
                   'Abs(TDD)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>1h',
                   'Abs(TDD)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>1h',
                   'Abs(TDD)>Macro_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(TDD)>Macro_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(TDD)>Macro_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(TDD)>Macro_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   # Absolute nonTDD
                   'Abs(NonTDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(NonTDD)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>3h',
                   'Abs(NonTDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>1h',
                   'Abs(NonTDD)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>1h',
                   'Abs(NonTDD)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(NonTDD)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>3h',
                   'Abs(NonTDD)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>1h',
                   'Abs(NonTDD)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>1h',
                   'Abs(NonTDD)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>3h',
                   'Abs(NonTDD)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>1h',
                   'Abs(NonTDD)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>3h',
                   'Abs(NonTDD)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>1h',
                   'Abs(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(NonTDD)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>3h',
                   'Abs(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>1h',
                   'Abs(NonTDD)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>1h',
                   'Abs(NonTDD)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(NonTDD)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>3h',
                   'Abs(NonTDD)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>1h',
                   'Abs(NonTDD)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>1h',
                   'Abs(NonTDD)>Macro_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(NonTDD)>Macro_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(NonTDD)>Macro_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(NonTDD)>Macro_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   # Absolute non Degradation
                   'Abs(NonDEG)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(NonDEG)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>3h',
                   'Abs(NonDEG)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>1h',
                   'Abs(NonDEG)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>1h',
                   'Abs(NonDEG)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(NonDEG)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>3h',
                   'Abs(NonDEG)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>1h',
                   'Abs(NonDEG)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>1h',
                   'Abs(NonDEG)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>3h',
                   'Abs(NonDEG)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>1h',
                   'Abs(NonDEG)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>3h',
                   'Abs(NonDEG)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>1h',
                   'Abs(NonDEG)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(NonDEG)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>3h',
                   'Abs(NonDEG)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>1h',
                   'Abs(NonDEG)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>1h',
                   'Abs(NonDEG)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(NonDEG)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>3h',
                   'Abs(NonDEG)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>1h',
                   'Abs(NonDEG)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>1h',
                   'Abs(NonDEG)>Macro_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(NonDEG)>Macro_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Abs(NonDEG)>Macro_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Abs(NonDEG)>Macro_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   # Relative TDD
                   'Rel(TDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Rel(TDD)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>3h',
                   'Rel(TDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>1h',
                   'Rel(TDD)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>1h',
                   'Rel(TDD)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Rel(TDD)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>3h',
                   'Rel(TDD)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>1h',
                   'Rel(TDD)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>1h',
                   'Rel(TDD)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>3h',
                   'Rel(TDD)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>1h',
                   'Rel(TDD)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>3h',
                   'Rel(TDD)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>1h',
                   'Rel(TDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Rel(TDD)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>3h',
                   'Rel(TDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>1h',
                   'Rel(TDD)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>1h',
                   'Rel(TDD)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   'Rel(TDD)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>3h',
                   'Rel(TDD)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>1h',
                   'Rel(TDD)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>1h',
                   'Rel(TDD)>Macro_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Rel(TDD)>Macro_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Rel(TDD)>Macro_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Rel(TDD)>Macro_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   # Relative NonTDD
                   'Rel(NonTDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Rel(NonTDD)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>3h',
                   'Rel(NonTDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>1h',
                   'Rel(NonTDD)>Lympho_Activated>Trip_CHX>Ref_untreated_0h>1h',
                   'Rel(NonTDD)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Rel(NonTDD)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>3h',
                   'Rel(NonTDD)>Lympho_Activated>Trip_Harr>Ref_Trip_0h>1h',
                   'Rel(NonTDD)>Lympho_Activated>Trip_Harr>Ref_untreated_0h>1h',
                   'Rel(NonTDD)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>3h',
                   'Rel(NonTDD)>Lympho_Activated>DRB_CHX>Ref_DRB_0h>1h',
                   'Rel(NonTDD)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>3h',
                   'Rel(NonTDD)>Lympho_Activated>DRB_Harr>Ref_DRB_0h>1h',
                   'Rel(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Rel(NonTDD)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>3h',
                   'Rel(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>1h',
                   'Rel(NonTDD)>Lympho_Resting>Trip_CHX>Ref_untreated_0h>1h',
                   'Rel(NonTDD)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   'Rel(NonTDD)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>3h',
                   'Rel(NonTDD)>Lympho_Resting>Trip_Harr>Ref_Trip_0h>1h',
                   'Rel(NonTDD)>Lympho_Resting>Trip_Harr>Ref_untreated_0h>1h',
                   'Rel(NonTDD)>Macro_Activated>Trip_CHX>Ref_Trip_0h>3h',
                   'Rel(NonTDD)>Macro_Activated>Trip_Harr>Ref_Trip_0h>3h',
                   'Rel(NonTDD)>Macro_Resting>Trip_CHX>Ref_Trip_0h>3h',
                   'Rel(NonTDD)>Macro_Resting>Trip_Harr>Ref_Trip_0h>3h',
                   'n4G_UTR5',
                   'n4G_CDS',
                   'n4G_UTR3',
                   'n4G_total',
                   'Hwang>Lympho_Resting>polyA_length_WT1',
                   'Hwang>Lympho_Resting>polyA_length_WT2',
                   'Hwang>Lympho_Resting>polyA_length_WT3',
                   'Hwang>Lympho_Activated>polyA_length_WT1',
                   'Hwang>Lympho_Activated>polyA_length_WT2',
                   'Hwang>transcript_half-life',
                   'Stretches_AAA_AAG_score',
                   'Stretches_AAA_AAG_pondarated_score',
                   'Stretches_GAC_GAT_score',
                   'Stretches_GAC_GAT_pondarated_score',
                   'Stretches_GAA_GAG_score',
                   'Stretches_GAA_GAG_pondarated_score',
                   'Stretches_AAA_AAG_GAC_GAT_GAA_GAG_score',
                   'Stretches_AAA_AAG_GAC_GAT_GAA_GAG_pondarated_score',
                   'percent_ALA_A',
                   'percent_ARG_R',
                   'percent_ASN_N',
                   'percent_ASP_D',
                   'percent_CYS_C',
                   'percent_GLU_E',
                   'percent_GLN_Q',
                   'percent_GLY_G',
                   'percent_HIS_H',
                   'percent_ILE_I',
                   'percent_LEU_L',
                   'percent_LYS_K',
                   'percent_MET_M',
                   'percent_PHE_F',
                   'percent_PRO_P',
                   'percent_SER_S',
                   'percent_THR_T',
                   'percent_TRP_W',
                   'percent_TYR_Y',
                   'percent_VAL_V',
                   'GO_Term',
                   'GO_Name',
                   'Wrong_Kozack_Environment',
                   'Wrong_Intron_Donor_Site',
                   'Wrong_Intron_Acceptor_Site',
                   'Trancript_duplication'
                   ]

# Keywords QC failed
list_QC_failed = ['gene_id',
                  'transcript_id',
                  'gene_name',
                  'QC_passed',
                  'In_frame_Stop_codon',
                  'N_in_sequence',
                  'CDS_too_Short',
                  'Wrong_Kozack_Environment',
                  'No_correct_Initiator_Codon',
                  'No_correct_Initiator_AA',
                  'No_Stop_Codon',
                  'No_correct_Terminator_AA',
                  'No_Reported_5UTR',
                  'No_Reported_3UTR',
                  'Wrong_Intron_Donor_Site',
                  'Wrong_Intron_Acceptor_Site',
                  'Part_of_Protein_sequence_unknown'
                  ]

# Expected columns for TDD_Drainer
expected_columns = ['ensemblID',
                    'normReadsCounts',
                    'cell',
                    'Activated/Resting',
                    'treatment',
                    'time',
                    'replicate']


# For unique TDD indexes file
parameters_to_drain = ['Length.utr5',
                       'Length.cds',
                       'Length.utr3']

# Conditions to Draw as GO
Conditions_GO = ['Abs(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h']
Conditions_GO_to_merge = ['Abs(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h']
Condition_Merge = 'Test'
number_stat_conditions = 1   # 'All' or a number.
max_cutoff = 400
min_cutoff = 40
go_key_words = ['innate immune response',
                'mitochondrial membrane',
                'regulation of alternative mRNA splicing; via spliceosome']
key_filter = 'Immunity GOs'

Gold_GO = path_Output
Gold_GO += 'asIndexTDD_t3TripTripCHX.0hTrip_Common_Mitochondria.csv'

# Path backup server
path_bckp = '/media/dcluet/Backup_IC/'
path_bckp += 'rmi2/David/TDD/'

# Max authorized values
max_cap = 1.5
min_cap = -0.5

# Figures format
Dimension = (8, 4)  # inches.
Bottom = 0.15  # proportion (0-1).
Left = 0.15  # proportion (0-1).
Square = 3  # inches.

W = 0.375  # Width (0-1) of the figure
H = 0.75  # Height (0-1) of the figure

# Number of random picking
n_Bootsrap = 50000
n_Bootsrap_prot = 100000

# Minimal number of protein to consider the complex for p-value estimation
n_min_prot = 4

# Number minimum of gene to Analyse
min_gene = 10
max_draw_go = 2000
plow = 0.05
do_filiation = False

# Figures format
Wgo = 10  # Width (inch) of the figure
Hgo = 10  # Height (inch) of the figure

# Conditions for GO Analysis
Cells = ['Lympho_Activated',
         'Lympho_Resting']

Time = ['3h']

Treatment = ['Trip_CHX']

Indexes = ['Abs(TDD)',
           'Abs(NonTDD)']


# complexes
path_core_complexes = path_Data + 'coreComplexes_mouse.csv'
path_UNIPROT_to_ENSEMBL = path_Data + 'mouse_UNIPROT_to_ENSEMBL.csv'
path_Interactions = path_Data + 'BIOGRID-MV-Physical-3.5.184.tab2_curated_mouse.csv'
path_UNIPROT_to_ENTREZ = path_Data + 'ENTREZ_to_UNIPROT.csv'

indexes_complexes = ['Abs(TDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h']
list_complexes = ['3047',
                  'RMI2.1',
                  'RMI2.2',
                  'RMI2.3',
                  'RMI2.4',
                  'RMI2.5',
                  'RMI2.6',
                  'RMI2.7',
                  'RMI2.8',
                  'RMI2.9',
                  'RMI2.10',
                  'RMI2.11',
                  'RMI2.12',
                  'RMI2.13',
                  'RMI2.14',
                  'RMI2.15',
                  'RMI2.16',
                  'RMI2.17',
                  'RMI2.18',
                  'RMI2.19',
                  'RMI2.20',
                  'RMI2.21',
                  'RMI2.22',
                  'RMI2.23',
                  'RMI2.24',
                  'RMI2.25'
                  ]
core_node = ['mRNA', 'mRNA']
