#!/usr/bin/python
# coding: utf-8
"""Complete pipeline for CDS scanning and parsing."""
from utils.Check_and_Start import Initiate
from utils.Extractor import Extract_Infos


def Pipeline():
    """Execute all steps of the pipeline."""
    print('==================================================================')
    print('Starting CDS Parser Pipeline')
    print('==================================================================')
    print('')
    print(message_time('Start'))

    # Sequential analysis
    log_file = Initiate()

    Extract_Infos(log_file)   # revision OK 2020/03/09

    # Get pvalues for protein Complexes
    log_file.close()

    print(message_time('End'))
    print('')
    print('==================================================================')
    print('End of CDS Parser Pipeline ')
    print('==================================================================')


if __name__ == "__main__":
    # execute only if run as a script
    Pipeline()
