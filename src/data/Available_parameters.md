# Available parameters

> **Nota Bene**
>
> **The transcripts.csv files contain more parameters than the following ones.**
> **The parameters presented herein are those we selected to reduce the size**
> **of the current_subset.csv**


|Column name|Description|
|:----------|:----------|
|transcript_id|Ensemble transcript ID (ENSMUSTXXXXXX)|
|gene_id|Ensemble gene ID (ENSMUSGXXXXXX)|
|gene_name|Explicit gene name like Ras|
|lincRNA|Is the the transcript a lincRNA|
|Lympho_Activated>GOLD|Validated transcript for the Activated Lymphocytes|
|Lympho_Resting>GOLD|Validated transcript for the Resting Lymphocytes|
|Length.utr5|Length of the 5' UTR computed before the database creation|
|UTR5_length|Length of the 5' UTR computed with the parent genome annotation used for the database creation|
|UTR5_G|Percentage of G in the 5' UTR|
|UTR5_C|Percentage of C in the 5' UTR|
|Length.cds|Length of the CDS computed before the database creation|
|CDS_length|Length of the 5' CDS computed with the parent genome annotation used for the database creation|
|CDS_G|Percentage of G in the CDS|
|CDS_C|Percentage of C in the CDS|
|Length.utr3|Length of the 3' UTR computed before the database creation|
|UTR3_length|Length of the 3' UTR computed with the parent genome annotation used for the database creation|
|UTR3_G|Percentage of G in the 3' UTR|
|UTR3_C|Percentage of C in the 3' UTR|
|trans_length|Length of the transcript computed with the parent genome annotation used for the database creation|
|perc_C3G_AT|Percentage of codons terminating with A or T|
|perc_C3G_GC|Percentage of codons terminating with G or C|
|perc_C3G_A|Percentage of codons terminating with A|
|perc_C3G_T|Percentage of codons terminating with T|
|perc_C3G_C|Percentage of codons terminating with C|
|perc_C3G_G|Percentage of codons terminating with G|
|QC_passed|Is the transcript validated for really coding and containing correctly annotated UTR,...|
|appris_level|Appris level of the transcript. Appris 1 selected in priority|
|n_m6ASeq_Peaks|Number of m6A peaks in the transcript from `m6A_quantification.tsv`|
|total_m6ASeq_score|m6A score for the transcript from `m6A_quantification.tsv`|
|m6A_peaks_in_UTR5|Number of m6A peaks in the 5' UTR from `m6A_quantification.tsv`|
|m6A_peaks_in_CDS|m6A score for the CDS from `m6A_quantification.tsv`|
|m6A_peaks_in_UTR3|Number of m6A peaks in the 3' UTR from `m6A_quantification.tsv`|
|m6A_peaks_on_ATG|Number of m6A peaks on the ATG codon from `m6A_quantification.tsv`|
|m6A_peaks_on_STOP|Number of m6A peaks on the STOP codon from `m6A_quantification.tsv`|
|m6A_score_in_UTR5|m6A score for the 5' UTR from `m6A_quantification.tsv`|
|m6A_score_in_CDS|m6A score for the CDS from `m6A_quantification.tsv`|
|m6A_score_in_UTR3|m6A score for the 3' UTR from `m6A_quantification.tsv`|
|m6A_score_on_ATG|m6A score for the ATG codon from `m6A_quantification.tsv`|
|m6A_score_on_STOP|m6A score for the STOP codon from `m6A_quantification.tsv`|
|m6A_peak_utr5_NF|Number of m6A peaks in the 5' UTR obtained from `m6A_NF_rank_merged_allpeaks_anno_curated.csv`|
|m6A_peak_CDS_NF|Number of m6A peaks in the CDS obtained from `m6A_NF_rank_merged_allpeaks_anno_curated.csv`|
|m6A_peak_intron_NF|Number of m6A peaks in introns obtained from `m6A_NF_rank_merged_allpeaks_anno_curated.csv`|
|m6A_peak_utr3_NF|Number of m6A peaks in the 3' UTR obtained from `m6A_NF_rank_merged_allpeaks_anno_curated.csv`|
|n_Exp_UTR3_ends|Number of experimental 3' UTR ends obtained from pAseq `PASeq_mergedPeaks_annotated_Count.tsv`|
|Exp_UTR3_Ends_validated|Number of validated (within or close tothe annotation of the transcript) experimental 3' UTR obtained from pAseq `PASeq_mergedPeaks_annotated_Count.tsv`|
|Exp_UTR3_Lympho_Activated_Mean|Mean length of the validated experimental 3' UTR obtained from pAseq in Activated Lymphocytes `PASeq_mergedPeaks_annotated_Count.tsv`|
|Score_PASeq_Lympho_Activated|Score of experimental 3' UTR obtained from pAseq in Activated Lymphocytes `PASeq_mergedPeaks_annotated_Count.tsv`|
|Exp_UTR3_Lympho_Resting_Mean|Mean length of the validated experimental 3' UTR obtained from pAseq in Resting Lymphocytes `PASeq_mergedPeaks_annotated_Count.tsv`|
|Score_PASeq_Lympho_Resting|Score of experimental 3' UTR obtained from pAseq in Resting Lymphocytes `PASeq_mergedPeaks_annotated_Count.tsv`|
|CDS_DG|CDS $\Delta G$ as `MFE` computed with `RNAfold`|
|UTR5_DG|5' UTR $\Delta G$ as `MFE` computed with `RNAfold`|
|UTR3_DG|3' UTR $\Delta G$ as `MFE` computed with `RNAfold`|
|47UTR5-30CDS_DG|$\Delta G$ of the 47 nucleotides upstream and the 30 nucleotides downstream of the ATG codon as `MFE` computed with `RNAfold`|
|31CDS-End_DG|$\Delta G$ of 31 last nucleotides of the CDS as `MFE` computed with `RNAfold`|
|UTR5_50|50 last nucleotides of the 5' UTR|
|Kozack_score_frequency|Score of the Kozack sequence based on the frequency of the nucleotide at each position||
|Kozack_score_efficiency|Score of the Kozack sequence based on the mono nucleotide efficiency|
|Kozack_score_dinucleotide|Score of the Kozack sequence based on the di-nucleotide efficiency|
|n_introns_in_UTR5|Number of introns present in the 5' UTR|
|n_CDS_fragments|Number of exons generating the CDS|
|n_introns_in_UTR3|Number of introns present in the 3' UTR|
|n_exon|Total number of exons in the transcript|
|n_intron|Total number of intron in the transcript|
|RiboDens>Lympho_Resting|Ribodensity of the transcript in the Resting Lymphocytes, from `RiboDensity.csv`|
|RiboDens>Lympho_Activated|Ribodensity of the transcript in the Activated Lymphocytes, from `RiboDensity.csv`|
|Total_AUUUA|Total number of AUUUA sites|
|N_AUUUA_UTR5|Number of AUUUA sites in the 5' UTR|
|N_AUUUA_CDS|Number of AUUUA sites in the CDS|
|N_AUUUA_UTR3|Number of AUUUA sites in the 3' UTR|
|Total_UUAUUUAUU|Total number of UUAUUUAUU sites|
|N_UUAUUUAUU_UTR5|Number of UUAUUUAUU sites in the 5' UTR|
|N_UUAUUUAUU_CDS|Number of UUAUUUAUU sites in the CDS|
|N_UUAUUUAUU_UTR3|Number of UUAUUUAUU sites in the 3' UTR|
|Total_[AU][AU][AU]UAUUUAU[AU][AU][AU]|Total number of [AU][AU][AU]UAUUUAU[AU][AU][AU] sites ([AU] meaning A or U)|
|N_[AU][AU][AU]UAUUUAU[AU][AU][AU]_UTR5|Number of [AU][AU][AU]UAUUUAU[AU][AU][AU] sites ([AU] meaning A or U) in the 5' UTR|
|N_[AU][AU][AU]UAUUUAU[AU][AU][AU]_CDS|Number of [AU][AU][AU]UAUUUAU[AU][AU][AU] sites ([AU] meaning A or U) in the CDS|
|N_[AU][AU][AU]UAUUUAU[AU][AU][AU]_UTR3|Number of [AU][AU][AU]UAUUUAU[AU][AU][AU] sites ([AU] meaning A or U) in the 3' UTR|
|N_miRNA|Number of targetting micro RNA from `miRTarBase_8_mmu.csv`|
|miRNA_names|Names of the targetting micro RNA from `miRTarBase_8_mmu.csv`|
|DegFold>Lympho_Activated>Trip>Ref_Trip_0h>3h|$Absolute Degradation Fold = {1 - {counts(t3h, Trip) \over counts(t0,Trip)}}$ in Activated Lymphocytes|
|DegFold>Lympho_Resting>Trip>Ref_Trip_0h>3h|$Absolute Degradation Fold = {1 - {counts(t3h, Trip) \over counts(t0,Trip)}}$ in Resting Lymphocytes|
|Abs(TDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h|$Absolute TDD_{index} = {{counts(t3h, TripCHX) - counts(t3h,Trip)} \over counts(t0, Trip)}$ in Activated Lymphocytes|
|Abs(TDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h|$Absolute TDD_{index} = {{counts(t3h, TripCHX) - counts(t3h,Trip)} \over counts(t0, Trip)}$ in Resting Lymphocytes
|Abs(NonTDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h|$Absolute TID_{index} = {{counts(t0, Trip) - counts(t3h, TripCHX)} \over counts(t0, Trip)}$in Activated Lymphocytes|
|Abs(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h|$Absolute TID_{index} = {{counts(t0, Trip) - counts(t3h, TripCHX)} \over counts(t0, Trip)}$ in Resting Lymphocytes|
|Rel(TDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h|$Relative TDD_{index} = {{counts(t3h, TripCHX) - counts(t3h,Trip)} \over {counts(t0, Trip) - counts(t3h,Trip)}}$ in Activated Lymphocytes|
|Rel(TDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h|$Relative TDD_{index} = {{counts(t3h, TripCHX) - counts(t3h,Trip)} \over {counts(t0, Trip) - counts(t3h,Trip)}}$ in Resting Lymphocytes
|Rel(NonTDD)>Lympho_Activated>Trip_CHX>Ref_Trip_0h>3h|$Relative TID_{index} = {{counts(t0, Trip) - counts(t3h, TripCHX)} \over {counts(t0, Trip) - counts(t3h, TripCHX)}}$ in Activated Lymphocytes|
|Rel(NonTDD)>Lympho_Resting>Trip_CHX>Ref_Trip_0h>3h|$Relative TID_{index} = {{counts(t0, Trip) - counts(t3h, TripCHX)} \over {counts(t0, Trip) - counts(t3h, TripCHX)}}$ in Resting Lymphocytes|
|n4G_UTR5|Number of G quadruplexes in the 5' UTR from `128_MM_results_4G_UTR5.csv`|
|n4G_CDS|Number of G quadruplexes in the CDS from `128_MM_results_4G_CDS.csv`|
|n4G_UTR3|Number of G quadruplexes in the 3' UTR from `128_MM_results_4G_UTR3.csv`|
|n4G_total|Total number of G quadruplexes|
|Hwang>Lympho_Resting>polyA_length_WT1|PolyA length in Resting Lymphocytes from `aax0194_Hwang_Table_S4.csv`|
|Hwang>Lympho_Resting>polyA_length_WT2|PolyA length in Resting Lymphocytes from `aax0194_Hwang_Table_S4.csv`|
|Hwang>Lympho_Resting>polyA_length_WT3|PolyA length in Resting Lymphocytes from `aax0194_Hwang_Table_S4.csv`|
|Hwang>Lympho_Activated>polyA_length_WT1|PolyA length in Activated Lymphocytes from `aax0194_Hwang_Table_S5.csv`|
|Hwang>Lympho_Activated>polyA_length_WT2|PolyA length in Activated Lymphocytes from `aax0194_Hwang_Table_S5.csv`|
|Hwang>transcript_half-life|Transcript half-life from `aax0194_Hwang_Table_S3.csv`|
|Stretches_AAA_AAG_score|Number of AAA and AAG stretches|
|Stretches_AAA_AAG_pondarated_score|Number of AAA and AAG stretches corrected by the sequence length|
|Stretches_GAC_GAT_score|Number of GAC and GAT strecches| 
|Stretches_GAC_GAT_pondarated_score|Number of GAC and GAT stretches corrected by the sequence length|
|Stretches_GAA_GAG_score|Number of GAA and GAG stretches|
|Stretches_GAA_GAG_pondarated_score|Number of GAA and GAG stretches corrected by the sequence length|
|Stretches_AAA_AAG_GAC_GAT_GAA_GAG_score|Number of AAA, AAG, GAC, GAT, GAA and GAG stretches|
|Stretches_AAA_AAG_GAC_GAT_GAA_GAG_pondarated_score|Number of AAA, AAG, GAC, GAT, GAA and GAG stretches corrected by the sequence length|
|percent_ALA_A|Percent of ALA in the translated protein|
|percent_ARG_R|Percent of ARG in the translated protein|
|percent_ASN_N|Percent of ASN in the translated protein|
|percent_ASP_D|Percent of ASP in the translated protein|
|percent_CYS_C|Percent of CYS in the translated protein|
|percent_GLU_E|Percent of GLU in the translated protein|
|percent_GLN_Q|Percent of GLN in the translated protein|
|percent_GLY_G|Percent of GLY in the translated protein|
|percent_HIS_H|Percent of HIS in the translated protein|
|percent_ILE_I|Percent of ILE in the translated protein|
|percent_LEU_L|Percent of LEU in the translated protein|
|percent_LYS_K|Percent of LYS in the translated protein|
|percent_MET_M|Percent of MET in the translated protein|
|percent_PHE_F|Percent of PHE in the translated protein|
|percent_PRO_P|Percent of PRO in the translated protein|
|percent_SER_S|Percent of SER in the translated protein|
|percent_THR_T|Percent of THR in the translated protein|
|percent_TRP_W|Percent of TRP in the translated protein|
|percent_TYR_Y|Percent of TYR in the translated protein|
|percent_VAL_V|Percent of VAL in the translated protein|
|GO_Term|GO Terms associated with the translated protein|
|GO_Name|GO Names associated with the translated protein|
|Wrong_Kozack_Environment|Warns if the Kozack environment is non canonical|
|Wrong_Intron_Donor_Site|Warns if a non canonical intron donor site is present in the annotation| 
|Wrong_Intron_Acceptor_Site|Warns if a non canonical intron acceptor site is present in the annotation|
|Trancript_duplication|Warns if another transcript of the same gene is present in the final database (few occurences)|
|UTR5_DG_MFEden|UTR5 $\Delta G$ as `MFEden`, see MFEden section|
|CDS_DG_MFEden|CDS $\Delta G$ as `MFEden`, see MFEden section|
|UTR3_DG_MFEden|UTR3 $\Delta G$ as `MFEden`, see MFEden section|
|47UTR5-30CDS_DG_MFEden|$\Delta G$ of the 47 nucleotides upstream and the 30 nucleotides downstream of the ATG codon as `MFEden`, see MFEden section|
|31CDS-End_DG_MFEden|$\Delta G$ of 31 last nucleotides of the CDS as `MFEden`, see MFEden section|
