#!/usr/bin/python
# coding: utf-8
"""Complete pipeline for CDS scanning and parsing."""
from utils.Check_and_Start import Initiate
from utils.CDS_scanner import CDS_scanner
from utils.MergeCDLM import Merger
from utils.Export_for_Ribosome_Profiling import Export_Ribo_Profil
from utils.Export_for_DeltaG import Export_DG
from utils.Import_Hwang_2020 import Add_Hwang
from utils.TDD_Drainer import Exploit_TDD
from utils.TDD_Goldfinger import Pointer
from utils.Perform_extra_Calculations import Extra_Calculations
from utils.Extractor import Extract_Infos
from utils.Generate_Gene_Transcript_link import Transcript_Gene_Converter
from utils.Attribute_GO import GO_obtainer
from utils.Append_PASeq_data import Add_PASeq
from utils.Append_m6ASeq_data import Add_m6ASeq
from utils.Append_m6ASeq_data_NF import Add_m6ASeq_NF
from utils.Transfert_GO import Add_GO
from utils.Backup_on_server import backup
from utils.Draw_Clusters import Bars_Clustering
from utils.Import_RiboDens import Add_RiboDens
from utils.time_stamp import message_time
from utils.Append_4G import Add_4G
from utils.Distributions_GO import Analyse_GO, clean_name
from utils.Extract_GO_Arborescence import Arborescence_obtainer
from utils.Display_Network_Distribution import Display_GO
from config import Conditions_GO, path_Output, path_Arborescence
from utils.Find_polyAU import Get_polyAU_Transcripts
from utils.Target_miRNA import Associate_miRNA
from utils.Merge_for_Morpheus import Merge_scores
from utils.Tree_Creator import Radagast
from utils.Correct_Tree_Stats import correct_stats
from utils.LinRNA_files_creator import create_files
from utils.TDD_Drainer_LincRNA import Exploit_TDD_lincRNA
from utils.study_one_complexe import distribution_complexes
from config import indexes_complexes
from config import path_core_complexes, list_complexes, core_node
from config import path_Interactions
from utils.stats_complexes import get_pvalues
from utils.Weighted_Biogird_Network import create_weighted_network
from utils.study_specific_GOs import Merge_specific_scores
from utils.Distributions_GO import Analyse_GO_by_keywords
from utils.C3G import Get_C3G


def Pipeline():
    """Execute all steps of the pipeline."""
    print('==================================================================')
    print('Starting CDS Parser Pipeline')
    print('==================================================================')
    print('')
    print(message_time('Start'))

    # Sequential analysis
    log_file = Initiate()
    CDS_scanner(log_file)   # revision OK 2020/03/10
    Transcript_Gene_Converter(log_file)   # revision OK 2020/03/09
    GO_obtainer(log_file)   # revision OK 2020/03/09
    Add_GO(log_file)   # revision OK 2020/03/09
    Get_C3G(log_file)
    # PASEQ Attribute a score for each condition
    Add_PASeq(log_file)   # revision OK 2020/03/11
    Add_4G()
    Add_m6ASeq(log_file)   # revision OK 2020/03/09
    Add_m6ASeq_NF(log_file)
    Get_polyAU_Transcripts(log_file)
    Associate_miRNA(log_file)
    Export_DG(log_file) 

    # Create the lincRNA files as geneID.csv
    create_files(log_file)
    # Merge raw database and attribute GOLD transcript for each TDD condition
    # Get all transcripts for all genes
    # Get Normalized in MANU database
    # Calculate TDD Indexes in MANU

    Exploit_TDD(log_file,
                ['Lympho', 'Macro'],
                ['Activated', 'Resting'],
                ['Trip'],
                ['CHX', 'Harr'],
                ['3h', '1h'],
                ['rep1', 'rep2', 'rep3'],
                'Trip',
                '0h')

    Exploit_TDD(log_file,
                ['Lympho', 'Macro'],
                ['Activated', 'Resting'],
                ['DRB'],
                ['CHX', 'Harr'],
                ['3h', '1h'],
                ['rep1', 'rep2', 'rep3'],
                'DRB',
                '0h')

    Exploit_TDD(log_file,
                ['Lympho'],
                ['Activated', 'Resting'],
                ['Trip'],
                ['CHX', 'Harr'],
                ['3h', '1h'],
                ['rep1', 'rep2', 'rep3'],
                'untreated',
                '0h')

    Exploit_TDD(log_file,
                ['Lympho'],
                ['Activated'],
                ['Trip'],
                ['CHX'],
                ['3h'],
                ['rep1', 'rep2', 'rep3'],
                'Trip',
                '0h')

    Exploit_TDD(log_file,
                ['Lympho'],
                ['Activated'],
                ['Trip'],
                ['CHX'],
                ['3h'],
                ['rep1', 'rep2', 'rep3'],
                'untreated',
                '0h')

    Exploit_TDD_lincRNA(log_file,
                        ['Lympho', 'Macro'],
                        ['Activated', 'Resting'],
                        ['Trip'],
                        ['CHX', 'Harr'],
                        ['3h', '1h'],
                        ['rep1', 'rep2', 'rep3'],
                        'Trip',
                        '0h')

    Exploit_TDD_lincRNA(log_file,
                        ['Lympho', 'Macro'],
                        ['Activated', 'Resting'],
                        ['DRB'],
                        ['CHX', 'Harr'],
                        ['3h', '1h'],
                        ['rep1', 'rep2', 'rep3'],
                        'DRB',
                        '0h')Processus arrêté

    Exploit_TDD_lincRNA(log_file,
                        ['Lympho'],
                        ['Activated', 'Resting'],
                        ['Trip'],
                        ['CHX', 'Harr'],
                        ['3h', '1h'],
                        ['rep1', 'rep2', 'rep3'],
                        'untreated',
                        '0h')

    Exploit_TDD_lincRNA(log_file,
                        ['Lympho'],
                        ['Activated'],
                        ['Trip'],
                        ['CHX'],
                        ['3h'],
                        ['rep1', 'rep2', 'rep3'],
                        'Trip',
                        '0h')

    Exploit_TDD_lincRNA(log_file,
                        ['Lympho'],
                        ['Activated'],
                        ['Trip'],
                        ['CHX'],
                        ['3h'],
                        ['rep1', 'rep2', 'rep3'],
                        'untreated',
                        '0h')

    # Get the Biomart values for the lengths
    Merger(log_file)

    # Add Hwang polyA and half-life
    Add_Hwang(log_file)

    # Add Ribodens
    Add_RiboDens(log_file)

    # Attribute the best transcript for all conditions using pAseq
    Pointer(log_file)

    Extra_Calculations(log_file)   # revision OK 2020/03/09

    Extract_Infos(log_file)   # revision OK 2020/03/09

    # Get pvalues for protein Complexes
    log_file.close()

    print(message_time('End'))
    print('')
    print('==================================================================')
    print('End of CDS Parser Pipeline ')
    print('==================================================================')


if __name__ == "__main__":
    # execute only if run as a script
    Pipeline()
