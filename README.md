# RMI2_GFF_Fasta_Compilation ![classification](https://img.shields.io/badge/SCRIPTS-Done-green) ![README](https://img.shields.io/badge/README-Awaiting_Validation-yellow) ![Maintained](https://img.shields.io/badge/Maintained%3F-yes-green.svg)

**Compatible with:**

![Linux](https://img.shields.io/badge/Ubuntu-FCC624?style=flat&labelColor=FCC624&logo=Ubuntu&logoColor=black)

---
**Powered by:**

![Python](https://img.shields.io/badge/python-3670A0?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![Matplotlib](https://img.shields.io/badge/Matplotlib-%23ffffff.svg?style=flat&labelColor=23ffffff&logo=Matplotlib&logoColor=black)
![NumPy](https://img.shields.io/badge/numpy-%23013243.svg?style=flat&labelColor=23013243&logo=numpy&logoColor=white)
![Pandas](https://img.shields.io/badge/pandas-%23150458.svg?style=flat&labelColor=23150458&logo=pandas&logoColor=white)
![SciPy](https://img.shields.io/badge/SciPy-%230C55A5.svg?style=flat&labelColor=230C55A5&logo=scipy&logoColor=%white)
![scikit-learn](https://img.shields.io/badge/scikit--learn-%23F7931E.svg?style=flat&labelColor=23F7931E&logo=scikit-learn&logoColor=white)
![R](https://img.shields.io/badge/r-%23276DC3.svg?style=flat&labelColor=23276DC3&logo=r&logoColor=white)

---

This repository contains all `R` and `Python` scripts required to generate
a Transcripts database and perform `Random Forest` on key features to predict
for each transcript the `Translation Dependent Degradation (TDD)` and
`Translation Independent Degradation (TID)` based on the `TDDindex` and
`TIDindex` metrics.

This project has been build and tested on the following configuration:

![Ubuntu](https://img.shields.io/badge/Ubuntu-20.04-blue?style=flat&labelColor=white&logo=Ubuntu&logoColor=red)
![CPU](https://img.shields.io/badge/INTEL_Xeon(R)-2.20GHz_x_40-0071C5)
![RAM](https://img.shields.io/badge/Memory-64GiB-blue)
![GPU](https://img.shields.io/badge/NVIDIA-GTX1080-76B900)
![CUDA](https://img.shields.io/badge/CUDA-12.1-76B900)
![Python](https://img.shields.io/badge/python-3.8.10-blue?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)

![biopython](https://img.shields.io/badge/biopython-1.81-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![datetime](https://img.shields.io/badge/datetime-5.3-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![gffutils](https://img.shields.io/badge/gffutils-0.12-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![glob](https://img.shields.io/badge/glob-0.7-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![Matplotlib](https://img.shields.io/badge/matplotlib-3.7.4-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![Multiprocess](https://img.shields.io/badge/multiprocess-0.70.15-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![networkx](https://img.shields.io/badge/networkx-3.1-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![NumPy](https://img.shields.io/badge/numpy-1.24.4-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![Pandas](https://img.shields.io/badge/pandas-2.0.3-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![SciPy](https://img.shields.io/badge/scipy-1.10.1-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![scikit-learn](https://img.shields.io/badge/scikit--learn-1.3.2-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![seaborn](https://img.shields.io/badge/seaborn-0.13.0-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![shutils](https://img.shields.io/badge/shutils-0.1.0-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![tqdm](https://img.shields.io/badge/tqdm-4.66.1-66ccff?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)

For all information or problem concerning this repository please contact:
david.cluet@ens-lyon.fr

Our complete processed database is available upon request:
emiliano.ricci@ens-lyon.fr

# TABLE OF CONTENT

- [RMI2\_GFF\_Fasta\_Compilation   ](#rmi2_gff_fasta_compilation---)
- [TABLE OF CONTENT](#table-of-content)
  - [Degradation Indexes](#degradation-indexes)
    - [Absolute Degradation Fold](#absolute-degradation-fold)
    - [Absolute TID index](#absolute-tid-index)
    - [Absolute TDD index](#absolute-tdd-index)
  - [Installation Procedure](#installation-procedure)
    - [Dependencies](#dependencies)
    - [Clone the repository](#clone-the-repository)
    - [Input files](#input-files)
    - [config.py file](#configpy-file)
  - [Build the database](#build-the-database)
    - [Flowchart](#flowchart)
    - [Generate\_subset.py](#generate_subsetpy)
  - [Compute new values and add them to current subset](#compute-new-values-and-add-them-to-current-subset)
    - [MFEden computation](#mfeden-computation)
    - [Linearity of the MFEref metric](#linearity-of-the-mferef-metric)
    - [Corrected MFE *versus* MFEden](#corrected-mfe-versus-mfeden)
    - [Merge the current subset with another database and perform calculations](#merge-the-current-subset-with-another-database-and-perform-calculations)


This repository is composed of several folders within the `src` one:

|Folder|Content|
|:-----|:------|
|**utils**|Python scripts allowing the database creation|
|**compute_MFEden**|Python scripts to compute the MFEden free energy metric for the transcript. This directory has been created to show how to append new values into the database.|
|**R_scripts**|Collection of Rscripts used to perform experimental analysis and/or figures|


## Degradation Indexes

Our reference degradation indexes are computed for the **3h** time point
as follow using **normalized count reads**.


### Absolute Degradation Fold

This metric corresponds to the total degradation fold at **3h** of each transcripts. It is expressed as a ratio to normalize the differences in abundance between the transcripts.

Absolute $Degradation Fold = {1 - {counts(t3h, Trip) \over counts(t0,Trip)}}$

### Absolute TID index

This metric corresponds to the degradation of the transcripts when the
translation is blocked. So it reveals the importance of the 
`Translation Independent Degradation` within the total degradation for each transcript.

Absolute $TID_{index} = {{counts(t0, Trip) - counts(t3h, TripCHX)} \over counts(t0, Trip)}$

### Absolute TDD index

The proportion of `Translation Dependent Degradation` for each transcript can be extracted combining the informations obtained from
the two previous metric.

Absolute $TDD_{index} = {{counts(t3h, TripCHX) - counts(t3h,Trip)} \over counts(t0, Trip)}$


## Installation Procedure

### Dependencies

The scripts have been initially written for `Python 2.7` and converted
`Python 3.8`. So far no bug was detected. Only the
`Compute_MFEden_and_update_ref_database.py` script integrate the newly added
Python type hints.

To execute these scripts several `Python` libraries are required:

- argparse
- biopython
- datetime
- gffutils
- glob
- matplotlib
- multiprocess
- networkx
- numpy
- pandas
- scikit-learn
- scipy
- seaborn
- shutils
- time
- tqdm

To install/upgrade them execute:

```bash
python3 -m pip install --upgrade argparse biopython datetime gffutils glob2 matplotlib multiprocess networkx numpy pandas scikit-learn scipy seaborn shutils tqdm
```

In order to compute raw Free Energy and MFEden, the `RNAfold` program is
mandatory. To install it follow the
[instructions of the developpers](https://github.com/ViennaRNA/ViennaRNA).

### Clone the repository

First clone this repository (*in your home by default*).

```Bash
cd ~/
git clone git@gitbio.ens-lyon.fr:LBMC/RMI2/rmi2_gff_fasta_compilation.git
```

Create the necessary folders:

```bash
cd rmi2_gff_fasta_compilation/src
mkdir data
mkdir output
```

### Input files

The most tiedous step is to collect all the `.fasta`, `.gff3`, `.csv` files that
permit to perform all computations.

Without the `gencode.vM23.annotation.gff3` and `GRCm38.p6.genome.fa` files, all
the other source files are available as a **57MB source_files.zip** file,
available upon request.

- [gencode.vM23.annotation.gff3](https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_mouse/release_M23/gencode.vM23.annotation.gff3.gz)
- [GRCm38.p6.genome.fa](https://www.ncbi.nlm.nih.gov/datasets/genome/GCF_000001635.26/)
- features_length_ensembl.csv
- dbNormCountsAll_exon.csv
- dbNormCountsOnly_lincRNA_exon.csv
- normalized_counts_all.csv
- RNAfold_2019-12-5_15-54-13_gencode.vM23.annotation_VS_GRCm38.p6.genome_DeltaG_uniq.dg
- PASeq_mergedPeaks_annotated_Count.tsv
- m6A_quantification.tsv
- m6A_NF_rank_merged_allpeaks_anno_curated.csv
- go-basic.obo
- association.tsv
- 128_MM_results_4G_UTR5.csv
- 128_MM_results_4G_CDS.csv
- 128_MM_results_4G_UTR3.csv
- aax0194_Hwang_Table_S3.csv
- aax0194_Hwang_Table_S4.csv
- aax0194_Hwang_Table_S5.csv
- RiboDensity.csv
- miRTarBase_8_mmu.csv
- GO_Arborescence.csv
- GO_tree.csv
- filtered_genes_Lympho_Resting.csv
- filtered_genes_Lympho_Activated.csv
- filtered_genes_Macro_Resting.csv
- filtered_genes_Macro_Activated.csv

All these files have to be stored in the `src/data` folder of our repository.

### config.py file

This file resumes all the key path and variables for the building pipeline.

If you follow our installation process and use **exactly** the same input files,
the only variable you may want to modify is `data_to_extract` (line 110).
This variable is the list of the columns you desire to extract at the end of
the process.

If after the initial build you want to add new values (see the MFEden section
for an exemple) and regenerate a new subset, just modify the `data_to_extract`
with new column names and use the `Generate_subset.py` script.

## Build the database

This script is the core program to perform all computations and generate the
database.

> The database is structured as a collection of a `transcript.csv` files.
> This structure permits to handle all possible variation in term of column
> number between transcripts, like the diversity of introns and exons numbers.
> Each database is stored into a dedicated folder which name is a combination of
> the 2 input `.gff3` and `.fa` files. Thus if you change these files (and
> modify the corresponding variables into the `config.py` file), a new folder
> with new files will be created.

To create the first instance of the database change your working directory:

```bash
cd ~/rmi2_gff_fasta_compilation/src
```

Then execute the script:

```bash
python3 Build_Database.py
```

### Flowchart

The script will execute the following tasks:

```mermaid
flowchart TD

    subgraph data
        gff(gencode.vM23.annotation.gff3)
        fa(GRCm38.p6.genome.fa)
        obo(go-basic.obo)
        pAseq(PASeq_mergedPeaks_annotated_Count.tsv)
        UTR5_4G(128_MM_results_4G_UTR5.csv)
        CDS_4G(128_MM_results_4G_CDS.csv)
        UTR3_4G(128_MM_results_4G_UTR3.csv)
        m6A(m6A_quantification.tsv)
        m6A_NF(m6A_NF_rank_merged_allpeaks_anno_curated.csv)
        miRNA(miRTarBase_8_mmu.csv)
        raw_tdd(dbNormCountsAll_exon.csv)
        raw_tdd_linc(dbNormCountsOnly_lincRNA_exon.csv)
        input_linc(features_length_ensembl.csv)
        hwang1(aax0194_Hwang_Table_S3.csv)
        hwang2(aax0194_Hwang_Table_S4.csv)
        hwang3(aax0194_Hwang_Table_S5.csv)
        ribodens(RiboDensity.csv)

        db(gencode.vM23.annotation.db)
        gene_to_transcript(Gene_to_Transcript.csv)
        go_to_phrase(GO_to_Phrase.tsv)
        subset(current_subset.csv)
    end

    subgraph gencode.vM23.annotation_VS_GRCm38.p6.genome
        transcript1(ENSMUST00000000001.csv)
        transcript2(ENSMUST00000000325.csv)
        transcriptn(ENSMUST00000186745.csv)

        linc1(ENSMUSG00000000031.csv)
        linc2(ENSMUSG00000006462.csv)
        lincn(ENSMUSG00000013562.csv)
    end

    subgraph output
        gencode.vM23.annotation_VS_GRCm38.p6.genome
    end

    initial_check[Paths Validation]
    initial_check --> |Create| gencode.vM23.annotation_VS_GRCm38.p6.genome
    initial_check --> create_db[Create SQL Database\nBE PATIENT!]
    gff --> |Input| create_db
    create_db --> |Create| db

    create_db --> find_CDS[Find CDS]
    fa --> |Input| find_CDS
    db --> |Input| find_CDS
    
    find_CDS --> find_introns[Add Introns]
    fa --> |Input| find_introns
    db --> |Input| find_introns
    
    find_introns --> QC[Quality Control]
    QC --> |Create| transcript1
    QC --> |Create| transcript2
    QC --> |Create| transcriptn

    QC --> converter[Gene to transcripts converter]
    converter --> |Create| gene_to_transcript

    converter --> go_obtainer[Attribute GO]
    obo --> |Input| go_obtainer
    go_obtainer --> |Create| go_to_phrase
    
    
    go_obtainer --> add_go[Add GO]
    go_to_phrase --> |Input| add_go
    add_go --> manipulator[Manipulator Object]
    manipulator --> |Add new lines| transcript1
    manipulator --> |Add new lines| transcript2
    manipulator --> |Add new lines| transcriptn
    manipulator --> |Add new lines| linc1
    manipulator --> |Add new lines| linc2
    manipulator --> |Add new lines| lincn

    add_go --> c3g[Compute C3G percent]
    c3g --> manipulator

    c3g --> add_pAseq[Add experimental UTR3 length]
    pAseq --> |Input| add_pAseq
    add_pAseq --> manipulator

    add_pAseq --> add_4G[Add G quadruplexes]
    UTR5_4G --> |Input| add_4G
    CDS_4G --> |Input| add_4G
    UTR3_4G --> |Input| add_4G
    add_4G --> manipulator
    
    add_4G --> add_m6A[Add m6A]
    m6A --> |Input| add_m6A
    m6A_NF --> |Input| add_m6A
    add_m6A --> manipulator

    add_m6A --> find_AU[Find poly AU]
    find_AU --> manipulator

    find_AU --> add_miRNA[Add micro RNA target]
    miRNA --> |Input| add_miRNA
    add_miRNA --> manipulator

    add_miRNA --> compute_DG[Compute DG\nRNAfold]
    compute_DG --> manipulator

    compute_DG --> lincRNA[Create lincRNA files]
    lincRNA --> |Create| linc1
    lincRNA --> |Create| linc2
    lincRNA --> |Create| lincn

    lincRNA --> compute_indexes[Compute Indexes]
    raw_tdd --> |Input| compute_indexes
    raw_tdd_linc --> |Input| compute_indexes
    compute_indexes --> manipulator

    compute_indexes --> add_feature_linc[Add features to lincRNAs]
    input_linc --> |Input| add_feature_linc
    add_feature_linc --> manipulator

    add_feature_linc --> add_Hwang[Add data from Hwang et al 2020]
    hwang1 --> |Input| add_Hwang
    hwang2 --> |Input| add_Hwang
    hwang3 --> |Input| add_Hwang
    add_Hwang --> manipulator

    add_Hwang --> add_ribodens[Add Ribodens]
    ribodens --> |Input| add_ribodens
    add_ribodens --> manipulator

    add_ribodens --> pointer[Identify best transcripts\nfor each gene]
    pointer --> manipulator

    pointer --> extractor[Generate Subset]
    extractor --> |Create| subset


    style initial_check fill: #ffcc00
    style create_db fill: #ffcc00
    style find_CDS fill: #ffcc00
    style find_introns fill: #ffcc00
    style QC fill: #ffcc00
    style converter fill: #ffcc00
    style go_obtainer fill: #ffcc00
    style add_go fill: #ffcc00
    style manipulator fill: #ffcc00
    style c3g fill: #ffcc00
    style add_pAseq fill: #ffcc00
    style add_4G fill: #ffcc00
    style add_m6A fill: #ffcc00
    style find_AU fill: #ffcc00
    style add_miRNA fill: #ffcc00
    style compute_DG fill: #ffcc00
    style lincRNA fill: #ffcc00
    style compute_indexes fill: #ffcc00
    style add_feature_linc fill: #ffcc00
    style add_Hwang fill: #ffcc00
    style add_ribodens fill: #ffcc00
    style pointer fill: #ffcc00
    style extractor fill: #ffcc00

    style gencode.vM23.annotation_VS_GRCm38.p6.genome fill: #99ccff
    style db fill: #99ccff
    style gene_to_transcript fill: #99ccff
    style go_to_phrase fill: #99ccff
    style subset fill: #99ccff

    style gff fill: #ff6600
    style fa fill: #ff6600
    style obo fill: #ff6600
    style pAseq fill: #ff6600
    style UTR5_4G fill: #ff6600
    style CDS_4G fill: #ff6600
    style UTR3_4G fill: #ff6600
    style m6A fill: #ff6600
    style m6A_NF fill: #ff6600
    style miRNA fill: #ff6600
    style raw_tdd fill: #ff6600
    style raw_tdd_linc fill: #ff6600
    style input_linc fill: #ff6600
    style hwang1 fill: #ff6600
    style hwang2 fill: #ff6600
    style hwang3 fill: #ff6600
    style ribodens fill: #ff6600
```

The output `current_subset.csv` file contains all the desired values as columns
and all transcripts that have at least one validated `TDDindex`.

> **The list of the available metrics is detailed**
> **[here](src/data/Available_parameters.md).**

### Generate_subset.py

In order to extand or reduce the set of wanted columns:

1. Modify the `data_to_extract` list in the `config.py`
2. Execute the `Generate_subset.py` script

```bash
cd ~/rmi2_gff_fasta_compilation/src
python3 Generate_subset.py
```

## Compute new values and add them to current subset

### MFEden computation

As describe above, the $\Delta G$ was calculated using `RNAfold` as
Minimal Free Energy (`MFE`). A corrected metric, MFE density (`MFEden`),
has been proposed by
[Edoardo Trotta](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0113380).

This metric is defined as following:

**Equation 1:** $MFEden = 100 * {(MFE - MFE_{ref(L)}) \over (L - L0)}$

Where:

- **L** is the length in nucleotide of the studied RNA sequence.
- The **MFE** is the raw result obtained for RNAfold with the studied sequence.
- The **$MFE_{ref(L)}$** is the **mean MFE** obtained with 2,000 random
sequences of length **L**, with equimolar nucleotide composition.
- **L0** is a predefined optimal constant amount (**8**) that shifts the
MFE-versus-length regression line to the origin of the graph.

In our computation we kept **L0 = 8** in our computations.

The **$MFE_{ref(L)}$** computations where performed using the
`src/compute_MFEden/setup_multi_DG.py` script.

Technically, we generated a set of nucleotide sequences length from 100bp to
9,900bp with a step of 100bp. For each set 2,000 random sequences are generated
but kipping an equimolar ratio between the 4 nucleotide. The `MFE` is then
computed with `RNAfold` for each of these 2,000 sequence. By calculating the
average of the 2,000 `MFE` values we obtained the **$MFE_{ref(L)}$**.

As the
[initial publication](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0113380)
pointed out that the `MFE` metric is linearly correlated with the sequence
length. But it was described only up to 600bp.

With our set of **$MFE_{ref(L)}$** we confirmed a perfect linear behaviour up to
9,900bp.

### Linearity of the MFEref metric
![Linearity](figures/MFE_Linearity.png)

Thus we can compute the **$MFE_{ref(L)}$** of any nucleotide sequence with a
sequence length **L** up to 9,900bp using a simple function:

**Equation 2:** $MFE_{rel(L)} = 17.5 - 0.326 * L$

By combining equations 1 and 2 we can then easily extract `MFEden` of a
nucleotide sequence just from its raw `MFE` obtained from `RNAfold`
computation and its length **L**:

$MFEden = 100 * {(MFE - (17.5 - 0.326 * L)) \over (L - L0)}$

We then validated that this new metric was indeed altering the $\Delta G$ values
distribution when compared to the `MFE` standardized by the sequence length.
Noteworthy we observed the appearance of positive values,
indicated the existence of CDS with highly instable conformational structure:

### Corrected MFE *versus* MFEden
![Distribution](figures/DG_vs_MFEden.png)

> **Nota Bene**
>
> **For the shortest sequences, 31 and 77 respectively corresponding to the**
> **31CDS-End_DG and 47UTR5-30CDS_DG, we proceeded as explained above but with**
> **two sets of 2,000 random sequences with L = 31 or 77, and computed**
> **constant  $MFE_{ref(L)}$**

### Merge the current subset with another database and perform calculations

The `MFEden` computations were simultaneously to the merging with another
database using the
`src/compute_MFEden/Compute_MFEden_and_update_ref_database.py` script

```bash
cd ~/rmi2_gff_compilation/src/compute_MFEden/
python3 Compute_MFEden_and_update_ref_database.py -h
usage: Compute_MFEden_and_update_ref_database.py [-h] reference new final

Compute MFEden and append the new columns to the TDD database.

positional arguments:
  reference   Path to the reference csv.
  new         Path to the new columns csv.
  final       Path to the final csv.

optional arguments:
  -h, --help  show this help message and exit
```

Here we present it as an example to add new columns from an external source, and
with computations using already present values.

The script can be used as follow:

```bash
cd ~/rmi2_gff_compilation/src/compute_MFEden/
python3 Compute_MFEden_and_update_ref_database.py <path to current_suset.csv> <path to csv with new column> <path of the updated version>
```

The external source .csv file needs to have a `transcript_id` column allowing a
perfect attribution of the new values to the correct transcript.

The reference database and the new one are loaded as `pandas.DataFrame` objects
and then merged via the following function.

```python
def attribute_new_values_to_genes(ref_df: pd.DataFrame,
                                  new_df: pd.DataFrame,
                                  path_to_final: str) -> None:
    """
        Merge the two sets of values using the transcript id.
        This way gene duplication is handled.
        The csv file is immediatly generated to speed up the process.
        Concatenation is much too slow.

    Args:
        ref_df (pd.DataFrame): Reference database of Manu
        new_df (pd.DataFrame): New set of values (miRNA and half-lives)
        path_to_final (str): Path to the final csv file.
    """

    # Simplify the new_df to speed up process
    wanted = ['transcript_id',
              'miRNA_canonical_sites']
    
    new_df = new_df[wanted]

    # Get all genes id (without version) associated to new values
    transcript_mi = new_df['transcript_id'].to_numpy()

    # Create dataframe for a gene that has no mi-RNA
    empty_row = {}
    for w in wanted:
        empty_row[w] = [np.nan]
    empty_df = pd.DataFrame.from_dict(empty_row)
    n_row = ref_df.shape[0]

    for row in tqdm(range(n_row),
                    desc='Adding the new columns',
                    colour='cyan'):

        # Get the reference row with its gene id (without version)
        row_df = ref_df.loc[[row]]
        transcript = row_df['transcript_id'].to_numpy()[0]

        # The transcript is associated with new values
        if transcript in transcript_mi:
            filter = new_df['transcript_id'] == transcript
            new_row_df = new_df.loc[filter]

        # No new values for this transcript
        else:
            new_row_df = empty_df.copy()
            new_row_df['transcript_id'] = [transcript]

        new_full_row_df = row_df.merge(new_row_df,
                                       on='transcript_id',
                                       how='outer')

        # Create the final file or appennd it (much faster that dataframe
        # concatenation 105it vs 5 per second).        
        if row == 0:
            new_full_row_df.to_csv(path_to_final,
                                   index=False)
        
        else:
            new_full_row_df.to_csv(path_to_final,
                                   index=False,
                                   header=False,
                                   mode='a')
```

It is to note that we add here only one column: `miRNA_canonical_sites`.
The `pandas.concat()` function is very efficient but depending on the number of
rows and columns, the process tends to slow down very fast when doing it in a
loop, like here when working transcript by transcript. Instead a temporary
dataframe is created for each transcript used to create the new .csv file (for
the first one) or append to it. This process allows to have up to 105
transcripts treated per second *versus* 5 with the concatenation approach.
moreover the RAM usage is not detectable.

Technically for each `transcript_id` of the reference if it is present in the
`transcript_id` column of the new database, the values are merged, else only
`np.nan` values are added. This permits to preserve the initial number of
transcripts.

The `MFEden` computations were then performed on a loaded `pandas.DataFrame` of
the new version of the database with the following function:

```python
def MFEden(df: pd.DataFrame,
           MFE: str,
           MFErefL,
           new_col_name:'str') -> pd.DataFrame:
    """
        Compute the MFEden using acolumn or constant

    Args:
        df (pd.DataFrame): Dataframe containing the values
        MFE (str): Column with raw RNAfold values
        MFErefL (_type_): Column name or constant
        new_col_name (str): Name f the column to be created

    Returns:
        pd.DataFrame: Updated dataframe
    """    
    
    # test if we use a column or constant for MFErefL
    if isinstance(MFErefL, str):
        df[new_col_name] = round(100 * (df[MFE] -
                                        (17.15 - 0.326 * df[MFErefL])) /
                                        (df[MFErefL] - 8),
                                        3)
    
    if isinstance(MFErefL, tuple):
        df[new_col_name] = round(100 * (df[MFE] - MFErefL[0]) /
                                 (MFErefL[1] - 8),
                                 3)
    
    return df
```

If the sequence is not `31CDS-End` or `47UTR5-30CDS`, the `MFErefL` variable is
a simple `string` like `Length.cds` referencing to a column of the dataframe,
triggering this computation:

```python
        df[new_col_name] = round(100 * (df[MFE] -
                                        (17.15 - 0.326 * df[MFErefL])) /
                                        (df[MFErefL] - 8),
                                        3)
```

*Ab contrario* if `MFErefL` variable is a `tuple` like (-17.244, 77),
corresponding to (`MFEref`, `L`), it triggers the computation:

```python
        df[new_col_name] = round(100 * (df[MFE] - MFErefL[0]) /
                                 (MFErefL[1] - 8),
                                 3)
```

> **Nota Bene**
>
> **This function takes advantage of the pandas library that allows to**
> **perform computations at the scale of a whole column whenever `np.nan`**
> **are present.**
