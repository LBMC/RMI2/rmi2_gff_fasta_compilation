Cluet David
![Scripts](https://img.shields.io/badge/Python-Scripts_generation-yellow?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![Scripts](https://img.shields.io/badge/Python-Upgrading_from_2.7_to_3.8-yellow?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![Scripts](https://img.shields.io/badge/Python-Maintenance-yellow?style=flat&labelColor=3670A0&logo=python&logoColor=ffdd54)
![Readme](https://img.shields.io/badge/Git-README-red?style=flat&labelColor=white&logo=git&logoColor=red)
![git](https://img.shields.io/badge/Git-Repository_Maintenance-red?style=flat&labelColor=white&logo=git&logoColor=red)

Labaronne Emmanuel
![Rstudio](https://img.shields.io/badge/Rstudio-Data_Analysis-blue?style=flat&labelColor=white&logo=RStudio&logoColor=blue)
![Rstudio](https://img.shields.io/badge/Rstudio-Figures_Creation-blue?style=flat&labelColor=white&logo=RStudio&logoColor=blue)

Ricci Emiliano
![management](https://img.shields.io/badge/Project-Management-yellow)
![Readme](https://img.shields.io/badge/Git-README_corrections-red?style=flat&labelColor=white&logo=git&logoColor=red)
